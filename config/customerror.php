<?php
return [
    /*
    |--------------------------------------------------------------------------
    | SYSTEM ERROR CODES
    |--------------------------------------------------------------------------
    |
    |
    */
    '101' => [
        'message'   => 'INVALID_EMAIL_ID',
        'meaning'   =>'You have entered an invalid email address'
    ],
    '102' => [
        'message'   =>'INVALID_PHONE_NUMBER',
        'meaning'   =>'You have entered an invalid mobile number'
    ],
    '103' => [
        'message'   =>'MOBILE_NUMBER_IS_REQUIRED',
        'meaning'   =>'Please enter your mobile no'
    ],
    '105' => [
        'message'   =>'SERVER_IS_UNDER_MAINTENANCE',
        'meaning'   =>'Currently we are under maintenance'
    ],
    '108' => [
        'message'=>'EMAIL_ID_REQUIRED_OR_MOBILE_NUMBER_REQUIRED',
        'meaning'=>'Email or Mobile number is required'
    ],
    '109' => [
        'message'=>'PASSWORD_FIELD_REQUIRED',
        'meaning'=>'Password field is required'
    ],
    '110' => [
        'message'=>'USER_NOT_FOUND',
        'meaning'=>'No user found with this email'
    ],
    '111' => [
        'message'=>'INVALID_EMAIL_OR_PASSWORD',
        'meaning'=>'Your login credentials is invalid'
    ],
    '112' => [
        'message'=>'ACCOUNT_SUSPENDED',
        'meaning'=>'Your account has been temporarily suspended.'
    ],
    '113' => [
        'message'=>'FIRST_NAME_IS_REQUIRED',
        'meaning'=>'First name is required'
    ],
    '114' => [
        'message'=>'LAST_NAME_IS_REQUIRED',
        'meaning'=>'Last name is required'
    ],
    '115' => [
        'message'=>'PASSWORD_IS_REQUIRED',
        'meaning'=>'Password is required'
    ],
    '116' => [
        'message'=>'CONFIRM_PASSWORD_IS_REQUIRED',
        'meaning'=>'Confirm password is required'
    ],
    '117' => [
        'message'=>'DOB_IS_REQUIRED',
        'meaning'=>'Please select your date of birth'
    ],
    '118' => [
        'message'=>'PASSWORD_AND_CONFIRM_PASSWORD_NOT_MATCHED',
        'meaning'=>'Password and confirm password not matched'
    ],
    '123' => [
        'message'=>'EMAIL_ID_ALREADY_EXISTS',
        'meaning'=>"An user has been allready registered with this email address"
    ],
    '126' => [
        'message'=>'DATABASE_INSERTION_ERROR',
        'meaning'=>"OOPS unable to handle this request"
    ],
    '127' => [
        'message'=>'TOKEN_FIELD_ID_REQUIRED',
        'meaning'=>"Token field is required"
    ],
    '128' => [
        'message'=>'DEVICE_REGISTRATION_ID_IS_REQUIRED',
        'meaning'=>"Device registration id is required"
    ],
    '400' => [
        'message'=>'BAD_REQUEST',
        'meaning'=>'OOPS seems 400 Bad-Request'
    ],
    '401' => [
        'message'=>'USER_UNAUTHORIZED',
        'meaning'=>'Your current session has been expired'
    ],
    '403' => [
        'message'=>'ACCESS_FORBIDDEN',
        'meaning'=>"Seems your session is being expired"
    ],
    '404' => [
        'message'=>'METHOD_NOT_FOUND',
        'meaning'=>'Opearation could not be performed'
    ],
    '500' => [
        'message'=>'INTERNAL_SERVER_ERROR',
        'meaning'=>'OOPS seems we are currently unable to handle this request'
    ],
    '502' => [
        'message'=>'BAD_GATEWAY_ERROR',
        'meaning'=>'Bad request from client'
    ],
    '503' => [
        'message'=>'SERVICE_NOT_AVAILABLE',
        'meaning'=>'Currently service is not available, try again after sometime'
    ],
    '504' => [
        'message'=>'MOBILE_NUMBER_ALREADY_EXIST',
        'meaning'=>"An user has been allready registered with this mobile number"
    ],
    '505' => [
        'message'=>'CHOOSE_ACCOUNT_TYPE',
        'meaning'=>"Please select your account type"
    ],
    '506' => [
        'message'=>'CHOOSE_POST_PRIVACY_TYPE',
        'meaning'=>"Please select who can select your post, if private then only friends otherwise it will be showned publically"
    ],
    '507' => [
        'message'=>'CHOOSE_PROFESSIONAL_ACCOUNT_TYPE',
        'meaning'=>"Please select professional account type"
    ],
    '508' => [
        'message'=>'BUSINESS_WEBSITE_URL_REQUIRED',
        'meaning'=>"Please enter your business website url"
    ],
    '509' => [
        'message'=>'BUSINESS_ADDRESS_REQUIRED',
        'meaning'=>"Please enter your business address"
    ],
    '510' => [
        'message'=>'INVALID_WEBSITE_URL',
        'meaning'=>"Please enter an valid website url"
    ],
    '511' => [
        'message'=>'NICK_NAME_ALREADY_EXIST',
        'meaning'=>"Please enter an valid website url"
    ],
    '512' => [
        'message'=>'SMS_GATEWAY_REQUIRED',
        'meaning'=>"Please provide SMS gateway"
    ],
    '513' => [
        'message'=>'OLD_PASSWORD_IS_REQUIRED',
        'meaning'=>"Please enter your old password"
    ],
    '514' => [
        'message'=>'USER_ID_REQUIRED',
        'meaning'=>"Authenticated User's id required"
    ],
    '515' => [
        'message'=>'NEW_PASSWORD_FIELD_IS_REQUIRED',
        'meaning'=>"Please enter your new password"
    ],
    '516' => [
        'message'=>'VERIFICATION_CODE_IS_REQUIRED',
        'meaning'=>"Please enter your 6 digit verification code"
    ],
    '517' => [
        'message'=>'VERIFICATION_CODE_NOTE_MATCHED',
        'meaning'=>"Seems You have entered an invalidd verification code"
    ],
    '518' => [
        'message'=>'UNAUTHORIZE_ACCESS',
        'meaning'=>"Unauthorized User"
    ],
    '519' => [
        'message'=>'WRONG_PASSWORD',
        'meaning'=>"You have entered an incorrect password"
    ],
    '520' => [
        'message'=>'POST_TOTAL_IMAGE_COUNTER_REQUIRED',
        'meaning'=>"Total number of images counter required"
    ],
    '521' => [
        'message'=>'POST_LOCATION_REQUIRED',
        'meaning'=>"For posting any recipe you need to enter the location"
    ],
    '522' => [
        'message'=>'POST_DESCRIPTION_REQUIRED',
        'meaning'=>"For posting any recipe you need to enter description"
    ],
    '523' => [
        'message'=>'POST_IMAGE_REQUIRED',
        'meaning'=>"For posting any recipe you need to upload at last one image"
    ],
    '524' => [
        'message'=>'POST_INGREDIENTS_COUNTER_REQUIRED',
        'meaning'=>"Please send post ingredients counter"
    ],
    '525' => [
        'message'=>'POST_STEPS_COUNTER_REQUIRED',
        'meaning'=>"Please send post steps counter"
    ],
    '526' => [
        'message'=>'POST_INGREDIENTS_REQUIRED',
        'meaning'=>"Please select at least one ingredients"
    ],
    '527' => [
        'message'=>'POST_STEP_IS_REQUIRED',
        'meaning'=>"Please enter at least one step"
    ],
    '530' => [
        'message'=>'UNSUPPORTED_MIME_TYPE',
        'meaning'=>"Unsupported media type, supported media types are (jpg, png, JPEG, jpeg, mkv, mp4, flv, m3u8, ts, 3gp, mov, avi, wmv)"
    ],
    '531' => [
        'message'=>'FILE_SIZE_GREATER_THAN_5_MEGA_BYTES',
        'meaning'=>"File size could not be greater than 5MB"
    ],
    '532' => [
        'message'=>'RECIPE_TITLE_IS_REQUIRED',
        'meaning'=>"Please enter recipe title"
    ],
    '533' => [
        'message'=>'RECIPE_STORY_IS_REQUIRED',
        'meaning'=>"Please enter recipe story"
    ],
    '534' => [
        'message'=>'WEBSITE_URL_IS_REQUIRED',
        'meaning'=>"Please enter a website or video url"
    ],
    '535' => [
        'message'=>'WEBSITE_URL_IS_INVALID',
        'meaning'=>"Please enter a valid website or video url"
    ],
    '536' => [
        'message'=>'WANNA_COOKED_IT_IS_REQUIRED',
        'meaning'=>"For posting recipe from cooked it items please select it from your cooked it list"
    ],
    '537' => [
        'message'=>'WANNA_COOKED_ITEM_NOT_FOUND',
        'meaning'=>"Recipe Not Found"
    ],
    '538' => [
        'message'=>'RECIPE_TYPE_REQUIRED',
        'meaning'=>"Please select recipe type"
    ],
    '539' => [
        'message'=>'BRAND_LIST_NOT_FOUND',
        'meaning'=>"Brand list not found"
    ],
    '540' => [
        'message'=>'INGREDIENT_LIST_NOT_FOUND',
        'meaning'=>"Ingredients list not found"
    ],
    '541' => [
        'message'=>'BRAND_ID_IS_REQUIRED',
        'meaning'=>"Please select a brand from brand list"
    ],
    '542' => [
        'message'=>'FRIEND_LIST_NOT_FOUND',
        'meaning'=>"Currently you don't have any friends"
    ],
    '543' => [
        'message'=>'BRAND_HAS_NO_INGREDEINTS',
        'meaning'=>"Ingredients not found with this Brand"
    ],
    '544' => [
        'message'=>'POST_OFFSET_IS_REQUIRED',
        'meaning'=>"For fetching post you need to send offset first"
    ],
    '545' => [
        'message'=>'POST_LIMIT_IS_REQUIRED',
        'meaning'=>"For fetching post you need to send limit first"
    ],
    '546' => [
        'message'=>'POST_NOT_FOUND',
        'meaning'=>"OOPS seems you or your friends dont have any active post"
    ],
    '547' => [
        'message'=>'POST_ID_IS_REQUIRED',
        'meaning'=>"Post id is required"
    ],
    '548' => [
        'message'=>'POST_COMMENT_IS_REQUIRED',
        'meaning'=>"Please enter post comment"
    ],
    '549' => [
        'message'=>'POST_ALREADY_ADDED_TO_COOKED_IT_ITEMS',
        'meaning'=>"Seems this post is allready added to your wanna cooked item"
    ],
    '550' => [
        'message'=>'HASH_TAG_FIELD_IS_REQUIRED',
        'meaning'=>"Please enter if you want to insert any hash tags into your post"
    ],
    '551' => [
        'message'=>'FRIEND_ID_IS_REQUIRED',
        'meaning'=>"Friend id is required"
    ],
    '552' => [
        'message'=>'FRIEND_ID_ALLREADY_REQUESTED',
        'meaning'=>"You have been request allready"
    ],
    '553' => [
        'message'=>'ALLREADY_FOLLOWING',
        'meaning'=>"You have been allready following"
    ],
    '554' => [
        'message'=>'POST_ALREADY_ADDED_TO_WANNA_VISIT',
        'meaning'=>"Seems this post is allready added to your wanna visit item"
    ],
    '555' => [
        'message'=>'COMMENT_ID_IS_REQUIRED',
        'meaning'=>"Post comment id is required"
    ],
    '556' => [
        'message'=>'WRONG_CODE',
        'meaning'=>"You have entered an incorrect code"
    ],
    '557' => [
        'message'=>'OTP_REQUIRED',
        'meaning'=>'OTP is required'
    ],
    '558' => [
        'message'=>'ALREADY_REGISTER',
        'meaning'=>'Already register'
    ],
    '559' => [
        'message'=>'COMMENT_ID_IS_REQUIRED',
        'meaning'=>"Comment id is required"
    ],
    '560' => [
        'message'=>'COMMENT_NOT_FOUND',
        'meaning'=>"OOPS seems you or your friends dont have any active comment"
    ],
    '561' => [
        'message'=>'USERNAME_ALREADY_EXISTS',
        'meaning'=>"A username has been already used"
    ],
    '562' => [
        'message'=>'CHAT_HISTORY_NOT_FOUND',
        'meaning'=>"Currently you don't have any chats"
    ],
    '563' => [
        'message'=>'SEARCH_KEYWORD_IS_REQUIRED',
        'meaning'=>"Please enter search keyword"
    ],
    '564' => [
        'message'=>'SEARCH_NOT_REQUIRED',
        'meaning'=>"Not found"
    ],

];