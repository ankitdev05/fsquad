<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>@yield('title')</title>
        @yield('style')
    </head>
    <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
        <div class="wrapper">
            @yield('navbar')
            @yield('sidebar')
            @yield('body')
            @yield('footer')
        </div>
        @yield('scripts')
        
        @if($errors->any())
            @foreach($errors->all() as $err)
                <script>
                    toastr.error('{{$err}}');
                </script>
            @endforeach
        @endif

        @if(session()->has('error'))
            <script>
                toastr.error('{{session()->get("error")}}');
            </script>
        @endif
        @if(session()->has('success'))
            <script>
                toastr.success('{{session()->get("success")}}');
            </script>
        @endif
        @if(session()->has('waning'))
            <script>
                toastr.warning('{{session()->get("warning")}}');
            </script>
        @endif
        @if(session()->has('info'))
            <script>
                toastr.info('{{session()->get("info")}}');
            </script>
        @endif
    </body>
</html>