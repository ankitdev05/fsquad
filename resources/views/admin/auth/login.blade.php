@extends('admin.layouts.app')
@section('title', 'Fsquad Admin | Login')
@section('style')
  @include('admin.includes.style')
@endsection
@section('body')
<style>
  body{
    background: url("{{URL::to("public/admin/dist/images/bg-image.jpg")}}");
    background-attachment: fixed;
    background-position: inherit;
    background-repeat: no-repeat;
    background-size: cover;
  }
  .login-box .login-logo a{
    color: #FFF;
  }
  .card, 
  .login-page,
  .register-page,
  .card-body.login-card-body {
    background-color: rgba(255,255,255,0.8) !important;
    border-radius: 8px !important;
  }
  .card button.btn.btn-primary{
    border-radius: 4px !important;
  }
  @media screen and (max-width: 768px){
    .login-box{
      position: absolute;
      top: 30%;
      left: 50%;
      transform: translate(-50%,30%);
    }
  }
</style>
<div class="login-box">
  <div class="login-logo">
    <a href="javascript:void(0);"><b>Welcome&nbsp;</b>Admin</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">
        <!-- <img src="{{URL::to("public/admin/dist/images/logo1.png")}}" class="img-fluid" style="max-width: 60px;" /> -->
        <h5 style="font-family: 'Pacifico', cursive; text-align: center; color: #a41456;">Fsquad Admin</h5>
      </p>
      @if($errors->any())
            <div class="row">
        @foreach($errors->all() as $err)
                <div class="col-md-12 col-sm-12 col-lg-12">
                    <span><p class="alert alert-danger">{{$err}}</p></span>    
                </div>
        @endforeach
            </div>
      @endif
      <form action="{{route('login')}}" method="post" id="myForm" name="myForm">
        @csrf

        <div class="input-group mb-1">
          <input type="text" name="email" id="email" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-1">
            <label for="email" generated="true" class="error"></label>
        </div>
        <div class="input-group mb-1">
          <input type="password" name="password" id="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          
        </div>
        <div class="input-group mb-1">
            <label for="password" generated="true" class="error"></label>
        </div>
        <div class="row">
          <div class="col-7">
            <div class="icheck-primary">
              <input type="checkbox" id="remember" name="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-5">
            <button type="submit" class="btn btn-info btn-block btn-flat"><i class="fas fa-lock"></i>&nbsp;Sign In&nbsp;<i class="fas fa-arrow-right"></i></button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <!--  -->
    </div>
  </div>
</div>
@endsection
@section('scripts')
    @include('admin.includes.scripts')
    <script>
        $('#myForm').validate({
            rules:{
                email:{
                    required:true,
                    email:true
                },
                password:{
                    required:true
                }
            }
        });
    </script>
@endsection