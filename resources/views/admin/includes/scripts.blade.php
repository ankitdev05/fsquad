<script src="{{URL::to('public/admin/plugins/jquery/jquery.min.js')}}"></script>

<script src="{{URL::to('public/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<script src="{{URL::to('public/admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>

<script src="{{URL::to('public/admin/dist/js/adminlte.js')}}"></script>

<script src="{{URL::to('public/admin/dist/js/demo.js')}}"></script>

<script src="{{URL::to('public/admin/dist/js/jquery.toast.js')}}"></script>
<script src="{{URL::to('public/admin/dist/js/jquery.validate.js')}}"></script>
<script src="{{URL::to('public/admin/dist/js/jquery-ui.min.js')}}"></script>
<script src="{{URL::to('public/admin/dist/js/jquery.timepicker.min.js')}}"></script>

<script src="{{URL::to('public/admin/plugins/jquery-mousewheel/jquery.mousewheel.js')}}"></script>

<script src="{{URL::to('public/admin/plugins/raphael/raphael.min.js')}}"></script>

<script src="{{URL::to('public/admin/plugins/jquery-mapael/jquery.mapael.min.js')}}"></script>

<script src="{{URL::to('public/admin/plugins/jquery-mapael/maps/world_countries.min.js')}}"></script>

<script src="{{URL::to('public/admin/plugins/chart.js/Chart.min.js')}}"></script>
<script language="javascript" type="text/javascript" src="{{URL::to('public/admin/dist/tiny_mce/tinymce.min.js')}}"></script>
@if(!@Auth::guard('web')->user()->id)
	<script src="{{URL::to('public/admin/dist/js/pages/dashboard2.js')}}"></script>
@endif