<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link">
    <img src="{{URL::to('public/admin/dist/img/AdminLTELogo.png')}}" alt="Fasquad Logo" class="brand-image img-circle elevation-3"
        style="opacity: .8">
    <span class="brand-text font-weight-light">Fsquad Admin</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{URL::to('public/admin/dist/images/b.png')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{route('home')}}" class="d-block">{{@Auth::guard('web')->user()->name}}</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Manage Profile
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.edit.profile')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Edit Profile</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-object-group"></i>
                        <p>
                            Brands & Ingredients
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.brands')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All Brands</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.ingredients')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All Ingredients</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-gamepad"></i>
                        <p>
                            Manage Gamification
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.cooked.it.gamification')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Cooked It Gamification</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.freind.gamification')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Freind Gamification</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.post.gamification')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Post Gamification</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.usage.gamification')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Usage Gamification</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.video.gamification')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Video Gamification</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-user"></i>
                        <p>
                            User Management
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.users')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All Users</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>