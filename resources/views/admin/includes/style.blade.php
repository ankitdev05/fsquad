	<style>
		.navbar {
		    background-color: #17a2b8 !important;
		}
    .error{
      color: red !important;
    }
	</style>
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{URL::to('public/admin/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{URL::to('public/admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{URL::to('public/admin/dist/css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{URL::to('public/admin/dist/css/jquery.toast.css')}}">
  <link rel="stylesheet" href="{{URL::to('public/admin/dist/css/jquery-ui.css')}}">
  <link rel="stylesheet" href="{{URL::to('public/admin/dist/css/jquery.timepicker.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">