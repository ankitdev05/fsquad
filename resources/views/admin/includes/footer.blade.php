<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
<!-- Main Footer -->
<footer class="main-footer">
    <strong>Copyright &copy; {{date('Y')}} <a href="{{route('home')}}" target="_blank">Food-Squad</a></strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Powered by</b> <a href="https://tek4pro.com" target="_blank">tek4pro.com</a>
    </div>
</footer>