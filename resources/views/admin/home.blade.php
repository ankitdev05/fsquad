@extends('admin.layouts.app')

@section('title', 'KTE Dashboard')

@section('style')
    @include('admin.includes.style')
@endsection

@section('navbar')
    @include('admin.includes.navbar')
@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('body')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <!-- <li class="breadcrumb-item"><a href="#">Home</a></li> -->
                            <!-- <li class="breadcrumb-item active">Dashboard v2</li> -->
                        </ol>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
        	<div class="container-fluid" style="width: 100%; height: 500px; background-color: #FFF; margin: 0 auto 20px auto;">
        		<div class="row">
        			<div class="col-lg-12">
        				<h2 class="text-center muted-text" style="margin-top: 30px; font-family: 'Great Vibes', cursive; letter-spacing: 5px;">Welcome Admin |
                        <text class="text-center muted-text" style="margin-top: 20px; font-size: 0.8em; font-family: 'Pacifico', cursive; letter-spacing: 2px;">Fsquad Admin Panel</text></h2>
                        
        			</div>

        			<!-- <div class="col-lg-12">
        				<img src="{{URL::to("public/admin/dist/images/logo1.png")}}" class="img-fluid" style="
						  	  max-width: 100px; 
						  	  top: 50%;
    						  left: 50%;
    						  position: absolute;
    						  transform: translate(-50%,10%);" />
        			</div> -->
        		</div>
        	</div>

            <div class="container-fluid">
                <!-- Info boxes -->
                <div class="row">
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                
                <!-- /.row -->
                
            </div>
            <!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('footer')
    @include('admin.includes.footer')
@endsection

@section('scripts')
    @include('admin.includes.scripts')
@endsection
