@extends('admin.layouts.app')
@section('title', 'Manage Users')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Manage Users</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Manage Users</li>
                    </ol>

                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Manage Users</h3>
                <div class="card-tools">
                    
                </div>
            </div>
            <div class="card-body">
                <div class="card-dashboard">
                    <form method="post" action="{{ route('admin.filter.users') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="keyword">Enter Keyword</label>
                                    <input type="text" id="keyword" class="form-control" placeholder="Enter keyword" name="keyword" value="{{ @$key['keyword'] }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control newdrop" name="status" id="status">
                                        <option value="">Search by status</option>
                                        <option value="A" @if(@$key['status']=="A") selected @endif>Active</option>
                                        <option value="I" @if(@$key['status']=="I") selected @endif>Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success">
                            <i class="fa fa-search"></i> Search
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    No
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                             @php
                                $i=1;
                            @endphp
                            @if(sizeof(@$users)>0)
                                @foreach(@$users as $user)
                                    <tr>
                                        <td>
                                            {{ $i }}
                                        </td>
                                        <td>
                                            {{ $user->name }}
                                        </td>
                                        <td>
                                            {{ $user->email }}
                                        </td>
                                        <td class="project-state">
                                            <span class="badge @if($user->status=='A')badge-success @else badge-warning @endif">{{ $user->status == "A" ? "Active": "Inactive" }}</span>
                                        </td>
                                        <td class="project-actions text-center">
                                            @if($user->status=="I")
                                                <a class="btn btn-primary btn-xs" href="{{ route('admin.user.status', ['id' => @$user->id]) }}" onclick="return confirm('Do you really want to activate this user\'s account?');">
                                                    <i class="fas fa-check">
                                                    </i>
                                                    Active
                                                </a>
                                            @else
                                                <a class="btn btn-danger btn-xs" href="{{ route('admin.user.status', ['id' => @$user->id]) }}" onclick="return confirm('Do you really want to deactivate this user\'s account?');">
                                                    <i class="fas fa-times">
                                                    </i>
                                                    Inactive
                                                </a>
                                            @endif
                                            
                                        </td>
                                    </tr>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5"><h3 align="center"><img height="60" width="60" src="{{URL::to('public/admin/images/exclamatory.png')}}"> {{"No users found!"}}</h3></td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
@endsection
