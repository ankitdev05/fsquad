@extends('admin.layouts.app')
@section('title', 'Video Gamifications')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Video Gamifications</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Video Gamifications</li>
                    </ol>

                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Video Gamifications</h3>
                <div class="card-tools">
                    <a href="{{route('admin.add.video.gamification')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-plus"></i> Add Gamification</a>
                </div>
            </div>
            <div class="card-body">
                <div class="card-dashboard">
                    <form method="get" action="{{route('admin.video.gamification')}}">
                        @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Enter Keyword</label>
                                <input type="text" id="keyword" class="form-control" placeholder="Enter keyword" name="keyword" value="{{@$key['keyword']}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Enter Level</label>
                                <input type="text" id="level" class="form-control" placeholder="Enter level" name="level" value="{{@$key['level']}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectinput2">Is Top</label>
                                <select class="form-control newdrop" name="is_top" id="is_top">
                                    <option value="">Select your choice</option>
                                    <option value="Y" @if(@$key['is_top']=="Y") selected @endif>Yes</option>
                                    <option value="N" @if(@$key['is_top']=="N") selected @endif>No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">
                        <i class="fa fa-search"></i> Search
                        </button>
                    </div>
                </form>
                </div>
                
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    Image
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    No of Post
                                </th>
                                <th>
                                    Level
                                </th>
                                <th>
                                    Is Top
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>     
                            @if(sizeof(@$usageGami)>0)
                                @foreach(@$usageGami as $cat)
                                    <tr>
                                        <td>
                                            <img class="img-fluid" width="50" src="{{URL::to('storage/app/public/video_gamification_images').'/'.@$cat->image}}">

                                        </td>
                                        <td>
                                            {{@$cat->name}}
                                        </td>
                                        <td>
                                            {{@$cat->no_of_video_posted}}
                                        </td>
                                        <td>
                                            {{@$cat->level}}
                                        </td>
                                        
                                        <td class="project-state">
                                            <span class="badge @if($cat->is_top=='Y')badge-success @else badge-warning @endif">{{@$cat->is_top == "Y" ? "Yes": "No"}}</span>
                                        </td>
                                        <td class="project-actions text-center">
                                            <a class="btn btn-info btn-xs" 
                                            href="{{route('admin.edit.video.gamification',['id'=>@$cat->id])}}">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                            Edit
                                            </a>
                                            <a class="btn btn-danger btn-xs" href="{{route('admin.remove.video.gamification',['id'=>@$cat->id])}}" onclick="return confirm('Do you really want to remove this gamification ?');">
                                            <i class="fas fa-trash">
                                            </i>
                                            Delete
                                            </a>
                                        </td>
                                    </tr>
                                    
                                @endforeach
                            @else
                                <tr>
                                    <td>
                                        <td colspan="6"><h3 align="center"><img height="60" width="60" src="{{URL::to('public/admin/images/exclamatory.png')}}"> {{"Video gamification list not found"}}</h3></td>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                    <div style="float: right;">
                        {!!@$usageGami->links()!!}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
@endsection