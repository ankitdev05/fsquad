@extends('admin.layouts.app')
@section('title', 'Edit Cooked It Gamifications')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Cooked It Gamifications</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Edit Cooked It Gamifications</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Edit Cooked It Gamifications</h3>

              <div class="card-tools">
                <a href="{{route('admin.cooked.it.gamification')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Back</a>
              </div>
            </div>
            <div class="card-body">
                <form name="myForm" id="myForm" action="{{route('admin.update.cooked.it.gamification',['id'=>@$editGami->id])}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Gamifications Name</label>
                                <input type="text" id="name" placeholder="Enter gamification name" name="name" class="form-control" value="{{@$editGami->name}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">No of No of Cooked It</label>
                                <input type="text" id="no_of_i_cokked_it" placeholder="Enter no of cooked it" name="no_of_i_cokked_it" class="form-control" value="{{@$editGami->no_of_i_cokked_it}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Level</label>
                                <input type="text" id="level" placeholder="Enter gamification level" name="level" class="form-control" value="{{@$editGami->level}}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="projectinput2">Is Top</label>
                                <select class="form-control newdrop" name="is_top" id="is_top">
                                    <option value="">Select your choice</option>
                                    <option value="Y" @if(@$editGami->is_top=="Y") selected @endif>Yes</option>
                                    <option value="N" @if(@$editGami->is_top=="N") selected @endif>No</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Gamification Image</label>
                                <input type="file" id="image"  name="image" class="form-control">
                            </div>
                        </div>
                        @if(file_exists(storage_path().'/app/public/cooked_it_gamification_images/'.@$editGami->image)&& @$editGami->image!=null)
                          <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                              <div class="form-group">
                                  <label>Existing Image</label><br>
                                  <img width="100" class="img-fluid img-responsieve" src="{{URL::to('storage/app/public/cooked_it_gamification_images').'/'.@$editGami->image}}">
                              </div>
                          </div>
                        @endif
                        
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xs-12">
                          <input type="submit" value="Save" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
          </div>
          <!-- /.card -->
        <!-- </div>
      </div> -->
      
    </section>
  </div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
    <script>
        $('#myForm').validate({
            rules:{
                name:{
                    required:true
                },
                level:{
                  required:true,
                  digits:true
                },
                no_of_i_cokked_it:{
                  required:true,
                  digits:true
                },
                is_top:{
                  required:true
                }
            }
        });
    </script>
@endsection