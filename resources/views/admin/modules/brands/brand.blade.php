@extends('admin.layouts.app')
@section('title', 'All Brands')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Brands</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Brands</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Brands</h3>
                <div class="card-tools">
                    <a href="{{route('admin.add.brands')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-plus"></i> Add Brands</a>
                </div>
            </div>
            <div class="card-body">
                <div class="card-dashboard">
                    <form method="get" action="{{route('admin.brands')}}">
                        @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Enter Keyword</label>
                                <input type="text" id="keyword" class="form-control" placeholder="Enter keyword" name="keyword" value="{{@$key['keyword']}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectinput2">Status</label>
                                <select class="form-control newdrop" name="status" id="status">
                                    <option value="">Search by status</option>
                                    <option value="A" @if(@$key['status']=="A") selected @endif>Active</option>
                                    <option value="I" @if(@$key['status']=="I") selected @endif>Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">
                        <i class="fa fa-search"></i> Search
                        </button>
                    </div>
                </form>
                </div>
                
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                   Image
                                </th>
                                <th>
                                    Brand Name
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                             @php
                                $i=1;
                            @endphp
                            @if(sizeof(@$brand)>0)
                                @foreach(@$brand as $cat)
                                    <tr>
                                        <td>
                                            <img src="{{URL::to('storage/app/public/brand_images').'/'.@$cat->image}}" class="img-fluid" width="100">
                                        </td>
                                        <td>
                                            {{@$cat->name}}
                                        </td>
                                        <td class="project-state">
                                            <span class="badge @if($cat->status=='A')badge-success @else badge-warning @endif">{{@$cat->status == "A" ? "Active": "Inactive"}}</span>
                                        </td>
                                        <td class="project-actions text-center">
                                            @if(@$cat->status=="I")
                                                <a class="btn btn-primary btn-xs" href="{{route('admin.status.brands',['id'=>@$cat->id])}}" onclick="return confirm('Do you really want to active this brand ?');">
                                                <i class="fas fa-check">
                                                </i>
                                                Active
                                                </a>
                                            @else
                                                <a class="btn btn-danger btn-xs" href="{{route('admin.status.brands',['id'=>@$cat->id])}}" onclick="return confirm('Do you really want to inactive this brand ?');">
                                                <i class="fas fa-times">
                                                </i>
                                                Inactive
                                                </a>
                                            @endif
                                            <a class="btn btn-info btn-xs" href="{{route('admin.edit.brands',['id'=>@$cat->id])}}">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                            Edit
                                            </a>
                                            <a class="btn btn-danger btn-xs" href="{{route('admin.remove.brands',['id'=>@$cat->id])}}" onclick="return confirm('Do you really want to remove this brand ?');">
                                            <i class="fas fa-trash">
                                            </i>
                                            Delete
                                            </a>
                                        </td>
                                    </tr>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td>
                                        <td colspan="6"><h3 align="center"><img height="60" width="60" src="{{URL::to('public/admin/images/exclamatory.png')}}"> {{"Brand list not found"}}</h3></td>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                    <div style="float: right;">
                        {!!@$brand->links()!!}
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
@endsection