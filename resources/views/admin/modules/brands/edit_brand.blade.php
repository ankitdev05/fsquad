@extends('admin.layouts.app')
@section('title', 'Edit Brand')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Brand</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Edit Brand</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Edit Brand</h3>

              <div class="card-tools">
                <a href="{{route('admin.brands')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Back</a>
              </div>
            </div>
            <div class="card-body">
                <form name="myForm" id="myForm" action="{{route('admin.update.brands',['id'=>@$brand->id])}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Brand Name</label>
                                <input type="text" id="name" placeholder="Enter brand name" name="name" class="form-control" value="{{@$brand->name}}">
                            </div>
                        </div>
                        @if(file_exists(storage_path().'/app/public/brand_images/'.@$brand->image)&& @$brand->image!=null)
                          <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                              <div class="form-group">
                                  <label>Brand Image</label><br>
                                  <img width="100" class="img-fluid img-responsieve" src="{{URL::to('storage/app/public/brand_images').'/'.@$brand->image}}">
                              </div>
                          </div>
                        @endif
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Upload Image</label>
                                <input type="file" name="image" id="image" class="form-control">
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xs-12">
                          <input type="submit" value="Save" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
          </div>
          <!-- /.card -->
        <!-- </div>
      </div> -->
      
    </section>
  </div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
    <script>
        $('#myForm').validate({
            rules:{
                name:{
                    required:true
                }
            }
        });
    </script>
@endsection