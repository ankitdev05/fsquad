@extends('admin.layouts.app')
@section('title', 'All Ingredients')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Ingredients</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Ingredients</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Ingredients</h3>
                <div class="card-tools">
                    <a href="{{route('admin.add.ingredients')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-plus"></i> Add Ingredient</a>
                </div>
            </div>
            <div class="card-body">
                <div class="card-dashboard">
                    <form method="get" action="{{route('admin.ingredients')}}">
                        @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Enter Keyword</label>
                                <input type="text" id="keyword" class="form-control" placeholder="Enter keyword" name="keyword" value="{{@$key['keyword']}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectinput2">Select Brands</label>
                                <select class="form-control newdrop" name="brand_id" id="brand_id">
                                    <option value="">Search by status</option>
                                    @foreach(@$brand as $bn)
                                        <option value="{{@$bn->id}}" @if(@$key['brand_id']==@$bn->id) selected @endif>{{@$bn->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectinput2">Status</label>
                                <select class="form-control newdrop" name="status" id="status">
                                    <option value="">Search by status</option>
                                    <option value="A" @if(@$key['status']=="A") selected @endif>Active</option>
                                    <option value="I" @if(@$key['status']=="I") selected @endif>Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">
                        <i class="fa fa-search"></i> Search
                        </button>
                    </div>
                </form>
                </div>
                
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                   Image
                                </th>
                                <th>
                                    Product Name
                                </th>
                                <th>
                                    Brand Name
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                             @php
                                $i=1;
                            @endphp
                            @if(sizeof(@$ingr)>0)
                                @foreach(@$ingr as $cat)
                                    <tr>
                                        <td>
                                            <img src="{{URL::to('storage/app/public/ingredient_images').'/'.@$cat->image}}" class="img-fluid" width="100">
                                        </td>
                                        <td>
                                            {{$cat->name}}
                                        </td>
                                        <td>
                                            {{@$cat->brandName->name!=null ? @$cat->brandName->name: "N.A"}}
                                        </td>
                                        <td class="project-state">
                                            <span class="badge @if($cat->status=='A')badge-success @else badge-warning @endif">{{$cat->status == "A" ? "Active": "Inactive"}}</span>
                                        </td>
                                        <td class="project-actions text-center">
                                            @if($cat->status=="I")
                                                <a class="btn btn-primary btn-xs" href="{{route('admin.status.ingredients',['id'=>@$cat->id])}}" onclick="return confirm('Do you really want to active this ingredient ?');">
                                                <i class="fas fa-check">
                                                </i>
                                                Active
                                                </a>
                                            @else
                                                <a class="btn btn-danger btn-xs" href="{{route('admin.status.ingredients',['id'=>@$cat->id])}}" onclick="return confirm('Do you really want to inactive this ingredient ?');">
                                                <i class="fas fa-times">
                                                </i>
                                                Inactive
                                                </a>
                                            @endif
                                            <a class="btn btn-info btn-xs" href="{{route('admin.edit.ingredients',['id'=>@$cat->id])}}">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                            Edit
                                            </a>
                                            <a class="btn btn-danger btn-xs" href="{{route('admin.remove.ingredients',['id'=>@$cat->id])}}" onclick="return confirm('Do you really want to remove this ingredient ?');">
                                            <i class="fas fa-trash">
                                            </i>
                                            Delete
                                            </a>
                                        </td>
                                    </tr>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td>
                                        <td colspan="6"><h3 align="center"><img height="60" width="60" src="{{URL::to('public/admin/images/exclamatory.png')}}"> {{"Ingredient list not found"}}</h3></td>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                    <div style="float: right;">
                        {!!@$ingr->links()!!}
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
@endsection