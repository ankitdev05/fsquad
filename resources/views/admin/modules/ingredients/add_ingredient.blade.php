@extends('admin.layouts.app')
@section('title', 'Add Ingredients')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Ingredients</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Add Ingredients</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Add New Ingredients</h3>

              <div class="card-tools">
                <a href="{{route('admin.ingredients')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i>Back</a>
              </div>
            </div>
            <div class="card-body">
                <form name="myForm" id="myForm" action="{{route('admin.store.ingredients')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Ingredient Name</label>
                                <input type="text" id="name" placeholder="Enter ingredient name" name="name" class="form-control" value="{{old('name')}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Has Brand</label>
                                <select id="is_brand"  name="is_brand" class="form-control">
                                    <option value="">Has Brand</option>
                                    <option value="Y">Yes</option>
                                    <option value="N">No</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Select Brands</label>
                                <select id="brand_id"  name="brand_id" class="form-control" disabled>
                                    <option value="">Selects Brands</option>
                                    @foreach(@$brand as $bn)
                                      <option value="{{@$bn->id}}">{{@$bn->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Upload Image</label>
                                <input type="file" id="image"  name="image" class="form-control">
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xs-12">
                          <input type="submit" value="Save" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
          </div>
          <!-- /.card -->
        <!-- </div>
      </div> -->
      
    </section>
  </div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
    <script>
        $('#myForm').validate({
            rules:{
                name:{
                    required:true
                },
                image:{
                  required:true
                },
                is_brand:{
                  required:true
                }
            }
        });

        $('#is_brand').change(function(){
          if($('#is_brand').val()=="Y"){
            $('#brand_id').addClass('required');
            $('#brand_id').removeAttr("disabled", "disabled");
          }
          else{
              $('#brand_id').val('');
              $('#brand_id').removeClass('required');
              $('#brand_id').attr("disabled", "disabled");
          }
        });
    </script>
@endsection