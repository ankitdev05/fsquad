@extends('admin.layouts.app')
@section('title', 'KTE Add Testimonials')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Testimonials</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Add Testimonials</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Add new Testimonials</h3>

              <div class="card-tools">
                <a href="{{route('admin.testimonials')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Back</a>
              </div>
            </div>
            <div class="card-body">
                <form name="myForm" id="myForm" action="{{route('admin.store.testimonials')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Customer name</label>
                                <input type="text" id="customer_name" placeholder="Enter customer name" name="customer_name" class="form-control" value="{{old('customer_name')}}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Location</label>
                                <input type="text" id="location" placeholder="Enter location" name="location" class="form-control" value="{{old('location')}}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Comment</label>
                                <input type="text" id="comment" placeholder="Enter comment" name="comment" class="form-control" value="{{old('comment')}}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Upload Image</label>
                                <input type="file" id="image" name="image" class="form-control">
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xs-12">
                          <input type="submit" value="Save" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
          </div>
          <!-- /.card -->
        <!-- </div>
      </div> -->
      
    </section>
  </div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
    <script>
        $('#myForm').validate({
            rules:{
                customer_name:{
                    required:true
                },
                comment:{
                  required:true
                },
                location:{
                  required:true
                },
                image:{
                  required:true
                }
            }
        });
    </script>
@endsection