@extends('admin.layouts.app')
@if (Request::segment(2) == 'add-blog-category')
    @section('title', 'KTE Add Blog Categories')
@else
    @section('title', 'KTE Edit Blog Categories')
@endif
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>
                        @if (Request::segment(2) == 'add-blog-category')
                            Add Blog Categories
                        @else
                            Edit Blog Categories
                        @endif
                    </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">
                            @if (Request::segment(2) == 'add-blog-category')
                                Add Blog Categories
                            @else
                                Edit Blog Categories
                            @endif
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <div class="card-tools">
                    <a href="{{ route('admin.blog.categories') }}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Back</a>
                </div>
            </div>
            <div class="card-body">
                <form name="myForm" id="myForm" action="@if (Request::segment(2) == 'add-blog-category'){{ route('admin.store.blog.categories') }}@else{{ route('admin.update.blog.categories', ['id' => @$category->id]) }}@endif" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Category Name</label>
                                <input type="text" id="name" placeholder="Enter category name" name="name" class="form-control" value="{{ @old('name') ? @old('name') : @$category->name }}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Meta Title</label>
                                <input type="text" id="meta_title" placeholder="Enter meta title" name="meta_title" class="form-control" value="{{ @old('meta_title') ? @old('meta_title') : @$category->meta_title }}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Meta Description</label>
                                <input type="text" id="meta_description" placeholder="Enter meta description" name="meta_description" class="form-control" value="{{ @old('meta_description') ? @old('meta_description') : @$category->meta_description }}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Meta Keywords</label>
                                <input type="text" id="meta_keywords" placeholder="Enter meta keywords" name="meta_keywords" class="form-control" value="{{ @old('meta_keywords') ? @old('meta_keywords') : @$category->meta_keywords }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <input type="submit" value="Save" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
    <script>
        'use strict';
        $(document).ready(function() {
            $('#myForm').validate({
                rules: {
                    name: {
                        required: true
                    },
                    meta_title: {
                        required: true
                    },
                    meta_description: {
                        required: true
                    },
                    meta_keywords: {
                        required: true
                    }
                }
            });
        });
    </script>
@endsection
