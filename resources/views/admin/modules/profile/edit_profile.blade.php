@extends('admin.layouts.app')
@section('title', 'Update Profile')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Update Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Update Profile</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Update Profile</h3>

          <div class="card-tools">
            <a href="{{route('home')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Home</a>
          </div>
        </div>
        <div class="card-body">
            <form name="myForm" id="myForm" action="{{route('admin.update.profile')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                        <div class="form-group">
                            <label for="inputName">Name</label>
                            <input type="text" id="name" placeholder="Enter name" name="name" class="form-control" value="{{Auth::guard('web')->user()->name}}">
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                        <div class="form-group">
                            <label for="inputName">Email</label>
                            <input type="text" id="email" placeholder="Enter email" name="email" class="form-control" value="{{Auth::guard('web')->user()->email}}">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                        <div class="form-group">
                            <label for="inputName">Old password</label>
                            <input type="password" id="old_password" placeholder="Enter old password" name="old_password" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                        <div class="form-group">
                            <label for="inputName">New password</label>
                            <input type="password" id="new_password" placeholder ="Enter new password" name="new_password" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                        <div class="form-group">
                            <label for="inputName">Confirm password</label>
                            <input type="password" id="confirm_password" placeholder ="Enter confirm password" name="confirm_password" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xs-12">
                      <input type="submit" value="Save" class="btn btn-success">
                    </div>
                </div>
            </form>
        </div>
      </div>
          <!-- /.card -->
        <!-- </div>
      </div> -->
    </section>
  </div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
    <script>
        $('#myForm').validate({
            rules:{
                name:{
                    required:true
                },
                email:{
                  required:true,
                  email:true
                },
                confirm_password:{
                  equalTo:"#new_password"
                }
            }
        });
        $('#old_password').change(function(){
          if($('#old_password').val()!=""){
            $('#old_password').addClass('required');
            $('#new_password').addClass('required');
          }
          else{
            $('#old_password').removeClass('required');
            $('#new_password').removeClass('required');
          }
        });
        $('#myForm').submit(function(){
          if($('#old_password').val()!=""){
            $('#old_password').addClass('required');
            $('#new_password').addClass('required');
          }
          else{
            $('#old_password').removeClass('required');
            $('#new_password').removeClass('required');
          }
        });
    </script>
@endsection