@extends('admin.layouts.app')
@section('title', 'KTE Newsletter Subscriptions')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Newsletter Subscriptions</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Newsletter Subscriptions</li>
                    </ol>

                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Newsletter Subscriptions</h3>
                <div class="card-tools">
                    <a href="{{route('admin.dashboard')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Home</a>
                </div>
            </div>
            <div class="card-body">
                <div class="card-dashboard">
                    <form method="post" action="{{route('admin.search.newsletter')}}">
                        @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Enter email</label>
                                <input type="text" id="email" class="form-control" placeholder="Enter email" name="email" value="{{@$key['email']}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">
                        <i class="fa fa-search"></i> Search
                        </button>
                    </div>
                </form>
                </div>
                
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    No
                                </th>
                                <th>
                                    Email
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                             @php
                                $i=1;
                            @endphp
                            @if(sizeof(@$nl)>0)
                                @foreach(@$nl as $cat)
                                    <tr>
                                        <td>
                                            {{$i}}
                                        </td>
                                        <td>
                                            {{$cat->email}}
                                        </td>
                                    </tr>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td>
                                        <td colspan="2"><h3 align="center"><img height="60" width="60" src="{{URL::to('public/admin/images/exclamatory.png')}}"> {{"Newsletter list not found"}}</h3></td>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
@endsection