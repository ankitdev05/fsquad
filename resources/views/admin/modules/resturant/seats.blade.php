@extends('admin.layouts.app')
@section('title', 'KTE manage seats')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Manage Seats</h1>

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Seats</li>

                    </ol>

                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><a href="{{route('admin.manage.slot',['id'=>@$res_id])}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i>Back</a></h3>
                <div class="card-tools">
                    <a href="{{route('admin.add.seat',['id'=>@$id, 'res'=>@$res_id])}}" class="btn btn-primary" style="float: right;"><i class="fas fa-plus"></i>Add Seat</a>
                </div>
            </div>
            
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    Seat Quantity
                                </th>
                                <th>
                                    From Time
                                </th>
                                <th>
                                    To Time
                                </th>

                                <th>
                                    Price
                                </th>

                                <th>
                                    Action
                                </th>
                                
                            </tr>
                        </thead>
                        <tbody>
                             @php
                                $i=1;
                            @endphp
                            @if(sizeof(@$seat)>0)
                                @foreach(@$seat as $rs)
                                    <tr>
                                        <td>
                                            {{@$rs->seat}}
                                        </td>
                                        <td>
                                            {{@$rs->from_time}}
                                        </td>
                                        <td>
                                            {{@$rs->to_time}}
                                        </td>

                                        <td>
                                            ${{@$rs->price}}
                                        </td>

                                        
                                        <td class="project-actions text-center">
                                            <a class="btn btn-info btn-xs" href="{{route('admin.edit.seat',['id'=>@$id, 'res'=>@$res_id,'seat'=>@$rs->id])}}">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                            Edit
                                            </a>
                                            
                                            <a class="btn btn-danger btn-xs" href="{{route('admin.remove.seat',['id'=>@$rs->id])}}" onclick="return confirm('Do you really want to remove this seat ?');">
                                            <i class="fas fa-trash">
                                            </i>
                                            Delete
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td>
                                        <td colspan="6"><h3 align="center"><img height="60" width="60" src="{{URL::to('public/admin/images/exclamatory.png')}}"> {{"Seat list not found"}}</h3></td>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                    
            </div>
        </div>

    </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
@endsection