@extends('admin.layouts.app')
@section('title', 'KTE Add Slot')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Slot</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Add Slot</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Add new slot</h3>

              <div class="card-tools">
                <a href="{{route('admin.manage.slot',['id'=>@$id])}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Back</a>
              </div>
            </div>
            <div class="card-body">
                <form name="myForm" id="myForm" action="{{route('admin.store.slot',['id'=>@$id])}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">From Date</label>
                                <input type="text" id="from_date" placeholder="Select From date" name="from_date" class="form-control" value="{{old('from_date')}}" readonly>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">To Date</label>
                                <input type="text" id="to_date" placeholder="Select To date" name="to_date" class="form-control" value="{{old('to_date')}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xs-12">
                          <input type="submit" value="Save" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
          </div>
          <!-- /.card -->
        <!-- </div>
      </div> -->
      
    </section>
  </div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
    <script>
        $('#myForm').validate({
            rules:{
                from_date:{
                    required:true
                },
                to_date:{
                    required:true
                }
            }
        });
    </script>
    <script>
        $(function(){
          if($("#from_date" ).val()!=""){
              var rdt = new Date($("#from_date" ).val());
              rdt.setDate(rdt.getDate() + 1);
              // $("#to_date").datepicker("option", "minDate", rdt);
              $( "#to_date" ).datepicker({
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                minDate:rdt
              });
          }
          else{
            $( "#to_date" ).datepicker({
              numberOfMonths: 1,
              dateFormat: 'yy-mm-dd'
            });
          }
        });

        $( "#from_date" ).datepicker({
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
            onSelect: function (selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() + 1);
                $("#to_date").datepicker("option", "minDate", dt);
            }
        });
    </script>
@endsection