@extends('admin.layouts.app')
@section('title', 'KTE Add Seat')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Seat</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Add Seat</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Add new seat</h3>

              <div class="card-tools">
                <a href="{{route('admin.manage.seat',['id'=>@$slot_id,'res'=>@$res_id])}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Back</a>
              </div>
            </div>
            <div class="card-body">
                <form name="myForm" id="myForm" action="{{route('admin.store.seat',['id'=>@$slot_id,'res'=>@$res_id])}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Seat Quantity</label>
                                <input type="text" id="qty" placeholder="Enter seat quantity" name="qty" class="form-control" value="{{old('qty')}}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Seat Price</label>
                                <input type="text" id="price" placeholder="Enter price" name="price" class="form-control" value="{{old('price')}}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">From Time</label>
                                <input type="text" id="from_time" placeholder="Select from time" name="from_time" class="form-control timepicker" value="{{old('from_time')}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">To Time</label>
                                <input type="text" id="to_time" placeholder="Select To time" name="to_time" class="form-control timepicker" value="{{old('to_time')}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xs-12">
                          <input type="submit" value="Save" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
          </div>
          <!-- /.card -->
        <!-- </div>
      </div> -->
      
    </section>
  </div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
    <script>
        $('#myForm').validate({
            rules:{
                from_time:{
                    required:true
                },
                to_time:{
                    required:true
                },
                price:{
                  required:true,
                  number:true
                },
                qty:{
                  required:true,
                  min:1
                }
            }
        });
    </script>
  <script>
      $('.timepicker').timepicker({
        timeFormat: 'h:mm p',
        interval: 10,
        minTime: '00',
        maxTime: '23:55am',
        dynamic: false,
        dropdown: true,
        scrollbar: true
      });
  </script>
@endsection