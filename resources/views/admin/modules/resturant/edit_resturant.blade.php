@extends('admin.layouts.app')
@section('title', 'KTE Edit Services')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Edit Services</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Edit Services</li>
          </ol>
        </div>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Edit Services</h3>
        <div class="card-tools">
          <a href="{{route('admin.resturant')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Back</a>
        </div>
      </div>
      <div class="card-body">
        <form name="myForm" id="myForm" action="{{route('admin.update.resturant', ['id'=>@$resturant->id])}}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
              <div class="form-group">
                <label for="inputName">Services Name</label>
                <input type="text" id="name" placeholder="Enter Services name" name="name" class="form-control required" value="{{@$resturant->name}}">
              </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                <div class="form-group">
                    <label for="name">Category</label>
                    <select class="form-control newdrop required" name="category_id" id="category_id">
                        <option value="">Search by Category</option>
                        @foreach(@$category as $ct)
                            <option value="{{@$ct->id}}" @if(@$ct->id==@$resCats->cat_id) selected @endif>{{@$ct->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                <div class="form-group">
                    <label for="inputName">From Date</label>
                    <input type="text" id="from_date" placeholder="Select from time" name="from_date" class="form-control timepicker" readonly value="{{@$resturant->from_date}}">
                </div>
            </div>

            <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                <div class="form-group">
                    <label for="inputName">To Date</label>
                    <input type="text" id="to_date" placeholder="Select To time" name="to_date" class="form-control timepicker" readonly value="{{@$resturant->to_date}}">
                </div>
            </div>

            <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                <div class="form-group">
                    <label for="inputName">Advance Amount</label>
                    <input type="text" id="price" placeholder="Enter Price" name="price" class="form-control timepicker" value="{{@$resturant->price}}">
                </div>
            </div>
            

            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
              <div class="form-group">
                <label for="inputName">Description</label>
                <textarea class="description form-control" name="description" id="description" placeholder="Enter Services description">{{$resturant->description}}</textarea>
                <p id="p1" style="font-weight: 700; margin-bottom: .5rem; cursor: default;" class="error"></p>
              </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
              <div class="form-group">
                <label for="inputName">Meta title</label>
                <input type="text" id="meta_title" placeholder="Enter meta title" name="meta_title" class="form-control required" value="{{$resturant->meta_title}}">
              </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
              <div class="form-group">
                <label for="inputName">Meta keyword</label>
                <input type="text" id="meta_keyword" placeholder="Enter meta keyword" name="meta_keyword" class="form-control required" value="{{$resturant->meta_keyword}}">
                
              </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
              <div class="form-group">
                <label for="inputName">Meta Description</label>
                <textarea class="meta_description form-control" name="meta_description" id="meta_description" placeholder="Enter meta description">{{@$resturant->meta_description}}</textarea>
                <p id="p2" style="font-weight: 700; margin-bottom: .5rem; cursor: default;" class="error"></p>
              </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 col-xs-4">
              <div class="form-group">
                
                <input type="file" class="form-control" name="image[]" id="image" multiple>
                
              </div>
            </div>
            
          </div>
          <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
              
            <p><h4>Existing Images</h4></p>
            </div>
            @foreach(@$resturant->resturantImage as $img)
              <div class="col-md-4 col-sm-12 col-lg-4 col-xs-12" style="border-radius: 10px;">
                <div class="form-group">
                  <span class="pull-right" style="float: right;"><a href="{{route('remove.image',['id'=>@$img->id])}}"><i class="fas fa-trash" title="Remove this image"></i></a>
                  </span><img style="border-radius: 5px;" src="{{URL::to('storage/app/public/resturant_images').'/'.@$img->image}}" class="img-responsive img-fluid">
                </div>
              </div>
            @endforeach
          </div>
          <div class="row">
            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xs-12">
              <input type="submit" value="Save" class="btn btn-success">
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- /.card -->
    <!-- </div>
      </div> -->
  </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
<script>
  $('#myForm').validate({
      rules:{
          name:{
              required:true
          },
          from_date:{
            required: true
          },
          to_date:{
            required:true
          },
          price:{
            required:true,
            number:true
          }
      }
  });
  $('#myForm').submit(function(){
    var editorContent1 = tinyMCE.get('description').getContent();
    var editorContent2 = tinyMCE.get('meta_description').getContent();
    if (editorContent1 == '')
    {
      $('#p1').html("This field is required.");
      return false;
    }
    else{
      $('#p1').html("");
    }
    if (editorContent2 == '')
    {
      $('#p2').html("This field is required.");
      return false;
    }
    else{
      $('#p2').html("");
    }
  });
  $('#description').change(function(){
    var editorContent1 = tinyMCE.get('description').getContent();
    if (editorContent1 == '')
    {
      $('#p1').html("This field is required.");
      return false;
    }
    else{
      $('#p1').html("");
    }
  });
  $('#meta_description').change(function(){
    var editorContent1 = tinyMCE.get('meta_description').getContent();
    if (editorContent1 == '')
    {
      $('#p2').html("This field is required.");
      return false;
    }
    else{
      $('#p2').html("");
    }
  });
</script>
<script>
  tinyMCE.init({
    mode : "specific_textareas",
    editor_selector : "description",
    plugins: [
      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
      'save table contextmenu directionality emoticons template paste textcolor'
    ],
    relative_urls : false,
    remove_script_host : false,
    convert_urls : true,
    toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
    images_upload_url: '{{ URL::to('storage/app/public/tiny/') }}',
    images_upload_handler: function(blobInfo, success, failure) {
        var formD = new FormData();
        formD.append('file', blobInfo.blob(), blobInfo.filename());
        formD.append( "_token", '{{csrf_token()}}');
        $.ajax({
            url: '{{ route('tiny.img.upload') }}',
            data: formD,
            type: 'POST',
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'JSON',
            success: function(jsn) {
                if(jsn.status == 'ERROR') {
                    failure(jsn.error);
                } else if(jsn.status == 'SUCCESS') {
                    success(jsn.location);
                }
            }
        });
    }, 
  });
  tinyMCE.init({
    mode : "specific_textareas",
    editor_selector : "meta_description",
    plugins: [
      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
      'save table contextmenu directionality emoticons template paste textcolor'
    ],
    relative_urls : false,
    remove_script_host : false,
    convert_urls : true,
    toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
    images_upload_url: '{{ URL::to('storage/app/public/tiny/') }}',
    images_upload_handler: function(blobInfo, success, failure) {
        var formD = new FormData();
        formD.append('file', blobInfo.blob(), blobInfo.filename());
        formD.append( "_token", '{{csrf_token()}}');
        $.ajax({
            url: '{{ route('tiny.img.upload') }}',
            data: formD,
            type: 'POST',
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'JSON',
            success: function(jsn) {
                if(jsn.status == 'ERROR') {
                    failure(jsn.error);
                } else if(jsn.status == 'SUCCESS') {
                    success(jsn.location);
                }
            }
        });
    }, 
  });
</script>
  <script>
      $(function(){
        if($("#from_date" ).val()!=""){
            var rdt = new Date($("#from_date" ).val());
            rdt.setDate(rdt.getDate() + 1);
            // $("#to_date").datepicker("option", "minDate", rdt);
            $( "#to_date" ).datepicker({
              numberOfMonths: 1,
              dateFormat: 'yy-mm-dd',
              minDate:rdt
            });
        }
        else{
          $( "#to_date" ).datepicker({
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd'
          });
        }
      });

      $( "#from_date" ).datepicker({
          numberOfMonths: 1,
          dateFormat: 'yy-mm-dd',
          onSelect: function (selected) {
              var dt = new Date(selected);
              dt.setDate(dt.getDate() + 1);
              $("#to_date").datepicker("option", "minDate", dt);
          }
      });
  </script>
@endsection