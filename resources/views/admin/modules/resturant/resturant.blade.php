@extends('admin.layouts.app')
@section('title', 'KTE All Services')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Services</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Services</li>
                    </ol>

                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Services</h3>
                <div class="card-tools">
                    <a href="{{route('admin.add.resturant')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-plus"></i> Add Services</a>
                </div>
            </div>
            <div class="card-body">
                <div class="card-dashboard">
                    <form method="post" action="{{route('admin.resturant.filter')}}">
                        @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name">Enter Keyword</label>
                                <input type="text" id="keyword" class="form-control" placeholder="Enter keyword" name="keyword" value="{{@$key['keyword']}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name">Category</label>
                                <select class="form-control newdrop" name="category_id" id="category_id">
                                    <option value="">Search by Category</option>
                                    @foreach(@$category as $ct)
                                        <option value="{{@$ct->id}}" @if(@$key['category_id']==@$ct->id) selected @endif>{{@$ct->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="projectinput2">Status</label>
                                <select class="form-control newdrop" name="status" id="status">
                                    <option value="">Search by status</option>
                                    <option value="A" @if(@$key['status']=="A") selected @endif>Active</option>
                                    <option value="I" @if(@$key['status']=="I") selected @endif>Inactive</option>
                                </select>
                            </div>
                        </div>

                        
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">
                        <i class="fa fa-search"></i> Search
                        </button>
                    </div>
                </form>
                </div>
                
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    Image
                                </th>
                                <th>
                                    Service Name
                                </th>
                                <th>
                                    Description
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                             @php
                                $i=1;
                            @endphp
                            @if(sizeof(@$resturant)>0)
                                @foreach(@$resturant as $cat)
                                    
                                    <tr>
                                        <td>
                                            <img src="{{URL::to('storage/app/public/resturant_images/thumb_image').'/'.@$cat->ServiceDeafultImage->image}}" class="img-responsive img-fluid" width="100">
                                        </td>
                                        <td>
                                            {{$cat->name}}
                                        </td>
                                        <td>
                                            {!!strip_tags(substr($cat->description,0,80),'<br>')!!}...
                                        </td>
                                        <td class="project-state">
                                            <span class="badge @if($cat->status=='A')badge-success @else badge-warning @endif">{{$cat->status == "A" ? "Active": "Inactive"}}</span>
                                        </td>
                                        <td class="project-actions text-center">
                                            @if($cat->status=="I")
                                                <a class="btn btn-primary btn-xs" href="{{route('admin.status.resturant',['id'=>@$cat->id])}}" onclick="return confirm('Do you really want to active this Service ?');">
                                                <i class="fas fa-check">
                                                </i>
                                                Active
                                                </a>
                                            @else
                                                <a class="btn btn-danger btn-xs" href="{{route('admin.status.resturant',['id'=>@$cat->id])}}" onclick="return confirm('Do you really want to inactive this Service ?');">
                                                <i class="fas fa-times">
                                                </i>
                                                Inactive
                                                </a>
                                            @endif

                                            <a class="btn btn-info btn-xs" href="{{route('admin.edit.resturant',['id'=>@$cat->id])}}">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                            Edit
                                            </a>
                                            @if($cat->is_featured=="Y")
                                                <a class="btn btn-primary btn-xs" title="Make un-featured" href="{{route('admin.featured.resturant',['id'=>@$cat->id])}}" onclick="return confirm('Do you really want to unfeatured this Service ?');">
                                                <i class="fas fa-star" style="color:yellow">
                                                </i>
                                                
                                                </a>
                                            @else
                                                <a class="btn btn-info btn-xs"  title="Make featured" href="{{route('admin.featured.resturant',['id'=>@$cat->id])}}" onclick="return confirm('Do you really want to featured this Service ?');">
                                                <i class="fas fa-star">
                                                </i>
                                                </a>
                                            @endif

                                            @if($cat->is_popular=="Y")
                                                <a class="btn btn-success btn-xs" title="Make un-popular" href="{{route('admin.popular.resturant',['id'=>@$cat->id])}}" onclick="return confirm('Do you really want to make un-popular this Service ?');">
                                                <i class="fas fa-thumbs-down">
                                                </i>
                                                
                                                </a>
                                            @else
                                                <a class="btn btn-danger btn-xs" title="Make popular" href="{{route('admin.popular.resturant',['id'=>@$cat->id])}}" onclick="return confirm('Do you really want to make popular this Service ?');">
                                                <i class="fas fa-thumbs-up">
                                                </i>
                                                </a>
                                            @endif
                                            <a class="btn btn-danger btn-xs" href="{{route('admin.remove.resturant',['id'=>@$cat->id])}}" onclick="return confirm('Do you really want to remove this Service ?');">
                                            <i class="fas fa-trash">
                                            </i>
                                            Delete
                                            </a>
                                        </td>
                                    </tr>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td>
                                        <td colspan="6"><h3 align="center"><img height="60" width="60" src="{{URL::to('public/admin/images/exclamatory.png')}}"> {{"Service list not found"}}</h3></td>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                    <div class="row">
                    <div class="col-md-6">
                      <div class="last_pagi">
                        <div class="pagination_area pull-right">
                          <p>{!! @$resturant->links() !!}</p>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>

    </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
    <script>
        $(function(){
            if($("#from_date" ).val()!=""){
                  var rdt = new Date($("#from_date" ).val());
                  rdt.setDate(rdt.getDate() + 1);
                  // $("#to_date").datepicker("option", "minDate", rdt);
                  $( "#to_date" ).datepicker({
                    numberOfMonths: 1,
                    dateFormat: 'yy-mm-dd',
                    minDate:rdt
                  });
            }
        else{
          $( "#to_date" ).datepicker({
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd'
          });
        }
    });

    $( "#from_date" ).datepicker({
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
            onSelect: function (selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() + 1);
                $("#to_date").datepicker("option", "minDate", dt);
            }
        });
    </script>
@endsection