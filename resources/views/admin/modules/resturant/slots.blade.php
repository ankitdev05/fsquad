@extends('admin.layouts.app')
@section('title', 'KTE '.@$resturant->name.' slots')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>All slots for {{@$resturant->name}}</h1>

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Slots</li>

                    </ol>

                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><a href="{{route('admin.resturant')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i>Back</a></h3>
                <div class="card-tools">
                    <a href="{{route('admin.add.slot',['id'=>@$id])}}" class="btn btn-primary" style="float: right;"><i class="fas fa-plus"></i>Add Slot</a>
                </div>
            </div>
            <div class="card-body">
                <div class="card-dashboard">
                    <form method="post" action="{{route('admin.filter.slot',['id'=>@$resturant->id])}}">
                        @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name">From Date</label>
                                <input type="text" id="from_date" class="form-control" placeholder="Select from date" name="from_date" value="{{@$key['from_date']}}" readonly>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name">To date</label>
                                <input type="text" id="to_date" class="form-control" placeholder="Select to date" name="to_date" value="{{@$key['to_date']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">
                        <i class="fa fa-search"></i> Search
                        </button>
                    </div>
                </form>
                </div>
                
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    Resturant Name
                                </th>
                                <th>
                                    From Date
                                </th>
                                <th>
                                    To Date
                                </th>

                                <th>
                                    Total Seats
                                </th>

                                <th>
                                    Action
                                </th>
                                
                            </tr>
                        </thead>
                        <tbody>
                             @php
                                $i=1;
                            @endphp
                            @if(sizeof(@$resturantSlot)>0)
                                @foreach(@$resturantSlot as $rs)
                                    <tr>
                                        <td>
                                            {{@$resturant->name}}
                                        </td>
                                        <td>
                                            {{@$rs->from_date}}
                                        </td>
                                        <td>
                                            {{@$rs->to_date}}
                                        </td>

                                        <td>
                                            {{@$rs->resturantSeats->count()}}
                                        </td>

                                        
                                        <td class="project-actions text-center">
                                            
                                            <a class="btn btn-success btn-xs" href="{{route('admin.manage.seat',['id'=>@$rs->id, 'res'=>@$resturant->id])}}">
                                            <i class="fas fa-chair">
                                            </i>
                                            Seats
                                            </a>

                                            <a class="btn btn-info btn-xs" href="{{route('admin.edit.slot',['id'=>@$rs->id])}}">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                            Edit
                                            </a>
                                            
                                            <a class="btn btn-danger btn-xs" href="{{route('admin.remove.slot',['id'=>@$rs->id])}}" onclick="return confirm('Do you really want to remove this slot ?');">
                                            <i class="fas fa-trash">
                                            </i>
                                            Delete
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td>
                                        <td colspan="6"><h3 align="center"><img height="60" width="60" src="{{URL::to('public/admin/images/exclamatory.png')}}"> {{"Slot list not found"}}</h3></td>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                    
            </div>
        </div>

    </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
    <script>
        $(function(){
            if($("#from_date" ).val()!=""){
                  var rdt = new Date($("#from_date" ).val());
                  rdt.setDate(rdt.getDate() + 1);
                  // $("#to_date").datepicker("option", "minDate", rdt);
                  $( "#to_date" ).datepicker({
                    numberOfMonths: 1,
                    dateFormat: 'yy-mm-dd',
                    minDate:rdt
                  });
            }
        else{
          $( "#to_date" ).datepicker({
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd'
          });
        }
    });

    $( "#from_date" ).datepicker({
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
            onSelect: function (selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() + 1);
                $("#to_date").datepicker("option", "minDate", dt);
            }
        });
    </script>
@endsection