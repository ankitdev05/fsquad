@extends('admin.layouts.app')
@section('title', 'KTE Add Slider')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Slider</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Add Slider</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Add new Slider</h3>

              <div class="card-tools">
                <a href="{{route('admin.sliders')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Back</a>
              </div>
            </div>
            <div class="card-body">
                <form name="myForm" id="myForm" action="{{route('admin.store.slider')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Slider text</label>
                                <input type="text" id="text" placeholder="Enter slider text" name="text" class="form-control" value="{{old('text')}}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Slider url</label>
                                <input type="text" id="url" placeholder="Enter slider url" name="url" class="form-control" value="{{old('url')}}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Upload Image</label>
                                <input type="file" id="image" name="image" class="form-control">
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xs-12">
                          <input type="submit" value="Save" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
          </div>
          <!-- /.card -->
        <!-- </div>
      </div> -->
      
    </section>
  </div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
    <script>
        $('#myForm').validate({
            rules:{
                text:{
                    required:true
                },
                url:{
                  required:true,
                  url:true
                },
                image:{
                  required:true
                }
            }
        });
    </script>
@endsection