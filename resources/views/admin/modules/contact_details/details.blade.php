@extends('admin.layouts.app')
@section('title', 'KTE Contact Details')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Contact Details</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Contact Details</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Contact Details</h3>

              <div class="card-tools">
                <a href="{{route('admin.dashboard')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Home</a>
              </div>
            </div>
            <div class="card-body">
                <form name="myForm" id="myForm" action="{{route('admin.update.contact.details')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Phone Number 1</label>
                                <input type="text" id="phone_1" placeholder="Enter phone 1" name="phone_1" class="form-control required" value="{{@$details->phone_1}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Phone Number 2</label>
                                <input type="text" id="phone_2" placeholder="Enter phone 2" name="phone_2" class="form-control required" value="{{@$details->phone_2}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Email 1</label>
                                <input type="text" id="email_1" placeholder="Enter email 1" name="email_1" class="form-control required" value="{{@$details->email_1}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Email 2</label>
                                <input type="text" id="email_2" placeholder="Enter email 2" name="email_2" class="form-control required" value="{{@$details->email_2}}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Facebook link</label>
                                <input type="text" id="fb_link" placeholder="Enter facebook link" name="fb_link" class="form-control required url" value="{{@$details->fb_link}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Twitter link</label>
                                <input type="text" id="twtr_link" placeholder="Enter twitter link" name="twtr_link" class="form-control required url" value="{{@$details->twtr_link}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Instagram link</label>
                                <input type="text" id="insta" placeholder="Enter instagram link" name="insta" class="form-control required url" value="{{@$details->insta}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Skype link</label>
                                <input type="text" id="skype" placeholder="Enter skype url" name="skype" class="form-control required" value="{{@$details->skype}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Homepage meta title</label>
                                <input type="text" id="home_meta_title" placeholder="Enter homepage meta title" name="home_meta_title" class="form-control required" value="{{@$details->home_meta_title}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Contact-us meta title</label>
                                <input type="text" id="contact_meta_title" placeholder="Enter contact us meta title" name="contact_meta_title" class="form-control required" value="{{@$details->contact_meta_title}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">About-us meta title</label>
                                <input type="text" id="about_meta_title" placeholder="Enter about us meta title" name="about_meta_title" class="form-control required" value="{{@$details->about_meta_title}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Homepage meta keyword</label>
                                <input type="text" id="home_meta_keyword" placeholder="Enter homepage meta keyword" name="home_meta_keyword" class="form-control required" value="{{@$details->home_meta_keyword}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Contact-us meta keyword</label>
                                <input type="text" id="contact_meta_keyword" placeholder="Enter contact meta keyword" name="contact_meta_keyword" class="form-control required" value="{{@$details->contact_meta_keyword}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">About-us meta keyword</label>
                                <input type="text" id="about_meta_keyword" placeholder="Enter about us meta keyword" name="about_meta_keyword" class="form-control required" value="{{@$details->about_meta_keyword}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Homepage meta description</label>
                                <input type="text" id="home_meta_description" placeholder="Enter homepage meta description" name="home_meta_description" class="form-control required" value="{{@$details->home_meta_description}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Contact-us meta description</label>
                                <input type="text" id="contact_meta_description" placeholder="Enter contact us meta description" name="contact_meta_description" class="form-control required" value="{{@$details->contact_meta_description}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">About-us meta description</label>
                                <input type="text" id="about_meta_description" placeholder="Enter about us meta description" name="about_meta_description" class="form-control required" value="{{@$details->about_meta_description}}">
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xs-12">
                          <input type="submit" value="Save" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
          </div>
          <!-- /.card -->
        <!-- </div>
      </div> -->
      
    </section>
  </div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
    <script>
        $('#myForm').validate({
            rules:{
                name:{
                    required:true
                }
            }
        });
    </script>
@endsection