@extends('admin.layouts.app')
@if (Request::segment(2) == 'add-blog')
    @section('title', 'KTE Add Blog')
@else
    @section('title', 'KTE Edit Blog')
@endif
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>
                        @if (Request::segment(2) == 'add-blog')
                            Add Blog
                        @else
                            Edit Blog
                        @endif
                    </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">
                            @if (Request::segment(2) == 'add-blog')
                                Add Blog
                            @else
                                Edit Blog
                            @endif
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <div class="card-tools">
                    <a href="{{ route('admin.blogs') }}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Back</a>
                </div>
            </div>
            <div class="card-body">
                <form name="myForm" id="myForm" action="@if (Request::segment(2) == 'add-blog'){{ route('admin.store.blog') }}@else{{ route('admin.update.blog', ['id' => @$blog->id]) }}@endif" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Blog Title</label>
                                <input type="text" id="title" placeholder="Enter blog title" name="title" class="form-control" value="{{ @old('title') ? @old('title') : @$blog->title }}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Category</label>
                                <select name="blog_category_id" id="blog_category_id" class="form-control">
                                    <option value="">Choose Category</option>
                                    @foreach ($categories as $cat)
                                        <option value="{{ $cat->id }}" @if ((@old('blog_category_id') ? @old('blog_category_id') : @$blog->blog_category_id) == $cat->id ) selected @endif>{{ $cat->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Meta Title</label>
                                <input type="text" id="meta_title" placeholder="Enter meta title" name="meta_title" class="form-control" value="{{ @old('meta_title') ? @old('meta_title') : @$blog->meta_title }}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Meta Description</label>
                                <input type="text" id="meta_description" placeholder="Enter meta description" name="meta_description" class="form-control" value="{{ @old('meta_description') ? @old('meta_description') : @$blog->meta_description }}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Meta Keywords</label>
                                <input type="text" id="meta_keywords" placeholder="Enter meta keywords" name="meta_keywords" class="form-control" value="{{ @old('meta_keywords') ? @old('meta_keywords') : @$blog->meta_keywords }}">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="meta_keywords">Event Description</label>
                                <textarea name="description" id="description">{{ @old('description') ? @old('description') : @$blog->description }}</textarea>
                                <label for="description" style="display: none;" class="error">This field is required.</label>
                            </div>
                        </div>
                        <div class="col-12">
                            <label class="custom-file-upload-btn btn btn-success">
                                <input type="file" name="blog_image" id="blog_image" accept=".png, .jpeg, .jpg" style="display: none;">
                                <i class="fas fa-cloud-upload-alt"></i> Upload Event Images
                            </label>
                            <label for="blog_image" style="display: none;" class="error">This field is required.</label>
                            <div class="chosen-event-images" id="chosen-event-images" style="margin-bottom: 5px;">
                                @if (@$blog)
                                    <div class="chosen-image">
                                        <img src="{{ URL::to(Config::get('globals.path_blog_image') . @$blog->blog_image) }}" alt="Blog Image" class="img-fluid img-responsieve" width="500">
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xs-12">
                            <input type="submit" value="Save" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
    <script>
        'use strict';
        $(document).ready(function () {
            tinymce.init({
                selector: '#description',
                height: 400,
                toolbar: [
                    'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
                ]
            });
            $('#myForm1').validate({
                ignore: [],
                rules: {
                    title: {
                        required: true
                    },
                    blog_category_id: {
                        required: true
                    },
                    meta_title: {
                        required: true
                    },
                    meta_description: {
                        required: true
                    },
                    meta_keywords: {
                        required: true
                    },
                    blog_image: {
                        required: true
                    }
                }
            });
            $('#blog_image').change(function() {
                $('#chosen-event-images').html('');
                if (this.files.length > 0) {
                    var validExts = ['image/jpeg', 'image/png']
                    var allValid = true;
                    var file = this.files[0];
                    if (validExts.indexOf(file.type) <= -1) {
                        allValid = false
                        return;
                    }
                    if (! allValid) {
                        toastr.options.positionClass = 'toast-bottom-full-width';
                        toastr.error('<strong>You must choose only image files (png, jpeg files).</strong>');
                        return
                    }
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#chosen-event-images').append('<div class="chosen-image"><img src="'+e.target.result+'" class="img-fluid img-responsieve" width="500"></div>');
                    };
                    reader.readAsDataURL(file);
                }
            });
        });
    </script>
@endsection
