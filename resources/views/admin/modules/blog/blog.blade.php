@extends('admin.layouts.app')
@section('title', 'KTE Blogs')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Blog</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Blog</li>
                    </ol>

                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                {{-- <h3 class="card-title">Blog Categories</h3> --}}
                <div class="card-tools">
                    <a href="{{ route('admin.add.blog') }}" class="btn btn-primary" style="float: right;"><i class="fas fa-plus"></i> Create New Blog</a>
                </div>
            </div>
            <div class="card-body">
                <div class="card-dashboard">
                    <form method="post" action="{{ route('admin.filter.blogs') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="keyword">Search By Keyword</label>
                                    <input type="text" id="keyword" class="form-control" placeholder="Enter keyword" name="keyword" value="{{ @$key['keyword'] }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="blog_category_id">Search By Category</label>
                                    <select class="form-control newdrop" name="blog_category_id" id="blog_category_id">
                                        <option value="">Search by category</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}" @if(@$key['blog_category_id'] == $category->id) selected @endif>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status">Search By Status</label>
                                    <select class="form-control newdrop" name="status" id="status">
                                        <option value="">Search by status</option>
                                        <option value="A" @if(@$key['status']=="A") selected @endif>Active</option>
                                        <option value="I" @if(@$key['status']=="I") selected @endif>Inactive</option>
                                    </select>
                                </div>
                            </div> -->
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success">
                            <i class="fa fa-search"></i> Search
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    No
                                </th>
                                <th>
                                    Blog Name
                                </th>
                                <th>
                                    Category
                                </th>
                                <th>
                                    Description
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i=1;
                            @endphp
                            @if(sizeof(@$blogs)>0)
                                @foreach(@$blogs as $blog)
                                    <tr>
                                        <td>
                                            {{ $i }}
                                        </td>
                                        <td>
                                            {{ $blog->title }}
                                        </td>
                                        <td>
                                            {{ $blog->blogCategory->name }}
                                        </td>
                                        <td class="project-state">
                                            {!! substr(strip_tags(@$blog->description.'<br>'),0,300) !!}...
                                        </td>
                                        <td class="project-actions text-center">
                                            <!-- @if($blog->status=="I")
                                                <a class="btn btn-primary btn-xs" href="{{ route('admin.status.blog', ['id' => @$blog->id]) }}" onclick="return confirm('Do you really want to activate this blog?');">
                                                    <i class="fas fa-check"></i>
                                                    Active
                                                </a>
                                            @else
                                                <a class="btn btn-danger btn-xs" href="{{ route('admin.status.blog', ['id' => @$blog->id]) }}" onclick="return confirm('Do you really want to deactivate this blog?');">
                                                    <i class="fas fa-times"></i>
                                                    Inactive
                                                </a>
                                            @endif -->
                                            <a class="btn btn-info btn-xs" href="{{ route('admin.edit.blog', ['id' => @$blog->id]) }}">
                                                <i class="fas fa-pencil-alt"></i>
                                                Edit
                                            </a>
                                            <a class="btn btn-danger btn-xs" href="{{ route('admin.remove.blog', ['id' => @$blog->id]) }}" onclick="return confirm('Do you really want to remove this blog?');">
                                                <i class="fas fa-trash"></i>
                                                Delete
                                            </a>
                                            <!-- <a class="btn btn-secondary btn-xs" href="{{ route('admin.blog.comments', ['id' => @$blog->id]) }}">
                                                <i class="far fa-comment"></i>
                                                Comments
                                            </a> -->
                                        </td>
                                    </tr>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5"><h3 align="center"><img height="60" width="60" src="{{ URL::to('public/admin/images/exclamatory.png') }}"> {{ "Blog list not found" }}</h3></td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
@endsection
