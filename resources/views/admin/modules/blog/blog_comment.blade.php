@extends('admin.layouts.app')
@section('title', 'KTE Blog Comments')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Blog Comments</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Blog Comments</li>
                    </ol>

                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                {{-- <h3 class="card-title">Blog Categories</h3> --}}
                <div class="card-tools">
                    <a href="{{ route('admin.blogs') }}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Back To Blogs</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    No
                                </th>
                                <th>
                                    User
                                </th>
                                <th>
                                    Comment
                                </th>
                                <th>
                                    Commented On
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i=1;
                            @endphp
                            @if(sizeof(@$comments)>0)
                                @foreach(@$comments as $comment)
                                    <tr>
                                        <td>
                                            {{ @$i }}
                                        </td>
                                        <td>
                                            {{ @$comment->blogUser->name }}
                                        </td>
                                        <td>
                                            {{ @$comment->comment }}
                                        </td>
                                        <td>
                                            {{ date('jS M, Y h:i A', strtotime($comment->created_at)) }}
                                        </td>
                                    </tr>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4"><h3 align="center"><img height="60" width="60" src="{{ URL::to('public/admin/images/exclamatory.png') }}"> {{ "No comments found!" }}</h3></td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
@endsection
