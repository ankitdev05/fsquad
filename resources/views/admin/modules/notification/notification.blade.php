@extends('admin.layouts.app')
@section('title', 'KTE Send Notifications')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Send Notifications</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Send Notifications</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Send Notifications</h3>

              <div class="card-tools">
                <a href="{{route('admin.dashboard')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Home</a>
              </div>
            </div>
            <div class="card-body">
                <form name="myForm" id="myForm" action="{{route('admin.send.notification')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Notification title</label>
                                <input type="text" id="title" name="title" placeholder="Enter notification title" title="Enter notification title." class="form-control" value="{{old('title')}}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Notification url</label>
                                <input type="text" id="url" name="url" placeholder="Enter notification url" title="Enter notification url." class="form-control" value="{{old('url')}}">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                            <div class="form-group">
                                <label for="inputName">Notification message</label>
                                <textarea id="message" name="message" rows="9" cols="80" placeholder="Enter notification message" title="Enter notification message." class="form-control">{{old('message')}}</textarea>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xs-12">
                          <input type="submit" value="Send" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
          </div>
          <!-- /.card -->
        <!-- </div>
      </div> -->
      
    </section>
  </div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
    <script>
        $('#myForm').validate({
            rules:{
                title:{
                    required:true
                },
                message:{
                    required:true
                },
                url:{
                    required:true,
                    url:true
                }
            }
        });
    </script>
@endsection