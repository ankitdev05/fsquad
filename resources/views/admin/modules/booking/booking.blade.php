@extends('admin.layouts.app')
@section('title', 'All Bookings')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar') 
@endsection
@section('body')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Bookings</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Bookings</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Bookings</h3>
                <div class="card-tools">
                    
                </div>
            </div>
            <div class="card-body">
                <div class="card-dashboard">
                    <form method="post" action="{{route('admin.bookings.filter')}}">
                        @csrf
                        <div class="row">
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name">Transaction id</label>
                                    <input type="text" id="transaction_id" class="form-control" placeholder="Enter transaction id" name="transaction_id" value="{{@$key['transaction_id']}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name">Email</label>
                                    <input type="text" id="email" class="form-control" placeholder="Enter email" name="email" value="{{@$key['email']}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name">Service Name</label>
                                    <select class="form-control newdrop" name="service" id="service">
                                        <option value="">Search by service name</option>
                                        @foreach(@$service as $ct)
                                            <option value="{{@$ct->id}}" @if(@$key['service']==@$ct->id) selected @endif>{{@$ct->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="projectinput2">Payment Status</label>
                                    <select class="form-control newdrop" name="status" id="status">
                                        <option value="">Search by status</option>
                                        <option value="Y" @if(@$key['status']=="Y") selected @endif>Paid</option>
                                        <option value="N" @if(@$key['status']=="N") selected @endif>Not Paid</option>
                                        <option value="F" @if(@$key['status']=="F") selected @endif>Failed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group">
                                <label for="name">From Date</label>
                                <input type="text" id="from_date" class="form-control" placeholder="Select from date" name="from_date" value="{{@$key['from_date']}}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name">To date</label>
                                <input type="text" id="to_date" class="form-control" placeholder="Select to date" name="to_date" value="{{@$key['to_date']}}">
                            </div>
                        </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success">
                            <i class="fa fa-search"></i> Search
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>User Name</th>
                                <th>Resturant Name</th>
                                <th>Booking Date</th>
                                <th>Amount</th>
                                <th>Approved</th>
                                <th>Payment Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(sizeof(@$booking)>0)
                            <tr>
                                @php
                                $i=1;
                                @endphp
                                @foreach(@$booking as $usr)
                                    <td scope="row">{{@$i}}</td>
                                    <td>{{@$usr->userDetails->name}}</td>
                                    <td>{{@$usr->serviceName->name}}</td>
                                    <td>{{date('Y-m-d', strtotime(@$usr->booking_date))}}</td>
                                    <td>₹{{@$usr->booking_amount}}</td>
                                    <td>{{@$usr->approved=="Y" ? "Yes": "No"}}</td>
                                    <td>
                                        @if(@$usr->is_paid=="N")
                                            <span class="badge badge-primary">Not Paid</span>
                                        @elseif(@$usr->is_paid=="F")
                                            <span class="badge badge-danger">Failed</span>
                                        @elseif(@$usr->is_paid=="Y")
                                            <span class="badge badge-success">Paid</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-primary btn-xs" href="{{route('admin.bookings.detail',['id'=>@$usr->transaction_id])}}"><i class="fa fa-eye"></i> View</a>

                                        @if(@$usr->approved=="N")
                                            <a class="btn btn-primary btn-xs" href="{{route('admin.approve.order',['id'=>@$usr->transaction_id])}}"><i class="fa fa-check"></i> Approve Order</a>
                                        @endif
                                        @if(@$usr->is_paid=="F")
                                            <a class="btn btn-success btn-xs" href="{{route('admin.mark.paid',['id'=>@$usr->transaction_id])}}" onclick="return confirm('Are you really want to mark this booking as paid ? ')"><i class="fas fa-money-bill-alt"></i>Mark as paid</a>
                                        @endif
                                    </td>
                            </tr>
                            @php
                            $i++;
                            @endphp
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6">
                                    <h3 align="center"><img height="60" width="60" src="{{URL::to('public/admin/images/exclamatory.png')}}"> {{"Booking list not found"}}</h3>
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="last_pagi">
                            <div class="pagination_area pull-right" style="float: right;">
                                <p>{!!$booking->links()!!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
<script>
    $(function(){
        if($("#from_date" ).val()!=""){
              var rdt = new Date($("#from_date" ).val());
              rdt.setDate(rdt.getDate() + 1);
              // $("#to_date").datepicker("option", "minDate", rdt);
              $( "#to_date" ).datepicker({
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                minDate:rdt
              });
        }
    else{
      $( "#to_date" ).datepicker({
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd'
      });
    }
});

$( "#from_date" ).datepicker({
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd',
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#to_date").datepicker("option", "minDate", dt);
        }
    });
</script>
@endsection