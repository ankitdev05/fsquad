@extends('admin.layouts.app')
@section('title', 'Bookings Details')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar') 
@endsection
@section('body')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Booking details</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Booking details</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="invoice p-3 mb-3" id="printableArea">
                        <!-- title row -->
                        <a href="{{route('admin.bookings')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Back</a>
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    <i class="fas fa-globe"></i> www.sherocks.in booking details.
                                  </h4>
                            </div>
                        </div>
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                                User Details
                                <address>
                                    <strong>{{@$booking->userDetails->name}}</strong><br>
                                    Phone: {{@$booking->userDetails->phone_no}}<br>
                                    Email: {{@$booking->userDetails->email}}
                                </address>
                            </div>
                            <div class="col-sm-4 invoice-col">
                                
                                <b>Transaction ID:</b> {{@$booking->transaction_id}}<br>
                                <b>Booking Date:</b>{{@$booking->booking_date}}<br>
                                <b>Amount:</b> ${{@$booking->booking_amount}}
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        <!-- Table row -->
                        <div class="row">
                            <div class="col-12 table-responsive">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Service Name</th>
                                            <th>Booking Date</th>
                                            <th>Approved</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{@$booking->serviceName->name}}</td>
                                            <td>{{date('Y-m-d',strtotime(@$booking->booking_date))}}</td>
                                            <td>{{@$booking->approved=="Y" ? "Yes": "Not Approved"}}</td>
                                            <td>₹{{@$booking->booking_amount}}</td>
                                            <td>
                                                @if(@$booking->is_paid=="Y")
                                                    <span class="badge badge-success">Paid</span>
                                                @elseif(@$booking->is_paid=="N")
                                                    <span class="badge badge-danger">Not Paid</span>
                                                @else
                                                    <span class="badge badge-primary">Failed</span>
                                                @endif
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        <div class="row">
                            
                            <!-- /.col -->
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        
                                        <tr>
                                            <th>Total:</th>
                                            <td>₹{{@$booking->booking_amount}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
@endsection