@extends('admin.layouts.app')
@section('title', 'Portfolios')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Portfolios</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Portfolios</li>
                    </ol>

                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Portfolios</h3>
                <div class="card-tools">
                    <a href="{{route('admin.add.portfolio')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-plus"></i> Add Portfolio</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    Image
                                </th>
                                <th>
                                    Text
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                             @php
                                $i=1;
                            @endphp
                            @if(sizeof(@$sl)>0)
                                @foreach(@$sl as $cat)
                                    <tr>
                                        <td>
                                            <img src="{{URL::to('storage/app/public/portfolio').'/'.@$cat->image}}" class="img-responsive img-fluid" width="100">
                                        </td>
                                        <td>
                                            {{$cat->text}}
                                        </td>

                                        <td>
                                            <a class="btn btn-info btn-xs" href="{{route('admin.edit.portfolio',['id'=>@$cat->id])}}">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                            Edit
                                            </a>

                                            <a class="btn btn-danger btn-xs" href="{{route('admin.remove.portfolio',['id'=>@$cat->id])}}" onclick="return confirm('Do you really want to remove this portfolio ?');">
                                            <i class="fas fa-trash">
                                            </i>
                                            Delete
                                            </a>
                                        </td>
                                    </tr>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td>
                                        <td colspan="3"><h3 align="center"><img height="60" width="60" src="{{URL::to('public/admin/images/exclamatory.png')}}"> {{"Portfolio list not found"}}</h3></td>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
@endsection