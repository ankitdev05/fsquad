@extends('admin.layouts.app')
@section('title', 'KTE About us')
@section('style')
@include('admin.includes.style')
@endsection
@section('navbar')
@include('admin.includes.navbar')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('body')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Update About-us</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Update About-us</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Update About-us</h3>

              <div class="card-tools">
                <a href="{{route('admin.dashboard')}}" class="btn btn-primary" style="float: right;"><i class="fas fa-arrow-left"></i> Home</a>
              </div>
            </div>
            <div class="card-body">
                <form name="myForm" id="myForm" action="{{route('admin.about.update')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                          <div class="form-group">
                            <label for="inputName">Description</label>
                            <textarea class="description form-control" name="description" id="description" placeholder="Enter resturant description">{{@$about->description}}</textarea>
                            <p id="p1" style="font-weight: 700; margin-bottom: .5rem; cursor: default;" class="error"></p>
                          </div>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                          <div class="form-group">
                            <label for="inputName">Our Mission</label>
                            <textarea class="our_mission form-control" name="our_mission" id="our_mission" placeholder="Our mission">{{@$about->our_mission}}</textarea>
                            <p id="p2" style="font-weight: 700; margin-bottom: .5rem; cursor: default;" class="error"></p>
                          </div>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                          <div class="form-group">
                            <label for="inputName">Our Vision</label>
                            <textarea class="our_vision form-control" name="our_vision" id="our_vision" placeholder="Our vision">{{@$about->our_vision}}</textarea>
                            <p id="p3" style="font-weight: 700; margin-bottom: .5rem; cursor: default;" class="error"></p>
                          </div>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                          <div class="form-group">
                            <label for="inputName">Why choose us</label>
                            <textarea class="why_choose_us form-control" name="why_choose_us" id="why_choose_us" placeholder="Why choose us">{{@$about->why_choose_us}}</textarea>
                            <p id="p4" style="font-weight: 700; margin-bottom: .5rem; cursor: default;" class="error"></p>
                          </div>
                        </div>

                        <div class="col-sm-4 col-md-4 col-lg-4 col-xs-4">
                          <div class="form-group">
                            <label for="inputName">Upload Image</label>
                            
                            <input type="file" class="form-control" name="image" id="image">
                          </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                          
                        <p><h4>Existing Image</h4></p>
                        </div>
                          <div class="col-md-4 col-sm-12 col-lg-4 col-xs-12" style="border-radius: 10px;">
                            <div class="form-group">
                              <img style="border-radius: 5px;" src="{{URL::to('storage/app/public/about_us').'/'.@$about->image}}" class="img-responsive img-fluid">
                            </div>
                          </div>
                      </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xs-12">
                          <input type="submit" value="Update" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
          </div>
          <!-- /.card -->
        <!-- </div>
      </div> -->
      
    </section>
  </div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
    <script>
        $('#myForm').submit(function(){
          var editorContent1 = tinyMCE.get('description').getContent();
          var editorContent2 = tinyMCE.get('our_mission').getContent();
          var editorContent3 = tinyMCE.get('our_vision').getContent();
          var editorContent4 = tinyMCE.get('why_choose_us').getContent();
          if (editorContent1 == '')
          {
            $('#p1').html("This field is required.");
            return false;
          }
          else{
            $('#p1').html("");
          }
          if (editorContent2 == '')
          {
            $('#p2').html("This field is required.");
            return false;
          }
          else{
            $('#p2').html("");
          }

          if (editorContent3 == '')
          {
            $('#p3').html("This field is required.");
            return false;
          }
          else{
            $('#p3').html("");
          }

          if (editorContent4 == '')
          {
            $('#p4').html("This field is required.");
            return false;
          }
          else{
            $('#p4').html("");
          }
        });
    </script>
    <script>
      tinyMCE.init({
        mode : "specific_textareas",
        editor_selector : "description",
        plugins: [
          'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
          'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
          'save table contextmenu directionality emoticons template paste textcolor'
        ],
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
        images_upload_url: '{{ URL::to('storage/app/public/tiny/') }}',
        images_upload_handler: function(blobInfo, success, failure) {
            var formD = new FormData();
            formD.append('file', blobInfo.blob(), blobInfo.filename());
            formD.append( "_token", '{{csrf_token()}}');
            $.ajax({
                url: '{{ route('tiny.img.upload') }}',
                data: formD,
                type: 'POST',
                contentType: false,
                cache: false,
                processData:false,
                dataType: 'JSON',
                success: function(jsn) {
                    if(jsn.status == 'ERROR') {
                        failure(jsn.error);
                    } else if(jsn.status == 'SUCCESS') {
                        success(jsn.location);
                    }
                }
            });
        }, 
      });
      tinyMCE.init({
        mode : "specific_textareas",
        editor_selector : "our_mission",
        plugins: [
          'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
          'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
          'save table contextmenu directionality emoticons template paste textcolor'
        ],
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
        images_upload_url: '{{ URL::to('storage/app/public/tiny/') }}',
        images_upload_handler: function(blobInfo, success, failure) {
            var formD = new FormData();
            formD.append('file', blobInfo.blob(), blobInfo.filename());
            formD.append( "_token", '{{csrf_token()}}');
            $.ajax({
                url: '{{ route('tiny.img.upload') }}',
                data: formD,
                type: 'POST',
                contentType: false,
                cache: false,
                processData:false,
                dataType: 'JSON',
                success: function(jsn) {
                    if(jsn.status == 'ERROR') {
                        failure(jsn.error);
                    } else if(jsn.status == 'SUCCESS') {
                        success(jsn.location);
                    }
                }
            });
        }, 
      });

      tinyMCE.init({
        mode : "specific_textareas",
        editor_selector : "our_vision",
        plugins: [
          'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
          'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
          'save table contextmenu directionality emoticons template paste textcolor'
        ],
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
        images_upload_url: '{{ URL::to('storage/app/public/tiny/') }}',
        images_upload_handler: function(blobInfo, success, failure) {
            var formD = new FormData();
            formD.append('file', blobInfo.blob(), blobInfo.filename());
            formD.append( "_token", '{{csrf_token()}}');
            $.ajax({
                url: '{{ route('tiny.img.upload') }}',
                data: formD,
                type: 'POST',
                contentType: false,
                cache: false,
                processData:false,
                dataType: 'JSON',
                success: function(jsn) {
                    if(jsn.status == 'ERROR') {
                        failure(jsn.error);
                    } else if(jsn.status == 'SUCCESS') {
                        success(jsn.location);
                    }
                }
            });
        }, 
      });

      tinyMCE.init({
        mode : "specific_textareas",
        editor_selector : "why_choose_us",
        plugins: [
          'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
          'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
          'save table contextmenu directionality emoticons template paste textcolor'
        ],
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
        images_upload_url: '{{ URL::to('storage/app/public/tiny/') }}',
        images_upload_handler: function(blobInfo, success, failure) {
            var formD = new FormData();
            formD.append('file', blobInfo.blob(), blobInfo.filename());
            formD.append( "_token", '{{csrf_token()}}');
            $.ajax({
                url: '{{ route('tiny.img.upload') }}',
                data: formD,
                type: 'POST',
                contentType: false,
                cache: false,
                processData:false,
                dataType: 'JSON',
                success: function(jsn) {
                    if(jsn.status == 'ERROR') {
                        failure(jsn.error);
                    } else if(jsn.status == 'SUCCESS') {
                        success(jsn.location);
                    }
                }
            });
        }, 
      });
    </script>
@endsection