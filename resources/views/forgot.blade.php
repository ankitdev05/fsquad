<!DOCTYPE html>
<html lang="en">
<head>
    <title> Fsquad </title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <style type="text/css">       
        @import url('https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap');
        body {
            margin: 0;
            padding: 0;
            font-family: 'Poppins', sans-serif !important;
            font-size: 13px;
            color: #3a3a3a;
            font-weight: 400;
        }

        .Wrapper{
            max-width: 770px;
            margin: auto;
            min-height: 100vh;
        }

        .Wrapper header{
            background-color: #2f2f3c;
            padding: 1px 0;
        }

        .Wrapper header .Logo{
            width: 113px;
            margin: 10px auto;
        } 

        .Wrapper header .Logo img{
            width: 100%
        }

        .ForgotBox{
            background-color: #f5f5f5;
            padding: 25px 30px;
            min-height: 57vh
        }

        .ForgotBox .form-group{
            margin: 0 0 20px;
        }

        .ForgotBox .form-group label{
            font-weight: 500;
            color: #404549;
            font-size: 14px;
            display: block;
            margin: 0 0 5px;
        } 

        .ForgotBox .form-group .form-control{
            box-shadow: none;
            border: 1px solid #ddd;
            border-radius: 0;
            height: 40px;
            padding: 0 10px;
            font-size: 14px;
            width: 100%;
            display: block;
            max-width: 100%;
            outline: 0;
        }

        .ForgotBox button {
            width: 100%;
            border: none;
            outline: 0;
            background-color: #3b3b47;
            color: #fff;
            padding: 10px 0;
            border-radius: 0;
            font-size: 15px;
            transition: 0.5s all ease-in-out;
            display: block;
            text-align: center;
        }

    </style>

</head>
<body>
    <div class="Wrapper">
        <header>
            <div class="Logo">
                <img src="http://18.220.123.51/storage/app/public/appicon.png">
            </div>
        </header>

        <div class="ForgotBox">
            <div id="res_message_error" style="display:none;"> 
                <div class="alert alert-danger">
                  <strong>Danger!</strong><span id="res_message_error_msg"> Indicates a dangerous or potentially negative action.</span>
                </div>
            </div>

            <div id="res_message_success" style="display:none;"> 
                <div class="alert alert-success">
                  <strong>Danger!</strong><span id="res_message_success_msg"> Indicates a dangerous or potentially negative action.</span>
                </div>
            </div>

            <form id="contact_us" action="javascript:void(0)" method="POST">
                @csrf
                <div class="form-group">
                    <label>Verification Code</label>
                    <input type="text" name="vcode" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="text" name="password" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password" name="confirm_password" class="form-control" />
                </div>
                <input type="hidden" name="user_id" value="{{ $email }}">
                <button type="button" id="send_form">Submit</button>
            </form>
        </div>
    </div>
</body>

<script type="text/javascript">
    $(document).ready(function() {
        $("#send_form").click(function(e) {
            e.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#send_form').html('Sending....');

            var vcode = $("input[name=vcode]").val();
            var password = $("input[name=password]").val();
            var confirm_password = $("input[name=confirm_password]").val();
            var user_id = $("input[name=user_id]").val();

            $.ajax({
                url : "{{ route('admin.update-forget-password') }}",
                method : "post",
                data : {"_token": "{{ csrf_token() }}", vcode:vcode, new_password:password, confirm_password:confirm_password, user_id:user_id},
                success : function(response) {
                    console.log(response);
                
                    if(response.status == 'ERROR') {
                        $('#send_form').html('Submit');    
                        $("#res_message_error").css('display','block');
                        $("#res_message_error_msg").html(response.error.message.meaning);

                    } else {
                        $("#res_message_error").css('display','none');
                        $("#res_message_success").css('display','block');
                        $("#res_message_success_msg").html('Your Password has been change successfully');

                        setTimeout(function() {
                            window.close();
                        }, 5000);
                    }
                },
                error : function() {
                    console.log('error');
                }
            });
        });
    });
</script>
</html>
