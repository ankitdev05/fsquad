<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login', 'Api\UserController@login');
Route::post('sendotp', 'Api\UserController@sendotp');
Route::post('matchotp', 'Api\UserController@matchotp');

Route::post('register', 'Api\UserController@register');

Route::post('forgot-password', 'Api\UserController@forgetPass');

Route::get('change-password/{user}', 'Api\UserController@changePass');

Route::post('verify-vcode', 'Api\UserController@verifyVcode');

Route::post('update-forget-password', 'Api\UserController@updateForgetPassword');

Route::post('update-password', 'Api\UserController@updatePassword');

Route::post('social-login', 'Api\UserController@socialLogin');

Route::post('search', 'Api\HomeController@searching');

Route::post('search_discover', 'Api\HomeController@discoversearching');

// Cron
Route::get('check_badges', 'Api\CronController@checkBadges');
Route::get('birthday_wishes', 'Api\CronController@birthday_wishes');
Route::get('random_push', 'Api\CronController@random_push');
Route::get('monthly_push', 'Api\CronController@monthly_push');

Route::post('get-badges', 'Api\ProfileController@getBadges');


// After Login With Token
Route::post('insert-image', 'Api\PostController@testImage');

Route::group(['middleware' => ['jwt.verify']], function() {

	Route::post('add-post', 'Api\PostController@addPost');

	Route::post('fetch-brand', 'Api\PostController@fecthBrand');
	
	Route::post('fetch-ingredients-without-brand', 'Api\PostController@fetchIngredientsWithOutBrand');

	Route::post('fetch-ingredients-with-brand', 'Api\PostController@fetchIngredientsWithBrand');

	Route::post('fetch-friends', 'Api\PostController@fetchFriendsList');

	Route::post('fetch-online-friends', 'Api\PostController@fetchOnlineFriendsList');

	Route::post('fetch-hash-tags', 'Api\PostController@fetchHashTags');
	
	Route::post('fetch-recipe', 'Api\PostController@fetchRecipe');

	Route::post('other-profile', 'Api\ProfileController@otherProfile');

	Route::post('get-post', 'Api\HomeController@fetchPost');

	Route::post('get-user-post', 'Api\HomeController@fetchUserPost');
	
	Route::post('put-like', 'Api\HomeController@putLike');

	Route::post('get-comment', 'Api\HomeController@fetchComment');
	
	Route::post('put-comment', 'Api\HomeController@putComment');

	Route::post('put-comment-like', 'Api\HomeController@putCommentLike');

	Route::post('send-friend-request', 'Api\HomeController@sendFriendRequest');
	
	Route::post('send-following-request', 'Api\HomeController@followProfile');
	
	Route::post('share-post', 'Api\HomeController@sharePost');

	Route::post('want-to-visit', 'Api\HomeController@addwannaVisit');

	Route::post('want-to-visit-list', 'Api\HomeController@wannaVisitList');

	Route::post('delete-post', 'Api\HomeController@deletePost');

	Route::post('want-to-cookedit', 'Api\HomeController@addwannaCookedIt');

	Route::post('get-want-to-cookedit', 'Api\HomeController@getwannaCookedIt');

	Route::post('post-details', 'Api\HomeController@postDetails');

	Route::post('edit-comment', 'Api\HomeController@editComment');

	Route::post('delete-comment', 'Api\HomeController@deleteComment');

	Route::post('cancel-friend-request', 'Api\HomeController@cancelFriendRequest');

	Route::post('cancel-following-request', 'Api\HomeController@cancelFollowingRequest');
	
	Route::post('remove-wanna-cooked-it', 'Api\HomeController@removeWannaCookedIt');

	Route::post('remove-wanna-visit', 'Api\HomeController@removeWannaVisit');

	Route::post('user-profile', 'Api\ProfileController@userProfile');

	Route::post('user-pending-requests', 'Api\ProfileController@userFriendRequest');
	
	Route::post('user-accept-friend-requests', 'Api\ProfileController@acceptFriendrequest');

	Route::post('user-cancel-friend-requests', 'Api\ProfileController@rejectFriendrequest');

	Route::post('my-details', 'Api\ProfileController@myDetails');

	Route::post('update-user-profile', 'Api\ProfileController@updateUserProfile');

	Route::post('update-user-status', 'Api\ProfileController@updateUserStatus');

	Route::post('device-online', 'Api\ProfileController@isDeviceOnline');

	Route::post('device-offline', 'Api\ProfileController@isDeviceOffline');

	Route::post('chat-history', 'Api\PostController@fetchChatHistory');

	Route::post('save-chat', 'Api\PostController@saveChat');

	Route::post('update-last-used', 'Api\ProfileController@updateLastUsed');

	Route::post('read-notification', 'Api\ProfileController@read_notification');

	Route::post('notification-badges', 'Api\ProfileController@notification_badges');

	Route::post('read-badges', 'Api\ProfileController@read_badges');
});
