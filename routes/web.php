<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return redirect()->route('login');
});

Route::get('change-password/{user}', 'UserController@changePass')->name('admin.change-password');
Route::post('update-forget-password', 'UserController@updateForgetPassword')->name('admin.update-forget-password');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout')->name('logout');

##########For Brand Management##################################

Route::get('brands', 'Modules\BrandController@brands')->name('admin.brands');
Route::get('add-brands', 'Modules\BrandController@addBrands')->name('admin.add.brands');
Route::post('store-brands', 'Modules\BrandController@addBrands')->name('admin.store.brands');
Route::get('edit-brands/{id}', 'Modules\BrandController@editBrands')->name('admin.edit.brands');
Route::post('update-brands/{id}', 'Modules\BrandController@editBrands')->name('admin.update.brands');
Route::get('brands/{id}', 'Modules\BrandController@statusBrands')->name('admin.status.brands');
Route::get('brands/{id}/remove', 'Modules\BrandController@removeBrands')->name('admin.remove.brands');

##########For Brand Management##################################

##########For Ingredients Management##################################

Route::get('ingredients', 'Modules\IngredientsController@ingredients')->name('admin.ingredients');
Route::get('add-ingredients', 'Modules\IngredientsController@addIngredients')->name('admin.add.ingredients');
Route::post('store-ingredients', 'Modules\IngredientsController@addIngredients')->name('admin.store.ingredients');
Route::get('edit-ingredients/{id}', 'Modules\IngredientsController@editIngredients')->name('admin.edit.ingredients');
Route::post('update-ingredients/{id}', 'Modules\IngredientsController@editIngredients')->name('admin.update.ingredients');
Route::get('ingredients/{id}/status', 'Modules\IngredientsController@statusIngredients')->name('admin.status.ingredients');
Route::get('ingredients/{id}/remove', 'Modules\IngredientsController@removeIngredients')->name('admin.remove.ingredients');

##########For Ingredients Management##################################

##########For edit profile##################################

Route::get('edit-profile', 'HomeController@editProfile')->name('admin.edit.profile');
Route::post('update-profile', 'HomeController@editProfile')->name('admin.update.profile');

##########For edit profile##################################

##########For User Management##################################
Route::get('users', 'Modules\UserController@index')->name('admin.users');
Route::post('filter-users', 'Modules\UserController@index')->name('admin.filter.users');
Route::get('filter-users', 'Modules\UserController@index');
Route::get('user/{id}/status', 'Modules\UserController@status')->name('admin.user.status');
##########For User Management##################################

##########For Gamification Management##################################
// Usage Gamification

Route::get('usage-gamification', 'Modules\GamificationController@showUsageGamification')->name('admin.usage.gamification');

Route::get('add-usage-gamification', 'Modules\GamificationController@addUsageGami')->name('admin.add.usage.gamification');

Route::post('store-usage-gamification', 'Modules\GamificationController@addUsageGami')->name('admin.store.usage.gamification');

Route::get('edit-usage-gamification/{id}', 'Modules\GamificationController@editUsageGami')->name('admin.edit.usage.gamification');

Route::post('update-usage-gamification/{id}', 'Modules\GamificationController@editUsageGami')->name('admin.update.usage.gamification');

Route::get('remove-usage-gamification/{id}', 'Modules\GamificationController@removeGami')->name('admin.remove.usage.gamification');

// Post Gamification

Route::get('post-gamification', 'Modules\GamificationController@showPostGamification')->name('admin.post.gamification');

Route::get('add-post-gamification', 'Modules\GamificationController@addPostGami')->name('admin.add.post.gamification');

Route::post('store-post-gamification', 'Modules\GamificationController@addPostGami')->name('admin.store.post.gamification');

Route::get('edit-post-gamification/{id}', 'Modules\GamificationController@editPostGami')->name('admin.edit.post.gamification');

Route::post('update-post-gamification/{id}', 'Modules\GamificationController@editPostGami')->name('admin.update.post.gamification');

Route::get('remove-post-gamification/{id}', 'Modules\GamificationController@removePostGami')->name('admin.remove.post.gamification');

// Video Post Gamification

Route::get('video-gamification', 'Modules\GamificationController@showVideoGamification')->name('admin.video.gamification');

Route::get('add-video-gamification', 'Modules\GamificationController@addVideoGami')->name('admin.add.video.gamification');

Route::post('store-video-gamification', 'Modules\GamificationController@addVideoGami')->name('admin.store.video.gamification');

Route::get('edit-video-gamification/{id}', 'Modules\GamificationController@editVideoGami')->name('admin.edit.video.gamification');

Route::post('update-video-gamification/{id}', 'Modules\GamificationController@editVideoGami')->name('admin.update.video.gamification');

Route::get('remove-video-gamification/{id}', 'Modules\GamificationController@removeVideoGami')->name('admin.remove.video.gamification');

// Freind Post Gamification

Route::get('freind-gamification', 'Modules\GamificationController@showfreindGamification')->name('admin.freind.gamification');

Route::get('add-freind-gamification', 'Modules\GamificationController@addfreindGami')->name('admin.add.freind.gamification');

Route::post('store-freind-gamification', 'Modules\GamificationController@addfreindGami')->name('admin.store.freind.gamification');

Route::get('edit-freind-gamification/{id}', 'Modules\GamificationController@editfreindGami')->name('admin.edit.freind.gamification');

Route::post('update-freind-gamification/{id}', 'Modules\GamificationController@editfreindGami')->name('admin.update.freind.gamification');

Route::get('remove-freind-gamification/{id}', 'Modules\GamificationController@removefreindGami')->name('admin.remove.freind.gamification');

// Cooked it Post Gamification

Route::get('cooked-it-gamification', 'Modules\GamificationController@showcookedItGamification')->name('admin.cooked.it.gamification');

Route::get('add-cooked-it-gamification', 'Modules\GamificationController@addcookedItGami')->name('admin.add.cooked.it.gamification');

Route::post('store-cooked-it-gamification', 'Modules\GamificationController@addcookedItGami')->name('admin.store.cooked.it.gamification');

Route::get('edit-cooked-it-gamification/{id}', 'Modules\GamificationController@editcookedItGami')->name('admin.edit.cooked.it.gamification');

Route::post('update-cooked-it-gamification/{id}', 'Modules\GamificationController@editcookedItGami')->name('admin.update.cooked.it.gamification');

Route::get('remove-cooked-it-gamification/{id}', 'Modules\GamificationController@removecookedItGami')->name('admin.remove.cooked.it.gamification');

##########For Gamification Management##################################

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/config-clear', function() {
    $exitCode = Artisan::call('config:clear');
    return 'config clear';
});

Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return 'config cache';
});
Route::get('/cache-clear', function() {
    $exitCode = Artisan::call('cache:clear');
    return 'cache clear';
});
