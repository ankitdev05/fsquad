<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagComment extends Model
{
    protected $table = "comment_to_tagged_user";
    protected $guarded = [];
}
