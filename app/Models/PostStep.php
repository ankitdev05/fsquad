<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostStep extends Model
{
    protected $table = "post_to_step";
    protected $guarded = [];
}
