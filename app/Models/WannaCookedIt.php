<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WannaCookedIt extends Model
{
    protected $table = "cooked_it";
    protected $guarded = [];
    
    public function postDetails(){
    	return $this->hasOne('App\Models\Post', 'id', 'post_id');
    }

    public function recipeDetails(){
    	return $this->hasOne('App\Models\PostRecipie', 'id', 'recipie_id');
    }
}
