<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagPost extends Model
{
    protected $table = "post_to_tagged_user";
    protected $guarded = [];
}
