<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WannaVisit extends Model
{
    protected $table = "wanna_visit";
    protected $guarded = [];
}
