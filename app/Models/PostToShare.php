<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostToShare extends Model
{
    protected $table = "post_to_share";
    protected $guarded = [];
}
