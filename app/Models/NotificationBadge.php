<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationBadge extends Model
{
    protected $guarded = [];
    protected $fillable = ['user_id','title','body','type','status'];
}