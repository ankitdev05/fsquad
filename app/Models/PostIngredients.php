<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostIngredients extends Model
{
    protected $table = "post_ingredients";
    protected $guarded = [];
}
