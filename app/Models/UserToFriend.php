<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserToFriend extends Model
{
    protected $table = "user_to_friend";
    protected $guarded = [];

    public function friendDetails(){
    	return $this->hasOne('App\User','id', 'friend_id');
    }

    public function friendDetailsOpposite(){
    	return $this->hasOne('App\User','id', 'user_id');
    }

    public function userDetails(){
    	return $this->hasOne('App\User','id', 'user_id');
    }
}
