<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostToCookedIt extends Model
{
    protected $table = "post_to_cooked_it";
    protected $guarded = [];
}
