<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = "brands";
    protected $guarded = [];

    public function hasIngredients(){
    	return $this->hasMany('App\Models\Ingredient', 'brand_id', 'id');
    }
}
