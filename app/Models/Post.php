<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "post";
    protected $guarded = [];

    public function authorDetails(){
    	return $this->hasOne('App\User', 'id', 'user_id');
    } 

    public function postDeafultImage(){
    	return $this->hasOne('App\Models\PostImages', 'post_id', 'id')->where('is_default', 'Y');
    }
    
    public function postAllImage(){
    	return $this->hasMany('App\Models\PostImages', 'post_id', 'id');
    }

    public function postAllComments(){
        return $this->hasMany('App\Models\PostComments', 'post_id', 'id');
    }

    public function postRecipe(){
        return $this->hasOne('App\Models\PostRecipie', 'post_id', 'id');
    }

    public function postSteps(){
        return $this->hasMany('App\Models\PostIngredients', 'post_id', 'id');
    }

    public function postIngredients(){
        return $this->hasMany('App\Models\PostStep', 'post_id', 'id');
    }

    
}
