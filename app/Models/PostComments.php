<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostComments extends Model
{
    protected $table = "post_to_comments";
    protected $guarded = [];

    public function commenterDetails(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
