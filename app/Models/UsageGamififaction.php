<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsageGamififaction extends Model
{
    protected $table = "usage_gamification";
    protected $guarded = [];
}
