<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $table = "ingredients";
    protected $guarded = [];

    public function brandName(){
    	return $this->hasOne('App\Models\Brand', 'id', 'brand_id');
    }
}
