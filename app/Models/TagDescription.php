<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagDescription extends Model
{
    protected $table = "description_to_tagged_user";
    protected $guarded = [];
}
