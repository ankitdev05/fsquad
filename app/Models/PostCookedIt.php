<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostCookedIt extends Model
{
    protected $table = "post_to_cooked_it";
    protected $guarded = [];
}
