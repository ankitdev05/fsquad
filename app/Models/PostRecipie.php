<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostRecipie extends Model
{
    protected $table = "post_recipe";
    protected $guarded = [];
}
