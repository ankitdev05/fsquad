<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserToBusiness extends Model
{
    protected $table = "user_to_business";
    protected $guarded = [];
}
