<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\TagComment;
use App\Models\CommentLike;
use App\Models\TagDescription;
use App\Models\Post;
use App\Models\PostStep;
use App\Models\PostIngredients;
use App\Models\PostRecipie;
use App\Models\PostImages;
use App\Models\PostToCookedIt;
use App\Models\WannaCookedIt;
use App\Models\CookedIt;
use App\Models\PostComments;
use App\Models\PostLike;
use App\Models\PostToShare;
use App\Models\WannaVisit;
use App\Models\TagPost;
use App\Models\UserToBusiness;
use App\Models\UserToFriend;
use App\Models\UsageGamififaction;
use App\Models\PostGamififaction;
use App\Models\FreindGamififaction;
use App\Models\CookeditGamififaction;
use App\Models\VideopostGamififaction;
use App\Models\UserGame;
use App\Models\Notification;
use App\Models\NotificationBadge;

class CronController extends Controller
{

    public function checkBadges() {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
        $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        if(@$user==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }

        // have or not badges data
        $usergame = UserGame::where(["user_id"=>$user->id, "status"=>1])->first();
        if(!$usergame) {
            $newusergame = new UserGame();
            $newusergame->user_id = $user->id;
            $newusergame->active = 0;
            $newusergame->post = 0;
            $newusergame->friend = 0;
            $newusergame->cooked = 0;
            $newusergame->video = 0;
            $newusergame->status = 1;
            $newusergame->save();
        }

        // Find Active Time
        $createdTime = $user->created_at;
        $lastUsed = $user->last_used;
        if(!empty($lastUsed)) {
            $daysDiff = $lastUsed - strtotime($createdTime);
            $calculateDays = abs(round($daysDiff / 86400));
            $this->findActiveUserBadge($calculateDays, $user->id);
        }

        // Find number of post
        $getPostCount = Post::where(['user_id'=>$user->id])->count();
        $this->findPostBadge($getPostCount, $user->id);

        // Find friends
        $friends = UserToFriend::with('friendDetails')->where([
                    'user_id'           =>  @$user->id,
                    'request_status'    =>  'FRIEND'
                ])->orderBy('id', 'desc')->get();

        $friendsMore = UserToFriend::with('friendDetailsOpposite')->where([
                    'friend_id'         =>  @$user->id,
                    'request_status'    =>  'FRIEND'
                ])->orderBy('id', 'desc')->get();

        $totalFriends = count($friends) + count($friendsMore);
        $this->findFriendBadge($totalFriends, $user->id);

        // Find I cooked it
        $getCookedCount = CookedIt::where(['user_id'=>$user->id])->count();
        $this->findCookedBadge($getCookedCount, $user->id);     

        // Find video posted   
        $getVideoCount = 0;
        $getPostVideo = Post::where(['user_id'=>$user->id])->get();
        if($getPostVideo) {
            foreach($getPostVideo as $getPostVid) {
                $getstepVideo = PostStep::where(['post_id'=>$getPostVid->id])->first();
                if($getstepVideo) {
                    $getVideoCount++;
                }
            }
            $this->findVideoBadge($getVideoCount, $user->id);
        }

        $response = [
                        'token'                         => null,
                        'error'                         => null,
                        'status'                        => "SUCCESS"
                    ];
        return response()->json($response);
    }

    public function findActiveUserBadge($data, $userid) {
        
        $getUsage = UsageGamififaction::where('no_of_days', $data)->first();
        $usergame = UserGame::where(["user_id"=>$userid, "status"=>1])->first();

        if($getUsage) {
            
            if($usergame) {
                if($usergame->active != $getUsage->level) {
                    NotificationBadge::create([
                        "user_id"=>$userid,
                        "title"=>"You earned a badge !",
                        "body"=> "YOU GOT " . $getUsage->name. " badge",
                        "type"=> $getUsage->image,
                        "status"=>1
                    ]);
                }
                $usergame->active = $getUsage->level;
                $usergame->save();
            }
        } else {

            $getUsage = UsageGamififaction::where('no_of_days', '<=', $data)->orderBy('no_of_days','DESC')->first();
            if($getUsage) {
                
                if($usergame) {
                    if($usergame->active != $getUsage->level) {
                        NotificationBadge::create([
                            "user_id"=>$userid,
                            "title"=>"You earned a badge !",
                            "body"=> "YOU GOT " . $getUsage->name. " badge",
                            "type"=> $getUsage->image,
                            "status"=>1
                        ]);
                    }
                    $usergame->active = $getUsage->level;
                    $usergame->save();
                }
            }
        }
    }

    public function findPostBadge($data, $userid) {
        
        $getUsage = PostGamififaction::where('no_of_post', $data)->first();
        $usergame = UserGame::where(["user_id"=>$userid, "status"=>1])->first();

        if($getUsage) {
            
            if($usergame) {
                if($usergame->active != $getUsage->level) {
                    NotificationBadge::create([
                        "user_id"=>$userid,
                        "title"=>"You earned a badge !",
                        "body"=> "YOU GOT " . $getUsage->name. " badge",
                        "type"=> $getUsage->image,
                        "status"=>1
                    ]);
                }
                $usergame->post = $getUsage->level;
                $usergame->save();
            }
        } else {

            $getUsage = PostGamififaction::where('no_of_post', '<=', $data)->orderBy('no_of_post','DESC')->first();
            if($getUsage) {
                
                if($usergame) {
                    if($usergame->active != $getUsage->level) {
                        NotificationBadge::create([
                            "user_id"=>$userid,
                            "title"=>"You earned a badge !",
                            "body"=> "YOU GOT " . $getUsage->name. " badge",
                            "type"=> $getUsage->image,
                            "status"=>1
                        ]);
                    }
                    $usergame->post = $getUsage->level;
                    $usergame->save();
                }
            }
        }
    }

    public function findFriendBadge($data, $userid) {
        
        $getUsage = FreindGamififaction::where('no_of_freinds', $data)->first();
        $usergame = UserGame::where(["user_id"=>$userid, "status"=>1])->first();

        if($getUsage) {

            if($usergame) {
                if($usergame->active != $getUsage->level) {
                    NotificationBadge::create([
                        "user_id"=>$userid,
                        "title"=>"You earned a badge !",
                        "body"=> "YOU GOT " . $getUsage->name. " badge",
                        "type"=> $getUsage->image,
                        "status"=>1
                    ]);
                }
                $usergame->friend = $getUsage->level;
                $usergame->save();
            }
        } else {

            $getUsage = FreindGamififaction::where('no_of_freinds', '<=', $data)->orderBy('no_of_freinds','DESC')->first();
            if($getUsage) {
                
                if($usergame) {
                    if($usergame->active != $getUsage->level) {
                        NotificationBadge::create([
                            "user_id"=>$userid,
                            "title"=>"You earned a badge !",
                            "body"=> "YOU GOT " . $getUsage->name. " badge",
                            "type"=> $getUsage->image,
                            "status"=>1
                        ]);
                    }
                    $usergame->friend = $getUsage->level;
                    $usergame->save();
                }
            }
        }
    }

    public function findCookedBadge($data, $userid) {
        $badges = 0;
        $getUsage = CookeditGamififaction::where('no_of_i_cokked_it', $data)->first();
        $usergame = UserGame::where(["user_id"=>$userid, "status"=>1])->first();

        if($getUsage) {
            
            if($usergame) {
                if($usergame->active != $getUsage->level) {
                    NotificationBadge::create([
                        "user_id"=>$userid,
                        "title"=>"You earned a badge !",
                        "body"=> "YOU GOT " . $getUsage->name. " badge",
                        "type"=> $getUsage->image,
                        "status"=>1
                    ]);
                }
                $usergame->cooked = $getUsage->level;
                $usergame->save();
            }
        } else {

            $getUsage = CookeditGamififaction::where('no_of_i_cokked_it', '<=', $data)->orderBy('no_of_i_cokked_it','DESC')->first();
            if($getUsage) {
                
                if($usergame) {
                    if($usergame->active != $getUsage->level) {
                        NotificationBadge::create([
                            "user_id"=>$userid,
                            "title"=>"You earned a badge !",
                            "body"=> "YOU GOT " . $getUsage->name. " badge",
                            "type"=> $getUsage->image,
                            "status"=>1
                        ]);
                    }
                    $usergame->cooked = $getUsage->level;
                    $usergame->save();
                }
            }
        }
    }

    public function findVideoBadge($data, $userid) {
        $badges = 0;
        $getUsage = VideopostGamififaction::where('no_of_video_posted', $data)->first();
        $usergame = UserGame::where(["user_id"=>$userid, "status"=>1])->first();

        if($getUsage) {

            if($usergame) {
                if($usergame->active != $getUsage->level) {
                    NotificationBadge::create([
                        "user_id"=>$userid,
                        "title"=>"You earned a badge !",
                        "body"=> "YOU GOT " . $getUsage->name. " badge",
                        "type"=> $getUsage->image,
                        "status"=>1
                    ]);
                }
                $usergame->video = $getUsage->level;
                $usergame->save();
            }
        } else {

            $getUsage = VideopostGamififaction::where('no_of_video_posted', '<=', $data)->orderBy('no_of_video_posted','DESC')->first();
            if($getUsage) {
                
                if($usergame) {
                    if($usergame->active != $getUsage->level) {
                        NotificationBadge::create([
                            "user_id"=>$userid,
                            "title"=>"You earned a badge !",
                            "body"=> "YOU GOT " . $getUsage->name. " badge",
                            "type"=> $getUsage->image,
                            "status"=>1
                        ]);
                    }
                    $usergame->video = $getUsage->level;
                    $usergame->save();
                }
            }
        }
    }

    public function birthday_wishes() {
        $currentDate = date('Y-m-d');

        $getUsers = User::where(["dob"=>$currentDate, "status"=>"A"])->get();
        if($getUsers) {

            foreach($getUsers as $getUser) {

                if(!empty($getUser->device_registrartion_id)) {
                    $checkBadge = Notification::where(["user_id"=>$getUser->id, "status"=>2])->count();
                    $title = "Happy Birthday";
                    $body = "Happy Birthday";
                    $data = ["user_id"=>$getUser->id, "title"=>$title, "body"=>$body, "type"=>"birthday"];
                    $notify = Notification::create($data);

                    $message = [$getUser->device_registrartion_id, $title, $body, $checkBadge, $notify->id];
                    $this->push_notification($message);
                }
            }
        }
    }

    public function random_push() {

        $getUsers = User::where(["status"=>"A"])->get();
        if($getUsers) {

            foreach($getUsers as $getUser) {

                if(!empty($getUser->device_registrartion_id)) {

                    $msgArray = ["You are very close to achieving a new badge.. Come on go for it", "It's time to challenge your friends by achieving the highest badge"];

                    $random_message = array_rand($msgArray,1);

                    $checkBadge = Notification::where(["user_id"=>$getUser->id, "status"=>2])->count();
                    $title = "Fsquad";
                    $body = $random_message;
                    $data = ["user_id"=>$getUser->id, "title"=>$title, "body"=>$body, "type"=>"random"];
                    $notify = Notification::create($data);

                    $message = [$getUser->device_registrartion_id, $title, $body, $checkBadge, $notify->id];
                    $this->push_notification($message);
                }
            }
        }
    }

    public function monthly_push() {

        $getUsers = User::where(["status"=>"A"])->get();
        if($getUsers) {

            foreach($getUsers as $getUser) {

                if(!empty($getUser->device_registrartion_id)) {
                    $checkBadge = Notification::where(["user_id"=>$getUser->id, "status"=>2])->count();
                    $title = "Fsquad";
                    $body = "Your badges will expire by November.. Hurry up and achieve all the badges to get Golden Spoon";
                    $data = ["user_id"=>$getUser->id, "title"=>$title, "body"=>$body, "type"=>"monthly"];
                    $notify = Notification::create($data);

                    $message = [$getUser->device_registrartion_id, $title, $body, $checkBadge, $notify->id];
                    $this->push_notification($message);
                }
            }
        }
    }


    public function push_notification($array) {

        $url = 'https://fcm.googleapis.com/fcm/send';  
        //print_r($array);die;
        $message = array('title' => $array[1], "body"=>$array[2], "type"=>$array[4], 'sound' => 'default', 'badge'=>$array[3]);
        //print_r($message);die;
        $registatoin_ids = array($array[0]);
        $fields = array('registration_ids' => $registatoin_ids, 'data' => $message, 'notification'=>$message);

        $GOOGLE_API_KEY = "AAAAfoJC8Wk:APA91bFi5XNtWjPvRufucrS5yGuScO9Ed9GmUuYWDhdJGQmjnvW5eliIL0OeZ6VQSuOh4_h0_v2a44C6JqIE7m3_Ql4NkKxgFepQst29OrAEDTpkTa07uUKvtSn3kx2Bgno-IKki57ER";
        $headers = array('Authorization: key=' . $GOOGLE_API_KEY , 'Content-Type: application/json',);
        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);    
        
        if ($result === false) {
            return $abc = 'FCM Send Error: ' . curl_error($ch);
        } else {
            return $abc = "Notification Send";
        }
        curl_close($ch);
    }

}
