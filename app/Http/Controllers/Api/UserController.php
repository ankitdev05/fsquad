<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use validator;
use App\User;
use App\Models\UserToBusiness;
use App\Models\UserVerify;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgetPassword;
use App\Mail\VerificationUser;
use AWS;

class UserController extends Controller
{
    public function login(){
        $json = file_get_contents('php://input');
        $data = json_decode($json);

        if(empty(@$data->username) || @$data->username==null || @$data->username==""){
            $response = [
                'token'            => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '108',
                        'message'   =>  config('customerror.108')
                    ]
            ];
            return response()->json($response);
        }

        if(empty(@$data->password) || @$data->password==null || @$data->password==""){
            $response = [
                'token'            => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '109',
                        'message'   =>  config('customerror.109')
                    ]
            ];
            return response()->json($response);
        }

        if(empty(@$data->device_registrartion_id) || @$data->device_registrartion_id==null || @$data->device_registrartion_id==""){
            $response = [
                'token'            => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '128',
                        'message'   =>  config('customerror.128')
                    ]
            ];
            return response()->json($response);
        }
        if(!empty(@$data->username)&&!empty(@$data->password)) {

            if(!is_numeric(@$data->username)){
            
                if(filter_var(@$data->username, FILTER_VALIDATE_EMAIL)){
                
                    $user = User::where([
                        'email'     =>  @$data->username,
                        'user_type' =>  "U"
                    ])->first();
                
                    if(@$user!=null){
                        if(@$user->status=="I"){
                            $response = [
                                'token'            => null,
                                'status'            => "ERROR",
                                    'error'         => [
                                        'code'      =>  '112',
                                        'message'   =>  config('customerror.112')
                                    ]
                            ];
                            return response()->json($response);
                        }
                        else{
                            if(\Hash::check(@$data->password, @$user->password)){
                                @$token = \Hash::make(str_random(27).'-'.@$user->id.'-'.str_random(6));
                                @$updateUser = User::where('id', @$user->id)->update([
                                                'user_token'    =>  @$token
                                            ]);

                                @$updateRegID = User::where('id', @$user->id)->update([
                                    'device_registrartion_id'   =>  @$data->device_registrartion_id
                                ]);
                                @$getUpdatedUser = User::where('id', @$user->id)->first();

                                $response = [
                                    'result'           => @$getUpdatedUser,
                                    'token'            => @$getUpdatedUser->user_token,
                                    'status'           => "SUCCESS",
                                    'error'            => null
                                ];
                                return response()->json($response);
                            }
                            else{
                                $response = [
                                    'token'            => null,
                                    'status'            => "ERROR",
                                        'error'         => [
                                            'code'      =>  '111',
                                            'message'   =>  config('customerror.111')
                                        ]
                                ];
                                return response()->json($response);
                            }
                        }
                    }
                    else{
                        $response = [
                            'token'            => null,
                            'status'            => "ERROR",
                                'error'         => [
                                    'code'      =>  '110',
                                    'message'   =>  config('customerror.110')
                                ]
                        ];
                        return response()->json($response);
                    }
                
                } else {
                    $response = [
                        'token'            => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '101',
                                'message'   =>  config('customerror.101')
                            ]
                    ];
                    return response()->json($response);
                }
            }
            elseif(is_numeric(@$data->username)){
                $user = User::where([
                    'mobile'     =>  @$data->username,
                    'user_type' =>  "U"
                ])->first();
                if(@$user!=null){
                    if(@$user->status=="I"){
                        $response = [
                            'token'            => null,
                            'status'            => "ERROR",
                                'error'         => [
                                    'code'      =>  '112',
                                    'message'   =>  config('customerror.112')
                                ]
                        ];
                        return response()->json($response);
                    }
                    else{
                        if(\Hash::check(@$data->password, @$user->password)){
                            @$token = \Hash::make(str_random(27).'-'.@$user->id.'-'.str_random(6));
                            @$updateUser = User::where('id', @$user->id)->update([
                                            'user_token'    =>  @$token
                                        ]);
                            @$updateRegID = User::where('id', @$user->id)->update([
                                    'device_registrartion_id'   =>  @$data->device_registrartion_id
                                ]);
                            @$getUpdatedUser = User::where('id', @$user->id)->first();
                            $response = [
                                'result'           => @$getUpdatedUser,
                                'token'            => @$getUpdatedUser->user_token,
                                'status'            => "SUCCESS",
                                'error'            => null
                            ];
                            return response()->json($response);
                        }
                        else{
                            $response = [
                                'token'             => null,
                                'status'            => "ERROR",
                                    'error'         => [
                                        'code'      =>  '111',
                                        'message'   =>  config('customerror.111')
                                    ]
                            ];
                            return response()->json($response);
                        }
                    }
                }
                else{
                    $response = [
                        'token'            => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '110',
                                'message'   =>  config('customerror.110')
                            ]
                    ];
                    return response()->json($response);
                }
            }
            else{
                $response = [
                    'token'            => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '502',
                            'message'   =>  config('customerror.502')
                        ]
                ];
                return response()->json($response);
            }
        }
        else{
            $response = [
                'token'            => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '502',
                        'message'   =>  config('customerror.502')
                    ]
            ];
            return response()->json($response);
        }
    }


    public function sendotp() {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        
        if(empty(@$data->username)) {
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '108',
                        'message'   =>  config('customerror.108')
                    ]
            ];
            return response()->json($response);
        }

        if(!is_numeric(@$data->username)) {

            if(filter_var(@$data->username, FILTER_VALIDATE_EMAIL)) {
            
                @$fndUser = User::where('email', @$data->username)->first();
                if(@$fndUser!=null) {
                    $response = [
                        'token'            => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '123',
                                'message'   =>  config('customerror.123')
                            ]
                    ];
                    return response()->json($response);
                
                } else {
                    $vcode = rand(1000, 9999);
                    $isCreated = UserVerify::create([
                        'email'                     =>  @$data->username,
                        'otpcode'   =>  $vcode
                    ]);
                    if(@$isCreated->id) {

                        Mail::to(@$data->username)->send(new VerificationUser(@$vcode));

                        $response = [
                            'result'            => 'Verification OTP has been send to your Email, Please verify your account.',
                            'status'            => "SUCCESS",
                            'token'             => null,
                            'error'         => null
                        ];
                        return response()->json($response);
                    } else {
                        $response = [
                            'token'             => null,
                            'status'            => "ERROR",
                                'error'         => [
                                    'code'      =>  '500',
                                    'message'   =>  config('customerror.500')
                                ]
                        ];
                        return response()->json($response);
                    }
                }

            } else {
                $response = [
                    'token'            => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '101',
                            'message'   =>  config('customerror.101')
                        ]
                ];
                return response()->json($response);
            }

        }  elseif(is_numeric(@$data->username)) {

            @$fndUser = User::where('mobile', @$data->username)->first();
            if(@$fndUser!=null) {
                $response = [
                    'token'            => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '123',
                            'message'   =>  config('customerror.123')
                        ]
                ];
                return response()->json($response);
            
            } else {
                $vcode = rand(1000, 9999);
                $isCreated = UserVerify::create([
                    'email'                     =>  @$data->username,
                    'otpcode'   =>  $vcode
                ]);
                if(@$isCreated->id) {

                    $smsMsg = "Your Fsquad verifiation code is ". $vcode;
                    $sms = AWS::createClient('sns');
                    $sms->publish([
                        'Message' => $smsMsg,
                        'PhoneNumber' => @$data->username, 
                        'MessageAttributes' => [
                            'AWS.SNS.SMS.SMSType'  => [
                                'DataType'    => 'String',
                                'StringValue' => 'Transactional',
                             ]
                         ],
                    ]);

                    $response = [
                        'result'            => 'Verification OTP has been send to your Mobile, Please verify your account.',
                        'status'            => "SUCCESS",
                        'token'             => null,
                        'error'         => $sms
                    ];
                    return response()->json($response);
                } else {
                    $response = [
                        'token'             => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '500',
                                'message'   =>  config('customerror.500')
                            ]
                    ];
                    return response()->json($response);
                }
            }

        }
    }


    public function matchotp() {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        
        if(empty(@$data->username)) {
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '108',
                        'message'   =>  config('customerror.108')
                    ]
            ];
            return response()->json($response);
        }

        if(empty(@$data->verifycode)) {
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '557',
                        'message'   =>  config('customerror.557')
                    ]
            ];
            return response()->json($response);
        }

        if(!is_numeric(@$data->username)) {

            if(filter_var(@$data->username, FILTER_VALIDATE_EMAIL)) {
            
                @$fndUser = UserVerify::where(['email'=>$data->username, 'otpcode'=>$data->verifycode])->first();
                if(@$fndUser==null) {
                    $response = [
                        'token'            => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '556',
                                'message'   =>  config('customerror.556')
                            ]
                    ];
                    return response()->json($response);
                } else {
                    UserVerify::where('email', @$data->username)->delete();
                    $response = [
                        'result'            => 'Verified',
                        'status'            => "SUCCESS",
                        'token'             => null,
                        'error'         => null
                    ];
                    return response()->json($response);
                }

            } else {
                $response = [
                    'token'            => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '101',
                            'message'   =>  config('customerror.101')
                        ]
                ];
                return response()->json($response);
            }

        }  elseif(is_numeric(@$data->username)) {

            $fndUser = UserVerify::where(['email'=>$data->username, 'otpcode'=>$data->verifycode])->first();

            if(@$fndUser==null) {
                $response = [
                    'token'            => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '556',
                            'message'   =>  config('customerror.556')
                        ]
                ];
                return response()->json($response);
            
            } else {
                UserVerify::where('email', @$data->username)->delete();
                $response = [
                    'result'            => 'Verified',
                    'status'            => "SUCCESS",
                    'token'             => null,
                    'error'         => null
                ];
                return response()->json($response);
            }

        }
    }



    public function register(){
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        
        if(empty(@$data->username) && @$data->mode=="S0"){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '108',
                        'message'   =>  config('customerror.108')
                    ]
            ];
            return response()->json($response);
        }

        if(empty(@$data->device_registrartion_id) && @$data->mode=="S0"){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '128',
                        'message'   =>  config('customerror.128')
                    ]
            ];
            return response()->json($response);
        }

        // Step 1: Registering User Name............First Itearation
        
        if(!empty(@$data->username) && @$data->mode=="S0") {
            
            //  Registering Email Address
            if(!is_numeric(@$data->username)) {
                
                if(filter_var(@$data->username, FILTER_VALIDATE_EMAIL)) {

                    @$fndUser = User::where('email', @$data->username)->first();
                    
                    if(@$fndUser!=null) {

                        if(@$data->device_registrartion_id == @$fndUser->device_registrartion_id) {

                            if(@$fndUser->registration_complete == "N") {

                                $response = [
                                    'result'            => $fndUser,
                                    'status'            => "SUCCESS",
                                    'S0'                => 'Complete',
                                    'registration_complete'     =>  "N",
                                    'token'             => null,    
                                        'error'         => null
                                ];
                                return response()->json($response);
                            }

                        } else {
                            $response = [
                                'token'            => null,
                                'status'            => "ERROR",
                                    'error'         => [
                                        'code'      =>  '123',
                                        'message'   =>  config('customerror.123')
                                    ]
                            ];
                            return response()->json($response);
                        }
                    }
                    else{
                        $isCreated = User::create([
                            'email'                     =>  @$data->username,
                            'device_registrartion_id'   =>  @$data->device_registrartion_id
                        ]);
                        if(@$isCreated->id){
                            $response = [
                                'result'            => @$isCreated,
                                'status'            => "SUCCESS",
                                'S0'                => 'Complete',
                                'registration_complete'     =>  "N",
                                'token'             => null,
                                    'error'         => null
                            ];
                            return response()->json($response);
                        }
                        else{
                            $response = [
                                'token'             => null,
                                'status'            => "ERROR",
                                    'error'         => [
                                        'code'      =>  '500',
                                        'message'   =>  config('customerror.500')
                                    ]
                            ];
                            return response()->json($response);
                        }
                    }
                }
                else {
                    
                    $response = [
                        'token'            => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '101',
                                'message'   =>  config('customerror.101')
                            ]
                    ];
                    return response()->json($response);
                    
                }
            }
            // Registering Mobile Number
            elseif(is_numeric(@$data->username)  && strlen(@$data->soicalType)  <= 0) {
                
                @$fndUser = User::where('mobile', @$data->username)->first();
                if(@$fndUser!=null){
                    $response = [
                        'token'             => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '504',
                                'message'   =>  config('customerror.504')
                            ]
                    ];
                    return response()->json($response);
                }
                else{
                    $isCreated = User::create([
                        'mobile'                    =>  @$data->username,
                        'device_registrartion_id'   =>  @$data->device_registrartion_id
                    ]);
                    if(@$isCreated->id){
                        $response = [
                            'result'                    => @$isCreated,
                            'status'                    => "SUCCESS",
                            'S0'                        => 'Complete',
                            'registration_complete'     =>  "N",
                            'token'                     => null,
                            'error'                     => null
                        ];
                        return response()->json($response);
                    }
                    else{
                        $response = [
                            'token'             => null,
                            'status'            => "ERROR",
                                'error'         => [
                                    'code'      =>  '500',
                                    'message'   =>  config('customerror.500')
                                ]
                        ];
                        return response()->json($response);
                    }
                }
            }
            // Bad GateWay Error
            else {

                if(strlen(@$data->soicalType)  > 0) {
                        
                    @$fndUser = User::where('email', @$data->username)->first();
                    if(@$fndUser!=null){
                        $response = [
                            'token'            => null,
                            'status'            => "ERROR",
                                'error'         => [
                                    'code'      =>  '123',
                                    'message'   =>  config('customerror.123')
                                ]
                        ];
                        return response()->json($response);
                    }
                    else{
                        $isCreated = User::create([
                            'email'                     =>  @$data->username,
                            'device_registrartion_id'   =>  @$data->device_registrartion_id
                        ]);
                        if(@$isCreated->id){
                            $response = [
                                'result'            => @$isCreated,
                                'status'            => "SUCCESS",
                                'S0'                => 'Complete',
                                'registration_complete'     =>  "N",
                                'token'             => null,
                                    'error'         => null
                            ];
                            return response()->json($response);
                        }
                        else{
                            $response = [
                                'token'             => null,
                                'status'            => "ERROR",
                                    'error'         => [
                                        'code'      =>  '500',
                                        'message'   =>  config('customerror.500')
                                    ]
                            ];
                            return response()->json($response);
                        }
                    }

                } else {

                    $response = [
                        'token'             => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '502',
                                'message'   =>  config('customerror.502')
                            ]
                    ];
                    return response()->json($response);
                }
            }
        }

        // Step 2: Updating User Details............Second Itearation
        if(!empty(@$data->user_id) && @$data->mode=="S1"){
            
            @$usDetails = User::where('id', @$data->user_id)->first();
            if(!empty($usDetails->first_name) && !empty($usDetails->last_name)  && !empty($usDetails->password)) {

                $response = [
                    'token'                      => null,
                    'result'                     => @$usDetails,
                    'status'                     => "SUCCESS",
                    'S1'                         =>  'Complete',
                    'registration_complete'      =>  "N"
                ];
                return response()->json($response);
            }

            if(empty(@$data->first_name)){
                $response = [
                    'token'             => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '113',
                            'message'   =>  config('customerror.113')
                        ]
                ];
                return response()->json($response);
            }
            elseif(empty(@$data->last_name)){
                $response = [
                    'token'             => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '114',
                            'message'   =>  config('customerror.114')
                        ]
                ];
                return response()->json($response);
            }
            elseif(empty(@$data->password)){
                $response = [
                    'token'             => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '115',
                            'message'   =>  config('customerror.115')
                        ]
                ];
                return response()->json($response);
            }
            elseif(empty(@$data->confirm_password)){
                $response = [
                    'token'             => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '116',
                            'message'   =>  config('customerror.116')
                        ]
                ];
                return response()->json($response);
            }
            elseif(@$data->password!=@$data->confirm_password){
                $response = [
                    'token'             => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '118',
                            'message'   =>  config('customerror.118')
                        ]
                ];
                return response()->json($response);
            }
            elseif(empty(@$data->dob)){
                $response = [
                    'token'             => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '117',
                            'message'   =>  config('customerror.117')
                        ]
                ];
                return response()->json($response);
            }
            elseif(empty(@$data->account_type)){
                $response = [
                    'token'             => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '505',
                            'message'   =>  config('customerror.505')
                        ]
                ];
                return response()->json($response);
            }
            else{
                $updateUser = User::where('id', @$data->user_id)
                            ->update([
                                'name'                      =>  @$data->first_name.'_'.@$data->user_id.str_random(2),
                                'first_name'                =>  @$data->first_name,
                                'last_name'                 =>  @$data->last_name,
                                'password'                  =>  \Hash::make(@$data->password),
                                'dob'                       =>  @date('Y-m-d',strtotime(@$data->dob)),
                                'account_type'              =>  @$data->account_type
                            ]);
                if($updateUser){
                    // User::where('id', @$data->user_id)->update([
                    //     'registration_complete'  =>  'Y'
                    // ]);
                    @$usDetails = User::where('id', @$data->user_id)->first();
                    $response = [
                        'token'                      => null,
                        'result'                     => @$usDetails,
                        'status'                     => "SUCCESS",
                        'S1'                         =>  'Complete',
                        'registration_complete'      =>  "N"
                    ];
                    return response()->json($response);
                }
                else{
                    $response = [
                        'token'             => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '500',
                                'message'   =>  config('customerror.500')
                            ]
                    ];
                    return response()->json($response);
                }
            }
        }

        // Step 3: Updating User Details............Second Itearation
        if(!empty(@$data->user_id) && @$data->mode=="S2"){
            if(empty(@$data->is_post_private)){
                $response = [
                    'token'             => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '506',
                            'message'   =>  config('customerror.506')
                        ]
                ];
                return response()->json($response);
            }
            else{
                if(!empty(@$data->nickname)){
                    $fndName = User::where('name', @$data->nickname)->where('id', '!=', @$data->user_id)->first();

                    if(@$fndName!=null){

                        $response = [
                            'token'             => null,
                            'status'            => "ERROR",
                                'error'         => [
                                    'code'      =>  '511',
                                    'message'   =>  config('customerror.511')
                                ]
                        ];
                        return response()->json($response);
                    }
                    else{
                        $updateUser = User::where('id',@$data->user_id)->update([
                            'name'   =>  @$data->nickname
                        ]);
                    }
                }
                $updateUser = User::where('id',@$data->user_id)->update([
                    'is_post_private'   =>  @$data->is_post_private
                ]);

                // dd(@$updateUser);
                // if(@$updateUser){
                    $regUser = User::where('id', @$data->user_id)->first();
                    if(@$regUser->account_type=="PER"){
                        @$token = \Hash::make(str_random(27).'-'.@$regUser->id.'-'.str_random(6));
                        $updtToken = User::where('id',@$regUser->id)->update([
                                        'user_token'   =>  @$token
                                    ]);
                        User::where('id', @$data->user_id)->update([
                            'registration_complete'  =>  'Y'
                        ]);
                        @$usDetails= User::where('id', @$data->user_id)->first();
                        $response = [
                            'token'                      => @$token,
                            'result'                     => @$usDetails,
                            'status'                     => "SUCCESS",
                            'S2'                         => 'Complete',
                            'registration_complete'      =>  "Y"
                        ];
                        return response()->json($response);
                    }
                    else{
                        @$usDetails= User::where('id', @$data->user_id)->first();
                        $response = [
                            'token'                      => null,
                            'result'                     => @$usDetails,
                            'status'                     => "SUCCESS",
                            'S2'                         => 'Complete',
                            'registration_complete'      =>  "N"
                        ];
                        return response()->json($response);
                    }
                // }
            }
        }

        // Step 4: Updating Professional Account Type............Third Itearation
        if(!empty(@$data->user_id) && @$data->mode=="S3"){
            $chekUser = User::where('id', @$data->user_id)->first();
            if($chekUser->account_type=="PER"){
                @$token = \Hash::make(str_random(27).'-'.@$chekUser->id.'-'.str_random(6));

                User::where('id', @$data->user_id)->update([
                        'registration_complete'  =>  'Y'
                    ]);
                
                $updtToken = User::where('id',@$chekUser->id)->update([
                                'user_token'   =>  @$token
                            ]);
                @$usDetails= User::where('id', @$chekUser->id)->first();
                $response = [
                    'token'                      => @$token,
                    'result'                     => @$usDetails,
                    'status'                     => "SUCCESS",
                    'S2'                         => 'Complete',
                    'registration_complete'      =>  "Y"
                ];
                return response()->json($response);
            }
            if($chekUser->account_type=="PRO"){
                if(empty(@$data->professional_account_type)){
                    $response = [
                        'token'             => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '507',
                                'message'   =>  config('customerror.507')
                            ]
                    ];
                    return response()->json($response);
                }

                //  For Blogger Account
                if(@$data->professional_account_type=="BL"){
                    if(!empty(@$data->nickname)){
                        $fndName = User::where('name', @$data->nickname)->where('id', '!=', @$data->user_id)->first();
                        if(@$fndName!=null){
                            $response = [
                                'token'             => null,
                                'status'            => "ERROR",
                                    'error'         => [
                                        'code'      =>  '511',
                                        'message'   =>  config('customerror.511')
                                    ]
                            ];
                            return response()->json($response);
                        }
                        else{
                            $updateUser = User::where('id',@$data->user_id)->update([
                                'name'   =>  @$data->nickname
                            ]);
                        }
                    }
                    @$token = \Hash::make(str_random(27).'-'.@$data->user_id.'-'.str_random(6));
                    
                    $updtToken = User::where('id',@$data->user_id)->update([
                                    'user_token'                    =>  @$token,
                                    'professional_account_type'     =>  "BL"
                                ]);
                    User::where('id', @$data->user_id)->update([
                        'registration_complete'  =>  'Y'
                    ]);
                    @$usDetails= User::where('id', @$data->user_id)->first();
                    $response = [
                        'token'                      => @$token,
                        'result'                     => @$usDetails,
                        'status'                     => "SUCCESS",
                        'S3'                         => 'Complete',
                        'registration_complete'      =>  "Y"
                    ];
                    return response()->json($response);
                }
                //  For Business Account
                if(@$data->professional_account_type=="BU"){
                    if(!empty(@$data->nickname)){
                        $fndName = User::where('name', @$data->nickname)->where('id', '!=', @$data->user_id)->first();
                        if(@$fndName!=null){
                            $response = [
                                'token'             => null,
                                'status'            => "ERROR",
                                    'error'         => [
                                        'code'      =>  '511',
                                        'message'   =>  config('customerror.511')
                                    ]
                            ];
                            return response()->json($response);
                        }
                        else{
                            $updateUser = User::where('id',@$data->user_id)->update([
                                'name'   =>  @$data->nickname
                            ]);
                        }
                    }
                    @$token = \Hash::make(str_random(27).'-'.@$data->user_id.'-'.str_random(6));
                    $updtToken = User::where('id',@$data->user_id)->update([
                                    'user_token'                  =>  @$token,
                                    'professional_account_type'   =>  "BU"
                                ]);
                    User::where('id', @$data->user_id)->update([
                        'registration_complete'  =>  'N'
                    ]);
                    @$usDetails= User::where('id', @$data->user_id)->first();
                    $response = [
                        'token'                      => @$token,
                        'result'                     => @$usDetails,
                        'status'                     => "SUCCESS",
                        'S3'                         => 'Complete',
                        'registration_complete'      =>  "N"
                    ];
                    return response()->json($response);
                }
            }
        }
        
        // Step 5: Updating Professional Business Address............Fourth Itearation

        if(!empty(@$data->user_id) && @$data->mode=="S4"){
            if(empty(@$data->business_address)){
                $response = [
                    'token'             => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '509',
                            'message'   =>  config('customerror.509')
                        ]
                ];
                return response()->json($response);
            }
            if(!empty(@$data->business_website_url)){
            
                $response = [
                    'token'             => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '508',
                            'message'   =>  config('customerror.508')
                        ]
                ];
                return response()->json($response);
                if(!filter_var(@$data->business_website_url, FILTER_VALIDATE_URL)){
                    $response = [
                        'token'             => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '510',
                                'message'   =>  config('customerror.510')
                            ]
                    ];
                    return response()->json($response);
                }
            }

            $day = array('SUN','MON', 'TUE', 'WED', 'THR', 'FRI', 'SAT');
            $updateBusinessAddress = User::where('id', @$data->user_id)->update([
                'business_address'      =>  @$data->business_address,
                'business_website_url'  =>  @$data->business_website_url
            ]);
            for(@$i=0; @$i<sizeof(@$day); @$i++){
                $text0 = @$day[@$i];
                $text1 = @$text0.'_from_time';
                $text2 = @$text0.'_to_time';
               if(!empty(@$data->$text1) && !empty(@$data->$text2)){
                   UserToBusiness::where('user_id', @$data->user_id)->delete();
                   $updateBusinessAddress = UserToBusiness::create([
                        'user_id'               =>  @$data->user_id,
                        'day'                   =>  @$text0,
                        'from_time'             =>  @$data->$text1,
                        'to_time'               =>  @$data->$text1
                    ]);
                }
            }
            @$token = \Hash::make(str_random(27).'-'.@$data->user_id.'-'.str_random(6));
            $updtToken = User::where('id',@$data->user_id)->update([
                            'user_token'   =>  @$token
                        ]);
            User::where('id', @$data->user_id)->update([
                        'registration_complete'  =>  'Y'
                    ]);
            @$usDetails= User::with('userBusinessAddress')->where('id', @$data->user_id)->first();
            $response = [
                'token'                      => @$token,
                'result'                     => @$usDetails,
                'status'                     => "SUCCESS",
                'S4'                         => 'Complete',
                'registration_complete'      =>  "Y"
            ];
            return response()->json($response);
        }

         // User's Existing Data: Getting User Existing Data............

        if(!empty(@$data->user_id) && @$data->mode!="S0" && @$data->mode!="S1" && @$data->mode!="S2" && @$data->mode!="S3" && @$data->mode!="S4"){
            $user = User::where('id', @$data->user_id)->first();
            if($user->professional_account_type=="BU"){
                $user = $user->load('UserToBusiness');
            }

            $response = [
                'token'             =>  null,
                'status'            =>  "SUCCESS",
                'prefill_details'   =>  @$user,
                'error'             =>  null
            ];
            return response()->json($response);
        }
    }

    public function forgetPass(){
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->username)){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '108',
                        'message'   =>  config('customerror.108')
                    ]
            ];
            return response()->json($response);
        }

        if(!is_numeric(@$data->username)) {
            
            if(filter_var(@$data->username, FILTER_VALIDATE_EMAIL)){
                $user = User::where('email', @$data->username)->where('user_type', '!=', 'A')->first();

                if($user!=null){
                    try{ 
                        @$vcode = "";
                        $user->vcode = @$vcode = mt_rand(100000,999999);
                        $user->save();
                        
                        Mail::to(@$data->username)->send(new ForgetPassword(@$vcode, @$data->username));
                        
                        $user = User::where('email', @$data->username)->first();
                        $response = [
                            'token'             => null,
                            'result'            => @$user,
                            'vcode'             => @$vcode,
                            'status'            => "SUCCESS",
                            'error'             => null
                        ];
                        return response()->json($response);
                    }
                    catch(\Exception $e){
                        $response = [
                            'token'            => null,
                            'status'            => "ERROR",
                                'error'         => [
                                    'code'      =>  '500',
                                    'message'   =>  config('customerror.500'),
                                    'errormsg'=> $e->getMessage()
                                ]
                        ];
                        return response()->json($response);
                    }
                
                } else {
                    $response = [
                        'token'            => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '110',
                                'message'   =>  config('customerror.110')
                            ]
                    ];
                    return response()->json($response);
                }
            }
            else{
                $response = [
                    'token'            => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '101',
                            'message'   =>  config('customerror.101')
                        ]
                ];
                return response()->json($response);
            }
        }

        if(is_numeric(@$data->username)){
            $user = User::where('mobile', @$data->username)->where('user_type', '!=', 'A')->first();
            if($user!=null) {
                try{ 
                    @$vcode = "";
                    $user->vcode = @$vcode = mt_rand(100000,999999);
                    $user->save();
                    
                    $url = 'http://18.220.123.51/change-password/'.$data->username;

                    $smsMsg = "Your Fsquad verifiation code is ". $vcode . "and forgot password link is :" .$url;
                    $sms = AWS::createClient('sns');
                    $sms->publish([
                        'Message' => $smsMsg,
                        'PhoneNumber' => '+91'.@$user->mobile, 
                        'MessageAttributes' => [
                            'AWS.SNS.SMS.SMSType'  => [
                                'DataType'    => 'String',
                                'StringValue' => 'Transactional',
                             ]
                         ],
                    ]);
                    

                    $user = User::where('email', @$data->username)->first();
                    $response = [
                        'token'             => null,
                        'result'            => @$user,
                        'vcode'             => @$vcode,
                        'status'            => "SUCCESS",
                        'error'             => null
                    ];
                    return response()->json($response);
                }
                catch(\Exception $e){
                    $response = [
                        'token'            => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '500',
                                'message'   =>  config('customerror.500')
                            ]
                    ];
                    return response()->json($response);
                }
            
            } else {
                $response = [
                    'token'            => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '110',
                            'message'   =>  config('customerror.110')
                        ]
                ];
                return response()->json($response);
            }

        } else {
            $response = [
                'token'            => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '502',
                        'message'   =>  config('customerror.502')
                    ]
            ];
            return response()->json($response);
        }
    }


    public function changePass($user) {
        $email = $user;
        return view('forgot', compact('email'));
    }

    // public function verifyVcode(){
    //     $json = file_get_contents('php://input');
    //     $data = json_decode($json);
        
    //     if(empty(@$data->user_id)) {
    //         $response = [
    //             'token'             => null,
    //             'status'            => "ERROR",
    //                 'error'         => [
    //                     'code'      =>  '514',
    //                     'message'   =>  config('customerror.514')
    //                 ]
    //         ];
    //         return response()->json($response);
    //     }
    //     if(empty(@$data->vcode)){
    //         $response = [
    //             'token'             => null,
    //             'status'            => "ERROR",
    //                 'error'         => [
    //                     'code'      =>  '516',
    //                     'message'   =>  config('customerror.516')
    //                 ]
    //         ];
    //         return response()->json($response);
    //     }

    //     $user = User::where([
    //         'id'        =>  @$data->user_id,
    //         'vcode'     =>  @$data->vcode
    //     ])->where('user_type', '!=', 'A')->first();
    //     if($user!=null){
    //         $user->vcode=null;
    //         $user->save();
    //         $user = User::where([
    //             'id'        =>  @$data->user_id
    //         ])->first();

    //         $response = [
    //             'token'             => null,
    //             'status'            => "SUCCESS",
    //             'result'            => @$user,
    //             'error'             => null
    //         ];
    //         return response()->json($response);
    //     }
    //     else{
    //         $response = [
    //             'token'             => null,
    //             'status'            => "ERROR",
    //                 'error'         => [
    //                     'code'      =>  '517',
    //                     'message'   =>  config('customerror.517')
    //                 ]
    //         ];
    //         return response()->json($response);
    //     }
    // }

    public function updateForgetPassword(Request $request) {
        //$json = file_get_contents('php://input');
        $data = request()->all();

        $user_id = $data->user_id;
        
        if(empty(@$data->vcode)) {
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '516',
                        'message'   =>  config('customerror.516')
                    ]
            ];
            return response()->json($response);
        }

        if(empty(@$data->new_password)){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '515',
                        'message'   =>  config('customerror.515')
                    ]
            ];
            return response()->json($response);
        }
        
        if(empty(@$data->confirm_password)){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '116',
                        'message'   =>  config('customerror.116')
                    ]
            ];
            return response()->json($response);
        }

        if(@$data->new_password!=@$data->confirm_password){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '118',
                        'message'   =>  config('customerror.118')
                    ]
            ];
            return response()->json($response);
        }

        $user = User::where(['vcode' => @$data->vcode])->where('user_type', '!=', 'A')->first();
        if($user!=null) {

            if(is_numeric(@$data->username)) {
                $userDataCheck = $user->phone;
            } else {
                $userDataCheck = $user->email;
            }

            if($userDataCheck == $user_id) {
        
                $user = User::where('id', @$user->id)->where('user_type', '!=', 'A')->first();
                if($user==null) {
                    $response = [
                        'token'             => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '518',
                                'message'   =>  config('customerror.518')
                            ]
                    ];
                    return response()->json($response);
                }
                @$user->password = \Hash::make(@$data->new_password);
                @$user->save();
                
                $response = [
                    'token'             => null,
                    'status'            => "SUCCESS",
                    'error'             => null
                ];
                return response()->json($response);
            
            } else {

                $response = [
                    'token'             => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '517',
                            'message'   =>  config('customerror.517')
                        ]
                ];
                return response()->json($response);
            }

        } else {

            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '517',
                        'message'   =>  config('customerror.517')
                    ]
            ];
            return response()->json($response);
        }
    }

    public function updatePassword(){
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->user_id)){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '514',
                        'message'   =>  config('customerror.514')
                    ]
            ];
            return response()->json($response);
        }

        if(empty(@$data->old_password)){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '513',
                        'message'   =>  config('customerror.513')
                    ]
            ];
            return response()->json($response);
        }
        if(empty(@$data->new_password)){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '515',
                        'message'   =>  config('customerror.515')
                    ]
            ];
            return response()->json($response);
        }
        
        if(empty(@$data->confirm_password)){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '116',
                        'message'   =>  config('customerror.116')
                    ]
            ];
            return response()->json($response);
        }

        if(@$data->new_password!=@$data->confirm_password){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '118',
                        'message'   =>  config('customerror.118')
                    ]
            ];
            return response()->json($response);
        }
        $user = User::where('id', @$data->user_id)->where('user_type', '!=', 'A')->first();
        if($user==null){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '518',
                        'message'   =>  config('customerror.518')
                    ]
            ];
            return response()->json($response);
        }
        if(\Hash::check(@$data->old_password, @$user->password)){
            $user->password = \Hash::make(@$data->new_password);
            $user->save();
            $response = [
                'token'             => null,
                'status'            => "SUCCESS",
                'error'             => null
            ];
            return response()->json($response);
        }
        else{
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '519',
                        'message'   =>  config('customerror.519')
                    ]
            ];
            return response()->json($response);
        }
    }

    public function socialLogin(){
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->username)){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '108',
                        'message'   =>  config('customerror.108')
                    ]
            ];
            return response()->json($response);
        }
        if(empty(@$data->username)){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '108',
                        'message'   =>  config('customerror.108')
                    ]
            ];
            return response()->json($response);
        }

        if(empty(@$data->device_registrartion_id) || @$data->device_registrartion_id==null || @$data->device_registrartion_id==""){
            $response = [
                'token'            => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '128',
                        'message'   =>  config('customerror.128')
                    ]
            ];
            return response()->json($response);
        }

        // if(!filter_var(@$data->username, FILTER_VALIDATE_EMAIL)){
        //     $response = [
        //         'token'            => null,
        //         'status'            => "ERROR",
        //             'error'         => [
        //                 'code'      =>  '101',
        //                 'message'   =>  config('customerror.101')
        //             ]
        //     ];
        //     return response()->json($response);
        // }

        $user = User::where([
                'email'     =>  @$data->username,
                'user_type' =>  "U"
            ])->first();

        //dd($data->username);

        if(@$user!=null){
            
            if(@$user->status=="I"){
                $response = [
                    'token'            => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '112',
                            'message'   =>  config('customerror.112')
                        ]
                ];
                return response()->json($response);
            }
            else {

                if(@$user->registration_complete == "Y") {

                    @$token = \Hash::make(str_random(27).'-'.@$user->id.'-'.str_random(6));
                    @$updateUser = User::where('id', @$user->id)->update([
                                    'user_token'    =>  @$token
                                ]);

                    @$updateRegID = User::where('id', @$user->id)->update([
                        'device_registrartion_id'   =>  @$data->device_registrartion_id
                    ]);
                    @$getUpdatedUser = User::where('id', @$user->id)->first();

                    $response = [
                        'result'           => @$getUpdatedUser,
                        'token'            => @$getUpdatedUser->user_token,
                        'status'           => "SUCCESS",
                        'error'            => null
                    ];
                    return response()->json($response);

                } else {

                    $response = [
                        'token'            => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '558',
                                'message'   =>  config('customerror.558')
                            ]
                    ];
                    return response()->json($response);
                }

            }
        }
        else{
            $response = [
                'token'            => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '110',
                        'message'   =>  config('customerror.110')
                    ]
            ];
            return response()->json($response);
        }
    }
}
