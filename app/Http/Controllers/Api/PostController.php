<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostStep;
use App\Models\PostIngredients;
use App\Models\PostRecipie;
use App\Models\PostImages;
use App\Models\PostToCookedIt;
use App\Models\UserToFriend;
use App\Models\WannaCookedIt;
use App\Models\TagPost;
use App\Models\TagDescription;
use App\Models\TagComment;
use App\Models\Brand;
use App\Models\CookedIt;
use App\Models\Ingredient;
use App\Models\Chat;
use App\User;

class PostController extends Controller
{

   public function addPost(Request $request){
   		//Validating user id
	   	if(empty(@$request->user_id) || @$request->user_id==null){
	   		$response = [
	            'token'            => null,
	            'status'            => "ERROR",
	                'error'         => [
	                    'code'      =>  '514',
	                    'message'   =>  config('customerror.514')
	                ]
	        ];
	        return response()->json($response);
	   	}
	   	//Validating Post Description
	   	
	   	// if(empty(@$request->post_description) || @$request->post_description==null){
	   	// 	$response = [
	    //         'token'            => null,
	    //         'status'            => "ERROR",
	    //             'error'         => [
	    //                 'code'      =>  '522',
	    //                 'message'   =>  config('customerror.522')
	    //             ]
	    //     ];
	    //     return response()->json($response);
	   	// }
	   	//Validating Post Location

	   	if(empty(@$request->location) || @$request->location==null){
	   		$response = [
	            'token'            => null,
	            'status'            => "ERROR",
	                'error'         => [
	                    'code'      =>  '521',
	                    'message'   =>  config('customerror.521')
	                ]
	        ];
	        return response()->json($response);
	   	}


	   	//Validating Post Images
	   	if(empty(@$request->post_images) && !is_array(@$request->post_images)){
			$response = [
	            'token'            => null,
	            'status'            => "ERROR",
	                'error'         => [
	                    'code'      =>  '523',
	                    'message'   =>  config('customerror.523')
	                ]
	        ];
	        return response()->json($response);
		}

		//Validating Post Images

	   	for($j=0;$j<@sizeof($request->post_images);$j++){
			$ext = array('jpg', 'png', 'JPEG', 'jpeg', 'mkv', 'mp4', 'flv', 'mp4', 'm3u8', 'ts', '3gp', 'mov', 'avi', 'wmv');

			if(!in_array(@$request->post_images[@$j]->getClientOriginalExtension(), @$ext)){
				$response = [
		            'token'            => null,
		            'status'            => "ERROR",
		                'error'         => [
		                    'code'      =>  '530',
		                    'message'   =>  config('customerror.530')
		                ]
	        		];
	        	return response()->json($response);
			}
	   	}

	   	//If User Cooked it the item

   		if(@$request->i_cooked_it=="Y"){
   			
   			// if(empty(@$request->manual_recipe)&&empty(@$request->manual_recipe)&&empty(@$request->tag_recipie)){

   			// 	$response = [
		    //         'token'            => null,
		    //         'status'            => "ERROR",
		    //             'error'         => [
		    //                 'code'      =>  '538',
		    //                 'message'   =>  config('customerror.538')
		    //             ]
	     //    		];
	     //    	return response()->json($response);
   			// }
   			// If recipe type is manual
   			if(@$request->manual_recipe=="Y"){
   				// Validating Post Recipe

   				if(empty(@$request->recipe_title) || @$request->recipe_title==null){
   					$response = [
			            'token'            => null,
			            'status'            => "ERROR",
			                'error'         => [
			                    'code'      =>  '532',
			                    'message'   =>  config('customerror.532')
			                ]
			        ];
			        return response()->json($response);
   				}
   				// Validating Post recipe story

   				if(empty(@$request->recipe_story) || @$request->recipe_story==null){
   					$response = [
			            'token'            => null,
			            'status'            => "ERROR",
			                'error'         => [
			                    'code'      =>  '533',
			                    'message'   =>  config('customerror.533')
			                ]
			        ];
			        return response()->json($response);
   				}

   				// Validating Post ingredients
   				if(empty(@$request->ingredents) && !is_array(@$request->ingredents)){
   					$response = [
			            'token'            => null,
			            'status'            => "ERROR",
			                'error'         => [
			                    'code'      =>  '526',
			                    'message'   =>  config('customerror.526')
			                ]
			        ];
			        return response()->json($response);
   				}

   				// Validating Post Step Counter
   				if(empty(@$request->total_steps) || @$request->total_steps==null){
   					$response = [
			            'token'            => null,
			            'status'            => "ERROR",
			                'error'         => [
			                    'code'      =>  '525',
			                    'message'   =>  config('customerror.525')
			                ]
			        ];
			        return response()->json($response);	
   				}

   				// Validating Post Steps
   				// for($i=0;$i<@$request->total_steps;$i++){
   				// 	$name = "step_".$i;
   				// 	if($i==0){
   				// 		if(empty(@$request->$name) || @$request->$name==null){
	   			// 			$response = [
					  //           'token'            => null,
					  //           'status'            => "ERROR",
					  //               'error'         => [
					  //                   'code'      =>  '527',
					  //                   'message'   =>  config('customerror.527')
					  //               ]
					  //       ];
					  //       return response()->json($response);
	   			// 		}
	   			// 		else{
	   			// 			break;
	   			// 		}
   				// 	}
   				// }
   				// Validating Post Steps Image and instructions
   				for($i=1;$i<=$request->total_steps;$i++){
   					$instruction_name = "step_".@$i."_instruction";
   					$instruction_image = "step_".@$i."_images";

   					if(is_array(@$request->$instruction_image)){

	   					if(empty(@$request->$instruction_name) || @$request->$instruction_name==null){
	   						$response = [
					            'token'            => null,
					            'status'            => "ERROR",
					                'error'         => [
					                    'code'      =>  '528',
					                    'message'   =>  [
					                    	'message'	=>	'STEPS_'.@$i.'_INSTRUCTION_IS_REQUIRED',
					                    	'meaning'	=>	'Please enter instruction for step:'.@$i
					                    ]
					                ]
					        ];
					        return response()->json($response);
	   					}

	   					for($j=0;$j<3;$j++){
		   					if(empty(@$request->$instruction_image[@$j]) || @$request->$instruction_image[@$j]==null){
		   						$response = [
						            'token'            => null,
						            'status'            => "ERROR",
						                'error'         => [
						                    'code'      =>  '529',
						                    'message'   =>  [
						                    	'message'	=>	'STEPS_'.@$i.'_IMAGE_IS_REQUIRED',
						                    	'meaning'	=>	'Please Upload the image or video for step-'.@$i.' no: '.@($j+1).' image'
						                    ]
						                ]
						        ];
						        return response()->json($response);
		   					}

		   					$ext = array('jpg', 'png', 'JPEG', 'jpeg', 'mkv', 'mp4', 'flv', 'mp4', 'm3u8', 'ts', '3gp', 'mov', 'avi', 'wmv');

		   					if(!in_array(@$request->$instruction_image[@$j]->getClientOriginalExtension(), @$ext)){
		   						$response = [
						            'token'            => null,
						            'status'            => "ERROR",
						                'error'         => [
						                    'code'      =>  '530',
						                    'message'   =>  config('customerror.530')
						                ]
						        ];
						        return response()->json($response);
		   					}
	   					}
   					}
   					else{

   						$response = [
				            'token'            => null,
				            'status'            => "ERROR",
				                'error'         => [
				                    'code'      =>  '98',
				                    'message'   =>  [
				                    	'message'	=>	'INSTRUCTION_IMAGE_IS_REQUIRED',
				                    	'meaning'	=>	'Please Upload step images'
				                    ]
				                ]
				        ];
				        return response()->json($response);
   					}
   				}

   				$userDetails = User::where('id', @$request->user_id)->where('user_type', 'U')->first();
   				if(@$userDetails==null){
   					$response = [
			            'token'            => null,
			            'status'            => "ERROR",
			                'error'         => [
			                    'code'      =>  '110',
			                    'message'   =>  config('customerror.110')
			                ]
			        ];
			        return response()->json($response);
   				}
   				
   				$createPost = Post::create([
   					'user_id'				=>	@$userDetails->id,
   					'description'			=>	@$request->post_description??'',
   					'hash_tags'				=>	@$request->hash_tags,
   					'is_i_cooked_it'		=>	"Y",
   					'location'				=>	@$request->location,
   					'latitude'				=>	@$request->latitude,
   					'longitude'				=>	@$request->longitude,
   					'recipe_type'			=>	"MANUAL",
   					'is_post_private'		=>	@$userDetails->is_post_private
   				]);
   				if(@$createPost->id){
   					// Now Uploading Post Images
   					for($i=0;$i<sizeof(@$request->post_images);$i++){
   						try {

   							$ext = array('mkv', 'mp4', 'flv', 'mp4', 'm3u8', 'ts', '3gp', 'mov', 'avi', 'wmv');
   							if(!in_array(@$request->post_images[@$i]->getClientOriginalExtension(), @$ext)) {
	   							$requestType = 1;
	   						} else {
	   							$requestType = 2;
	   						}

	   						$postImgName = @$createPost->id.'-'.str_random(8).time().'.'.@$request->post_images[@$i]->getClientOriginalExtension();
	   						
	   						@$request->post_images[@$i]->move("storage/app/public/post_images/", $postImgName);

	   						$postToImages = PostImages::create([
	   							'post_id'		=>	@$createPost->id,
	   							'image'			=>	@$postImgName,
	   							'is_default'	=>	@$i==0 ? "Y": "N",
	   							'type'          =>  @$requestType
	   						]);
   						}
   						catch(\Exception $e){
   							$response = [
					            'token'            => null,
					            'status'            => "ERROR",
					                'error'         => [
					                    'code'      =>  '500',
					                    'message'   =>  config('customerror.500')
					                ]
					        ];
					        return response()->json($response);
   						}
   					}
   					// Now adding recipe
   					$createRecipe = PostRecipie::create([
   						'post_id'	=>	@$createPost->id,
   						'title'		=>	@$request->recipe_title,
   						'story'		=>	@$request->recipe_story
   					]);
   					if(@$createRecipe->id){
   						// Now adding ingredients
   						for($i=0;$i<sizeof(@$request->ingredents); $i++){
	   						@$createIngredients = PostIngredients::create([
	   							'post_id'			=>	@$createPost->id,
	   							'recipe_id'			=>	@$createRecipe->id,
	   							'ingredients_id'	=>	@$request->ingredents[@$i]
	   						]);
   						}

   						// Now adding Steps
   						for($i=0;$i<@$request->total_steps;$i++){

		   					$instruction_name = "step_".@($i+1)."_instruction";
   							$instruction_image = "step_".@($i+1)."_images";
		   					$createSteps = PostStep::create([
		   						'post_id'		=>	@$createPost->id,
		   						'recipe_id'		=>	@$createRecipe->id,
		   						'step'			=>	@$i+1,
		   						'instruction'	=>	@$request->$instruction_name
		   					]);
		   					
		   					for($k=0; $k<sizeof(@$request->$instruction_image);$k++){

			   					@$stepImageName = @$createSteps->id.str_random(6).time().'.'.@$request->$instruction_image[@$k]->getClientOriginalExtension();
			   					
			   					@$request->$instruction_image[@$k]->move("storage/app/public/post_step_images/", $stepImageName);
		   						
		   						$updateSteps = PostStep::where('id', @$createSteps->id)->update([
			   						'media_file_'.(@$k+1)	=>	@$stepImageName
		   						]);
		   					}
		   				}

		   				//if(!empty(@$request->tag_friends)&&is_array(@$request->tag_friends)){
		   				if(!empty(@$request->tag_friends)) {
							$tagss = explode(",", $request->tag_friends);
		   					foreach($tagss as $tf) {
			   					$postTagFriends = TagPost::create([
			   						'post_id'	=>	@$createPost->id,
			   						'user_id'	=>	@$createPost->user_id,
			   						'friend_id'	=>	@$tf
			   					]);
		   					}
		   				}

		   				if(!empty(@$request->desc_tag_friends)) {
							$tagss1 = explode(",", $request->desc_tag_friends);
		   					foreach($tagss1 as $tf1) {
			   					$postTagFriends = TagDescription::create([
			   						'post_id'	=>	@$createPost->id,
			   						'user_id'	=>	@$createPost->user_id,
			   						'friend_id'	=>	@$tf1
			   					]);
		   					}
		   				}

		   				if(!empty(@$request->comment_tag_friends)) {
							$tagss2 = explode(",", $request->comment_tag_friends);
		   					foreach($tagss2 as $tf2) {
			   					$postTagFriends = TagComment::create([
			   						'post_id'	=>	@$createPost->id,
			   						'user_id'	=>	@$createPost->user_id,
			   						'friend_id'	=>	@$tf2
			   					]);
		   					}
		   				}
		   				$response = [
                            'token'                      => null,
                            'result'                     => @$createPost,
                            'status'                     => "SUCCESS"
                        ];
                        return response()->json($response);
   					}
   					else{
   						$response = [
				            'token'            => null,
				            'status'            => "ERROR",
				                'error'         => [
				                    'code'      =>  '500',
				                    'message'   =>  config('customerror.500')
				                ]
				        ];
				        return response()->json($response);
   					}
   				}
   				else{
   					$response = [
			            'token'            => null,
			            'status'            => "ERROR",
			                'error'         => [
			                    'code'      =>  '500',
			                    'message'   =>  config('customerror.500')
			                ]
			        ];
			        return response()->json($response);
   				}

   			}
   			
   			//If Recipe type is video Source
   			elseif(@$request->video_recipe=="Y"){

   				if(empty(@$request->recipe_title) || @$request->recipe_title==null){
   					$response = [
			            'token'            => null,
			            'status'            => "ERROR",
			                'error'         => [
			                    'code'      =>  '532',
			                    'message'   =>  config('customerror.532')
			                ]
			        ];
			        return response()->json($response);
   				}

   				if(empty(@$request->video_url) || @$request->video_url==null){
   					$response = [
			            'token'            => null,
			            'status'            => "ERROR",
			                'error'         => [
			                    'code'      =>  '534',
			                    'message'   =>  config('customerror.534')
			                ]
			        ];
			        return response()->json($response);
   				}

   				if(!filter_var(@$request->video_url, FILTER_VALIDATE_URL)){
   					$response = [
			            'token'            => null,
			            'status'            => "ERROR",
			                'error'         => [
			                    'code'      =>  '535',
			                    'message'   =>  config('customerror.535')
			                ]
			        ];
			        return response()->json($response);
   				}
   				$userDetails = User::where('id', @$request->user_id)->where('user_type', 'U')->first();

   				$createPost = Post::create([
   					'user_id'				=>	@$userDetails->id,
   					'description'			=>	@$request->post_description??'',
   					'hash_tags'				=>	@$request->hash_tags,
   					'is_i_cooked_it'		=>	"Y",
   					'location'				=>	@$request->location,
   					'latitude'				=>	@$request->latitude,
   					'longitude'				=>	@$request->longitude,
   					'recipe_type'			=>	"VIDEO",
   					'is_post_private'		=>	@$userDetails->is_post_private
   				]);
   				if(@$createPost->id){
   					// Now Uploading Post Images
   					for($i=0;$i<sizeof(@$request->post_images);$i++){
   						try{

   							$ext = array('mkv', 'mp4', 'flv', 'mp4', 'm3u8', 'ts', '3gp', 'mov', 'avi', 'wmv');
   							if(!in_array(@$request->post_images[@$i]->getClientOriginalExtension(), @$ext)) {
	   							$requestType = 1;
	   						} else {
	   							$requestType = 2;
	   						}

	   						$postImgName = @$createPost->id.'-'.str_random(8).time().'.'.@$request->post_images[@$i]->getClientOriginalExtension();
	   						@$request->post_images[@$i]->move("storage/app/public/post_images/", $postImgName);
	   						$postToImages = PostImages::create([
	   							'post_id'		=>	@$createPost->id,
	   							'image'			=>	@$postImgName,
	   							'is_default'	=>	@$i==0 ? "Y": "N",
	   							'type'          =>  $requestType
	   						]);
   						}
   						catch(\Exception $e){
   							$response = [
					            'token'            => null,
					            'status'            => "ERROR",
					                'error'         => [
					                    'code'      =>  '500',
					                    'message'   =>  config('customerror.500')
					                ]
					        ];
					        return response()->json($response);
   						}
   					}

   					//if(!empty(@$request->tag_friends)&&is_array(@$request->tag_friends)){
	   				if(!empty(@$request->tag_friends)) {
						$tagss = explode(",", $request->tag_friends);
	   					foreach($tagss as $tf) {
		   					$postTagFriends = TagPost::create([
		   						'post_id'	=>	@$createPost->id,
		   						'user_id'	=>	@$createPost->user_id,
		   						'friend_id'	=>	@$tf
		   					]);
	   					}
	   				}

	   				if(!empty(@$request->desc_tag_friends)) {
						$tagss1 = explode(",", $request->desc_tag_friends);
	   					foreach($tagss1 as $tf1) {
		   					$postTagFriends = TagDescription::create([
		   						'post_id'	=>	@$createPost->id,
		   						'user_id'	=>	@$createPost->user_id,
		   						'friend_id'	=>	@$tf1
		   					]);
	   					}
	   				}

	   				if(!empty(@$request->comment_tag_friends)) {
						$tagss2 = explode(",", $request->comment_tag_friends);
	   					foreach($tagss2 as $tf2) {
		   					$postTagFriends = TagComment::create([
		   						'post_id'	=>	@$createPost->id,
		   						'user_id'	=>	@$createPost->user_id,
		   						'friend_id'	=>	@$tf2
		   					]);
	   					}
	   				}
   					$createRecipe = PostRecipie::create([
   						'post_id'			=>	@$createPost->id,
   						'title'				=>	@$request->recipe_title,
   						'youtube_video_url'	=>	@$request->video_url
   					]);

   					$response = [
                        'token'                      => null,
                        'result'                     => @$createPost,
                        'status'                     => "SUCCESS"
                    ];
                    return response()->json($response);
   				}
   				else{
   					$response = [
			            'token'            => null,
			            'status'            => "ERROR",
			                'error'         => [
			                    'code'      =>  '500',
			                    'message'   =>  config('customerror.500')
			                ]
			        ];
			        return response()->json($response);
   				}

   			}
   			//If Recipe From Favourites
   			elseif(@$request->tag_recipie=="Y"){
   				if(empty(@$request->wanna_cooked_it)||@$request->wanna_cooked_it==""){
   					$response = [
			            'token'            => null,
			            'status'            => "ERROR",
			                'error'         => [
			                    'code'      =>  '536',
			                    'message'   =>  config('customerror.536')
			                ]
			        ];
			        return response()->json($response);
   				}
   				$userDetails = User::where('id', @$request->user_id)->where('user_type', 'U')->first();
   				if(@$userDetails==null){
   					$response = [
			            'token'            => null,
			            'status'            => "ERROR",
			                'error'         => [
			                    'code'      =>  '110',
			                    'message'   =>  config('customerror.110')
			                ]
			        ];
			        return response()->json($response);
   				}
				$myCookedIt = WannaCookedIt::where([
					'id'		=>	@$request->wanna_cooked_it,
					'user_id'	=>	@$request->user_id
				])->first();
				if($myCookedIt==null){
					$response = [
			            'token'            => null,
			            'status'            => "ERROR",
			                'error'         => [
			                    'code'      =>  '537',
			                    'message'   =>  config('customerror.537')
			                ]
			        ];
			        return response()->json($response);
				}
				$createPost = Post::create([
   					'user_id'				=>	@$userDetails->id,
   					'description'			=>	@$request->post_description??'',
   					'hash_tags'				=>	@$request->hash_tags,
   					'is_i_cooked_it'		=>	"Y",
   					'location'				=>	@$request->location,
   					'latitude'				=>	@$request->latitude,
   					'longitude'				=>	@$request->longitude,
   					'recipe_type'			=>	"TAGGED",
   					'is_post_private'		=>	@$userDetails->is_post_private
   				]);

				if(@$createPost->id){
					// Now Uploading Post Images
   					for($i=0;$i<sizeof(@$request->post_images);$i++){
   						try{

   							$ext = array('mkv', 'mp4', 'flv', 'mp4', 'm3u8', 'ts', '3gp', 'mov', 'avi', 'wmv');
   							if(!in_array(@$request->post_images[@$i]->getClientOriginalExtension(), @$ext)) {
	   							$requestType = 1;
	   						} else {
	   							$requestType = 2;
	   						}

	   						$postImgName = @$createPost->id.'-'.str_random(8).time().'.'.@$request->post_images[@$i]->getClientOriginalExtension();
	   						@$request->post_images[@$i]->move("storage/app/public/post_images/", $postImgName);
	   						$postToImages = PostImages::create([
	   							'post_id'		=>	@$createPost->id,
	   							'image'			=>	@$postImgName,
	   							'is_default'	=>	@$i==0 ? "Y": "N",
	   							'type'          =>   $requestType
	   						]);
   						}
   						catch(\Exception $e){
   							$response = [
					            'token'            => null,
					            'status'            => "ERROR",
					                'error'         => [
					                    'code'      =>  '500',
					                    'message'   =>  config('customerror.500')
					                ]
					        ];
					        return response()->json($response);
   						}
   					}
					$postToCookedIt = PostToCookedIt::create([
						'user_id'		=>	@$userDetails->id,
						'post_id'		=>	@$createPost->id,
						'cooked_it_id'	=>	@$myCookedIt->id
					]);

					//if(!empty(@$request->tag_friends)&&is_array(@$request->tag_friends)){
	   				if(!empty(@$request->tag_friends)) {
						$tagss = explode(",", $request->tag_friends);
	   					foreach($tagss as $tf) {
		   					$postTagFriends = TagPost::create([
		   						'post_id'	=>	@$createPost->id,
		   						'user_id'	=>	@$createPost->user_id,
		   						'friend_id'	=>	@$tf
		   					]);
	   					}
	   				}

	   				if(!empty(@$request->desc_tag_friends)) {
						$tagss1 = explode(",", $request->desc_tag_friends);
	   					foreach($tagss1 as $tf1) {
		   					$postTagFriends = TagDescription::create([
		   						'post_id'	=>	@$createPost->id,
		   						'user_id'	=>	@$createPost->user_id,
		   						'friend_id'	=>	@$tf1
		   					]);
	   					}
	   				}

	   				if(!empty(@$request->comment_tag_friends)) {
						$tagss2 = explode(",", $request->comment_tag_friends);
	   					foreach($tagss2 as $tf2) {
		   					$postTagFriends = TagComment::create([
		   						'post_id'	=>	@$createPost->id,
		   						'user_id'	=>	@$createPost->user_id,
		   						'friend_id'	=>	@$tf2
		   					]);
	   					}
	   				}
					$response = [
                        'token'                      => null,
                        'result'                     => @$createPost,
                        'status'                     => "SUCCESS"
                    ];
                    return response()->json($response);
				}
				else{
					$response = [
			            'token'            => null,
			            'status'            => "ERROR",
			                'error'         => [
			                    'code'      =>  '500',
			                    'message'   =>  config('customerror.500')
			                ]
			        ];
			        return response()->json($response);
				}
   			}
   			else{
   				$response = [
		            'token'            => null,
		            'status'            => "ERROR",
		                'error'         => [
		                    'code'      =>  '538',
		                    'message'   =>  config('customerror.538')
		                ]
	        		];
	        	return response()->json($response);
   			}
   		}
   		else{

   			// $response = [
      //           'token'                      => null,
      //           'result'                     => @$request,
      //           'status'                     => "SUCCESS"
      //       ];
      //       return response()->json($response);
   			
   			$userDetails = User::where('id', @$request->user_id)->where('user_type', 'U')->first();
   				if(@$userDetails==null){
   					$response = [
			            'token'            => null,
			            'status'            => "ERROR",
			                'error'         => [
			                    'code'      =>  '110',
			                    'message'   =>  config('customerror.110')
			                ]
			        ];
			        return response()->json($response);
   				}



				$createPost = Post::create([
   					'user_id'				=>	@$userDetails->id,
   					'description'			=>	@$request->post_description??'',
   					'hash_tags'				=>	@$request->hash_tags,
   					'is_i_cooked_it'		=>	"N",
   					'location'				=>	@$request->location,
   					'latitude'				=>	@$request->latitude,
   					'longitude'				=>	@$request->longitude,
   					'recipe_type'			=>	"NONE",
   					'is_post_private'		=>	@$userDetails->is_post_private
   				]);

			if(@$createPost->id){
				// Now Uploading Post Images
					
					for($i=0;$i<sizeof(@$request->post_images);$i++){
						try{

							$ext = array('mkv', 'mp4', 'flv', 'mp4', 'm3u8', 'ts', '3gp', 'mov', 'avi', 'wmv');
   							if(!in_array(@$request->post_images[@$i]->getClientOriginalExtension(), @$ext)) {
	   							$requestType = 1;
	   						} else {
	   							$requestType = 2;
	   						}

   						$postImgName = @$createPost->id.'-'.str_random(8).time().'.'.@$request->post_images[@$i]->getClientOriginalExtension();
   						@$request->post_images[@$i]->move("storage/app/public/post_images/", $postImgName);
   						$postToImages = PostImages::create([
   							'post_id'		=>	@$createPost->id,
   							'image'			=>	@$postImgName,
   							'is_default'	=>	@$i==0 ? "Y": "N",
   							'type'          =>  $requestType
   						]);
						}
						catch(\Exception $e){
							$response = [
				            'token'            => null,
				            'status'            => "ERROR",
				                'error'         => [
				                    'code'      =>  '500',
				                    'message'   =>  config('customerror.500')
				                ]
				        ];
				        return response()->json($response);
						}
					}

				//if(!empty(@$request->tag_friends)&&is_array(@$request->tag_friends)){
				if(!empty(@$request->tag_friends)) {
					$tagss = explode(",", $request->tag_friends);
   					foreach($tagss as $tf) {
	   					$postTagFriends = TagPost::create([
	   						'post_id'	=>	@$createPost->id,
	   						'user_id'	=>	@$createPost->user_id,
	   						'friend_id'	=>	@$tf
	   					]);
   					}
   				}

   				if(!empty(@$request->desc_tag_friends)) {
					$tagss1 = explode(",", $request->desc_tag_friends);
   					foreach($tagss1 as $tf1) {
	   					$postTagFriends = TagDescription::create([
	   						'post_id'	=>	@$createPost->id,
	   						'user_id'	=>	@$createPost->user_id,
	   						'friend_id'	=>	@$tf1
	   					]);
   					}
   				}

   				if(!empty(@$request->comment_tag_friends)) {
					$tagss2 = explode(",", $request->comment_tag_friends);
   					foreach($tagss2 as $tf2) {
	   					$postTagFriends = TagComment::create([
	   						'post_id'	=>	@$createPost->id,
	   						'user_id'	=>	@$createPost->user_id,
	   						'friend_id'	=>	@$tf2
	   					]);
   					}
   				}

				$response = [
                    'token'                      => null,
                    'result'                     => @$createPost,
                    'status'                     => "SUCCESS"
                ];
                return response()->json($response);
			}
			else{
				$response = [
		            'token'            => null,
		            'status'            => "ERROR",
		                'error'         => [
		                    'code'      =>  '500',
		                    'message'   =>  config('customerror.500')
		                ]
		        ];
		        return response()->json($response);
			}
   		}
   	}

   	public function sendFCMIos(){
   		$ch = curl_init("https://fcm.googleapis.com/fcm/send");

	    //The device token.
	    $token = ""; //token here

	    //Title of the Notification.
	    $title = "Carbon";

	    //Body of the Notification.
	    $body = "Bear island knows no king but the king in the north, whose name is stark.";

	    //Creating the notification array.
	    $notification = array('title' =>$title , 'text' => $body);

	    //This array contains, the token and the notification. The 'to' attribute stores the token.
	    $arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');

	    //Generating JSON encoded string form the above array.
	    $json = json_encode($arrayToSend);
	    //Setup headers:
	    $headers = array();
	    $headers[] = 'Content-Type: application/json';
	    $headers[] = 'Authorization: key= $key'; // key here

	    //Setup curl, add headers and post parameters.
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
	    curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);       

	    //Send the request
	    $response = curl_exec($ch);

	    //Close request
	    curl_close($ch);
	    return $response;
   	}  

   	public function sendFCMAndroid(){
   		define('API_ACCESS_KEY','Api key from Fcm add here');
		$fcmUrl = 'https://fcm.googleapis.com/fcm/send';
		$token='235zgagasd634sdgds46436';
	    $notification = [
	            'title' =>'title',
	            'body' => 'body of message.',
	            'icon' =>'myIcon', 
	            'sound' => 'mySound'
	        ];
	        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];
	        $fcmNotification = [
	            //'registration_ids' => $tokenList, //multple token array
	            'to'        => $token, //single token
	            'notification' => $notification,
	            'data' => $extraNotificationData
	        ];
	        $headers = [
	            'Authorization: key=' . API_ACCESS_KEY,
	            'Content-Type: application/json'
	        ];
	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
	        curl_setopt($ch, CURLOPT_POST, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
	        $result = curl_exec($ch);
	        curl_close($ch);
        	echo $result;
   	}

   	public function fecthBrand(){
   		$brand = Brand::where('status', 'A')->orderBy('name')->get();
   		if($brand!=null){
			$response = [
                        'token'                      => null,
                        'result'                     => @$brand,
                        'status'                     => "SUCCESS"
                    ];
   			return response()->json($response);
   		}
   		else{
   			$response = [
	            'token'            => null,
	            'status'            => "ERROR",
	                'error'         => [
	                    'code'      =>  '539',
	                    'message'   =>  config('customerror.539')
	                ]
	        ];
	        return response()->json($response);
   		}
   	}

   	public function fetchIngredientsWithOutBrand(){
   		$ingr = Ingredient::where('status', 'A')->orderBy('name')->get();
   		if($ingr!=null){
			$response = [
                        'token'                      => null,
                        'result'                     => @$ingr,
                        'status'                     => "SUCCESS"
                    ];
   			return response()->json($response);
   		}
   		else{
   			$response = [
	            'token'            => null,
	            'status'            => "ERROR",
	                'error'         => [
	                    'code'      =>  '539',
	                    'message'   =>  config('customerror.539')
	                ]
	        ];
	        return response()->json($response);
   		}
   	}

   	public function fetchIngredientsWithBrand(Request $request){
   		$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(!empty(@$data->brand_id) && @$data->brand_id!=null){
        	$ingr = Ingredient::where([
	   			'brand_id'	=>	@$data->brand_id
	   		])->orderBy('name')->get();
	   		if(sizeof(@$ingr)>0){
				$response = [
	                        'token'                      => null,
	                        'result'                     => @$ingr,
	                        'status'                     => "SUCCESS"
	                    ];
	   			return response()->json($response);
	   		}
	   		else{
	   			$response = [
		            'token'            => null,
		            'status'            => "ERROR",
		                'error'         => [
		                    'code'      =>  '543',
		                    'message'   =>  config('customerror.543')
		                ]
		        ];
		        return response()->json($response);
	   		}
        }
        elseif(!empty(@$request->brand_id) && @$request->brand_id!="" && @$request->brand_id!=null){
        	$ingr = Ingredient::where([
	   			'brand_id'	=>	@$request->brand_id
	   		])->orderBy('name')->get();
	   		
	   		if(sizeof(@$ingr)>0){
				$response = [
	                        'token'                      => null,
	                        'result'                     => @$ingr,
	                        'status'                     => "SUCCESS"
	                    ];
	   			return response()->json($response);
	   		}
	   		else{
        	
	   			$response = [
		            'token'            => null,
		            'status'            => "ERROR",
		                'error'         => [
		                    'code'      =>  '543',
		                    'message'   =>  config('customerror.543')
		                ]
		        ];
		        return response()->json($response);
	   		}
        }
        else{
        	$response = [
	            'token'            => null,
	            'status'            => "ERROR",
	                'error'         => [
	                    'code'      =>  '541',
	                    'message'   =>  config('customerror.541')
	                ]
	        ];
	        return response()->json($response);
        }	
   	}


   	public function fetchOnlineFriendsList(Request $request){
   		$json = file_get_contents('php://input');
        $data = json_decode($json);
        
        if(!empty(@$data->token) && @$data->token!=null){
        
            $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        
            if(@$user==null) {
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            
            } else {

            	$friendsAll = array();

                $friends = UserToFriend::with('friendDetails')->where([
                	'user_id'			=>	@$user->id,
                	'request_status'	=>	'FRIEND'
                ])->orderBy('id', 'desc')->get();
                
                $friendArray = array();
                $ii = 0;

                if(sizeof(@$friends)>0) {

	                foreach($friends as $friend) {
	                	$userFriend = User::where(['id'=>$friend->friend_id])->first();
	                	
	                	if($userFriend) {

	                		if($userFriend->is_online == "Y" || $userFriend->is_online == "y") {

		                		$friendArray[$ii] = ['id'=>$friend->id, 
		                							'user_id'=>$friend->user_id, 
		                							'friend_id'=>$friend->friend_id, 
		                							'friend_name'=>$userFriend->first_name.' '.$userFriend->last_name, 
		                							'friend_image'=>$userFriend->profile_pic, 
		                							'request_status'=>$friend->request_status, 
		                							'created_at'=>$friend->created_at, 
		                							'updated_at'=>$friend->updated_at, 
		                							'is_online'=>$userFriend->is_online
		                						];
		                		$ii++;
		                	}
	                	}
	                }
	            }

	            $friends = UserToFriend::with('friendDetailsOpposite')->where([
                	'friend_id'			=>	@$user->id,
                	'request_status'	=>	'FRIEND'
                ])->orderBy('id', 'desc')->get();

	            $friendArray1 = array();

                if(sizeof(@$friends)>0) {

	            	foreach($friends as $friend) {

	                	$userFriend = User::where(['id'=>$friend->user_id])->first();
	                	if($userFriend) {

	                		if($userFriend->is_online == "Y" || $userFriend->is_online == "y") {

		                		$friendArray1[$ii] = ['id'=>$friend->id, 'user_id'=>$friend->friend_id, 'friend_id'=>$friend->user_id, 'friend_name'=>$userFriend->first_name.' '.$userFriend->last_name, 'friend_image'=>$userFriend->profile_pic, 'request_status'=>$friend->request_status, 'created_at'=>$friend->created_at, 'updated_at'=>$friend->updated_at, 'is_online'=>$userFriend->is_online];

		                		$ii++;
		                	}
	                	}
	                }
		        }

		        $friendsAll = array_merge($friendArray , $friendArray1);

                $response = [
	                    'token'                      => null,
	                    'result'                     => @$friendsAll,
	                    'status'                     => "SUCCESS"
	            ];
				return response()->json($response);
            }
        } else {
        	$response = [
	            'token'            => null,
	            'status'            => "ERROR",
	                'error'         => [
	                    'code'      =>  '541',
	                    'message'   =>  config('customerror.541')
	                ]
	        ];
	        return response()->json($response);
        }
    }

   	public function fetchFriendsList(Request $request){
   		$json = file_get_contents('php://input');
        $data = json_decode($json);
        
        if(!empty(@$data->token) && @$data->token!=null){
        
            $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        
            if(@$user==null) {
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            
            } else {

            	$friendsAll = array();

                $friends = UserToFriend::with('friendDetails')->where([
                	'user_id'			=>	@$user->id,
                	'request_status'	=>	'FRIEND'
                ])->orderBy('id', 'desc')->get();
                
                $friendArray = array();
                $ii = 0;

                if(sizeof(@$friends)>0) {

	                foreach($friends as $friend) {
	                	$userFriend = User::where(['id'=>$friend->friend_id])->first();
	                	
	                	if($userFriend) {

	                		$userChat = Chat::where(['sender_id'=>$friend->user_id])->orderBy('id','DESC')->limit(1)->first();
	                		if($userChat) {
	                			$chatMessage = $userChat->message;
	                		} else {
	                			$chatMessage = "";
	                		}

	                		$friendArray[$ii] = ['id'=>$friend->id, 
	                							'user_id'=>$friend->user_id, 
	                							'friend_id'=>$friend->friend_id, 
	                							'friend_name'=>$userFriend->first_name.' '.$userFriend->last_name, 
	                							'friend_image'=>$userFriend->profile_pic, 
	                							'request_status'=>$friend->request_status, 
	                							'created_at'=>$friend->created_at, 
	                							'updated_at'=>$friend->updated_at, 
	                							'is_online'=>$userFriend->is_online, 
	                							'last_message'=>$chatMessage
	                						];
	                	}
	                	$ii++;
	                }
	            }

	            $friends = UserToFriend::with('friendDetailsOpposite')->where([
                	'friend_id'			=>	@$user->id,
                	'request_status'	=>	'FRIEND'
                ])->orderBy('id', 'desc')->get();

	            $friendArray1 = array();

                if(sizeof(@$friends)>0) {

	            	foreach($friends as $friend) {

	                	$userFriend = User::where(['id'=>$friend->user_id])->first();
	                	if($userFriend) {

	                		$userChat = Chat::where(['sender_id'=>$friend->friend_id])->orderBy('id','DESC')->limit(1)->first();
	                		if($userChat) {
	                			$chatMessage = $userChat->message;
	                		} else {
	                			$chatMessage = "";
	                		}

	                		$friendArray1[$ii] = ['id'=>$friend->id, 'user_id'=>$friend->friend_id, 'friend_id'=>$friend->user_id, 'friend_name'=>$userFriend->first_name.' '.$userFriend->last_name, 'friend_image'=>$userFriend->profile_pic, 'request_status'=>$friend->request_status, 'created_at'=>$friend->created_at, 'updated_at'=>$friend->updated_at, 'is_online'=>$userFriend->is_online, 'last_message'=>$chatMessage];
	                	}
	                	$ii++;
	                }
		        }

		        $friendsAll = array_merge($friendArray , $friendArray1);

                $response = [
	                    'token'                      => null,
	                    'result'                     => @$friendsAll,
	                    'status'                     => "SUCCESS"
	            ];
				return response()->json($response);
            }
        
        } elseif(!empty(@$request->token) && @$request->token!=null) {

        	$friendsAll = array();

            $user = User::where([
                'user_token'    =>  @$request->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        
            if(@$user==null) {
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            
            } else {

                $friends = UserToFriend::with('friendDetails')->where([
                	'user_id'			=>	@$user->id,
                	'request_status'	=>	'FRIEND'
                ])->orderBy('id', 'desc')->get();
                
                $friendArray = array();
                $ii = 0;

                if(sizeof(@$friends)>0) {

	                foreach($friends as $friend) {
	                	$userFriend = User::where(['id'=>$friend->friend_id])->first();
	                	
	                	if($userFriend) {

	                		$userChat = Chat::where(['sender_id'=>$friend->user_id])->orderBy('id','DESC')->limit(1)->first();
	                		if($userChat) {
	                			$chatMessage = $userChat->message;
	                		} else {
	                			$chatMessage = "";
	                		}

	                		$friendArray[] = ['id'=>$friend->id, 'user_id'=>$friend->user_id, 'friend_id'=>$friend->friend_id, 'friend_name'=>$userFriend->first_name.' '.$userFriend->last_name, 'friend_image'=>$userFriend->profile_pic, 'request_status'=>$friend->request_status, 'created_at'=>$friend->created_at, 'updated_at'=>$friend->updated_at, 'is_online'=>$userFriend->is_online, 'last_message'=>$userChat->message];
	                	}
	                	$ii++;
	                }
	            }

	            $friends = UserToFriend::with('friendDetailsOpposite')->where([
                	'friend_id'			=>	@$user->id,
                	'request_status'	=>	'FRIEND'
                ])->orderBy('id', 'desc')->get();

	            $friendArray1 = array();

                if(sizeof(@$friends)>0) {

	            	foreach($friends as $friend) {

	                	$userFriend = User::where(['id'=>$friend->user_id])->first();
	                	if($userFriend) {

	                		$userChat = Chat::where(['sender_id'=>$friend->user_id])->orderBy('id','DESC')->limit(1)->first();
	                		if($userChat) {
	                			$chatMessage = $userChat->message;
	                		} else {
	                			$chatMessage = "";
	                		}

	                		$friendArray1[] = ['id'=>$friend->id, 'user_id'=>$friend->friend_id, 'friend_id'=>$friend->user_id, 'friend_name'=>$userFriend->first_name.' '.$userFriend->last_name, 'friend_image'=>$userFriend->profile_pic, 'request_status'=>$friend->request_status, 'created_at'=>$friend->created_at, 'updated_at'=>$friend->updated_at, 'is_online'=>$userFriend->is_online, 'last_message'=>$chatMessage];
	                	}
	                	$ii++;
	                }
		        }

		        $friendsAll = array_merge($friendArray , $friendArray1);

                $response = [
	                    'token'                      => null,
	                    'result'                     => @$friendsAll,
	                    'status'                     => "SUCCESS"
	            ];
				return response()->json($response);
            }
        }
   	}

   	public function fetchHashTags(){
   		$json = file_get_contents('php://input');
        $data = json_decode($json);

        if(empty(@$data->hash_tags) && @$data->hash_tags==null){
        	$response = [
	            'token'            => null,
	            'status'            => "ERROR",
	                'error'         => [
	                    'code'      =>  '550',
	                    'message'   =>  config('customerror.550')
	                ]
	        ];
	        return response()->json($response);
        }

        $post = Post::select('hash_tags')->where('hash_tags', 'like', '%'.@$data->hash_tags.'%')->groupBy('hash_tags')->orderBy('id', 'desc')->get();
        // if(sizeof($post)<=0){
    	$response = [
                'token'                      => null,
                'result'                     => @$post,
                'status'                     => "SUCCESS"
            ];
		return response()->json($response);
        // }
   	}

   	public function fetchRecipe(){
   		$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
        	$response = [
	            'token'            => null,
	            'status'            => "ERROR",
	                'error'         => [
	                    'code'      =>  '127',
	                    'message'   =>  config('customerror.127')
	                ]
	        ];
	        return response()->json($response);
        }

        $user = User::where([
        	'user_token'	=>	@$data->token,
        	'user_type'		=>	"U",
        	'status'		=>	"A"
        ])->first();
        if(@$user==null){
        	$response = [
	            'token'            => null,
	            'status'            => "ERROR",
	                'error'         => [
	                    'code'      =>  '110',
	                    'message'   =>  config('customerror.110')
	                ]
	        ];
	        return response()->json($response);
        }
        $cokkedIt = CookedIt::with('postDetails.postDeafultImage', 'recipeDetails')
        					->where('user_id', @$user->id)
        					->get();
        $response = [
                'token'                      => null,
                'result'                     => @$cokkedIt,
                'status'                     => "SUCCESS"
            ];
		return response()->json(@$response);
   	}

   	public function testImage(Request $request){
   		if($request->image){	
			@$img = str_random(12).'-'.time().'.'.@$request->image->getClientOriginalExtension();
			@$request->image->move("storage/app/public/testpath/", $img);
			 $response = [
                'status'                     => "SUCCESS"
            ];
			return response()->json(@$response);
   		}
   	}

   	public function fetchChatHistory() {
   		$json = file_get_contents('php://input');
        $data = json_decode($json);

        $history = Chat::where(['room_id'=>@$data->room_id])->orderBy('id','DESC')->get();
        if(sizeof(@$history)<=0) {
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '562',
                        'message'   =>  config('customerror.562')
                    ]
            ];
            return response()->json(@$response);
        }

        $response = [
                'token'                      => null,
                'result'                     => @$history,
                'status'                     => "SUCCESS"
            ];
		return response()->json(@$response);
   	}

   	public function saveChat() {
   		$json = file_get_contents('php://input');
        $data = json_decode($json);

        $chat = Chat::create([
					'sender_id'	=> @$data->sender_id,
					'reciver_id' =>	@$data->reciver_id,
					'message' => @$data->reciver_id,
					'read_status' => 0,
					'room_id' => @$data->room_id
				]);

        $response = [
                'token'                      => null,
                'result'                     => @$chat,
                'status'                     => "SUCCESS"
            ];
		return response()->json(@$response);
   	}

}
