<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\TagComment;
use App\Models\CommentLike;
use App\Models\TagDescription;
use App\Models\Post;
use App\Models\PostStep;
use App\Models\PostIngredients;
use App\Models\PostRecipie;
use App\Models\PostImages;
use App\Models\PostToCookedIt;
use App\Models\WannaCookedIt;
use App\Models\CookedIt;
use App\Models\PostComments;
use App\Models\PostLike;
use App\Models\PostToShare;
use App\Models\WannaVisit;
use App\Models\TagPost;
use App\Models\UserToBusiness;
use App\Models\UserToFriend;
use App\Models\UsageGamififaction;
use App\Models\PostGamififaction;
use App\Models\FreindGamififaction;
use App\Models\CookeditGamififaction;
use App\Models\VideopostGamififaction;
use App\Models\UserGame;
use App\Models\Notification;

class ProfileController extends Controller
{
    public function userProfile(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
        $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        if(@$user==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }
        @$totalPost = Post::where('user_id', @$user->id)->count();
        
        @$totalFriend = UserToFriend::where([
        	'friend_id'			=>	@$user->id,
        	'request_status'	=>	"FRIEND"
        ])->count();

        @$totalPendingFriendRequest = UserToFriend::where([
        	'friend_id'			=>	@$user->id,
        	'request_status'	=>	"WAITING"
        ])->count();

        @$posts = Post::with('postDeafultImage')->where('user_id', @$user->id)->orderBy('id','desc')->get();
        $response = [
	                    'token'                      	=> null,
	                    'error'                      	=> null,
	                    'result'                      	=> [
	                    	'userDetails'				=>	@$user,
	                    	'totalFriend'				=>	@$totalFriend,
	                    	'pendingFriendRequest'		=>	@$posts,
	                    	'myPost'					=>	@$posts
	                    ],
	                    'status'                     	=> "SUCCESS"
                    ];
   		return response()->json($response);
    }

    public function otherProfile() {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->user_id) && @$data->user_id==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '514',
                        'message'   =>  config('customerror.514')
                    ]
            ];
            return response()->json($response);
        }
        $user = User::where([
                'id'    =>  @$data->user_id,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        if(@$user==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }
        @$totalPost = Post::where('user_id', @$user->id)->count();
        
        @$totalFriend = UserToFriend::where([
            'friend_id'         =>  @$user->id,
            'request_status'    =>  "FRIEND"
        ])->count();

        @$totalPendingFriendRequest = UserToFriend::where([
            'friend_id'         =>  @$user->id,
            'request_status'    =>  "WAITING"
        ])->count();

        @$posts = Post::with('postDeafultImage')->where('user_id', @$user->id)->orderBy('id','desc')->get();
        $response = [
                        'token'                         => null,
                        'error'                         => null,
                        'result'                        => [
                            'userDetails'               =>  @$user,
                            'totalFriend'               =>  @$totalFriend,
                            'pendingFriendRequest'      =>  @$posts,
                            'myPost'                    =>  @$posts
                        ],
                        'status'                        => "SUCCESS"
                    ];
        return response()->json($response);
    }

    public function userFriendRequest(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
        $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        if(@$user==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }
        
        @$requests = UserToFriend::with('userDetails')
        ->where([
        	'friend_id'			=>	@$user->id,
        	'request_status'	=>	"WAITING"
        ])->get();
        $response = [
	                    'token'                      	=> null,
	                    'error'                      	=> null,
	                    'result'                      	=> @$requests,
	                    'status'                     	=> "SUCCESS"
                    ];
   		return response()->json($response);
    }

    public function acceptFriendrequest(){
		$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
        $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        if(@$user==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }
        if(empty(@$data->friend_id) && @$data->friend_id==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '551',
                        'message'   =>  config('customerror.551')
                    ]
            ];
            return response()->json($response);
        }
        
        @$totalFriend = UserToFriend::where([
        	'user_id'			=>	@$data->friend_id,
        	'friend_id'			=>	@$user->id,
        	'request_status'	=>	"FRIEND"
        ])->count();

        UserToFriend::where(['user_id'=>@$data->friend_id,'friend_id'=>@$user->id])->update([
        	'request_status'	=>	"FRIEND"
        ]);

         $response = [
	                    'token'                      	=> null,
	                    'error'                      	=> null,
	                    'status'                     	=> "SUCCESS"
                    ];
        return response()->json($response);
    }

    public function rejectFriendrequest() {
		$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
        $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

        if(@$user==null) {
        
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }
        if(empty(@$data->friend_id) && @$data->friend_id==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '551',
                        'message'   =>  config('customerror.551')
                    ]
            ];
            return response()->json($response);
        }
        
        @$totalFriend = UserToFriend::where([
        	'user_id'			=>	@$data->friend_id,
        	'friend_id'			=>	@$user->id,
        	'request_status'	=>	"FRIEND"
        ])->delete();

        $response = [
	                    'token'                      	=> null,
	                    'error'                      	=> null,
	                    'status'                     	=> "SUCCESS"
                    ];
        return response()->json($response);
    }

    public function myDetails(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
        $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        if(@$user==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }
        if(@$user->account_type=="PER"){
        	$response = [
	                    'token'                      	=> null,
	                    'error'                      	=> null,
	                    'result'                      	=> @$user,
	                    'status'                     	=> "SUCCESS"
                    ];
        	return response()->json($response);
        }
        else{
        	if(@$user->professional_account_type=="BL"){
        		$response = [
	                    'token'                      	=> null,
	                    'error'                      	=> null,
	                    'result'                      	=> @$user,
	                    'status'                     	=> "SUCCESS"
                    ];
        		return response()->json($response);
        	}

        	if(@$user->professional_account_type=="BU"){
        		$user->load('userBusinessAddress');
        		$response = [
	                    'token'                      	=> null,
	                    'error'                      	=> null,
	                    'result'                      	=> @$user,
	                    'status'                     	=> "SUCCESS"
                    ];
        		return response()->json($response);
        	}
        }
    }

    public function updateUserProfile(Request $request) {

    	if(empty(@$request->token) || @$request->token==null){
	   		$response = [
	            'token'            => null,
	            'status'            => "ERROR",
	                'error'         => [
	                    'code'      =>  '127',
	                    'message'   =>  config('customerror.127')
	                ]
	        ];
	        return response()->json($response);
	   	}

	   	$user = User::where([
                'user_token'    =>  @$request->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

        if(@$user==null) {
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }

        if(!empty(@$request->user_name) && @$request->user_name!=null) {

            if(@$request->user_name != $user->name) {

                $user = User::where([
                    'name'   =>  @$request->user_name,
                ])->first();

                if(@$user==null) {
                    $response = [
                        'token'            => null,
                            'error'         => [
                                'code'      =>  '561',
                                'message'   =>  config('customerror.561')
                            ]
                    ];
                    return response()->json(@$response);   
                }
            }
        }
		
		$ext = array('jpg', 'png', 'JPEG', 'jpeg', 'mkv', 'mp4', 'flv', 'mp4', 'm3u8', 'ts', '3gp', 'mov', 'avi', 'wmv');
		if(!empty(@$request->profile_pic) && @$request->profile_pic!=null){
			if(!in_array(@$request->profile_pic->getClientOriginalExtension(), @$ext)){
				$response = [
		            'token'            => null,
		            'status'            => "ERROR",
		                'error'         => [
		                    'code'      =>  '530',
		                    'message'   =>  config('customerror.530')
		                ]
	        		];
	        	return response()->json($response);
			}
		}

        if(!empty(@$request->cover_pic) && @$request->cover_pic!=null){
            if(!in_array(@$request->cover_pic->getClientOriginalExtension(), @$ext)){
                $response = [
                    'token'            => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '530',
                            'message'   =>  config('customerror.530')
                        ]
                    ];
                return response()->json($response);
            }
        }




		if(!empty(@$request->user_name)&&@$request->user_name!=null) {
			User::where('id', @$user->id)->update([
				'name'	=>	@$request->user_name
			]);
		}
		if(!empty(@$request->mobile)&&@$request->mobile!=null){
            User::where('id', @$user->id)->update([
                'mobile'    =>  @$request->mobile
            ]);
        }
        if(!empty(@$request->email)&&@$request->email!=null){
            User::where('id', @$user->id)->update([
                'email'    =>  @$request->email
            ]);
        }
        if(!empty(@$request->first_name)&&@$request->first_name!=null){
			User::where('id', @$user->id)->update([
				'first_name'	=>	@$request->first_name
			]);
		}
		if(!empty(@$request->last_name)&&@$request->last_name!=null){
			User::where('id', @$user->id)->update([
				'last_name'	=>	@$request->last_name
			]);
		}
		if(!empty(@$request->bio)&&@$request->bio!=null){
			User::where('id', @$user->id)->update([
				'bio'	=>	@$request->bio
			]);
		}
		if(!empty(@$request->profile_pic) && @$request->profile_pic!=null){
			if(@$user->profile_pic!=null && file_exists('storage/app/public/profile_images/'.@$user->profile_pic)){
				unlink('storage/app/public/profile_images/'.@$user->profile_pic);
			}
			@$img = @$user->id.'-'.str_random(8).time().'.'.@$request->profile_pic->getClientOriginalExtension();
	   						
	   		@$request->profile_pic->move("storage/app/public/profile_images/", @$img);
	   		@$request->profile_pic = @$img;

            User::where('id', @$user->id)->update([
				'profile_pic'	=>	@$request->profile_pic
			]);
		}
        if(!empty(@$request->cover_pic) && @$request->cover_pic!=null){
            if(@$user->cover_pic!=null && file_exists('storage/app/public/profile_images/'.@$user->cover_pic)){
                unlink('storage/app/public/profile_images/'.@$user->cover_pic);
            }
            @$img = @$user->id.'-'.str_random(8).time().'.'.@$request->cover_pic->getClientOriginalExtension();
                            
            @$request->cover_pic->move("storage/app/public/profile_images/", @$img);
            @$request->cover_pic = @$img;

            User::where('id', @$user->id)->update([
                'cover_pic'   =>  @$request->cover_pic
            ]);
        }

		if(!empty(@$request->new_password) && !empty(@$request->confirm_password)){
            if(@$request->new_password!=@$request->confirm_password){
	            $response = [
	                'token'             => null,
	                'status'            => "ERROR",
	                    'error'         => [
	                        'code'      =>  '118',
	                        'message'   =>  config('customerror.118')
	                    ]
	            ];
	            return response()->json($response);
	        }

	        User::where('id', @$user->id)->update([
				'password'	=>	\Hash::make(@$request->new_password)
			]);
        }
        
        @$user = User::where([
                'user_token'    =>  @$request->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

        @$response = [
                'token'                      	=> null,
                'error'                      	=> null,
                'result'                      	=> @$user,
                'status'                     	=> "SUCCESS"
            ];
		return response()->json(@$response);
    }


    public function updateUserStatus(Request $request) {
        
        if(empty(@$request->token) || @$request->token==null){
            $response = [
                'token'            => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }

        $user = User::where([
                'user_token'    =>  @$request->token,
                'user_type'     =>  'U'
            ])->first();

        if(@$user==null) {
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }

        if(@$user->status == 'I') {
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }

        if(@$request->type == "deactivate") {
            
            //if(!empty(@$request->status)&&@$request->status!=null) {
                User::where('id', @$user->id)->update([
                    'status'  =>  'I'
                ]);
            //}
        
            @$user = User::where('id', @$user->id)->first();

            @$response = [
                'token'                         => null,
                'error'                         => null,
                'status'                        => "SUCCESS"
            ];
        
        } else {

            User::where('id', @$user->id)->delete();
            TagComment::where('user_id', @$user->id)->delete();
            TagComment::where('friend_id', @$user->id)->delete();
            CommentLike::where('user_id', @$user->id)->delete();
            CookedIt::where('user_id', @$user->id)->delete();
            TagDescription::where('user_id', @$user->id)->delete();
            TagDescription::where('friend_id', @$user->id)->delete();
            TagDescription::where('friend_id', @$user->id)->delete();
            Post::where('user_id', @$user->id)->delete();
            PostComments::where('user_id', @$user->id)->delete();
            PostToCookedIt::where('user_id', @$user->id)->delete();
            PostLike::where('user_id', @$user->id)->delete();
            PostToShare::where('user_id', @$user->id)->delete();
            TagPost::where('user_id', @$user->id)->delete();
            UserToBusiness::where('user_id', @$user->id)->delete();
            UserToFriend::where('user_id', @$user->id)->delete();
            UserToFriend::where('friend_id', @$user->id)->delete();


            @$response = [
                'token'                         => null,
                'error'                         => null,
                'status'                        => "SUCCESS"
            ];
        }

        return response()->json(@$response);
    }


    public function isDeviceOnline(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
        $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        if(@$user==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }
        @$user->is_online = "Y";
        @$user->save();
        $response = [
	                    'token'                      	=> null,
	                    'error'                      	=> null,
	                    'status'                     	=> "SUCCESS"
                    ];
        return response()->json($response);
    }

    public function isDeviceOffline(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
        $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        if(@$user==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }
        @$user->is_online = "N";
        @$user->save();
        $response = [
	                    'token'                      	=> null,
	                    'error'                      	=> null,
	                    'status'                     	=> "SUCCESS"
                    ];
        return response()->json($response);
    }

    public function updateLastUsed(){
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
        $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        if(@$user==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }
        @$user->last_used = strtotime(date('d-m-y'));
        @$user->save();
        $response = [
                        'token'                         => null,
                        'error'                         => null,
                        'status'                        => "SUCCESS"
                    ];
        return response()->json($response);
    }

    public function getBadges() {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->user_id) && @$data->user_id==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '514',
                        'message'   =>  config('customerror.514')
                    ]
            ];
            return response()->json($response);
        }
        $user = User::where([
                'id'    =>  @$data->user_id,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        if(@$user==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }

        $resultArray = array();
        $flag = 'False';
        // have or not badges data
        $usergame = UserGame::where(["user_id"=>$user->id, "status"=>1])->first();
        if($usergame) {
            
            $getUsage = UsageGamififaction::where('level', $usergame->active)->first();
            if($getUsage) {
                $getActive = ["level"=>$usergame->active,"image"=>'usage_gamification_images/'.$getUsage->image];

                $gethigher = UsageGamififaction::orderBy('level','DESC')->first(); 
                if($gethigher->level == $usergame->active) {
                    $flag = "True";
                } else {
                    $flag = "False";
                }

            } else {
                $getActive = ["level"=>0,"image"=>""];
                $flag = "False";
            }

            $getUsage1 = PostGamififaction::where('level', $usergame->post)->first();
            if($getUsage1) {
                $getPost = ["level"=>$usergame->post,"image"=>'post_gamification_images/'.$getUsage1->image];

                $gethigher = PostGamififaction::orderBy('level','DESC')->first(); 
                if($gethigher->level == $usergame->post) {
                    $flag = "True";
                } else {
                    $flag = "False";
                }
            } else {
                $getPost = ["level"=>0,"image"=>""];
                $flag = "False";
            }

            $getUsage2 = FreindGamififaction::where('level', $usergame->friend)->first();
            if($getUsage2) {
                $getFriend = ["level"=>$usergame->friend,"image"=>'freind_gamification_images/'.$getUsage2->image];
                $gethigher = FreindGamififaction::orderBy('level','DESC')->first(); 
                if($gethigher->level == $usergame->friend) {
                    $flag = "True";
                } else {
                    $flag = "False";
                }
            } else {
                $getFriend = ["level"=>0,"image"=>""];
                $flag = "False";
            }

            $getUsage3 = CookeditGamififaction::where('level', $usergame->cooked)->first();
            if($getUsage3) {
                $getCooked = ["level"=>$usergame->cooked,"image"=>'cooked_it_gamification_images/'.$getUsage3->image];
                $gethigher = CookeditGamififaction::orderBy('level','DESC')->first(); 
                if($gethigher->level == $usergame->cooked) {
                    $flag = "True";
                } else {
                    $flag = "False";
                }
            } else {
                $getCooked = ["level"=>0,"image"=>""];
                $flag = "False";
            }

            $getUsage4 = VideopostGamififaction::where('level', $usergame->video)->first();
            if($getUsage4) {
                $getVideo = ["level"=>$usergame->video,"image"=>'video_gamification_images/'.$getUsage4->image];
                $gethigher = VideopostGamififaction::orderBy('level','DESC')->first(); 
                if($gethigher->level == $usergame->video) {
                    $flag = "True";
                } else {
                    $flag = "False";
                }
            } else {
                $getVideo = ["level"=>0,"image"=>""];
                $flag = "False";
            }
        } else {
            $getActive = ["level"=>0,"image"=>""];
            $getPost = ["level"=>0,"image"=>""];
            $getFriend = ["level"=>0,"image"=>""];
            $getCooked = ["level"=>0,"image"=>""];
            $getVideo = ["level"=>0,"image"=>""];
            $flag = "False";
        }

        if($flag == "True") {
            $win = ["level"=>0, "image"=>""];
            $resultArray = ["winner"=>$flag, "spoon"=>$win, "active_user"=>$getActive, "post"=>$getPost, "friend"=>$getFriend, "cooked"=>$getCooked, "video"=>$getVideo];
        } else {
            $win = ["level"=>0, "image"=>""];
            $resultArray = ["winner"=>$flag, "spoon"=>$win, "active_user"=>$getActive, "post"=>$getPost, "friend"=>$getFriend, "cooked"=>$getCooked, "video"=>$getVideo];
        }

        $response = [
                        'token'                         => null,
                        'result'                         => $resultArray,
                        'status'                        => "SUCCESS"
                    ];
        return response()->json($response);
    }

    public function read_notification() {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
        $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        if(@$user==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }
        $check = Notification::where(["user_id"=>$user->id, "id"=>$data->notify_id])->first();
        $check->status = 2;
        $check->save();
        $response = [
                        'token'                         => null,
                        'error'                         => null,
                        'status'                        => "SUCCESS"
                    ];
        return response()->json($response);
    }

    public function notification_badges() {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
        $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        if(@$user==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }
        $getNotify = NotificationBadge::where(["user_id"=>$user->id, "status"=>1])->get();
        
        $response = [
                        'token'                         => null,
                        'result'                         => $getNotify,
                        'status'                        => "SUCCESS"
                    ];
        return response()->json($response);
    }

    public function read_badges() {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
        $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
        if(@$user==null){
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);   
        }
        $check = NotificationBadge::where(["user_id"=>$user->id, "id"=>$data->notify_id])->first();
        $check->status = 2;
        $check->save();
        $response = [
                        'token'                         => null,
                        'error'                         => null,
                        'status'                        => "SUCCESS"
                    ];
        return response()->json($response);
    }

}
