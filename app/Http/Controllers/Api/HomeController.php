<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostStep;
use App\Models\PostIngredients;
use App\Models\PostRecipie;
use App\Models\PostImages;
use App\Models\PostToCookedIt;
use App\Models\UserToFriend;
use App\Models\WannaCookedIt;
use App\Models\CookedIt;
use App\Models\PostComments;
use App\Models\PostLike;
use App\Models\PostToShare;
use App\Models\WannaVisit;
use App\Models\TagPost;
use App\Models\TagDescription;
use App\Models\TagComment;
use App\Models\CommentLike;
use App\User;
class HomeController extends Controller
{
    
    public function searching(Request $request) {

        if(empty(@$request->keyword) || @$request->keyword==null){
            $response = [
                'token'            => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '563',
                        'message'   =>  config('customerror.563')
                    ]
            ];
            return response()->json($response);
        }

        $searchData = array();
        $post = Post::with('authorDetails', 'postDeafultImage', 'postAllImage')->where('description', 'like', '%'.@$request->keyword.'%')->orWhere('location', 'like', '%'.@$request->keyword.'%')->orWhere('hash_tags', 'like', '%'.@$request->keyword.'%')->groupBy('id')->orderBy('id','desc')->get();

        foreach(@$post as $pp) {
            if(isset($pp->postDeafultImage)) {
                $postImage = $pp->postDeafultImage->image;
            } else {
                $postImage = '';
            }
            if(strlen($pp->description) > 0) {
                $searchData[] = ["id"=>$pp->id, "image"=>$postImage, "value"=>substr($pp->description, 0,40), "type"=>"post"];
            }
        }

        $user = User::where('name', 'like', '%'.@$request->keyword.'%')->orWhere('first_name', 'like', '%'.@$request->keyword.'%')->orWhere('last_name', 'like', '%'.@$request->keyword.'%')->groupBy('id')->orderBy('id','desc')->get();

        foreach(@$user as $pp) {
            if(strlen($pp->profile_pic) > 0) {
                $postImage = $pp->profile_pic;
            } else {
                $postImage = '';
            }
            $searchData[] = ["id"=>$pp->id, "image"=>$postImage, "value"=>$pp->first_name.' '.$pp->last_name, "type"=>"user"];
        }

        if(sizeof(@$searchData)<=0) {
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '564',
                        'message'   =>  config('customerror.564')
                    ]
            ];
            return response()->json($response);
        }

        $response = [
                    'token'                      => null,
                    'result'                     => @$searchData,
                    'status'                     => "SUCCESS"
                ];
        return response()->json($response);

    }


    public function discoversearching(Request $request) {

        if(empty(@$request->keyword) || @$request->keyword==null){
            $response = [
                'token'            => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '563',
                        'message'   =>  config('customerror.563')
                    ]
            ];
            return response()->json($response);
        }

        $searchData1 = array();
        $searchData2 = array();
        $searchData3 = array();
        $searchData4 = array();

        $post = Post::with('authorDetails', 'postDeafultImage', 'postAllImage')->where('description', 'like', '%'.@$request->keyword.'%')->groupBy('id')->orderBy('id','desc')->get();
        foreach(@$post as $pp) {
            if(isset($pp->postDeafultImage)) {
                $postImage = $pp->postDeafultImage->image;
            } else {
                $postImage = '';
            }
            if(strlen($pp->description) > 0) {
                $searchData1[] = ["id"=>$pp->id, "image"=>$postImage, "value"=>substr($pp->description, 0,40), "type"=>"post"];
            }
        }

        $post = Post::with('authorDetails', 'postDeafultImage', 'postAllImage')->where('hash_tags', 'like', '%'.@$request->keyword.'%')->groupBy('id')->orderBy('id','desc')->get();
        foreach(@$post as $pp) {
            if(isset($pp->postDeafultImage)) {
                $postImage = $pp->postDeafultImage->image;
            } else {
                $postImage = '';
            }
            if(strlen($pp->description) > 0) {
                $searchData2[] = ["id"=>$pp->id, "image"=>$postImage, "value"=>substr($pp->description, 0,40), "type"=>"post"];
            }
        }


        $user = User::where('name', 'like', '%'.@$request->keyword.'%')->orWhere('first_name', 'like', '%'.@$request->keyword.'%')->orWhere('last_name', 'like', '%'.@$request->keyword.'%')->groupBy('id')->orderBy('id','desc')->get();
        foreach(@$user as $pp) {
            if(strlen($pp->profile_pic) > 0) {
                $postImage = $pp->profile_pic;
            } else {
                $postImage = '';
            }
            $searchData3[] = ["id"=>$pp->id, "image"=>$postImage, "value"=>$pp->first_name.' '.$pp->last_name, "type"=>"user"];
        }


        $post = Post::with('authorDetails', 'postDeafultImage', 'postAllImage')->where('description', 'like', '%'.@$request->keyword.'%')->orWhere('location', 'like', '%'.@$request->keyword.'%')->orWhere('hash_tags', 'like', '%'.@$request->keyword.'%')->groupBy('id')->orderBy('id','desc')->get();
        foreach(@$post as $pp) {
            if(isset($pp->postDeafultImage)) {
                $postImage = $pp->postDeafultImage->image;
            } else {
                $postImage = '';
            }
            if(strlen($pp->description) > 0) {
                $searchData4[] = ["id"=>$pp->id, "image"=>$postImage, "value"=>substr($pp->description, 0,40), "type"=>"post"];
            }
        }

        $resultData = ["account"=>$searchData3, "post"=>$searchData1, "tags"=>$searchData2, "place"=>$searchData4];

        $response = [
                    'token'                      => null,
                    'result'                     => @$resultData,
                    'status'                     => "SUCCESS"
                ];
        return response()->json($response);

    }


    public function fetchPost() {

    	$json = file_get_contents('php://input');
        $data = json_decode($json);

        if(!empty(@$data->token) && @$data->token!=null){
        
            if(empty(@$data->offset) && @$data->offset==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '544',
	                        'message'   =>  config('customerror.544')
	                    ]
	            ];
	            return response()->json($response);

            }

            if(empty(@$data->limit) && @$data->limit==null) {
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '545',
	                        'message'   =>  config('customerror.545')
	                    ]
	            ];
	            return response()->json($response);

            }

            $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

            if(@$user==null) {
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            
            } else {
                
                $post = Post::with('authorDetails', 'postDeafultImage', 'postAllImage')->skip(@$data->offset)->take(@$data->limit)->orderBy('id','desc')->get();
                
                if(sizeof(@$post)<=0) {
                	$response = [
		                'token'            => null,
		                    'error'         => [
		                        'code'      =>  '546',
		                        'message'   =>  config('customerror.546')
		                    ]
		            ];
		            return response()->json($response);

                }

                
                $pushedPost = $post;
                $i=0;
                foreach(@$post as $pp) {

                	if(@$user->id!=@$pp->user_id) {

                		if(@$pp->authorDetails->account_type=="PER") {

	                		if(@$pp->authorDetails->is_post_private=="Y") {

		                		$UserToFriend = UserToFriend::where([
			                		'user_id'			=>	@$user->id,
			                		'friend_id'			=>	@$pp->user_id
			                	])->first();
		                		
		                		// if($UserToFriend==null){
		                		// 	$UserToFriend = UserToFriend::where([
				                // 		'user_id'			=>	@$user->id,
				                // 		'friend_id'			=>	@$pp->user_id
				                // 	])->first();
		                		// }
		                		//dd($UserToFriend->request_status);
			                	if(@$UserToFriend->request_status=="FRIEND"){

			                		$post[@$i]['is_my_post']= "N";
			                		$post[@$i]['is_visible_by_me']= "Y";
									$post[@$i]['viewer_is_friend']= "Y";
			                	}
			                	
			                	if(@$UserToFriend->request_status=="WAITING"){
			                		$post[@$i]['is_my_post']= "N";
			                		$post[@$i]['viewer_is_friend']= "W";
			                		$post[@$i]['is_visible_by_me']= "N";
			                	}
			                	
			                	if(@$UserToFriend->request_status=="REJECTED") {
			                		$post[@$i]['is_my_post']= "N";
			                		$post[@$i]['viewer_is_friend']= "N";
			                		$post[@$i]['is_visible_by_me']= "N";
			                	}

	                		} else {

                                $UserToFriend = UserToFriend::where([
			                		'user_id'			=>	@$user->id,
			                		'friend_id'			=>	@$pp->user_id
			                	])->first();

	                			 
			                	if(@$UserToFriend->request_status=="FRIEND"){

			                		$post[@$i]['is_my_post']= "N";
			                		$post[@$i]['is_visible_by_me']= "Y";
									$post[@$i]['viewer_is_friend']= "Y";

			                	}
			                	
			                	if(@$UserToFriend->request_status=="WAITING"){
			                		$post[@$i]['is_my_post']= "N";
			                		$post[@$i]['viewer_is_friend']= "W";
			                		$post[@$i]['is_visible_by_me']= "Y";
			                	}
			                	
			                	if(@$UserToFriend->request_status=="REJECTED"){
			                		$post[@$i]['is_my_post']= "N";
			                		$post[@$i]['viewer_is_friend']= "N";
			                		$post[@$i]['is_visible_by_me']= "Y";
			                	}
	                			$post[@$i]['is_visible_by_me']= "Y";
	                		}

                		} else {

                			if(@$pp->authorDetails->is_post_private=="Y") {

		                		$UserToFriend = UserToFriend::where([
			                		'user_id'			=>	@$pp->user_id,
			                		'friend_id'			=>	@$user->id
			                	])->first();
		                		
			                	if(@$UserToFriend->request_status=="FOLLOWING") {

			                		$post[@$i]['is_my_post']= "N";
			                		$post[@$i]['is_visible_by_me']= "Y";
									$post[@$i]['viewer_is_friend']= "Y";

			                	}

	                		} else {
	                			$UserToFriend = UserToFriend::where([
			                		'user_id'			=>	@$pp->user_id,
			                		'friend_id'			=>	@$user->id
			                	])->first();
		                		
		                		// if($UserToFriend==null){
		                		// 	$UserToFriend = UserToFriend::where([
				                // 		'user_id'			=>	@$user->id,
				                // 		'friend_id'			=>	@$pp->user_id
				                // 	])->first();
		                		// }

			                	if(@$UserToFriend->request_status=="FOLLOWING"){

			                		$post[@$i]['is_my_post']= "N";
			                		$post[@$i]['is_visible_by_me']= "Y";
									$post[@$i]['viewer_is_friend']= "Y";
			                	}
	                		}
                		}
                	}
                	else {
                		$post[@$i]['is_my_post']= "Y";
                		$post[@$i]['is_visible_by_me']= "Y";
                	}

                    // Post tags

                    $getTags = TagPost::where(['post_id'=>@$pp->id])->get();
                    $taging = array();
                    if($getTags) {
                        foreach($getTags as $getTag) {
                            $user = User::where(['id'=>@$getTag->friend_id])->first();

                            $taging[] = ["user_id"=>$getTag->friend_id, "name"=>$user->first_name.' '.$user->last_name];
                        }
                    } else {
                        $taging = [];   
                    }
                    @$post[@$i]['tags'] =  $taging;

                    // Description tags

                    $getDescTags = TagDescription::where(['post_id'=>@$pp->id])->get();
                    $tagingDesc = array();
                    if($getDescTags) {
                        foreach($getDescTags as $getDescTag) {
                            $user = User::where(['id'=>@$getDescTag->friend_id])->first();
                            $tagingDesc[] = ["user_id"=>$getDescTag->friend_id, "name"=>$user->first_name.' '.$user->last_name];
                        }
                    } else {
                        $tagingDesc = [];   
                    }
                    @$post[@$i]['descTags'] =  $tagingDesc;

                    // Comment tags

                    $getCommentTags = TagComment::where(['post_id'=>@$pp->id])->get();
                    $tagingComment = array();
                    if($getCommentTags) {
                        foreach($getCommentTags as $getCommentTag) {
                            $user = User::where(['id'=>@$getCommentTag->friend_id])->first();
                            $tagingComment[] = ["user_id"=>$getCommentTag->friend_id, "name"=>$user->first_name.' '.$user->last_name];
                        }
                    } else {
                        $tagingComment = [];   
                    }
                    @$post[@$i]['commentTags'] =  $tagingComment;

                    // Post Comment

                    $getComments = PostComments::where(['post_id'=>@$pp->id])->get();
                    $commenting = array();
                    if($getComments) {
                        foreach($getComments as $getComment) {
                            $user = User::where(['id'=>@$getComment->user_id])->first();

                            $commenting[] = ["id"=>$getComment->id,"comment"=>$getComment->comment,"date"=>date("d-m-Y H:i:s",strtotime($getComment->created_at)), "user_id"=>$getComment->user_id, "name"=>$user->first_name.' '.$user->last_name];
                        }
                    } else {
                        $commenting = [];
                    }
                    @$post[@$i]['comments'] =  $commenting;

                	$postLike = PostLike::where([
                		'post_id'	=>	@$pp->id,
                		'user_id'	=>	@$user->id,
                		'is_liked'	=>	'Y'
                	])->first();
                	
                	@$postLike!=null ? @$post[@$i]['is_liked_by_me']= "Y": @$post[@$i]['is_liked_by_me']= "N";
                	$i++;
                }
                $response = [
	                        'token'                      => null,
	                        'result'                     => @$post,
	                        'status'                     => "SUCCESS"
	                    ];
	   			return response()->json($response);
            }
        }
        else{
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }

    public function fetchUserPost() {

        $json = file_get_contents('php://input');
        $data = json_decode($json);

        if(!empty(@$data->token) && @$data->token!=null){
        
            if(empty(@$data->offset) && @$data->offset==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '544',
                            'message'   =>  config('customerror.544')
                        ]
                ];
                return response()->json($response);

            }

            if(empty(@$data->limit) && @$data->limit==null) {
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '545',
                            'message'   =>  config('customerror.545')
                        ]
                ];
                return response()->json($response);
            }

            $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

            if(@$user==null) {
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            
            } else {
                
                $post = Post::with('authorDetails', 'postDeafultImage', 'postAllImage')->where(["user_id"=>@$user->id])->skip(@$data->offset)->take(@$data->limit)->orderBy('id','desc')->get();
                
                if(sizeof(@$post)<=0) {
                    $response = [
                        'token'            => null,
                            'error'         => [
                                'code'      =>  '546',
                                'message'   =>  config('customerror.546')
                            ]
                    ];
                    return response()->json($response);
                }

                $pushedPost = $post;
                $i=0;
                foreach(@$post as $pp) {

                    if(@$user->id!=@$pp->user_id) {

                        if(@$pp->authorDetails->account_type=="PER") {

                            if(@$pp->authorDetails->is_post_private=="Y") {

                                $UserToFriend = UserToFriend::where([
                                    'user_id'           =>  @$user->id,
                                    'friend_id'         =>  @$pp->user_id
                                ])->first();
                                
                                if(@$UserToFriend->request_status=="FRIEND"){

                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                    $post[@$i]['viewer_is_friend']= "Y";
                                }
                                
                                if(@$UserToFriend->request_status=="WAITING"){
                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['viewer_is_friend']= "W";
                                    $post[@$i]['is_visible_by_me']= "N";
                                }
                                
                                if(@$UserToFriend->request_status=="REJECTED") {
                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['viewer_is_friend']= "N";
                                    $post[@$i]['is_visible_by_me']= "N";
                                }

                            } else {

                                $UserToFriend = UserToFriend::where([
                                    'user_id'           =>  @$user->id,
                                    'friend_id'         =>  @$pp->user_id
                                ])->first();

                                 
                                if(@$UserToFriend->request_status=="FRIEND"){

                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                    $post[@$i]['viewer_is_friend']= "Y";

                                }
                                
                                if(@$UserToFriend->request_status=="WAITING"){
                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['viewer_is_friend']= "W";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                }
                                
                                if(@$UserToFriend->request_status=="REJECTED"){
                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['viewer_is_friend']= "N";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                }
                                $post[@$i]['is_visible_by_me']= "Y";
                            }

                        } else {

                            if(@$pp->authorDetails->is_post_private=="Y") {

                                $UserToFriend = UserToFriend::where([
                                    'user_id'           =>  @$pp->user_id,
                                    'friend_id'         =>  @$user->id
                                ])->first();
                                
                                if(@$UserToFriend->request_status=="FOLLOWING") {

                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                    $post[@$i]['viewer_is_friend']= "Y";

                                }

                            } else {
                                $UserToFriend = UserToFriend::where([
                                    'user_id'           =>  @$pp->user_id,
                                    'friend_id'         =>  @$user->id
                                ])->first();

                                if(@$UserToFriend->request_status=="FOLLOWING"){

                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                    $post[@$i]['viewer_is_friend']= "Y";
                                }
                            }
                        }
                    }
                    else {
                        $post[@$i]['is_my_post']= "Y";
                        $post[@$i]['is_visible_by_me']= "Y";
                    }

                    // Post tags

                    $getTags = TagPost::where(['post_id'=>@$pp->id])->get();
                    $taging = array();
                    if($getTags) {
                        foreach($getTags as $getTag) {
                            $user = User::where(['id'=>@$getTag->friend_id])->first();
                            if($user) {
                                $taging[] = ["user_id"=>$getTag->friend_id, "name"=>$user->first_name.' '.$user->last_name];
                            }
                        }
                    } else {
                        $taging = [];   
                    }
                    @$post[@$i]['tags'] =  $taging;

                    // Description tags

                    $getDescTags = TagDescription::where(['post_id'=>@$pp->id])->get();
                    $tagingDesc = array();
                    if($getDescTags) {
                        foreach($getDescTags as $getDescTag) {
                            $user = User::where(['id'=>@$getDescTag->friend_id])->first();
                            $tagingDesc[] = ["user_id"=>$getDescTag->friend_id, "name"=>$user->first_name.' '.$user->last_name];
                        }
                    } else {
                        $tagingDesc = [];   
                    }
                    @$post[@$i]['descTags'] =  $tagingDesc;

                    // Comment tags

                    $getCommentTags = TagComment::where(['post_id'=>@$pp->id])->get();
                    $tagingComment = array();
                    if($getCommentTags) {
                        foreach($getCommentTags as $getCommentTag) {
                            $user = User::where(['id'=>@$getCommentTag->friend_id])->first();
                            $tagingComment[] = ["user_id"=>$getCommentTag->friend_id, "name"=>$user->first_name.' '.$user->last_name];
                        }
                    } else {
                        $tagingComment = [];   
                    }
                    @$post[@$i]['commentTags'] =  $tagingComment;

                    // Post Comment

                    $getComments = PostComments::where(['post_id'=>@$pp->id])->get();
                    $commenting = array();
                    if($getComments) {
                        foreach($getComments as $getComment) {
                            $user = User::where(['id'=>@$getComment->user_id])->first();
                            if($user) {
                                $commenting[] = ["id"=>$getComment->id,"comment"=>$getComment->comment,"date"=>date("d-m-Y H:i:s",strtotime($getComment->created_at)), "user_id"=>$getComment->user_id, "name"=>$user->first_name.' '.$user->last_name];
                            }
                        }
                    } else {
                        $commenting = [];
                    }
                    @$post[@$i]['comments'] =  $commenting;

                    $postLike = PostLike::where([
                        'post_id'   =>  @$pp->id,
                        'user_id'   =>  @$user->id,
                        'is_liked'  =>  'Y'
                    ])->first();
                    
                    @$postLike!=null ? @$post[@$i]['is_liked_by_me']= "Y": @$post[@$i]['is_liked_by_me']= "N";
                    $i++;
                }
                $response = [
                            'token'                      => null,
                            'result'                     => @$post,
                            'status'                     => "SUCCESS"
                        ];
                return response()->json($response);
            }
        }
        else{
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }

    public function putLike(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(!empty(@$data->token) && @$data->token!=null){

        	$user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

            if(@$user==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            }

            if(empty(@$data->post_id) && @$data->post_id==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '547',
	                        'message'   =>  config('customerror.547')
	                    ]
	            ];
	            return response()->json($response);

            }

            $fndPost = Post::where('id', @$data->post_id)->first();
            if(@$fndPost==null){
            	$response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '546',
                            'message'   =>  config('customerror.546')
                        ]
                ];
                return response()->json(@$response);
            }
            $postLike = PostLike::where([
		        		'post_id'	=>	@$data->post_id,
		        		'user_id'	=>	@$user->id
		        	])->first();
		    
        	if(@$postLike!=null){
	        	
	        	@$postLike->is_liked = @$postLike->is_liked== "Y" ? @$postLike->is_liked="N": @$postLike->is_liked="Y";
	            
	        	@$postLike->save();
        	}
        	else{
        		PostLike::create([
		        		'post_id'	=>	@$data->post_id,
		        		'user_id'	=>	@$user->id,
		        		'is_liked'	=>	'Y'
		        	]);
        	}
        	$postLike = PostLike::where([
		        		'post_id'	=>	@$data->post_id,
		        		'user_id'	=>	@$user->id
		        	])->first();

        	if(@$postLike->is_liked=="Y"){
        		$updatePost=Post::where('id', @$data->post_id)->first();
        		@$updatePost->total_like=@$updatePost->total_like+1;
        		@$updatePost->save();
        	}
        	else{
        		@$updatePost=Post::where('id', @$data->post_id)->first();
        		@$updatePost->total_like= @$updatePost->total_like >0 ? @$updatePost->total_like-1: 0;
        		@$updatePost->save();
        	}
        	$post = Post::where('id', @$data->post_id)->first();

        	$response = [
                        'token'                      => null,
                        'status'                     => "SUCCESS"
                    ];
   			return response()->json($response);

        }
        else{
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }


    public function fetchComment() {

        $json = file_get_contents('php://input');
        $data = json_decode($json);

        if(!empty(@$data->token) && @$data->token!=null) {

            $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

            if(@$user==null) {
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);

            } else {
                
                $post = Post::where(['id'=>@$data->post_id])->orderBy('id','desc')->get();
                if(sizeof(@$post)<=0) {
                    $response = [
                        'token'            => null,
                            'error'         => [
                                'code'      =>  '546',
                                'message'   =>  config('customerror.546')
                            ]
                    ];
                    return response()->json($response);
                }

                $comment = PostComments::where(['post_id'=>@$data->post_id])->orderBy('id','desc')->get();
                if(sizeof(@$comment)<=0) {
                    $response = [
                        'token'            => null,
                            'error'         => [
                                'code'      =>  '560',
                                'message'   =>  config('customerror.560')
                            ]
                    ];
                    return response()->json($response);
                }
                
                $i=0;
                $commenting = array();

                foreach(@$comment as $getComment) {

                    $commentUser = User::where(['id'=>@$getComment->user_id])->first();

                    $commentLike = CommentLike::where([
                        'comment_id'   =>  @$getComment->id,
                        'user_id'   =>  @$user->id,
                        'is_liked'  =>  'Y'
                    ])->first();

                    if($commentLike) {
                        $is_liked_by_me = $commentLike->is_liked;
                    } else {
                        $is_liked_by_me = "N";
                    }

                    $commenting[] = ["id"=>$getComment->id, "comment"=>$getComment->comment, "date"=>date("Y-m-d H:i:s",strtotime($getComment->created_at)), "total_like"=>$getComment->total_like, "user_id"=>$getComment->user_id, "name"=>$commentUser->first_name.' '.$commentUser->last_name, "is_liked_by_me"=>$is_liked_by_me];

                    $i++;
                }

                @$post =  $commenting;

                $response = [
                            'token'                      => null,
                            'result'                     => @$post,
                            'status'                     => "SUCCESS"
                        ];
                return response()->json($response);
            }
        }
        else{
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }


    public function putComment(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(!empty(@$data->token) && @$data->token!=null){
        	$user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

            if(@$user==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            }
        	
        	if(empty(@$data->post_id) && @$data->post_id==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '547',
	                        'message'   =>  config('customerror.547')
	                    ]
	            ];
	            return response()->json($response);
            }

            if(empty(@$data->post_comment) && @$data->post_comment==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '548',
	                        'message'   =>  config('customerror.548')
	                    ]
	            ];
	            return response()->json($response);
            }

            $fndPost = Post::where('id', @$data->post_id)->first();
            if(@$fndPost==null){
            	$response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '546',
                            'message'   =>  config('customerror.546')
                        ]
                ];
                return response()->json(@$response);
            }

            // $isCommented =PostComments::where([
            // 	"post_id"	=>	@$data->post_id,
            // 	"user_id"	=>	@$user->id
            // ])->first();
            // if(@$isCommented==null){
	            $enterComment = PostComments::create([
	            	"post_id"	=>	@$data->post_id,
	            	"user_id"	=>	@$user->id,
	            	"comment"	=>	@$data->post_comment
	            ]);
	            Post::where('id', @$data->post_id)->update([
	            	'last_comment'	=>	@$data->post_comment
	            ]);
	            @$updatePost=Post::where('id', @$data->post_id)->first();
	    		@$updatePost->total_comments=@$updatePost->total_comments+1;
	    		@$updatePost->save();
            // }
            // else{
            // 	$enterComment = PostComments::where([
            // 		'post_id'	=>	@$data->post_id,
            // 		"user_id"	=>	@$user->id
            // 	])->update([
	           //  	"post_id"	=>	@$data->post_id,
	           //  	"user_id"	=>	@$user->id,
	           //  	"comment"	=>	@$data->post_comment
	           //  ]);
            // }
            // $post = Post::where('id', @$data->post_id)->first();

        	$response = [
                        'id'                         => $enterComment->id,
                        'token'                      => null,
                        'status'                     => "SUCCESS"
                    ];
   			return response()->json($response);
        }
        else{
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }


    public function putCommentLike(){
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        if(!empty(@$data->token) && @$data->token!=null){

            $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

            if(@$user==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            }

            if(empty(@$data->comment_id) && @$data->comment_id==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '559',
                            'message'   =>  config('customerror.559')
                        ]
                ];
                return response()->json($response);
            }

            $fndPost = PostComments::where('id', @$data->comment_id)->first();
            if(@$fndPost==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '560',
                            'message'   =>  config('customerror.560')
                        ]
                ];
                return response()->json(@$response);
            }
            $commentLike = CommentLike::where([
                        'comment_id'   =>  @$data->comment_id,
                        'user_id'   =>  @$user->id
                    ])->first();
            
            if(@$commentLike!=null){
                
                @$commentLike->is_liked = @$commentLike->is_liked== "Y" ? @$commentLike->is_liked="N": @$commentLike->is_liked="Y";
                @$commentLike->save();
            }
            else{
                CommentLike::create([
                        'comment_id'   =>  @$data->comment_id,
                        'user_id'   =>  @$user->id,
                        'is_liked'  =>  'Y'
                    ]);
            }
            $commentLike = CommentLike::where([
                        'comment_id'   =>  @$data->comment_id,
                        'user_id'   =>  @$user->id
                    ])->first();

            if(@$commentLike->is_liked=="Y") {

                $updateComment = PostComments::where('id', @$data->comment_id)->first();
                @$updateComment->total_like=@$updateComment->total_like+1;
                @$updateComment->save();
            }
            else {
                @$updateComment = PostComments::where('id', @$data->comment_id)->first();
                @$updateComment->total_like= @$updateComment->total_like >0 ? @$updateComment->total_like-1: 0;
                @$updateComment->save();
            }
            $comment = PostComments::where('id', @$data->comment_id)->first();

            $response = [
                        'token'                      => null,
                        'status'                     => "SUCCESS"
                    ];
            return response()->json($response);

        }
        else{
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }

    public function addwannaCookedIt() {

    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(!empty(@$data->token) && @$data->token!=null){
        	$user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

            if(@$user==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            }
        	
        	if(empty(@$data->post_id) && @$data->post_id==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '547',
	                        'message'   =>  config('customerror.547')
	                    ]
	            ];
	            return response()->json($response);
            }
            
            $fndPost = Post::where('id', @$data->post_id)->first();
            if(@$fndPost==null){
            	$response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '546',
                            'message'   =>  config('customerror.546')
                        ]
                ];
                return response()->json(@$response);
            }
            $recipeId = PostRecipie::where([
            	'post_id'	=>	@$data->post_id
            ])->first();

            if($recipeId) {
                $cookedIt = CookedIt::where([
                	"post_id"		=>	@$data->post_id,
                	"recipie_id"	=>	@$recipeId->id,
                	"user_id"		=>	@$user->id
                ])->first();

                if(@$cookedIt!=null){
    	   			$response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '549',
                            'message'   =>  config('customerror.549')
                        ]
                	];
                	return response()->json($response);
                }
                else{
                	$cookedIt = CookedIt::create([
                		"post_id"		=>	@$data->post_id,
    	            	"recipie_id"	=>	@$recipeId->id,
    	            	"user_id"		=>	@$user->id
                	]);
                	
                	$updatePost = Post::where('id', @$data->post_id)->first();
            		@$updatePost->total_cooked_it=@$updatePost->total_cooked_it+1;
            		@$updatePost->save();

                	// $post = Post::where('id', @$data->post_id)->first();
    	        	$response = [
    	                        'token'                      => null,
    	                        // 'result'                     => @$post,
    	                        'status'                     => "SUCCESS"
    	                    ];
    	   			return response()->json($response);
                }
            } else {
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '537',
                            'message'   =>  config('customerror.537')
                        ]
                ];
                return response()->json(@$response);
            }
        }
        else{
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }


    public function getwannaCookedIt() {

        $json = file_get_contents('php://input');
        $data = json_decode($json);

        if(!empty(@$data->token) && @$data->token!=null) {

            $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

            if(@$user==null) {
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            
            } else {

                $postidArr = array();
                $cookedIts = CookedIt::where(["user_id"=>@$user->id])->get();
                if(sizeof(@$cookedIts)<=0) {
                    $response = [
                        'token'            => null,
                            'error'         => [
                                'code'      =>  '546',
                                'message'   =>  config('customerror.546')
                            ]
                    ];
                    return response()->json($response);
                }

                foreach($cookedIts as $cookedIt) {
                    $postidArr[] = $cookedIt->post_id;
                }
                
                $post = Post::with('authorDetails', 'postDeafultImage', 'postAllImage')->whereIn('id', $postidArr)->orderBy('id','desc')->get();
                
                if(sizeof(@$post)<=0) {
                    $response = [
                        'token'            => null,
                            'error'         => [
                                'code'      =>  '546',
                                'message'   =>  config('customerror.546')
                            ]
                    ];
                    return response()->json($response);
                }

                
                $pushedPost = $post;
                $i=0;
                foreach(@$post as $pp) {

                    if(@$user->id!=@$pp->user_id) {

                        if(@$pp->authorDetails->account_type=="PER") {

                            if(@$pp->authorDetails->is_post_private=="Y") {

                                $UserToFriend = UserToFriend::where([
                                    'user_id'           =>  @$user->id,
                                    'friend_id'         =>  @$pp->user_id
                                ])->first();
                                
                                // if($UserToFriend==null){
                                //  $UserToFriend = UserToFriend::where([
                                //      'user_id'           =>  @$user->id,
                                //      'friend_id'         =>  @$pp->user_id
                                //  ])->first();
                                // }
                                //dd($UserToFriend->request_status);
                                if(@$UserToFriend->request_status=="FRIEND"){

                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                    $post[@$i]['viewer_is_friend']= "Y";
                                }
                                
                                if(@$UserToFriend->request_status=="WAITING"){
                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['viewer_is_friend']= "W";
                                    $post[@$i]['is_visible_by_me']= "N";
                                }
                                
                                if(@$UserToFriend->request_status=="REJECTED") {
                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['viewer_is_friend']= "N";
                                    $post[@$i]['is_visible_by_me']= "N";
                                }

                            } else {

                                $UserToFriend = UserToFriend::where([
                                    'user_id'           =>  @$user->id,
                                    'friend_id'         =>  @$pp->user_id
                                ])->first();

                                 
                                if(@$UserToFriend->request_status=="FRIEND"){

                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                    $post[@$i]['viewer_is_friend']= "Y";

                                }
                                
                                if(@$UserToFriend->request_status=="WAITING"){
                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['viewer_is_friend']= "W";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                }
                                
                                if(@$UserToFriend->request_status=="REJECTED"){
                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['viewer_is_friend']= "N";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                }
                                $post[@$i]['is_visible_by_me']= "Y";
                            }

                        } else {

                            if(@$pp->authorDetails->is_post_private=="Y") {

                                $UserToFriend = UserToFriend::where([
                                    'user_id'           =>  @$pp->user_id,
                                    'friend_id'         =>  @$user->id
                                ])->first();
                                
                                if(@$UserToFriend->request_status=="FOLLOWING") {

                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                    $post[@$i]['viewer_is_friend']= "Y";

                                }

                            } else {
                                $UserToFriend = UserToFriend::where([
                                    'user_id'           =>  @$pp->user_id,
                                    'friend_id'         =>  @$user->id
                                ])->first();

                                if(@$UserToFriend->request_status=="FOLLOWING"){

                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                    $post[@$i]['viewer_is_friend']= "Y";
                                }
                            }
                        }
                    }
                    else {
                        $post[@$i]['is_my_post']= "Y";
                        $post[@$i]['is_visible_by_me']= "Y";
                    }

                    // Post tags

                    $getTags = TagPost::where(['post_id'=>@$pp->id])->get();
                    $taging = array();
                    if($getTags) {
                        foreach($getTags as $getTag) {
                            $user = User::where(['id'=>@$getTag->friend_id])->first();

                            $taging[] = ["user_id"=>$getTag->friend_id, "name"=>$user->first_name.' '.$user->last_name];
                        }
                    } else {
                        $taging = [];   
                    }
                    @$post[@$i]['tags'] =  $taging;

                    // Description tags

                    $getDescTags = TagDescription::where(['post_id'=>@$pp->id])->get();
                    $tagingDesc = array();
                    if($getDescTags) {
                        foreach($getDescTags as $getDescTag) {
                            $user = User::where(['id'=>@$getDescTag->friend_id])->first();
                            $tagingDesc[] = ["user_id"=>$getDescTag->friend_id, "name"=>$user->first_name.' '.$user->last_name];
                        }
                    } else {
                        $tagingDesc = [];   
                    }
                    @$post[@$i]['descTags'] =  $tagingDesc;

                    // Comment tags

                    $getCommentTags = TagComment::where(['post_id'=>@$pp->id])->get();
                    $tagingComment = array();
                    if($getCommentTags) {
                        foreach($getCommentTags as $getCommentTag) {
                            $user = User::where(['id'=>@$getCommentTag->friend_id])->first();
                            $tagingComment[] = ["user_id"=>$getCommentTag->friend_id, "name"=>$user->first_name.' '.$user->last_name];
                        }
                    } else {
                        $tagingComment = [];   
                    }
                    @$post[@$i]['commentTags'] =  $tagingComment;

                    // Post Comment

                    $getComments = PostComments::where(['post_id'=>@$pp->id])->get();
                    $commenting = array();
                    if($getComments) {
                        foreach($getComments as $getComment) {
                            $user = User::where(['id'=>@$getComment->user_id])->first();

                            $commenting[] = ["id"=>$getComment->id,"comment"=>$getComment->comment,"date"=>date("d-m-Y H:i:s",strtotime($getComment->created_at)), "user_id"=>$getComment->user_id, "name"=>$user->first_name.' '.$user->last_name];
                        }
                    } else {
                        $commenting = [];
                    }
                    @$post[@$i]['comments'] =  $commenting;

                    $postLike = PostLike::where([
                        'post_id'   =>  @$pp->id,
                        'user_id'   =>  @$user->id,
                        'is_liked'  =>  'Y'
                    ])->first();
                    
                    @$postLike!=null ? @$post[@$i]['is_liked_by_me']= "Y": @$post[@$i]['is_liked_by_me']= "N";
                    $i++;
                }
                $response = [
                            'token'                      => null,
                            'result'                     => @$post,
                            'status'                     => "SUCCESS"
                        ];
                return response()->json($response);
            }
        }
        else{
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }


    public function sendFriendRequest(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }

        if(empty(@$data->friend_id) && @$data->friend_id==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '551',
                        'message'   =>  config('customerror.551')
                    ]
            ];
            return response()->json($response);
        }

        $user = User::where('user_token', @$data->token)->first();
        if($user==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);
        }

        $userFriend = UserToFriend::where([
        	'user_id'			=>	@$user->id,
        	'friend_id'			=>	@$data->friend_id,
        	'request_status'	=>	'WAITING'
        ])->first();
        
        if(@$userFriend!=null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '552',
                        'message'   =>  config('customerror.552')
                    ]
            ];
            return response()->json(@$response);
        
        } else {

        	$userFriend = UserToFriend::create([
        		'user_id'			=>	@$user->id,
	        	'friend_id'			=>	@$data->friend_id,
	        	'request_status'	=>	'WAITING'
        	]);
	    	$response = [
                        'token'                      => null,
                        'status'                     => "SUCCESS"
                    ];
   			return response()->json($response);
        }
    }

    public function followProfile() {
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }

        if(empty(@$data->friend_id) && @$data->friend_id==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '551',
                        'message'   =>  config('customerror.551')
                    ]
            ];
            return response()->json($response);
        }

        $user = User::where('user_token', @$data->token)->first();

        if($user==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);
        }

        $userFriend = UserToFriend::where([
        	'user_id'			=>	@$user->id,
        	'friend_id'			=>	@$data->friend_id,
        	'request_status'	=>	'FOLLOWING'
        ])->first();
        
        if($userFriend!=null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '553',
                        'message'   =>  config('customerror.553')
                    ]
            ];
            return response()->json(@$response);
        }
        else{
        	$UserToFriend = UserToFriend::create([
        		'user_id'			=>	@$user->id,
	        	'friend_id'			=>	@$data->friend_id,
	        	'request_status'	=>	'FOLLOWING'
        	]);
	    	$response = [
                        'token'                      => null,
                        'status'                     => "SUCCESS"
                    ];
   			return response()->json($response);
        }
    }

    public function sharePost(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(!empty(@$data->token) && @$data->token!=null){

        	$user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

            if(@$user==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            }

            if(empty(@$data->post_id) && @$data->post_id==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '547',
	                        'message'   =>  config('customerror.547')
	                    ]
	            ];
	            return response()->json($response);

            }

            $fndPost = Post::where('id', @$data->post_id)->first();
            if(@$fndPost==null){
            	$response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '546',
                            'message'   =>  config('customerror.546')
                        ]
                ];
                return response()->json(@$response);
            }
            
    		PostToShare::create([
        		'post_id'	=>	@$data->post_id,
        		'user_id'	=>	@$user->id
        	]);
        	
    		@$updatePost=Post::where('id', @$data->post_id)->first();
    		@$updatePost->total_share=@$updatePost->total_share+1;
    		@$updatePost->save();
        	
        	$response = [
                        'token'                      => null,
                        'status'                     => "SUCCESS"
                    ];
   			return response()->json($response);

        }
        else{
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }

    public function addwannaVisit(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(!empty(@$data->token) && @$data->token!=null){
        	$user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

            if(@$user==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            }
        	
        	if(empty(@$data->post_id) && @$data->post_id==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '547',
	                        'message'   =>  config('customerror.547')
	                    ]
	            ];
	            return response()->json($response);
            }
            
            $fndPost = Post::where('id', @$data->post_id)->first();
            if(@$fndPost==null){
            	$response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '546',
                            'message'   =>  config('customerror.546')
                        ]
                ];
                return response()->json(@$response);
            }

            $wannaVisit = WannaVisit::where([
            	"post_id"		=>	@$data->post_id,
            	"user_id"		=>	@$user->id
            ])->first();

            if(@$wannaVisit!=null){
	   			$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '554',
                        'message'   =>  config('customerror.554')
                    ]
            	];
            	return response()->json($response);
            }
            else{
            	$cookedIt = WannaVisit::create([
            		"post_id"		=>	@$data->post_id,
	            	"user_id"		=>	@$user->id
            	]);
            	
            	$updatePost=Post::where('id', @$data->post_id)->first();
        		@$updatePost->total_wanna_visit=@$updatePost->total_wanna_visit+1;
        		@$updatePost->save();

            	$response = [
	                        'token'                      => null,
	                        'status'                     => "SUCCESS"
	                    ];
	   			return response()->json($response);
            }
        }
        else{
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }

    public function wannaVisitList() {

        $json = file_get_contents('php://input');
        $data = json_decode($json);

        if(!empty(@$data->token) && @$data->token!=null) {

            $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

            if(@$user==null) {
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            
            } else {

                $postidArr = array();
                $cookedIts = WannaVisit::where(["user_id"=>@$user->id])->get();
                if(sizeof(@$cookedIts)<=0) {
                    $response = [
                        'token'            => null,
                            'error'         => [
                                'code'      =>  '546',
                                'message'   =>  config('customerror.546')
                            ]
                    ];
                    return response()->json($response);
                }

                foreach($cookedIts as $cookedIt) {
                    $postidArr[] = $cookedIt->post_id;
                }
                
                $post = Post::with('authorDetails', 'postDeafultImage', 'postAllImage')->whereIn('id', $postidArr)->orderBy('id','desc')->get();
                
                if(sizeof(@$post)<=0) {
                    $response = [
                        'token'            => null,
                            'error'         => [
                                'code'      =>  '546',
                                'message'   =>  config('customerror.546')
                            ]
                    ];
                    return response()->json($response);
                }

                
                $pushedPost = $post;
                $i=0;
                foreach(@$post as $pp) {

                    if(@$user->id!=@$pp->user_id) {

                        if(@$pp->authorDetails->account_type=="PER") {

                            if(@$pp->authorDetails->is_post_private=="Y") {

                                $UserToFriend = UserToFriend::where([
                                    'user_id'           =>  @$user->id,
                                    'friend_id'         =>  @$pp->user_id
                                ])->first();
        
                                if(@$UserToFriend->request_status=="FRIEND"){

                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                    $post[@$i]['viewer_is_friend']= "Y";
                                }
                                
                                if(@$UserToFriend->request_status=="WAITING"){
                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['viewer_is_friend']= "W";
                                    $post[@$i]['is_visible_by_me']= "N";
                                }
                                
                                if(@$UserToFriend->request_status=="REJECTED") {
                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['viewer_is_friend']= "N";
                                    $post[@$i]['is_visible_by_me']= "N";
                                }

                            } else {

                                $UserToFriend = UserToFriend::where([
                                    'user_id'           =>  @$user->id,
                                    'friend_id'         =>  @$pp->user_id
                                ])->first();

                                 
                                if(@$UserToFriend->request_status=="FRIEND"){

                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                    $post[@$i]['viewer_is_friend']= "Y";

                                }
                                
                                if(@$UserToFriend->request_status=="WAITING"){
                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['viewer_is_friend']= "W";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                }
                                
                                if(@$UserToFriend->request_status=="REJECTED"){
                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['viewer_is_friend']= "N";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                }
                                $post[@$i]['is_visible_by_me']= "Y";
                            }

                        } else {

                            if(@$pp->authorDetails->is_post_private=="Y") {

                                $UserToFriend = UserToFriend::where([
                                    'user_id'           =>  @$pp->user_id,
                                    'friend_id'         =>  @$user->id
                                ])->first();
                                
                                if(@$UserToFriend->request_status=="FOLLOWING") {

                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                    $post[@$i]['viewer_is_friend']= "Y";

                                }

                            } else {
                                $UserToFriend = UserToFriend::where([
                                    'user_id'           =>  @$pp->user_id,
                                    'friend_id'         =>  @$user->id
                                ])->first();

                                if(@$UserToFriend->request_status=="FOLLOWING"){

                                    $post[@$i]['is_my_post']= "N";
                                    $post[@$i]['is_visible_by_me']= "Y";
                                    $post[@$i]['viewer_is_friend']= "Y";
                                }
                            }
                        }
                    }
                    else {
                        $post[@$i]['is_my_post']= "Y";
                        $post[@$i]['is_visible_by_me']= "Y";
                    }

                    // Post tags

                    $getTags = TagPost::where(['post_id'=>@$pp->id])->get();
                    $taging = array();
                    if($getTags) {
                        foreach($getTags as $getTag) {
                            $user = User::where(['id'=>@$getTag->friend_id])->first();

                            $taging[] = ["user_id"=>$getTag->friend_id, "name"=>$user->first_name.' '.$user->last_name];
                        }
                    } else {
                        $taging = [];   
                    }
                    @$post[@$i]['tags'] =  $taging;

                    // Description tags

                    $getDescTags = TagDescription::where(['post_id'=>@$pp->id])->get();
                    $tagingDesc = array();
                    if($getDescTags) {
                        foreach($getDescTags as $getDescTag) {
                            $user = User::where(['id'=>@$getDescTag->friend_id])->first();
                            $tagingDesc[] = ["user_id"=>$getDescTag->friend_id, "name"=>$user->first_name.' '.$user->last_name];
                        }
                    } else {
                        $tagingDesc = [];   
                    }
                    @$post[@$i]['descTags'] =  $tagingDesc;

                    // Comment tags

                    $getCommentTags = TagComment::where(['post_id'=>@$pp->id])->get();
                    $tagingComment = array();
                    if($getCommentTags) {
                        foreach($getCommentTags as $getCommentTag) {
                            $user = User::where(['id'=>@$getCommentTag->friend_id])->first();
                            $tagingComment[] = ["user_id"=>$getCommentTag->friend_id, "name"=>$user->first_name.' '.$user->last_name];
                        }
                    } else {
                        $tagingComment = [];   
                    }
                    @$post[@$i]['commentTags'] =  $tagingComment;

                    // Post Comment

                    $getComments = PostComments::where(['post_id'=>@$pp->id])->get();
                    $commenting = array();
                    if($getComments) {
                        foreach($getComments as $getComment) {
                            $user = User::where(['id'=>@$getComment->user_id])->first();

                            $commenting[] = ["id"=>$getComment->id,"comment"=>$getComment->comment,"date"=>date("d-m-Y H:i:s",strtotime($getComment->created_at)), "user_id"=>$getComment->user_id, "name"=>$user->first_name.' '.$user->last_name];
                        }
                    } else {
                        $commenting = [];
                    }
                    @$post[@$i]['comments'] =  $commenting;

                    $postLike = PostLike::where([
                        'post_id'   =>  @$pp->id,
                        'user_id'   =>  @$user->id,
                        'is_liked'  =>  'Y'
                    ])->first();
                    
                    @$postLike!=null ? @$post[@$i]['is_liked_by_me']= "Y": @$post[@$i]['is_liked_by_me']= "N";
                    $i++;
                }
                $response = [
                            'token'                      => null,
                            'result'                     => @$post,
                            'status'                     => "SUCCESS"
                        ];
                return response()->json($response);
            }
        }
        else{
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }

    public function deletePost(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(!empty(@$data->token) && @$data->token!=null){
        	$user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

            if(@$user==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            }
        	
        	if(empty(@$data->post_id) && @$data->post_id==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '547',
	                        'message'   =>  config('customerror.547')
	                    ]
	            ];
	            return response()->json($response);
            }
            
            $fndPost = Post::where([
            	'id'	=>	@$data->post_id,
            	'user_id'	=>	@$user->id
            ])->first();

            if(@$fndPost==null){
            	$response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '546',
                            'message'   =>  config('customerror.546')
                        ]
                ];
                return response()->json(@$response);
            }
            else{
            	PostRecipie::where('post_id', @$data->post_id)->delete();
            	PostIngredients::where('post_id', @$data->post_id)->delete();
            	TagPost::where('post_id', @$data->post_id)->delete();
            	PostComments::where('post_id', @$data->post_id)->delete();
            	PostLike::where('post_id', @$data->post_id)->delete();
            	PostToShare::where('post_id', @$data->post_id)->delete();
            	WannaCookedIt::where('post_id', @$data->post_id)->delete();
            	PostToCookedIt::where('post_id', @$data->post_id)->delete();
            	$postImages = PostImages::where('post_id', @$data->post_id)->get();
            	foreach(@$postImages as $pm){
            		if(file_exists('storage/app/public/post_images/'.$pm->image) AND !empty(@$pm->image)){ 
			            unlink('storage/app/public/post_images/'.$pm->image);
			         } 
            	}
            	PostImages::where('post_id', @$data->post_id)->delete();
            	$postSteps = PostStep::where('post_id', @$data->post_id)->get();
            	if(@sizeof($postSteps)>0){
	            	foreach(@$postSteps as $pm){
	            		if(file_exists('storage/app/public/post_step_images/'.$pm->media_file_1) AND !empty(@$pm->media_file_1)){ 
				            unlink('storage/app/public/post_step_images/'.$pm->media_file_1);
				         }
				         if(file_exists('storage/app/public/post_step_images/'.$pm->media_file_2) AND !empty(@$pm->media_file_2)){ 
				            unlink('storage/app/public/post_step_images/'.$pm->media_file_2);
				         }
				         if(file_exists('storage/app/public/post_step_images/'.$pm->media_file_3) AND !empty(@$pm->media_file_3)){ 
				            unlink('storage/app/public/post_step_images/'.$pm->media_file_3);
				         } 
	            	}
            		PostStep::where('post_id', @$data->post_id)->delete();

            	}
            	Post::where('id', @$data->post_id)->delete();
            	$response = [
                        'token'                      => null,
                        'status'                     => "SUCCESS"
                    ];
   				return response()->json($response);
            }
            
        }
        else{
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }

    public function postDetails(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(!empty(@$data->token) && @$data->token!=null){
        	$user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
            if(@$user==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            }
        	
        	if(empty(@$data->post_id) && @$data->post_id==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '547',
	                        'message'   =>  config('customerror.547')
	                    ]
	            ];
	            return response()->json($response);
            }
            
            $fndPost = Post::where('id', @$data->post_id)->first();
            if(@$fndPost==null){
            	$response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '546',
                            'message'   =>  config('customerror.546')
                        ]
                ];
                return response()->json(@$response);
            }
            $fndPost->load('postDeafultImage', 'postAllImage', 'authorDetails', 'postAllComments.commenterDetails','postRecipe', 'postIngredients','postSteps');
            
            if(@$user->id==@$fndPost->user_id){
            	@$fndPost->is_my_post = "Y";
            }

        	if(@$user->id!=@$fndPost->user_id){
        		if(@$fndPost->authorDetails->account_type=="PER"){

            		if(@$fndPost->authorDetails->is_post_private=="Y"){
                		$UserToFriend = UserToFriend::where([
	                		'user_id'			=>	@$fndPost->user_id,
	                		'friend_id'			=>	@$user->id
	                	])->first();
                		
                		// if($UserToFriend==null){
                		// 	$UserToFriend = UserToFriend::where([
		                // 		'user_id'			=>	@$user->id,
		                // 		'friend_id'			=>	@$pp->user_id
		                // 	])->first();
                		// }

	                	if(@$UserToFriend->request_status=="FRIEND"){

	                		$fndPost['is_my_post']= "N";
	                		$fndPost['is_visible_by_me']= "Y";
							$fndPost['viewer_is_friend']= "Y";

	                	}
	                	
	                	if(@$UserToFriend->request_status=="WAITING"){
	                		$fndPost['is_my_post']= "N";
	                		$fndPost['viewer_is_friend']= "W";
	                		$fndPost['is_visible_by_me']= "N";
	                	}
	                	
	                	if(@$UserToFriend->request_status=="REJECTED"){
	                		$fndPost['is_my_post']= "N";
	                		$fndPost['viewer_is_friend']= "N";
	                		$fndPost['is_visible_by_me']= "N";
	                	}
            		}
            		else{
            			$UserToFriend = UserToFriend::where([
	                		'user_id'			=>	@$fndPost->user_id,
	                		'friend_id'			=>	@$user->id
	                	])->first();
                		
                		// if($UserToFriend==null){
                		// 	$UserToFriend = UserToFriend::where([
		                // 		'user_id'			=>	@$user->id,
		                // 		'friend_id'			=>	@$pp->user_id
		                // 	])->first();
                		// }

	                	if(@$UserToFriend->request_status=="FRIEND"){

	                		$fndPost['is_my_post']= "N";
	                		$fndPost['is_visible_by_me']= "Y";
							$fndPost['viewer_is_friend']= "Y";

	                	}
	                	
	                	if(@$UserToFriend->request_status=="WAITING"){
	                		$fndPost['is_my_post']= "N";
	                		$fndPost['viewer_is_friend']= "W";
	                		$fndPost['is_visible_by_me']= "Y";
	                	}
	                	
	                	if(@$UserToFriend->request_status=="REJECTED"){
	                		$fndPost['is_my_post']= "N";
	                		$fndPost['viewer_is_friend']= "N";
	                		$fndPost['is_visible_by_me']= "Y";
	                	}
            			$fndPost['is_visible_by_me']= "Y";
            		}
        		}
        		else{
        			if(@$fndPost->authorDetails->is_post_private=="Y"){
                		$UserToFriend = UserToFriend::where([
	                		'user_id'			=>	@$fndPost->user_id,
	                		'friend_id'			=>	@$user->id
	                	])->first();
                		
                		// if($UserToFriend==null){
                		// 	$UserToFriend = UserToFriend::where([
		                // 		'user_id'			=>	@$user->id,
		                // 		'friend_id'			=>	@$pp->user_id
		                // 	])->first();
                		// }

	                	if(@$UserToFriend->request_status=="FOLLOWING"){

	                		@$fndPost['is_my_post']= "N";
	                		@$fndPost['is_visible_by_me']= "Y";
							@$fndPost['viewer_is_friend']= "Y";

	                	}
            		}
            		else{
            			$UserToFriend = UserToFriend::where([
	                		'user_id'			=>	@$fndPost->user_id,
	                		'friend_id'			=>	@$user->id
	                	])->first();
                		
                		// if($UserToFriend==null){
                		// 	$UserToFriend = UserToFriend::where([
		                // 		'user_id'			=>	@$user->id,
		                // 		'friend_id'			=>	@$pp->user_id
		                // 	])->first();
                		// }

	                	if(@$UserToFriend->request_status=="FOLLOWING"){

	                		@$fndPost['is_my_post']= "N";
	                		@$fndPost['is_visible_by_me']= "Y";
							@$fndPost['viewer_is_friend']= "Y";
	                	}
            		}
        		}
        	}
        	else{
        		@$fndPost['is_my_post']= "Y";
        		@$fndPost['is_visible_by_me']= "Y";
        	}

        	$postLike = PostLike::where([
        		'post_id'	=>	@$fndPost->id,
        		'user_id'	=>	@$user->id,
        		'is_liked'	=>	'Y'
        	])->first();
        	@$postLike!=null ? @$fndPost['is_liked_by_me']= "Y": @$fndPost['is_liked_by_me']= "N";            
            $response = [
                        'token'                      	=> null,
                        'result'                      	=> @$fndPost,
                        'status'                     	=> "SUCCESS"
                    ];
   			return response()->json($response);
        }
        else{
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }

    public function editComment(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(!empty(@$data->token) && @$data->token!=null){
        	$user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
            if(@$user==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            }
        	
        	if(empty(@$data->post_id) && @$data->post_id==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '547',
	                        'message'   =>  config('customerror.547')
	                    ]
	            ];
	            return response()->json($response);
            }

            if(empty(@$data->comment_id) && @$data->comment_id==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '555',
	                        'message'   =>  config('customerror.555')
	                    ]
	            ];
	            return response()->json($response);
            }

            if(empty(@$data->post_comment) && @$data->post_comment==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '548',
	                        'message'   =>  config('customerror.548')
	                    ]
	            ];
	            return response()->json($response);
            }
            
            $fndPost = Post::where('id', @$data->post_id)->first();

            if(@$fndPost==null){
            	$response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '546',
                            'message'   =>  config('customerror.546')
                        ]
                ];
                return response()->json(@$response);
            }
			$updateComment = PostComments::where([
				"id"	=>	@$data->comment_id,
				"post_id"	=>	@$data->post_id,
				"user_id"	=>	@$user->id,
			])->update(["comment"	=>	@$data->post_comment]);

			$response = [
                'token'                      => null,
                'status'                     => "SUCCESS"
            ];
   			return response()->json($response);
        }
        else{
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }	
    }

    public function deleteComment(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(!empty(@$data->token) && @$data->token!=null){
        	$user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
            
            if(@$user==null) {
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            }
        	
        	if(empty(@$data->post_id) && @$data->post_id==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '547',
	                        'message'   =>  config('customerror.547')
	                    ]
	            ];
	            return response()->json($response);
            }

            if(empty(@$data->comment_id) && @$data->comment_id==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '555',
	                        'message'   =>  config('customerror.555')
	                    ]
	            ];
	            return response()->json($response);
            }

            // if(empty(@$data->post_comment) && @$data->post_comment==null){
	           //  $response = [
	           //      'token'            => null,
	           //          'error'         => [
	           //              'code'      =>  '548',
	           //              'message'   =>  config('customerror.548')
	           //          ]
	           //  ];
	           //  return response()->json($response);
            // }
            
            $fndPost = Post::where('id', @$data->post_id)->first();

            if(@$fndPost==null){
            	$response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '546',
                            'message'   =>  config('customerror.546')
                        ]
                ];
                return response()->json(@$response);
            }
			$updateComment = PostComments::where([
				"id"		=>	@$data->comment_id,
				"post_id"	=>	@$data->post_id,
				"user_id"	=>	@$user->id,
			])->delete();

			$response = [
                'token'                      => null,
                'status'                     => "SUCCESS"
            ];
   			return response()->json($response);
        }
        else{
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }	
    }  

    public function cancelFriendRequest(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }

        if(empty(@$data->friend_id) && @$data->friend_id==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '551',
                        'message'   =>  config('customerror.551')
                    ]
            ];
            return response()->json($response);
        }

        $user = User::where('user_token', @$data->token)->first();
        if($user==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);
        }

        $userFriend = UserToFriend::where([
        	'user_id'			=>	@$user->id,
        	'friend_id'			=>	@$data->friend_id,
        	'request_status'	=>	'WAITING'
        ])->first();
        
        if(@$userFriend!=null){
        	$userFriend = UserToFriend::where([
        		'user_id'			=>	@$user->id,
	        	'friend_id'			=>	@$data->friend_id,
	        	'request_status'	=>	'WAITING'
        	])->delete();
	    	$response = [
                        'token'                      => null,
                        'status'                     => "SUCCESS"
                    ];
   			return response()->json($response);
        }
    }

    public function cancelFollowingRequest(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(empty(@$data->token) && @$data->token==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }

        if(empty(@$data->friend_id) && @$data->friend_id==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '551',
                        'message'   =>  config('customerror.551')
                    ]
            ];
            return response()->json($response);
        }

        $user = User::where('user_token', @$data->token)->first();

        if($user==null){
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '403',
                        'message'   =>  config('customerror.403')
                    ]
            ];
            return response()->json(@$response);
        }

        $userFriend = UserToFriend::where([
        	'user_id'			=>	@$user->id,
        	'friend_id'			=>	@$data->friend_id,
        	'request_status'	=>	'FOLLOWING'
        ])->first();
        
        if($userFriend!=null){
        	$UserToFriend = UserToFriend::where([
        		'user_id'			=>	@$user->id,
	        	'friend_id'			=>	@$data->friend_id,
	        	'request_status'	=>	'FOLLOWING'
        	])->delete();
	    	$response = [
                        'token'                      => null,
                        'status'                     => "SUCCESS"
                    ];
   			return response()->json($response);
        }
    }

    public function removeWannaCookedIt(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(!empty(@$data->token) && @$data->token!=null){
        	$user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
            if(@$user==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            }
        	
        	if(empty(@$data->post_id) && @$data->post_id==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '547',
	                        'message'   =>  config('customerror.547')
	                    ]
	            ];
	            return response()->json($response);
            }
            
            $fndPost = Post::where('id', @$data->post_id)->first();
            if(@$fndPost==null){
            	$response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '546',
                            'message'   =>  config('customerror.546')
                        ]
                ];
                return response()->json(@$response);
            }
            $recipeId = PostRecipie::where([
            	'post_id'	=>	@$data->post_id
            ])->first();

            $cookedIt = CookedIt::where([
            	"post_id"		=>	@$data->post_id,
            	"recipie_id"	=>	@$recipeId->id,
            	"user_id"		=>	@$user->id
            ])->first();

            if(@$cookedIt!=null){
            	$cookedIt = CookedIt::where([
            		"post_id"		=>	@$data->post_id,
	            	"recipie_id"	=>	@$recipeId->id,
	            	"user_id"		=>	@$user->id
            	])->delete();
            	
            	$updatePost=Post::where('id', @$data->post_id)->first();
        		@$updatePost->total_cooked_it = @$updatePost->total_cooked_it >0 ? @$updatePost->total_cooked_it-1: 0;
        		@$updatePost->save();

            	// $post = Post::where('id', @$data->post_id)->first();
	        	$response = [
	                        'token'                      => null,
	                        // 'result'                     => @$post,
	                        'status'                     => "SUCCESS"
	                    ];
	   			return response()->json($response);
            }
        }
        else{
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }

    public function removeWannaVisit(){
    	$json = file_get_contents('php://input');
        $data = json_decode($json);
        if(!empty(@$data->token) && @$data->token!=null){
        	$user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();

            if(@$user==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            }
        	
        	if(empty(@$data->post_id) && @$data->post_id==null){
	            $response = [
	                'token'            => null,
	                    'error'         => [
	                        'code'      =>  '547',
	                        'message'   =>  config('customerror.547')
	                    ]
	            ];
	            return response()->json($response);
            }
            
            $fndPost = Post::where('id', @$data->post_id)->first();
            if(@$fndPost==null){
            	$response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '546',
                            'message'   =>  config('customerror.546')
                        ]
                ];
                return response()->json(@$response);
            }
           

            $wannaVisit = WannaVisit::where([
            	"post_id"		=>	@$data->post_id,
            	"user_id"		=>	@$user->id
            ])->first();

            if(@$wannaVisit!=null){
            	$cookedIt = WannaVisit::where([
            		"post_id"		=>	@$data->post_id,
	            	"user_id"		=>	@$user->id
            	])->delete();
            	
            	$updatePost=Post::where('id', @$data->post_id)->first();
        		@$updatePost->total_wanna_visit=@$updatePost->total_wanna_visit>0 ? @$updatePost->total_wanna_visit-1: 0;
        		@$updatePost->save();

            	$response = [
	                        'token'                      => null,
	                        'status'                     => "SUCCESS"
	                    ];
	   			return response()->json($response);
            }
        }
        else{
        	$response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }  
}
