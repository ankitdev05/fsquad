<?php

namespace App\Http\Controllers\Modules;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $users = User::where('id', '!=', 0)->where('user_type', 'U');
        $key = [];
        if ($request->all()) {
            if ($request->keyword) {
                $users->where('name', 'like', '%' . $request->keyword . '%');
            }
            if ($request->status) {
                $users->where('status', $request->status);
            }
            $key = $request->all();
        }
        $users = $users->get();
        return view('admin.modules.user.user', [
            'users' => $users,
            'key' => $key
        ]);
    }

    /**
     * @method status
     * @purpose For changing User status
     */
    public function status($id = null)
    {
        $user = User::where('id', $id)->where('user_type', 'U')->first();
        if ($user == null) {
            session()->flash('warning', 'Unauthorized access!');
            return redirect()->back();
        }
        $user->status = $user->status == "A" ? "I" : "A";
        $user->save();
        session()->flash('success', 'User status updated.');
        return redirect()->back();
    }
}
