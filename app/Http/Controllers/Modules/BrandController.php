<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\Ingredient;
class BrandController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function brands(Request $request){
    	$brand = Brand::where('id', '!=', 0);
    	$key = [];
    	if($request->all()){
    		if($request->keyword){
    			$brand = $brand->where('name', 'like','%'.$request->keyword.'%');
    		}
    		if($request->status){
    			$brand = $brand->where('status', @$request->status);
    		}
    	}
		$brand = $brand->orderBy('id', 'desc')->paginate(10);
		$brand->appends(request()->all())->render();
		$key = $request->all();
    	return view('admin.modules.brands.brand')->with([
    		'brand'	=>	@$brand,
    		'key'	=>	@$key
    	]);
    }

    public function addBrands(Request $request){
    	if($request->all()){
    		$request->validate([
    			'name'	=>	'required|unique:brands',
    			'image'	=>	'required|mimes:jpeg,png,jpg,gif,bmp'
    		]);
    		$is_created = Brand::create([
    			'name'	=>	@$request->name
    		]);
    		if($is_created->id){
    			$name = str_random(12).'-'.time().'.'.@$request->image->getClientOriginalExtension();
            	$request->image->move('storage/app/public/brand_images', $name);
            	$is_updated = Brand::where([
            		'id'	=>	@$is_created->id
            	])->update([
            		'image'	=>	@$name
            	]);
            	if(@$is_updated){
    				session()->flash('success', 'Brand added successfully.');
    				return redirect()->back();
            	}
            	else{
            		session()->flash('error', 'Internal server error.');
    				return redirect()->back();	
            	}
    		}
    		else{
    			session()->flash('error', 'Internal server error.');
    			return redirect()->back();
    		}
    	}
    	return view('admin.modules.brands.add_brand');
    }

    public function editBrands($id,Request $request){
    	$brand = Brand::where('id', @$id)->first();
    	if(@$brand==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	if($request->all()){
    		$request->validate([
    			'name'	=>	'required|unique:brands,name,'.@$id,
    			'image'	=>	'nullable|mimes:jpeg,png,jpg,gif,bmp'
    		]);
    		$updated = Brand::where('id', @$id)
    		->update([
    			'name'	=>	@$request->name
    		]);
    		if($request->image){
    			if(file_exists(storage_path().'/app/public/brand_images/'.@$brand->image)&& @$brand->image!=null){
    				unlink(storage_path().'/app/public/brand_images/'.@$brand->image);
    			}
    			
    			$name = str_random(12).'-'.time().'.'.@$request->image->getClientOriginalExtension();
            	
            	$request->image->move(storage_path().'/app/public/brand_images', $name);
            	
            	$is_updated = Brand::where([
            		'id'	=>	@$id
            	])->update([
            		'image'	=>	@$name
            	]);
            	if(@$is_updated){
    				session()->flash('success', 'Brand updated successfully.');
    				return redirect()->back();
            	}
            	else{
            		session()->flash('error', 'Internal server error.');
    				return redirect()->back();	
            	}
    		}
    		else{
    			session()->flash('success', 'Brand updated successfully.');
    			return redirect()->back();
    		}
    	}
    	@$brand = Brand::where([
    		'id'	=>	@$id
    	])->first();
    	return view('admin.modules.brands.edit_brand')->with([
    		'brand'	=>	@$brand
    	]);
    }

    public function statusBrands($id){
    	$brand = Brand::where('id', @$id)->first();
    	if(@$brand==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	$brand->status = $brand->status == "A" ? "I": "A";
    	$brand->save();
    	session()->flash('success', 'Status successfully updated.');
    	return redirect()->back();
    }


    public function removeBrands($id){
    	$brand = Brand::where('id', @$id)->first();
    	if(@$brand==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	$brand->load('hasIngredients');
        
    	if(count(@$brand->hasIngredients)>0){
    		session()->flash('error', 'Unable to remove this Brand, because this brand has several products.');
    		return redirect()->back();
    	}
    	if(file_exists(storage_path().'app/public/brand_images/'.@$brand->image)&& @$brand->image!=null){
			unlink(storage_path().'app/public/brand_images/'.@$brand->image);
		}
		$brand->delete();
		session()->flash('success', 'Brand removed successfully');
		return redirect()->back();
    }
}
