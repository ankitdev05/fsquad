<?php

namespace App\Http\Controllers\Modules;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UsageGamififaction;
use App\Models\PostGamififaction;
use App\Models\VideopostGamififaction;
use App\Models\FreindGamififaction;
use App\Models\CookeditGamififaction;
use validate;
class GamificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showUsageGamification(Request $request){
    	$usageGami = UsageGamififaction::where('id', '!=', 0);
    	@$key = [];
    	if(@$request->all()){
    		if(@$request->keyword){
    			$usageGami = $usageGami->where('name', 'Like', '%'.$request->keyword.'%');
    		}
    		if(@$request->level){
    			$usageGami = $usageGami->where('level', @$request->level);
    		}
    		if(@$request->is_top){
    			$usageGami = $usageGami->where('is_top', @$request->is_top);
    		}
    	}
    	$usageGami = @$usageGami->orderBy('id', 'desc')->paginate(10);
		$usageGami->appends(request()->all())->render();
		$key = @$request->all();
    	return view('admin.modules.gamification.usage_gami')->with([
    		'usageGami'	=>	@$usageGami,
    		'key'		=>	@$key
    	]);
    }

    public function addUsageGami(Request $request){
    	if(@$request->all()){
    		$request->validate([
				'name'				=>	'required|unique:usage_gamification',
				'image'				=>	'required|mimes:jpeg,png,jpg,gif,bmp',
				'no_of_days'		=>	'required|unique:usage_gamification',
				'level'				=>	'required|unique:usage_gamification',
				'is_top'			=>	'required'
			],[
				'level.required'	=>	'Allready Level: '.@$request->level.' Exist.'
			]);

			$is_created = UsageGamififaction::create([
				'name'				=>	@$request->name,
				'no_of_days'		=>	@$request->no_of_days,
				'level'				=>	@$request->level 
			]);
			if(@$is_created->id){
				if(@$request->is_top=="Y"){
					UsageGamififaction::where('id', '!=', 0)->update(['is_top'=>'N']);
				}
				$name = str_random(12).'-'.time().'.'.@$request->image->getClientOriginalExtension();
            	$request->image->move('storage/app/public/usage_gamification_images', $name);
            	$is_updated = UsageGamififaction::where([
            		'id'	=>	@$is_created->id
            	])->update([
            		'image'		=>	@$name,
            		'is_top'	=>	@$request->is_top
            	]);
            	if(@$is_updated){
    				session()->flash('success', 'Usage Gamification added successfully.');
    				return redirect()->back();
            	}
            	else{
            		session()->flash('error', 'Internal server error.');
    				return redirect()->back();	
            	}
			}
			else{
        		session()->flash('error', 'Internal server error.');
				return redirect()->back();	
        	}
    	}
    	return view('admin.modules.gamification.add_usage_gami');
    }

    public function editUsageGami($id, Request $request){
    	@$editGami = UsageGamififaction::where('id', @$id)->first();
    	if(@$editGami==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	if(@$request->all()){
    		$request->validate([
				'name'			=>	'required|unique:usage_gamification,name,'.@$id,
				'level'			=>	'required|unique:usage_gamification,level,'.@$id,
				'no_of_days'	=>	'required|unique:usage_gamification,no_of_days,'.@$id,
				'is_top'		=>	'required'
			],[
				'level.unique'		=>	'Allready Level: '.@$request->level.' Exist.',
				'no_of_days.unique'	=>	@$request->no_of_days.' already exist.'
			]);
    		if(@$request->is_top=="Y"){
				UsageGamififaction::where('id', '!=', 0)->update(['is_top'=>'N']);
			}
			$is_updated = UsageGamififaction::where('id', @$id)
			->update([
				'name'				=>	@$request->name,
				'no_of_days'		=>	@$request->no_of_days,
				'is_top'			=>	@$request->is_top,
				'level'				=>	@$request->level 
			]);
			if(@$is_updated){
				if(@$request->image){
					if(file_exists(storage_path().'/app/public/usage_gamification_images'.@$editGami->image)&& @$editGami->image!=null){
	    				unlink(storage_path().'/app/public/usage_gamification_images/'.@$editGami->image);
	    			}

					$name = str_random(12).'-'.time().'.'.@$request->image->getClientOriginalExtension();
	            	$request->image->move('storage/app/public/usage_gamification_images', $name);
	            	
	            	$is_updated = UsageGamififaction::where([
	            		'id'	=>	@$id
	            	])->update([
	            		'image'		=>	@$name,
	            		'is_top'	=>	@$request->is_top
	            	]);
				}
            	session()->flash('success', 'Usage Gamification updated successfully.');
    			return redirect()->back();
			}
			else{
        		session()->flash('error', 'Internal server error.');
				return redirect()->back();	
        	}
    	}
    	@$editGami = UsageGamififaction::where('id', @$id)->first();
    	return view('admin.modules.gamification.edit_usage_gami')->with([
    		'editGami'	=>	@$editGami
    	]);
    }

    public function removeGami($id){
    	$gami = UsageGamififaction::where('id', @$id)->first();
    	if(@$gami==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	if(file_exists(storage_path().'/app/public/usage_gamification_images'.@$gami->image)&& @$gami->image!=null){
			unlink(storage_path().'/app/public/usage_gamification_images/'.@$gami->image);
		}
		$gami = UsageGamififaction::where('id', @$id)->delete();
		session()->flash('success', 'Usage Gamification removed successfully.');
    	return redirect()->back();
    }

    public function showPostGamification(Request $request){
    	$usageGami = PostGamififaction::where('id', '!=', 0);
    	@$key = [];
    	if(@$request->all()){
    		if(@$request->keyword){
    			$usageGami = $usageGami->where('name', 'Like', '%'.$request->keyword.'%');
    		}
    		if(@$request->level){
    			$usageGami = $usageGami->where('level', @$request->level);
    		}
    		if(@$request->is_top){
    			$usageGami = $usageGami->where('is_top', @$request->is_top);
    		}
    	}
    	$usageGami = @$usageGami->orderBy('id', 'desc')->paginate(10);
		$usageGami->appends(request()->all())->render();
		$key = @$request->all();
    	return view('admin.modules.gamification.post_gami')->with([
    		'usageGami'	=>	@$usageGami,
    		'key'		=>	@$key
    	]);
    }

    public function addPostGami(Request $request){
    	if(@$request->all()){
    		$request->validate([
				'name'				=>	'required|unique:post_gamification',
				'image'				=>	'required|unique:post_gamification|mimes:jpeg,png,jpg,gif,bmp',
				'no_of_post'		=>	'required|unique:post_gamification',
				'level'				=>	'required|unique:post_gamification',
				'is_top'			=>	'required'
			],[
				'level.required'	=>	'Allready Level: '.@$request->level.' Exist.'
			]);

			$is_created = PostGamififaction::create([
				'name'				=>	@$request->name,
				'no_of_post'		=>	@$request->no_of_post,
				'level'				=>	@$request->level 
			]);
			if(@$is_created->id){
				if(@$request->is_top=="Y"){
					PostGamififaction::where('id', '!=', 0)->update(['is_top'=>'N']);
				}
				$name = str_random(12).'-'.time().'.'.@$request->image->getClientOriginalExtension();
            	$request->image->move('storage/app/public/post_gamification_images', $name);
            	$is_updated = PostGamififaction::where([
            		'id'	=>	@$is_created->id
            	])->update([
            		'image'		=>	@$name,
            		'is_top'	=>	@$request->is_top
            	]);
            	if(@$is_updated){
    				session()->flash('success', 'Post Gamification added successfully.');
    				return redirect()->back();
            	}
            	else{
            		session()->flash('error', 'Internal server error.');
    				return redirect()->back();	
            	}
			}
			else{
        		session()->flash('error', 'Internal server error.');
				return redirect()->back();	
        	}
    	}
    	return view('admin.modules.gamification.add_post_gami');
    }

    public function editPostGami($id, Request $request){
    	@$editGami = PostGamififaction::where('id', @$id)->first();
    	if(@$editGami==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	if(@$request->all()){
    		$request->validate([
				'name'			=>	'required|unique:post_gamification,name,'.@$id,
				'level'			=>	'required|unique:post_gamification,level,'.@$id,
				'no_of_post'	=>	'required|unique:post_gamification,no_of_post,'.@$id,
				'is_top'		=>	'required'
			],[
				'level.unique'		=>	'Allready Level: '.@$request->level.' Exist.',
				'no_of_post.unique'	=>	@$request->no_of_post.' already exist.'
			]);
    		if(@$request->is_top=="Y"){
				PostGamififaction::where('id', '!=', 0)->update(['is_top'=>'N']);
			}
			$is_updated = PostGamififaction::where('id', @$id)
			->update([
				'name'				=>	@$request->name,
				'no_of_post'		=>	@$request->no_of_post,
				'is_top'			=>	@$request->is_top,
				'level'				=>	@$request->level 
			]);
			if(@$is_updated){
				if(@$request->image){
					if(file_exists(storage_path().'/app/public/post_gamification_images'.@$editGami->image)&& @$editGami->image!=null){
	    				unlink(storage_path().'/app/public/post_gamification_images/'.@$editGami->image);
	    			}

					$name = str_random(12).'-'.time().'.'.@$request->image->getClientOriginalExtension();
	            	$request->image->move('storage/app/public/post_gamification_images', $name);
	            	
	            	$is_updated = PostGamififaction::where([
	            		'id'	=>	@$id
	            	])->update([
	            		'image'		=>	@$name,
	            		'is_top'	=>	@$request->is_top
	            	]);
				}
            	session()->flash('success', 'Post Gamification updated successfully.');
    			return redirect()->back();
			}
			else{
        		session()->flash('error', 'Internal server error.');
				return redirect()->back();	
        	}
    	}
    	@$editGami = PostGamififaction::where('id', @$id)->first();
    	return view('admin.modules.gamification.edit_post_gami')->with([
    		'editGami'	=>	@$editGami
    	]);
    }

    public function removePostGami($id){
    	$gami = PostGamififaction::where('id', @$id)->first();
    	if(@$gami==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	if(file_exists(storage_path().'/app/public/post_gamification_images'.@$gami->image)&& @$gami->image!=null){
			unlink(storage_path().'/app/public/post_gamification_images/'.@$gami->image);
		}
		$gami = PostGamififaction::where('id', @$id)->delete();
		session()->flash('success', 'Post Gamification removed successfully.');
    	return redirect()->back();
    }

    public function showVideoGamification(Request $request){
    	$usageGami = VideopostGamififaction::where('id', '!=', 0);
    	@$key = [];
    	if(@$request->all()){
    		if(@$request->keyword){
    			$usageGami = $usageGami->where('name', 'Like', '%'.$request->keyword.'%');
    		}
    		if(@$request->level){
    			$usageGami = $usageGami->where('level', @$request->level);
    		}
    		if(@$request->is_top){
    			$usageGami = $usageGami->where('is_top', @$request->is_top);
    		}
    	}
    	$usageGami = @$usageGami->orderBy('id', 'desc')->paginate(10);
		$usageGami->appends(request()->all())->render();
		$key = @$request->all();
    	return view('admin.modules.gamification.video_gami')->with([
    		'usageGami'	=>	@$usageGami,
    		'key'		=>	@$key
    	]);
    }

    public function addVideoGami(Request $request){
    	if(@$request->all()){
    		$request->validate([
				'name'						=>	'required|unique:video_post_gamification',
				'image'						=>	'required|mimes:jpeg,png,jpg,gif,bmp',
				'no_of_video_posted'		=>	'required|unique:video_post_gamification',
				'level'						=>	'required|unique:video_post_gamification',
				'is_top'					=>	'required'
			],[
				'level.required'	=>	'Allready Level: '.@$request->level.' Exist.'
			]);

			$is_created = VideopostGamififaction::create([
				'name'						=>	@$request->name,
				'no_of_video_posted'		=>	@$request->no_of_video_posted,
				'level'						=>	@$request->level 
			]);
			if(@$is_created->id){
				if(@$request->is_top=="Y"){
					VideopostGamififaction::where('id', '!=', 0)->update(['is_top'=>'N']);
				}
				$name = str_random(12).'-'.time().'.'.@$request->image->getClientOriginalExtension();
            	$request->image->move('storage/app/public/video_gamification_images', $name);

            	$is_updated = VideopostGamififaction::where([
            		'id'	=>	@$is_created->id
            	])->update([
            		'image'		=>	@$name,
            		'is_top'	=>	@$request->is_top
            	]);
            	if(@$is_updated){
    				session()->flash('success', 'Video Gamification added successfully.');
    				return redirect()->back();
            	}
            	else{
            		session()->flash('error', 'Internal server error.');
    				return redirect()->back();	
            	}
			}
			else{
        		session()->flash('error', 'Internal server error.');
				return redirect()->back();	
        	}
    	}
    	return view('admin.modules.gamification.add_video_gami');
    }

    public function editVideoGami($id, Request $request){
    	@$editGami = VideopostGamififaction::where('id', @$id)->first();
    	if(@$editGami==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	if(@$request->all()){
    		$request->validate([
				'name'					=>	'required|unique:video_post_gamification,name,'.@$id,
				'level'					=>	'required|unique:video_post_gamification,level,'.@$id,
				'no_of_video_posted'	=>	'required|unique:video_post_gamification,no_of_video_posted,'.@$id,
				'is_top'				=>	'required'
			],[
				'level.unique'		=>	'Allready Level: '.@$request->level.' Exist.',
				'no_of_post.unique'	=>	@$request->no_of_post.' already exist.'
			]);
    		if(@$request->is_top=="Y"){
				VideopostGamififaction::where('id', '!=', 0)->update(['is_top'=>'N']);
			}
			$is_updated = VideopostGamififaction::where('id', @$id)
			->update([
				'name'						=>	@$request->name,
				'no_of_video_posted'		=>	@$request->no_of_video_posted,
				'is_top'					=>	@$request->is_top,
				'level'						=>	@$request->level 
			]);
			if(@$is_updated){
				if(@$request->image){
					if(file_exists(storage_path().'/app/public/video_gamification_images'.@$editGami->image)&& @$editGami->image!=null){
	    				unlink(storage_path().'/app/public/video_gamification_images/'.@$editGami->image);
	    			}

					$name = str_random(12).'-'.time().'.'.@$request->image->getClientOriginalExtension();
	            	$request->image->move('storage/app/public/video_gamification_images', $name);
	            	
	            	$is_updated = VideopostGamififaction::where([
	            		'id'	=>	@$id
	            	])->update([
	            		'image'		=>	@$name,
	            		'is_top'	=>	@$request->is_top
	            	]);
				}
            	session()->flash('success', 'Video Gamification updated successfully.');
    			return redirect()->back();
			}
			else{
        		session()->flash('error', 'Internal server error.');
				return redirect()->back();	
        	}
    	}
    	@$editGami = VideopostGamififaction::where('id', @$id)->first();
    	return view('admin.modules.gamification.edit_video_gami')->with([
    		'editGami'	=>	@$editGami
    	]);
    }

    public function removeVideoGami($id){
    	$gami = VideopostGamififaction::where('id', @$id)->first();
    	if(@$gami==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	if(file_exists(storage_path().'/app/public/video_gamification_images'.@$gami->image)&& @$gami->image!=null){
			unlink(storage_path().'/app/public/video_gamification_images/'.@$gami->image);
		}
		$gami = VideopostGamififaction::where('id', @$id)->delete();
		session()->flash('success', 'Video Gamification removed successfully.');
    	return redirect()->back();
    }

    public function showfreindGamification(Request $request){
    	$usageGami = FreindGamififaction::where('id', '!=', 0);
    	@$key = [];
    	if(@$request->all()){
    		if(@$request->keyword){
    			$usageGami = $usageGami->where('name', 'Like', '%'.$request->keyword.'%');
    		}
    		if(@$request->level){
    			$usageGami = $usageGami->where('level', @$request->level);
    		}
    		if(@$request->is_top){
    			$usageGami = $usageGami->where('is_top', @$request->is_top);
    		}
    	}
    	$usageGami = @$usageGami->orderBy('id', 'desc')->paginate(10);
		$usageGami->appends(request()->all())->render();
		$key = @$request->all();
    	return view('admin.modules.gamification.freind_gami')->with([
    		'usageGami'	=>	@$usageGami,
    		'key'		=>	@$key
    	]);
    }

    public function addfreindGami(Request $request){
    	if(@$request->all()){
    		$request->validate([
				'name'						=>	'required|unique:freind_gamification',
				'image'						=>	'required|mimes:jpeg,png,jpg,gif,bmp',
				'no_of_freinds'		=>	'required|unique:freind_gamification',
				'level'						=>	'required|unique:freind_gamification',
				'is_top'					=>	'required'
			],[
				'level.required'	=>	'Allready Level: '.@$request->level.' Exist.'
			]);

			$is_created = FreindGamififaction::create([
				'name'						=>	@$request->name,
				'no_of_freinds'		=>	@$request->no_of_freinds,
				'level'						=>	@$request->level 
			]);
			if(@$is_created->id){
				if(@$request->is_top=="Y"){
					FreindGamififaction::where('id', '!=', 0)->update(['is_top'=>'N']);
				}
				$name = str_random(12).'-'.time().'.'.@$request->image->getClientOriginalExtension();
            	$request->image->move('storage/app/public/freind_gamification_images', $name);

            	$is_updated = FreindGamififaction::where([
            		'id'	=>	@$is_created->id
            	])->update([
            		'image'		=>	@$name,
            		'is_top'	=>	@$request->is_top
            	]);
            	if(@$is_updated){
    				session()->flash('success', 'Freind Gamification added successfully.');
    				return redirect()->back();
            	}
            	else{
            		session()->flash('error', 'Internal server error.');
    				return redirect()->back();	
            	}
			}
			else{
        		session()->flash('error', 'Internal server error.');
				return redirect()->back();	
        	}
    	}
    	return view('admin.modules.gamification.add_freind_gami');
    }

    public function editfreindGami($id, Request $request){
    	@$editGami = FreindGamififaction::where('id', @$id)->first();
    	if(@$editGami==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	if(@$request->all()){
    		$request->validate([
				'name'					=>	'required|unique:freind_gamification,name,'.@$id,
				'level'					=>	'required|unique:freind_gamification,level,'.@$id,
				'no_of_freinds'	=>	'required|unique:freind_gamification,no_of_freinds,'.@$id,
				'is_top'				=>	'required'
			],[
				'level.unique'		=>	'Allready Level: '.@$request->level.' Exist.',
				'no_of_post.unique'	=>	@$request->no_of_post.' already exist.'
			]);
    		if(@$request->is_top=="Y"){
				FreindGamififaction::where('id', '!=', 0)->update(['is_top'=>'N']);
			}
			$is_updated = FreindGamififaction::where('id', @$id)
			->update([
				'name'						=>	@$request->name,
				'no_of_freinds'		=>	@$request->no_of_freinds,
				'is_top'					=>	@$request->is_top,
				'level'						=>	@$request->level 
			]);
			if(@$is_updated){
				if(@$request->image){
					if(file_exists(storage_path().'/app/public/freind_gamification_images'.@$editGami->image)&& @$editGami->image!=null){
	    				unlink(storage_path().'/app/public/freind_gamification_images/'.@$editGami->image);
	    			}

					$name = str_random(12).'-'.time().'.'.@$request->image->getClientOriginalExtension();
	            	$request->image->move('storage/app/public/freind_gamification_images', $name);
	            	
	            	$is_updated = FreindGamififaction::where([
	            		'id'	=>	@$id
	            	])->update([
	            		'image'		=>	@$name,
	            		'is_top'	=>	@$request->is_top
	            	]);
				}
            	session()->flash('success', 'Freind Gamification updated successfully.');
    			return redirect()->back();
			}
			else{
        		session()->flash('error', 'Internal server error.');
				return redirect()->back();	
        	}
    	}
    	@$editGami = FreindGamififaction::where('id', @$id)->first();
    	return view('admin.modules.gamification.edit_freind_gami')->with([
    		'editGami'	=>	@$editGami
    	]);
    }

    public function removefreindGami($id){
    	$gami = FreindGamififaction::where('id', @$id)->first();
    	if(@$gami==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	if(file_exists(storage_path().'/app/public/freind_gamification_images'.@$gami->image)&& @$gami->image!=null){
			unlink(storage_path().'/app/public/freind_gamification_images/'.@$gami->image);
		}
		$gami = FreindGamififaction::where('id', @$id)->delete();
		session()->flash('success', 'Freind Gamification removed successfully.');
    	return redirect()->back();
    }

    public function showcookedItGamification(Request $request){
    	$usageGami = CookeditGamififaction::where('id', '!=', 0);
    	@$key = [];
    	if(@$request->all()){
    		if(@$request->keyword){
    			$usageGami = $usageGami->where('name', 'Like', '%'.$request->keyword.'%');
    		}
    		if(@$request->level){
    			$usageGami = $usageGami->where('level', @$request->level);
    		}
    		if(@$request->is_top){
    			$usageGami = $usageGami->where('is_top', @$request->is_top);
    		}
    	}
    	$usageGami = @$usageGami->orderBy('id', 'desc')->paginate(10);
		$usageGami->appends(request()->all())->render();
		$key = @$request->all();
    	return view('admin.modules.gamification.cooked_it_gami')->with([
    		'usageGami'	=>	@$usageGami,
    		'key'		=>	@$key
    	]);
    }

    public function addcookedItGami(Request $request){
    	if(@$request->all()){
    		$request->validate([
				'name'						=>	'required|unique:cookedit_gamification',
				'image'						=>	'required|mimes:jpeg,png,jpg,gif,bmp',
				'no_of_i_cokked_it'		=>	'required|unique:cookedit_gamification',
				'level'						=>	'required|unique:cookedit_gamification',
				'is_top'					=>	'required'
			],[
				'level.required'	=>	'Allready Level: '.@$request->level.' Exist.'
			]);

			$is_created = CookeditGamififaction::create([
				'name'						=>	@$request->name,
				'no_of_i_cokked_it'			=>	@$request->no_of_i_cokked_it,
				'level'						=>	@$request->level 
			]);
			if(@$is_created->id){
				if(@$request->is_top=="Y"){
					CookeditGamififaction::where('id', '!=', 0)->update(['is_top'=>'N']);
				}
				$name = str_random(12).'-'.time().'.'.@$request->image->getClientOriginalExtension();
            	$request->image->move('storage/app/public/cooked_it_gamification_images', $name);

            	$is_updated = CookeditGamififaction::where([
            		'id'	=>	@$is_created->id
            	])->update([
            		'image'		=>	@$name,
            		'is_top'	=>	@$request->is_top
            	]);
            	if(@$is_updated){
    				session()->flash('success', 'Freind Gamification added successfully.');
    				return redirect()->back();
            	}
            	else{
            		session()->flash('error', 'Internal server error.');
    				return redirect()->back();	
            	}
			}
			else{
        		session()->flash('error', 'Internal server error.');
				return redirect()->back();	
        	}
    	}
    	return view('admin.modules.gamification.add_cooked_it_gami');
    }

    public function editcookedItGami($id, Request $request){
    	@$editGami = CookeditGamififaction::where('id', @$id)->first();
    	if(@$editGami==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	if(@$request->all()){
    		$request->validate([
				'name'					=>	'required|unique:cookedit_gamification,name,'.@$id,
				'level'					=>	'required|unique:cookedit_gamification,level,'.@$id,
				'no_of_i_cokked_it'	=>	'required|unique:cookedit_gamification,no_of_i_cokked_it,'.@$id,
				'is_top'				=>	'required'
			],[
				'level.unique'		=>	'Allready Level: '.@$request->level.' Exist.',
				'no_of_i_cokked_it.unique'	=>	@$request->no_of_i_cokked_it.' already exist.'
			]);
    		if(@$request->is_top=="Y"){
				CookeditGamififaction::where('id', '!=', 0)->update(['is_top'=>'N']);
			}
			$is_updated = CookeditGamififaction::where('id', @$id)
			->update([
				'name'						=>	@$request->name,
				'no_of_i_cokked_it'		=>	@$request->no_of_i_cokked_it,
				'is_top'					=>	@$request->is_top,
				'level'						=>	@$request->level 
			]);
			if(@$is_updated){
				if(@$request->image){
					if(file_exists(storage_path().'/app/public/cooked_it_gamification_images'.@$editGami->image)&& @$editGami->image!=null){
	    				unlink(storage_path().'/app/public/cooked_it_gamification_images/'.@$editGami->image);
	    			}

					$name = str_random(12).'-'.time().'.'.@$request->image->getClientOriginalExtension();
	            	$request->image->move('storage/app/public/cooked_it_gamification_images', $name);
	            	
	            	$is_updated = CookeditGamififaction::where([
	            		'id'	=>	@$id
	            	])->update([
	            		'image'		=>	@$name,
	            		'is_top'	=>	@$request->is_top
	            	]);
				}
            	session()->flash('success', 'Cooked It Gamification updated successfully.');
    			return redirect()->back();
			}
			else{
        		session()->flash('error', 'Internal server error.');
				return redirect()->back();	
        	}
    	}
    	@$editGami = CookeditGamififaction::where('id', @$id)->first();
    	return view('admin.modules.gamification.edit_cooked_it_gami')->with([
    		'editGami'	=>	@$editGami
    	]);
    }

    public function removecookedItGami($id){
    	$gami = CookeditGamififaction::where('id', @$id)->first();
    	if(@$gami==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	if(file_exists(storage_path().'/app/public/cooked_it_gamification_images'.@$gami->image)&& @$gami->image!=null){
			unlink(storage_path().'/app/public/cooked_it_gamification_images/'.@$gami->image);
		}
		$gami = CookeditGamififaction::where('id', @$id)->delete();
		session()->flash('success', 'Freind Gamification removed successfully.');
    	return redirect()->back();
    }
}
