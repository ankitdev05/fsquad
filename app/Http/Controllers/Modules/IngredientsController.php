<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\Ingredient;
use validate;
class IngredientsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function ingredients(Request $request){
    	$ingr = Ingredient::where('id', '!=', 0)->where('status', '!=', 'D');
    	$key = [];
    	if($request->all()){
    		if($request->keyword){
    			$ingr = $ingr->where('name', 'like','%'.$request->keyword.'%');
    		}
    		if($request->brand_id){
    			$ingr = $ingr->where('brand_id', @$request->brand_id);
    		}
    		if($request->status){
    			$ingr = $ingr->where('status', @$request->status);
    		}
    	}
		$ingr = $ingr->orderBy('id', 'desc')->paginate(2);
		$ingr->appends(request()->all())->render();
		$brand = Brand::where('status', 'A')->orderBy('name')->get();
		$key = $request->all();

    	return view('admin.modules.ingredients.ingredients')->with([
    		'ingr'	=>	@$ingr,
    		'brand'	=>	@$brand,
    		'key'	=>	@$key
    	]);
    }

    public function addIngredients(Request $request){
    	if($request->all()){
    		$request->validate([
    			'name'	=>	'required|unique:ingredients',
    			'image'	=>	'required|mimes:jpeg,png,jpg,gif,bmp'
    		]);

    		if($request->is_brand=="Y"){
    			$request->validate([
	    			'brand_id'	=>	'required'
	    		],
	    		[
	    			'brand_id.required'	=>	'Please select ingredient brand.'
	    		]);
	    		$is_created = Ingredient::create([
	    			'name'		=>	@$request->name,
	    			'is_brand'	=>	"Y",
	    			'brand_id'	=>	@$request->brand_id
	    		]);
    		}
    		else{
    			$is_created = Ingredient::create([
	    			'name'	=>	@$request->name
	    		]);	
    		}

    		if($is_created->id){
    			$name = str_random(12).'-'.time().'.'.@$request->image->getClientOriginalExtension();

            	$request->image->move('storage/app/public/ingredient_images', $name);
            	$is_updated = Ingredient::where([
            		'id'	=>	@$is_created->id
            	])->update([
            		'image'	=>	@$name
            	]);
            	
            	if(@$is_updated){
    				session()->flash('success', 'Ingredient added successfully.');
    				return redirect()->back();
            	}
            	else{
            		session()->flash('error', 'Internal server error.');
    				return redirect()->back();	
            	}
    		}
    		else{
    			session()->flash('error', 'Internal server error.');
    			return redirect()->back();
    		}
    	}
    	$brand = Brand::where('status', 'A')->orderBy('name')->get();
    	return view('admin.modules.ingredients.add_ingredient')->with([
    		'brand'	=>	@$brand
    	]);
    }

    public function editIngredients($id,Request $request){
    	$ingr = Ingredient::where('id', @$id)->first();
    	if(@$ingr==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	if($request->all()){
    		$request->validate([
    			'name'	=>	'required|unique:ingredients,name,'.@$id,
    			'image'	=>	'nullable|mimes:jpeg,png,jpg,gif,bmp'
    		]);

    		if($request->is_brand=="Y"){
    			$request->validate([
	    			'brand_id'	=>	'required'
	    		],
	    		[
	    			'brand_id.required'	=>	'Please select ingredient brand.'
	    		]);
	    		$is_created = Ingredient::where('id', @$id)
	    		->update([
	    			'name'		=>	@$request->name,
	    			'is_brand'	=>	"Y",
	    			'brand_id'	=>	@$request->brand_id
	    		]);
    		}
    		else{
    			$is_created = Ingredient::where('id', @$id)
	    		->update([
	    			'name'		=>	@$request->name,
	    			'is_brand'	=>	"N",
	    			'brand_id'	=>	0
	    		]);
    		}
    		if($request->image){
    			if(file_exists(storage_path().'/app/public/ingredient_images/'.@$ingr->image)&& @$ingr->image!=null){
    				unlink(storage_path().'/app/public/ingredient_images/'.@$ingr->image);
    			}
    			
    			$name = str_random(12).'-'.time().'.'.@$request->image->getClientOriginalExtension();
            	
            	$request->image->move(storage_path().'/app/public/ingredient_images', $name);
            	
            	$is_updated = Ingredient::where([
            		'id'	=>	@$id
            	])->update([
            		'image'	=>	@$name
            	]);
            	if(@$is_updated){
    				session()->flash('success', 'Ingredient updated successfully.');
    				return redirect()->back();
            	}
            	else{
            		session()->flash('error', 'Internal server error.');
    				return redirect()->back();	
            	}
    		}
    		else{
    			session()->flash('success', 'Ingredient updated successfully.');
    			return redirect()->back();
    		}
    	}
    	
    	@$ingr = Ingredient::where([
    		'id'	=>	@$id
    	])->first();
    	@$brand = Brand::where([
    		'status'	=>	"A"
    	])->orderBy('name')->get();

    	return view('admin.modules.ingredients.edit_ingredient')->with([
    		'ingr'	=>	@$ingr,
    		'brand'	=>	@$brand
    	]);
    }

    public function statusIngredients($id){
    	$ingr = Ingredient::where('id', @$id)->first();
    	if(@$ingr==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	$ingr->status = $ingr->status == "A" ? "I": "A";
    	$ingr->save();
    	session()->flash('success', 'Status successfully updated.');
    	return redirect()->back();
    }


    public function removeIngredients($id){
    	$inger = Ingredient::where('id', @$id)->first();
    	if(@$inger==null){
    		session()->flash('error', 'Unauthorize access.');
    		return redirect()->back();
    	}
    	// $inger->load('hasIngredients');
        
    	// if(count(@$inger->hasIngredients)>0){
    	// 	session()->flash('error', 'Unable to remove this ingredient, because this brand has several products.');
    	// 	return redirect()->back();
    	// }
  //   	if(file_exists(storage_path().'app/public/ingredient_images/'.@$ingr->image)&& @$ingr->image!=null){

		// 	unlink(storage_path().'app/public/ingredient_images/'.@$ingr->image);
		// }
		@$inger->status = "D";
		$inger->save();
		session()->flash('success', 'Ingredient removed successfully');
		return redirect()->back();
    }
}
