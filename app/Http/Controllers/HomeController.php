<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use validate;
use App\User;
use AUth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.home');
    }

    public function editProfile(Request $request){
        if(@$request->all()){
            @$request->validate([
                'name'      =>  'required',
                'email'     =>  'required|email'
            ]);

            if(@$request->old_password!=null && @$request->old_password!=""){
               $request->validate([
                    'old_password'          =>  'required',
                    'new_password'          =>  'required',
                    'confirm_password'      =>  'required'
                ]);
                if(@$request->new_password!=@$request->confirm_password){
                    session()->flash('error', "New password and confirm password do not match.");
                    return redirect()->back();
                }
                else{
                    if(\Hash::check(@$request->old_password, Auth::guard('web')->user()->password)){
                        $is_update = User::where([
                            'id'    =>  Auth::guard('web')->user()->id
                        ])->update([
                            'password'  => \Hash::make(@$request->new_password)
                        ]);
                        session()->flash('success', 'Your profile is successfully updated.');
                        return redirect()->back();
                    }
                    else{
                        session()->flash('error', "You have entered an incoreect password");
                        return redirect()->back();
                    }
                }
            }
            else{
                $is_update = User::where([
                    'id'    =>  Auth::guard('web')->user()->id
                ])->update([
                    'name'      =>  @$request->name,
                    'email'     =>  @$request->email
                ]);
                session()->flash('success', 'Your profile is successfully updated.');
                return redirect()->back();
            }
        }
        return view('admin.modules.profile.edit_profile');
    }
}
