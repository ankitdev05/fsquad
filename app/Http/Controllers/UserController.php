<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use validator;
use App\User;
use App\Models\UserToBusiness;
use App\Models\UserVerify;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgetPassword;
use App\Mail\VerificationUser;
use AWS;

class UserController extends Controller
{


    public function changePass($user) {
        $email = $user;
        return view('forgot', compact('email'));
    }

    // public function verifyVcode(){
    //     $json = file_get_contents('php://input');
    //     $data = json_decode($json);
        
    //     if(empty(@$data->user_id)) {
    //         $response = [
    //             'token'             => null,
    //             'status'            => "ERROR",
    //                 'error'         => [
    //                     'code'      =>  '514',
    //                     'message'   =>  config('customerror.514')
    //                 ]
    //         ];
    //         return response()->json($response);
    //     }
    //     if(empty(@$data->vcode)){
    //         $response = [
    //             'token'             => null,
    //             'status'            => "ERROR",
    //                 'error'         => [
    //                     'code'      =>  '516',
    //                     'message'   =>  config('customerror.516')
    //                 ]
    //         ];
    //         return response()->json($response);
    //     }

    //     $user = User::where([
    //         'id'        =>  @$data->user_id,
    //         'vcode'     =>  @$data->vcode
    //     ])->where('user_type', '!=', 'A')->first();
    //     if($user!=null){
    //         $user->vcode=null;
    //         $user->save();
    //         $user = User::where([
    //             'id'        =>  @$data->user_id
    //         ])->first();

    //         $response = [
    //             'token'             => null,
    //             'status'            => "SUCCESS",
    //             'result'            => @$user,
    //             'error'             => null
    //         ];
    //         return response()->json($response);
    //     }
    //     else{
    //         $response = [
    //             'token'             => null,
    //             'status'            => "ERROR",
    //                 'error'         => [
    //                     'code'      =>  '517',
    //                     'message'   =>  config('customerror.517')
    //                 ]
    //         ];
    //         return response()->json($response);
    //     }
    // }

    public function updateForgetPassword(Request $request) {
        //$json = file_get_contents('php://input');
        $data = request()->all();

        $user_id = $data['user_id'];
        
        if(empty(@$data['vcode'])) {
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '516',
                        'message'   =>  config('customerror.516')
                    ]
            ];
            return response()->json($response);
        }

        if(empty(@$data['new_password'])){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '515',
                        'message'   =>  config('customerror.515')
                    ]
            ];
            return response()->json($response);
        }
        
        if(empty(@$data['confirm_password'])){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '116',
                        'message'   =>  config('customerror.116')
                    ]
            ];
            return response()->json($response);
        }

        if(@$data['new_password']!=@$data['confirm_password']){
            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '118',
                        'message'   =>  config('customerror.118')
                    ]
            ];
            return response()->json($response);
        }

        $user = User::where(['vcode' => @$data['vcode']])->where('user_type', '!=', 'A')->first();
        if($user!=null) {

            if(is_numeric(@$data['username'])) {
                $userDataCheck = $user->mobile;
            } else {
                $userDataCheck = $user->email;
            }

            if($userDataCheck == $user_id) {
        
                $user = User::where('id', @$user->id)->where('user_type', '!=', 'A')->first();
                if($user==null) {
                    $response = [
                        'token'             => null,
                        'status'            => "ERROR",
                            'error'         => [
                                'code'      =>  '518',
                                'message'   =>  config('customerror.518')
                            ]
                    ];
                    return response()->json($response);
                }
                @$user->password = \Hash::make(@$data['new_password']);
                @$user->vcode = "";
                @$user->save();
                
                $response = [
                    'token'             => null,
                    'status'            => "SUCCESS",
                    'error'             => null
                ];
                return response()->json($response);
            
            } else {

                $response = [
                    'token'             => null,
                    'status'            => "ERROR",
                        'error'         => [
                            'code'      =>  '517',
                            'message'   =>  config('customerror.517')
                        ]
                ];
                return response()->json($response);
            }

        } else {

            $response = [
                'token'             => null,
                'status'            => "ERROR",
                    'error'         => [
                        'code'      =>  '517',
                        'message'   =>  config('customerror.517')
                    ]
            ];
            return response()->json($response);
        }
    }

}
