<?php
namespace App\Http\Middleware;
use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use App\User;
class JwtMiddleware extends BaseMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        if(!empty(@$data->token) && @$data->token!=null){
            $user = User::where([
                'user_token'    =>  @$data->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
            if(@$user==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            }
            else{
                return $next($request);
            }
        }
        elseif(!empty(@$request->token) && @$request->token!=null){
            $user = User::where([
                'user_token'    =>  @$request->token,
                'status'        =>  'A',
                'user_type'     =>  'U'
            ])->first();
            if(@$user==null){
                $response = [
                    'token'            => null,
                        'error'         => [
                            'code'      =>  '403',
                            'message'   =>  config('customerror.403')
                        ]
                ];
                return response()->json(@$response);   
            }
            else{
                return $next($request);
            }
        }
        else{
            $response = [
                'token'            => null,
                    'error'         => [
                        'code'      =>  '127',
                        'message'   =>  config('customerror.127')
                    ]
            ];
            return response()->json($response);
        }
    }
}