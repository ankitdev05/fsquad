<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerificationUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $code;
    public function __construct($vcode)
    {
        //
        $this->code = $vcode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['vcode'] = @$this->code;
        return $this->view('mail.verify_user', $data);
    }
}
