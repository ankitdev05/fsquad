<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgetPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    public $code;
    public $user;

    public function __construct($vcode, $user)
    {
        //
        $this->code = $vcode;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['vcode'] = @$this->code;
        $data['user'] = @$this->user;

        return $this->view('mail.forget_password', $data);
    }
}
