-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 25, 2020 at 03:11 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_fsquad`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 'PizzaHut', 'lNHagI16SkUR-1583387289.png', 'A', '2020-03-05 00:06:23', '2020-03-05 00:18:09'),
(3, 'Cafe Cofee Day', 'y5qVvjOxpua4-1583387486.jpg', 'A', '2020-03-05 00:21:26', '2020-03-05 00:21:26'),
(4, 'McDonalds', 'uAVzrWStioto-1583387510.jpg', 'A', '2020-03-05 00:21:50', '2020-03-05 00:21:50'),
(6, 'KFC', '0mm8zEXq9lSL-1583387893.png', 'A', '2020-03-05 00:28:12', '2020-03-05 00:28:13'),
(7, 'Nestle', '9YiI7uXffkuJ-1583417982.png', 'A', '2020-03-05 08:49:42', '2020-03-05 08:49:42'),
(8, 'Amul', 'Kw5G5TLzCEuR-1583417992.jpg', 'A', '2020-03-05 08:49:52', '2020-03-05 08:49:52');

-- --------------------------------------------------------

--
-- Table structure for table `cookedit_gamification`
--

CREATE TABLE `cookedit_gamification` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `no_of_i_cokked_it` bigint(20) NOT NULL DEFAULT 0,
  `level` bigint(20) NOT NULL DEFAULT 0,
  `is_top` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cookedit_gamification`
--

INSERT INTO `cookedit_gamification` (`id`, `name`, `image`, `no_of_i_cokked_it`, `level`, `is_top`, `created_at`, `updated_at`) VALUES
(1, 'o cooked', '0QMcEiI8X05V-1584197681.jpg', 1, 1, 'N', '2020-03-14 09:23:25', '2020-03-14 09:24:41');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `freind_gamification`
--

CREATE TABLE `freind_gamification` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `no_of_freinds` bigint(20) NOT NULL DEFAULT 0,
  `level` bigint(20) NOT NULL DEFAULT 0,
  `is_top` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `freind_gamification`
--

INSERT INTO `freind_gamification` (`id`, `name`, `image`, `no_of_freinds`, `level`, `is_top`, `created_at`, `updated_at`) VALUES
(1, '1st freind', 'iEe90u9viYEd-1584196450.png', 11, 0, 'N', '2020-03-14 09:04:10', '2020-03-14 09:06:06');

-- --------------------------------------------------------

--
-- Table structure for table `ingredients`
--

CREATE TABLE `ingredients` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `is_brand` enum('Y','N') NOT NULL DEFAULT 'N',
  `brand_id` bigint(20) NOT NULL DEFAULT 0,
  `status` enum('A','I','D') DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ingredients`
--

INSERT INTO `ingredients` (`id`, `name`, `image`, `is_brand`, `brand_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cofee', 'o1gfu5ygfRY5-1583425049.jpg', 'Y', 3, 'A', '2020-03-05 10:47:28', '2020-03-05 21:59:55'),
(2, 'Egg', 'Zos8c8Yi6Cp6-1583466286.jpg', 'N', 0, 'A', '2020-03-05 21:58:11', '2020-03-05 22:16:32');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `description` text DEFAULT NULL,
  `hash_tags` text DEFAULT NULL,
  `has_manual_recipie` enum('Y','N') NOT NULL DEFAULT 'N',
  `has_video_source_recipie` enum('Y','N') NOT NULL DEFAULT 'N',
  `has_tagged_recipie` enum('Y','N') NOT NULL DEFAULT 'N',
  `total_like` bigint(20) NOT NULL DEFAULT 0,
  `total_comments` bigint(20) NOT NULL DEFAULT 0,
  `total_cooked_it` bigint(20) NOT NULL DEFAULT 0,
  `location` text DEFAULT NULL,
  `latitude` text DEFAULT NULL,
  `longitude` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `post_gamification`
--

CREATE TABLE `post_gamification` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `no_of_post` bigint(20) NOT NULL DEFAULT 0,
  `level` bigint(20) NOT NULL DEFAULT 0,
  `is_top` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post_gamification`
--

INSERT INTO `post_gamification` (`id`, `name`, `image`, `no_of_post`, `level`, `is_top`, `created_at`, `updated_at`) VALUES
(3, '1st post', 'PihW2LjLVImg-1584195051.png', 0, 1, 'N', '2020-03-14 07:36:21', '2020-03-14 08:40:51'),
(4, '10 post', 'nPVNNntHomW4-1584191487.jpg', 10, 2, 'N', '2020-03-14 07:41:27', '2020-03-14 07:44:03'),
(5, '20 Post', 'e7jl9SgLIUQv-1584191621.png', 20, 3, 'Y', '2020-03-14 07:43:41', '2020-03-14 07:44:03');

-- --------------------------------------------------------

--
-- Table structure for table `post_ingredients`
--

CREATE TABLE `post_ingredients` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL DEFAULT 0,
  `ingredients_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `post_to_comments`
--

CREATE TABLE `post_to_comments` (
  `id` int(11) NOT NULL,
  `post_id` bigint(20) NOT NULL DEFAULT 0,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `comment` bigint(20) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `post_to_cooked_it`
--

CREATE TABLE `post_to_cooked_it` (
  `id` int(11) NOT NULL,
  `post_id` bigint(20) NOT NULL DEFAULT 0,
  `recipie_id` bigint(20) NOT NULL DEFAULT 0,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `is_cokked` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `post_to_like`
--

CREATE TABLE `post_to_like` (
  `id` int(11) NOT NULL,
  `post_id` bigint(20) NOT NULL DEFAULT 0,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `is_liked` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `post_to_step`
--

CREATE TABLE `post_to_step` (
  `id` int(11) NOT NULL,
  `post_id` bigint(20) NOT NULL DEFAULT 0,
  `instruction` text DEFAULT NULL,
  `media_file_1` text DEFAULT NULL,
  `media_file_2` text DEFAULT NULL,
  `media_file_3` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `usage_gamification`
--

CREATE TABLE `usage_gamification` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `no_of_days` bigint(20) NOT NULL DEFAULT 0,
  `is_top` enum('Y','N') NOT NULL DEFAULT 'N',
  `level` bigint(20) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usage_gamification`
--

INSERT INTO `usage_gamification` (`id`, `name`, `image`, `no_of_days`, `is_top`, `level`, `created_at`, `updated_at`) VALUES
(4, 'New Registered', 'ppaNlk9DsGgn-1584169529.jpg', 0, 'N', 1, '2020-03-14 01:20:50', '2020-03-14 01:46:39'),
(5, '10 day use', 'R2l5Y48h89eL-1584169667.jpg', 10, 'N', 2, '2020-03-14 01:37:46', '2020-03-14 01:46:29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` enum('A','U') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'U',
  `account_type` enum('PER','PRO') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PER',
  `is_post_private` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `professional_account_type` enum('BU','BL') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_website_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_profile_complete` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `status` enum('I','A') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `device_registrartion_id` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `first_name`, `last_name`, `dob`, `email_verified_at`, `password`, `remember_token`, `user_token`, `user_type`, `account_type`, `is_post_private`, `professional_account_type`, `business_website_url`, `business_address`, `is_profile_complete`, `status`, `device_registrartion_id`, `created_at`, `updated_at`) VALUES
(1, 'Fsquad Admin', 'admin@fsquad.com', NULL, NULL, NULL, NULL, NULL, '$2y$10$KyX/mImhv/OX888JQJDjIuW9N7B8Vdl6pTPNA6zI8f.ziPQyQdqBm', NULL, NULL, 'A', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, '2020-02-29 09:29:42', '2020-03-06 09:19:37'),
(9, '@Gupta-Abhisek', 'user@fsquad.com', NULL, 'Abhisek', 'Gupta', '1996-04-03', NULL, '$2y$10$KyX/mImhv/OX888JQJDjIuW9N7B8Vdl6pTPNA6zI8f.ziPQyQdqBm', NULL, '$2y$10$f4STKbsoFiu.W7oDbnIcdOCD1OJKYuhweo/KwQ/JlJHqOJSIBXly.', 'U', 'PER', 'Y', NULL, NULL, NULL, 'N', 'A', 'xxx1112wqstr', '2020-03-20 01:17:07', '2020-03-21 11:08:39'),
(10, '@Blogger-Abhisek#10yQSA78', 'blogger@fsquad.com', NULL, 'Blogger', 'Abhisek', '1992-03-12', NULL, '$2y$10$KyX/mImhv/OX888JQJDjIuW9N7B8Vdl6pTPNA6zI8f.ziPQyQdqBm', NULL, '$2y$10$Qs1l95M8KLIdXar.JGOcOuktBU/vmykdWOK8DDsZPZ2KVI8VVB7nS', 'U', 'PRO', 'Y', 'BU', NULL, 'Bhowanipore', 'N', 'A', 'xx1111111111', '2020-03-21 09:33:00', '2020-03-21 10:52:47');

-- --------------------------------------------------------

--
-- Table structure for table `user_to_business`
--

CREATE TABLE `user_to_business` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `day` enum('MON','TUE','WED','THR','FRI','SAT','SUN') DEFAULT NULL,
  `from_time` text DEFAULT NULL,
  `to_time` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_to_business`
--

INSERT INTO `user_to_business` (`id`, `user_id`, `day`, `from_time`, `to_time`, `created_at`, `updated_at`) VALUES
(2, 10, 'SUN', '10:20', '10:20', '2020-03-21 10:52:46', '2020-03-21 10:52:46');

-- --------------------------------------------------------

--
-- Table structure for table `video_post_gamification`
--

CREATE TABLE `video_post_gamification` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `no_of_video_posted` bigint(20) NOT NULL DEFAULT 0,
  `level` int(11) DEFAULT 0,
  `is_top` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `video_post_gamification`
--

INSERT INTO `video_post_gamification` (`id`, `name`, `image`, `no_of_video_posted`, `level`, `is_top`, `created_at`, `updated_at`) VALUES
(4, '0 Videos', 'U7dDU42cyehx-1584195155.png', 0, 1, 'N', '2020-03-14 08:42:35', '2020-03-14 08:49:38'),
(5, '1 Video', 'uP1jNQau9NUm-1584195501.jpg', 1, 2, 'Y', '2020-03-14 08:48:21', '2020-03-14 08:49:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cookedit_gamification`
--
ALTER TABLE `cookedit_gamification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `freind_gamification`
--
ALTER TABLE `freind_gamification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_gamification`
--
ALTER TABLE `post_gamification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_ingredients`
--
ALTER TABLE `post_ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_to_comments`
--
ALTER TABLE `post_to_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_to_cooked_it`
--
ALTER TABLE `post_to_cooked_it`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_to_like`
--
ALTER TABLE `post_to_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_to_step`
--
ALTER TABLE `post_to_step`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usage_gamification`
--
ALTER TABLE `usage_gamification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_to_business`
--
ALTER TABLE `user_to_business`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_post_gamification`
--
ALTER TABLE `video_post_gamification`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cookedit_gamification`
--
ALTER TABLE `cookedit_gamification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `freind_gamification`
--
ALTER TABLE `freind_gamification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ingredients`
--
ALTER TABLE `ingredients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post_gamification`
--
ALTER TABLE `post_gamification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `post_ingredients`
--
ALTER TABLE `post_ingredients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post_to_comments`
--
ALTER TABLE `post_to_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post_to_cooked_it`
--
ALTER TABLE `post_to_cooked_it`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post_to_like`
--
ALTER TABLE `post_to_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post_to_step`
--
ALTER TABLE `post_to_step`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usage_gamification`
--
ALTER TABLE `usage_gamification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_to_business`
--
ALTER TABLE `user_to_business`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `video_post_gamification`
--
ALTER TABLE `video_post_gamification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
