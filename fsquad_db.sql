-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 21, 2020 at 02:20 PM
-- Server version: 5.7.31-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fsquad_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` text,
  `image` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 'PizzaHut', 'lNHagI16SkUR-1583387289.png', 'A', '2020-03-05 00:06:23', '2020-03-05 00:18:09'),
(3, 'Cafe Cofee Day', 'y5qVvjOxpua4-1583387486.jpg', 'A', '2020-03-05 00:21:26', '2020-03-05 00:21:26'),
(4, 'McDonalds', 'uAVzrWStioto-1583387510.jpg', 'A', '2020-03-05 00:21:50', '2020-03-05 00:21:50'),
(6, 'KFC', '0mm8zEXq9lSL-1583387893.png', 'A', '2020-03-05 00:28:12', '2020-03-05 00:28:13'),
(7, 'Nestle', '9YiI7uXffkuJ-1583417982.png', 'A', '2020-03-05 08:49:42', '2020-03-05 08:49:42'),
(8, 'Amul', 'Kw5G5TLzCEuR-1583417992.jpg', 'A', '2020-03-05 08:49:52', '2020-03-05 08:49:52');

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `reciver_id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  `read_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT ' 0 for unread 1 for read',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `room_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `sender_id`, `reciver_id`, `message`, `read_status`, `created_at`, `room_id`) VALUES
(1, 18, 109, 'Hi', '0', '2020-07-17 09:45:09', '10'),
(2, 18, 109, 'Hfhfhfhf', '0', '2020-07-17 10:29:07', '10'),
(3, 18, 109, 'Hi', '0', '2020-07-17 10:39:39', '10'),
(4, 18, 109, 'Jjj', '0', '2020-07-17 10:43:49', '10'),
(5, 18, 109, 'J', '0', '2020-07-17 10:47:03', '10'),
(6, 18, 109, 'hjj', '0', '2020-07-17 13:38:19', '10'),
(7, 18, 42, 'hi', '0', '2020-07-21 07:36:08', '32'),
(8, 18, 103, 'gghhjhb', '0', '2020-07-21 08:16:11', '29'),
(9, 18, 103, 'bhhhh', '0', '2020-07-21 08:16:13', '29'),
(10, 110, 109, 'There should be a screen anme', '0', '2020-07-21 10:44:43', '37'),
(11, 110, 109, 'Gkhhkh', '0', '2020-07-21 10:44:50', '37'),
(12, 159, 18, 'Hi', '0', '2020-07-23 07:35:18', '38'),
(13, 18, 159, 'hi', '0', '2020-07-23 07:35:23', '38'),
(14, 18, 159, 'hr', '0', '2020-07-23 07:35:28', '38'),
(15, 159, 18, 'Demo', '0', '2020-07-23 07:35:35', '38'),
(16, 18, 159, 'gg', '0', '2020-07-23 07:38:06', '38'),
(17, 18, 159, 'hg', '0', '2020-07-23 07:52:59', '38'),
(18, 18, 159, 'cf', '0', '2020-07-23 07:53:03', '38'),
(19, 18, 159, 'gg', '0', '2020-07-23 07:53:11', '38'),
(20, 159, 18, 'Jjj', '0', '2020-07-23 07:53:23', '38'),
(21, 159, 18, 'Hh', '0', '2020-07-23 07:53:24', '38'),
(22, 159, 18, 'Sss', '0', '2020-07-23 08:24:40', '38'),
(23, 159, 18, 'Sss', '0', '2020-07-23 08:24:44', '38'),
(24, 18, 159, 'zfg', '0', '2020-07-23 08:24:46', '38'),
(25, 103, 18, 'hi', '0', '2020-08-14 11:44:47', '29'),
(26, 109, 77781, 'Ffgg', '0', '2020-08-14 13:13:13', '52'),
(27, 77781, 109, 'Hello', '0', '2020-08-14 13:14:06', '52'),
(28, 109, 77781, 'Grvrvrbrbrbrbthtbtbtbtbt', '0', '2020-08-14 13:14:15', '52'),
(29, 109, 77781, 'Btbtnynynyny', '0', '2020-08-14 13:14:19', '52'),
(30, 77781, 109, 'Hi kkk', '0', '2020-08-14 13:14:24', '52'),
(31, 77781, 109, 'Jou', '0', '2020-08-14 13:14:30', '52'),
(32, 77779, 77778, 'Helli', '0', '2020-08-14 16:16:04', '43');

-- --------------------------------------------------------

--
-- Table structure for table `chat_room`
--

CREATE TABLE `chat_room` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `reciver_id` int(11) NOT NULL,
  `room_id` varchar(255) NOT NULL,
  `last_message` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat_room`
--

INSERT INTO `chat_room` (`id`, `sender_id`, `reciver_id`, `room_id`, `last_message`, `created_at`, `updated_at`) VALUES
(3, 1, 3, 'room13', 'HHHHHHHHHHHHHHHH', '2019-11-08 10:18:26', '2019-11-08 11:07:08'),
(4, 3, 1, 'room13', 'HHHHHHHHHHHHHHHH', '2019-11-08 10:18:26', '2019-11-08 11:07:08'),
(5, 9, 3, 'room93', NULL, '2019-11-09 08:55:02', '2019-11-09 08:55:02'),
(6, 3, 9, 'room93', NULL, '2019-11-09 08:55:02', '2019-11-09 08:55:02'),
(7, 9, 1, 'room91', NULL, '2019-11-09 08:57:34', '2019-11-09 08:57:34'),
(8, 1, 9, 'room91', NULL, '2019-11-09 08:57:34', '2019-11-09 08:57:34'),
(9, 9, 12, 'room912', 'ger', '2019-11-09 09:45:26', '2019-11-15 09:12:21'),
(10, 12, 9, 'room912', 'ger', '2019-11-09 09:45:26', '2019-11-15 09:12:21'),
(11, 9, 25, 'room925', 'Ggggg', '2019-11-09 10:17:40', '2019-11-09 10:17:57'),
(12, 25, 9, 'room925', 'Ggggg', '2019-11-09 10:17:40', '2019-11-09 10:17:57'),
(13, 21, 12, 'room2112', 'Cool......You will enjoy it lot', '2019-11-09 11:34:36', '2019-11-13 09:35:46'),
(14, 12, 21, 'room2112', 'Cool......You will enjoy it lot', '2019-11-09 11:34:36', '2019-11-13 09:35:46'),
(15, 21, 38, 'room2138', NULL, '2019-11-09 11:38:54', '2019-11-09 11:38:54'),
(16, 38, 21, 'room2138', NULL, '2019-11-09 11:38:54', '2019-11-09 11:38:54'),
(17, 21, 21, 'room2121', 'hh', '2019-11-11 05:58:45', '2019-11-11 14:17:43'),
(18, 21, 21, 'room2121', 'hh', '2019-11-11 05:58:45', '2019-11-11 14:17:43'),
(19, 9, 27, 'room927', NULL, '2019-11-11 06:00:08', '2019-11-11 06:00:08'),
(20, 27, 9, 'room927', NULL, '2019-11-11 06:00:08', '2019-11-11 06:00:08'),
(21, 21, 36, 'room2136', 'hii', '2019-11-11 06:12:16', '2019-11-12 12:29:00'),
(22, 36, 21, 'room2136', 'hii', '2019-11-11 06:12:16', '2019-11-12 12:29:00'),
(23, 9, 28, 'room928', 'Hii', '2019-11-11 06:35:14', '2019-11-11 06:35:21'),
(24, 28, 9, 'room928', 'Hii', '2019-11-11 06:35:14', '2019-11-11 06:35:21'),
(25, 21, 9, 'room219', 'to hjj', '2019-11-11 07:04:41', '2019-11-12 05:53:18'),
(26, 9, 21, 'room219', 'to hjj', '2019-11-11 07:04:41', '2019-11-12 05:53:18'),
(27, 12, 12, 'room1212', 'hii', '2019-11-11 07:44:50', '2019-11-11 10:08:41'),
(28, 12, 12, 'room1212', 'hii', '2019-11-11 07:44:50', '2019-11-11 10:08:41'),
(29, 9, 19, 'room919', NULL, '2019-11-11 09:57:21', '2019-11-11 09:57:21'),
(30, 19, 9, 'room919', NULL, '2019-11-11 09:57:21', '2019-11-11 09:57:21'),
(31, 9, 9, 'room99', NULL, '2019-11-11 10:16:43', '2019-11-11 10:16:43'),
(32, 9, 9, 'room99', NULL, '2019-11-11 10:16:43', '2019-11-11 10:16:43'),
(33, 9, 0, 'room90', NULL, '2019-11-11 10:21:48', '2019-11-11 10:21:48'),
(34, 0, 9, 'room90', NULL, '2019-11-11 10:21:48', '2019-11-11 10:21:48'),
(35, 41, 36, 'room4136', 'Type a message...', '2019-11-11 11:51:40', '2019-11-11 11:58:01'),
(36, 36, 41, 'room4136', 'Type a message...', '2019-11-11 11:51:40', '2019-11-11 11:58:01'),
(37, 36, 12, 'room3612', 'fried', '2019-11-11 11:59:29', '2019-11-12 11:40:58'),
(38, 12, 36, 'room3612', 'fried', '2019-11-11 11:59:29', '2019-11-12 11:40:58'),
(39, 36, 9, 'room369', 'gvr', '2019-11-11 12:06:36', '2019-11-11 16:17:10'),
(40, 9, 36, 'room369', 'gvr', '2019-11-11 12:06:36', '2019-11-11 16:17:10'),
(41, 21, 27, 'room2127', NULL, '2019-11-11 14:25:01', '2019-11-11 14:25:01'),
(42, 27, 21, 'room2127', NULL, '2019-11-11 14:25:01', '2019-11-11 14:25:01'),
(43, 9, 37, 'room937', 'Hey', '2019-11-11 14:53:07', '2019-11-11 15:34:23'),
(44, 37, 9, 'room937', 'Hey', '2019-11-11 14:53:07', '2019-11-11 15:34:23'),
(45, 41, 9, 'room419', 'tata', '2019-11-11 15:40:48', '2019-11-24 13:53:53'),
(46, 9, 41, 'room419', 'tata', '2019-11-11 15:40:48', '2019-11-24 13:53:53'),
(47, 41, 37, 'room4137', 'hi', '2019-11-11 15:42:20', '2019-11-11 15:46:04'),
(48, 37, 41, 'room4137', 'hi', '2019-11-11 15:42:20', '2019-11-11 15:46:04'),
(49, 37, 21, 'room3721', 'test', '2019-11-11 15:47:22', '2019-11-13 09:31:53'),
(50, 21, 37, 'room3721', 'test', '2019-11-11 15:47:22', '2019-11-13 09:31:53'),
(51, 37, 36, 'room3736', 'hi', '2019-11-11 16:32:00', '2019-11-11 16:32:02'),
(52, 36, 37, 'room3736', 'hi', '2019-11-11 16:32:00', '2019-11-11 16:32:02'),
(53, 21, 41, 'room2141', 'hi', '2019-11-12 06:46:58', '2019-11-12 13:27:02'),
(54, 41, 21, 'room2141', 'hi', '2019-11-12 06:46:58', '2019-11-12 13:27:02'),
(55, 21, 25, 'room2125', 'ghhh', '2019-11-12 06:58:39', '2019-11-12 12:11:25'),
(56, 25, 21, 'room2125', 'ghhh', '2019-11-12 06:58:39', '2019-11-12 12:11:25'),
(57, 41, 25, 'room4125', 'hfhfjcjfjf', '2019-11-12 07:08:14', '2019-11-12 09:37:28'),
(58, 25, 41, 'room4125', 'hfhfjcjfjf', '2019-11-12 07:08:14', '2019-11-12 09:37:28'),
(59, 25, 36, 'room2536', 'hi abhishek', '2019-11-12 07:31:42', '2019-11-12 08:45:25'),
(60, 36, 25, 'room2536', 'hi abhishek', '2019-11-12 07:31:42', '2019-11-12 08:45:25'),
(61, 25, 37, 'room2537', '\n', '2019-11-12 07:32:43', '2019-11-14 08:51:41'),
(62, 37, 25, 'room2537', '\n', '2019-11-12 07:32:43', '2019-11-14 08:51:41'),
(63, 19, 12, 'room1912', 'hope you are doing good', '2019-11-12 08:56:42', '2019-11-13 08:39:41'),
(64, 12, 19, 'room1912', 'hope you are doing good', '2019-11-12 08:56:42', '2019-11-13 08:39:41'),
(65, 19, 28, 'room1928', 'hello', '2019-11-12 08:56:54', '2019-11-13 07:19:21'),
(66, 28, 19, 'room1928', 'hello', '2019-11-12 08:56:54', '2019-11-13 07:19:21'),
(67, 19, 20, 'room1920', 'How funny how much', '2019-11-12 08:57:48', '2020-04-24 05:36:49'),
(68, 20, 19, 'room1920', 'How funny how much', '2019-11-12 08:57:48', '2020-04-24 05:36:49'),
(69, 37, 12, 'room3712', 'gi', '2019-11-12 11:05:06', '2019-11-13 08:04:14'),
(70, 12, 37, 'room3712', 'gi', '2019-11-12 11:05:06', '2019-11-13 08:04:14'),
(71, 41, 12, 'room4112', 'are', '2019-11-12 11:09:10', '2019-11-12 11:38:21'),
(72, 12, 41, 'room4112', 'are', '2019-11-12 11:09:10', '2019-11-12 11:38:21'),
(73, 41, 41, 'room4141', NULL, '2019-11-12 11:33:29', '2019-11-12 11:33:29'),
(74, 41, 41, 'room4141', NULL, '2019-11-12 11:33:29', '2019-11-12 11:33:29'),
(75, 21, 42, 'room2142', 'hey', '2019-11-12 12:14:44', '2019-11-12 12:14:47'),
(76, 42, 21, 'room2142', 'hey', '2019-11-12 12:14:44', '2019-11-12 12:14:47'),
(77, 41, 38, 'room4138', 'hey', '2019-11-12 12:22:37', '2019-11-12 12:22:44'),
(78, 38, 41, 'room4138', 'hey', '2019-11-12 12:22:37', '2019-11-12 12:22:44'),
(79, 41, 39, 'room4139', NULL, '2019-11-12 12:22:58', '2019-11-12 12:22:58'),
(80, 39, 41, 'room4139', NULL, '2019-11-12 12:22:58', '2019-11-12 12:22:58'),
(81, 21, 0, 'room210', NULL, '2019-11-12 12:55:18', '2019-11-12 12:55:18'),
(82, 0, 21, 'room210', NULL, '2019-11-12 12:55:18', '2019-11-12 12:55:18'),
(83, 19, 21, 'room1921', 'hii', '2019-11-13 07:33:55', '2019-11-13 09:32:19'),
(84, 21, 19, 'room1921', 'hii', '2019-11-13 07:33:55', '2019-11-13 09:32:19'),
(85, 37, 0, 'room370', NULL, '2019-11-13 07:57:00', '2019-11-13 07:57:00'),
(86, 0, 37, 'room370', NULL, '2019-11-13 07:57:00', '2019-11-13 07:57:00'),
(87, 12, 20, 'room1220', 'how you doing?', '2019-11-13 08:37:41', '2019-11-13 08:37:58'),
(88, 20, 12, 'room1220', 'how you doing?', '2019-11-13 08:37:41', '2019-11-13 08:37:58'),
(89, 12, 28, 'room1228', 'cool', '2019-11-13 08:39:15', '2019-11-13 08:50:11'),
(90, 28, 12, 'room1228', 'cool', '2019-11-13 08:39:15', '2019-11-13 08:50:11'),
(91, 25, 12, 'room2512', 'hey', '2019-11-14 08:49:31', '2019-11-15 07:14:40'),
(92, 12, 25, 'room2512', 'hey', '2019-11-14 08:49:31', '2019-11-15 07:14:40'),
(93, 48, 19, 'room4819', 'ok', '2019-11-18 16:09:50', '2019-11-18 16:12:05'),
(94, 19, 48, 'room4819', 'ok', '2019-11-18 16:09:50', '2019-11-18 16:12:05'),
(95, 20, 71, 'room2071', NULL, '2019-11-19 20:06:43', '2019-11-19 20:06:43'),
(96, 71, 20, 'room2071', NULL, '2019-11-19 20:06:43', '2019-11-19 20:06:43'),
(97, 51, 66, 'room5166', NULL, '2019-11-20 02:59:10', '2019-11-20 02:59:10'),
(98, 66, 51, 'room5166', NULL, '2019-11-20 02:59:10', '2019-11-20 02:59:10'),
(99, 51, 59, 'room5159', 'test', '2019-11-20 03:02:22', '2019-11-20 03:02:25'),
(100, 59, 51, 'room5159', 'test', '2019-11-20 03:02:22', '2019-11-20 03:02:25'),
(101, 72, 20, 'room7220', 'Hi Ricardo. once we have the confirmation from speakers for sharing the presentation. I will send it to you', '2019-11-21 19:47:46', '2019-11-21 21:45:16'),
(102, 20, 72, 'room7220', 'Hi Ricardo. once we have the confirmation from speakers for sharing the presentation. I will send it to you', '2019-11-21 19:47:46', '2019-11-21 21:45:16'),
(103, 19, 108, 'room19108', 'hy', '2019-11-27 10:01:39', '2019-11-27 10:01:41'),
(104, 108, 19, 'room19108', 'hy', '2019-11-27 10:01:39', '2019-11-27 10:01:41'),
(105, 127, 132, 'room127132', 'Some important info:\nexhibtors can move in from 3pm onward and till 8pm. i request you to plan it accordingly, i also know that we had communicated from 6pm onward. so, here i would appreciate if you finish by 8pm.', '2019-11-30 10:10:10', '2019-11-30 13:38:23'),
(106, 132, 127, 'room127132', 'Some important info:\nexhibtors can move in from 3pm onward and till 8pm. i request you to plan it accordingly, i also know that we had communicated from 6pm onward. so, here i would appreciate if you finish by 8pm.', '2019-11-30 10:10:10', '2019-11-30 13:38:23'),
(107, 162, 127, 'room162127', 'today at 8:00 am', '2019-12-02 21:53:41', '2019-12-03 04:47:32'),
(108, 127, 162, 'room162127', 'today at 8:00 am', '2019-12-02 21:53:41', '2019-12-03 04:47:32'),
(109, 174, 20, 'room17420', 'I am fine. How about you?', '2020-04-24 07:05:37', '2020-04-26 05:06:12'),
(110, 20, 174, 'room17420', 'I am fine. How about you?', '2020-04-24 07:05:37', '2020-04-26 05:06:12'),
(111, 175, 20, 'room17520', 'Are we meeting', '2020-04-24 12:28:23', '2020-04-26 05:18:46'),
(112, 20, 175, 'room17520', 'Are we meeting', '2020-04-24 12:28:23', '2020-04-26 05:18:46'),
(113, 176, 174, 'room176174', 'Hi', '2020-04-24 19:28:48', '2020-04-24 19:28:50'),
(114, 174, 176, 'room176174', 'Hi', '2020-04-24 19:28:48', '2020-04-24 19:28:50'),
(115, 176, 19, 'room17619', 'Hello', '2020-04-24 19:29:07', '2020-04-24 19:29:10'),
(116, 19, 176, 'room17619', 'Hello', '2020-04-24 19:29:07', '2020-04-24 19:29:10'),
(117, 177, 176, 'room177176', 'good', '2020-04-24 23:28:55', '2020-04-24 23:30:09'),
(118, 176, 177, 'room177176', 'good', '2020-04-24 23:28:55', '2020-04-24 23:30:09'),
(119, 19, 174, 'room19174', 'hy', '2020-04-25 07:03:43', '2020-04-25 07:03:45'),
(120, 174, 19, 'room19174', 'hy', '2020-04-25 07:03:43', '2020-04-25 07:03:45');

-- --------------------------------------------------------

--
-- Table structure for table `comment_to_tagged_user`
--

CREATE TABLE `comment_to_tagged_user` (
  `id` int(11) NOT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `friend_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `commment_to_like`
--

CREATE TABLE `commment_to_like` (
  `id` int(11) NOT NULL,
  `comment_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `is_liked` enum('Y','N') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commment_to_like`
--

INSERT INTO `commment_to_like` (`id`, `comment_id`, `user_id`, `is_liked`, `created_at`, `updated_at`) VALUES
(1, 61, 18, 'Y', '2020-07-15 07:16:38', '2020-07-15 07:16:38'),
(2, 63, 18, 'Y', '2020-07-15 09:02:21', '2020-07-15 09:02:27'),
(3, 64, 18, 'Y', '2020-07-15 09:02:29', '2020-07-15 09:02:29'),
(4, 65, 18, 'Y', '2020-07-15 09:02:31', '2020-07-15 09:02:31'),
(5, 66, 18, 'Y', '2020-07-15 09:02:33', '2020-07-15 09:02:33'),
(6, 67, 18, 'Y', '2020-07-15 09:02:35', '2020-07-15 09:02:35'),
(7, 70, 18, 'Y', '2020-07-15 09:04:05', '2020-07-15 09:04:05'),
(10, 75, 156, 'Y', '2020-07-21 08:50:43', '2020-07-21 08:50:43'),
(11, 76, 156, 'Y', '2020-07-21 08:55:28', '2020-07-21 09:04:15'),
(12, 84, 156, 'Y', '2020-07-21 09:04:07', '2020-07-21 09:04:07'),
(13, 83, 156, 'Y', '2020-07-21 09:04:07', '2020-07-21 09:04:07'),
(14, 82, 156, 'Y', '2020-07-21 09:04:07', '2020-07-21 09:04:07'),
(15, 81, 156, 'Y', '2020-07-21 09:04:08', '2020-07-21 09:04:21'),
(16, 78, 156, 'Y', '2020-07-21 09:04:09', '2020-07-21 09:04:22'),
(17, 79, 156, 'Y', '2020-07-21 09:04:10', '2020-07-21 09:04:21'),
(18, 80, 156, 'Y', '2020-07-21 09:04:11', '2020-07-21 09:04:21'),
(19, 85, 156, 'Y', '2020-07-21 09:04:12', '2020-07-21 09:04:12'),
(20, 77, 156, 'Y', '2020-07-21 09:04:17', '2020-07-21 09:04:17'),
(22, 90, 18, 'N', '2020-07-23 11:07:52', '2020-07-23 11:07:53'),
(23, 89, 18, 'Y', '2020-07-23 11:07:54', '2020-07-23 11:07:54'),
(24, 146, 18, 'Y', '2020-08-06 08:06:58', '2020-08-06 08:07:46'),
(25, 151, 103, 'Y', '2020-08-10 05:24:43', '2020-08-10 05:24:43'),
(26, 149, 103, 'Y', '2020-08-10 05:36:42', '2020-08-10 05:36:44'),
(27, 150, 103, 'Y', '2020-08-10 05:36:56', '2020-08-10 05:36:56'),
(28, 156, 77781, 'Y', '2020-08-14 10:42:24', '2020-08-14 10:42:24'),
(29, 165, 103, 'Y', '2020-08-20 11:06:07', '2020-08-20 11:06:07');

-- --------------------------------------------------------

--
-- Table structure for table `cookedit_gamification`
--

CREATE TABLE `cookedit_gamification` (
  `id` int(11) NOT NULL,
  `name` text,
  `image` text,
  `no_of_i_cokked_it` bigint(20) NOT NULL DEFAULT '0',
  `level` bigint(20) NOT NULL DEFAULT '0',
  `is_top` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cookedit_gamification`
--

INSERT INTO `cookedit_gamification` (`id`, `name`, `image`, `no_of_i_cokked_it`, `level`, `is_top`, `created_at`, `updated_at`) VALUES
(2, '1st I Cooked It', 'USA3jRktiwRK-1597295054.gif', 1, 1, 'N', '2020-08-13 05:04:14', '2020-08-13 05:04:14'),
(3, '3 I Cooked it', 'oTrEEIVGqFPC-1597295088.gif', 3, 2, 'N', '2020-08-13 05:04:48', '2020-08-13 05:04:48'),
(4, '5 I Cooked it', 'M19F05VmSkVO-1597295109.gif', 5, 3, 'N', '2020-08-13 05:05:09', '2020-08-13 05:05:09'),
(5, '10 I Cooked it', 'DyWkprR5c3l0-1597295132.gif', 10, 4, 'N', '2020-08-13 05:05:32', '2020-08-13 05:05:32'),
(6, '15 I Cooked it', 'J6WYU22z4xRN-1597295149.gif', 15, 5, 'N', '2020-08-13 05:05:49', '2020-08-13 05:05:49');

-- --------------------------------------------------------

--
-- Table structure for table `cooked_it`
--

CREATE TABLE `cooked_it` (
  `id` int(11) NOT NULL,
  `post_id` bigint(20) NOT NULL DEFAULT '0',
  `recipie_id` bigint(20) NOT NULL DEFAULT '0',
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cooked_it`
--

INSERT INTO `cooked_it` (`id`, `post_id`, `recipie_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 10, 9, NULL, NULL),
(4, 10, 4, 10, '2020-04-16 08:48:13', '2020-04-16 08:48:13'),
(5, 227, 7, 146, '2020-08-18 13:42:13', '2020-08-18 13:42:13');

-- --------------------------------------------------------

--
-- Table structure for table `description_to_tagged_user`
--

CREATE TABLE `description_to_tagged_user` (
  `id` int(11) NOT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `friend_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `freind_gamification`
--

CREATE TABLE `freind_gamification` (
  `id` int(11) NOT NULL,
  `name` text,
  `image` text,
  `no_of_freinds` bigint(20) NOT NULL DEFAULT '0',
  `level` bigint(20) NOT NULL DEFAULT '0',
  `is_top` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `freind_gamification`
--

INSERT INTO `freind_gamification` (`id`, `name`, `image`, `no_of_freinds`, `level`, `is_top`, `created_at`, `updated_at`) VALUES
(2, 'First Friend Added', 'CJdEfGnq57rs-1597210852.png', 1, 1, 'N', '2020-08-12 05:40:52', '2020-08-12 05:40:52'),
(3, '10 Friends added', 'qMbkNdxLwRbM-1597211008.png', 10, 2, 'N', '2020-08-12 05:43:28', '2020-08-12 05:43:28'),
(4, '25 Friends added', 'aEfRyIQ32DxS-1597211047.png', 25, 3, 'N', '2020-08-12 05:44:07', '2020-08-12 05:44:07'),
(5, '50 Friends added', 'KF7dJvsDOuc2-1597211327.png', 50, 4, 'N', '2020-08-12 05:48:47', '2020-08-12 05:48:47');

-- --------------------------------------------------------

--
-- Table structure for table `ingredients`
--

CREATE TABLE `ingredients` (
  `id` int(11) NOT NULL,
  `name` text,
  `image` text,
  `is_brand` enum('Y','N') NOT NULL DEFAULT 'N',
  `brand_id` bigint(20) NOT NULL DEFAULT '0',
  `status` enum('A','I','D') DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ingredients`
--

INSERT INTO `ingredients` (`id`, `name`, `image`, `is_brand`, `brand_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cofee', 'o1gfu5ygfRY5-1583425049.jpg', 'Y', 3, 'A', '2020-03-05 10:47:28', '2020-03-05 21:59:55'),
(2, 'Egg', 'Zos8c8Yi6Cp6-1583466286.jpg', 'N', 0, 'A', '2020-03-05 21:58:11', '2020-03-05 22:16:32');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `body` mediumtext,
  `type` varchar(100) DEFAULT NULL,
  `type_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `title`, `body`, `type`, `type_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:01', '2020-08-17 04:05:01'),
(2, 9, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:01', '2020-08-17 04:05:01'),
(3, 10, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:01', '2020-08-17 04:05:01'),
(4, 11, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:01', '2020-08-17 04:05:01'),
(5, 12, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:01', '2020-08-17 04:05:01'),
(6, 13, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:01', '2020-08-17 04:05:01'),
(7, 14, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:02', '2020-08-17 04:05:02'),
(8, 15, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:02', '2020-08-17 04:05:02'),
(9, 16, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:02', '2020-08-17 04:05:02'),
(10, 18, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:02', '2020-08-17 04:05:02'),
(11, 19, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:02', '2020-08-17 04:05:02'),
(12, 21, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:02', '2020-08-17 04:05:02'),
(13, 22, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:02', '2020-08-17 04:05:02'),
(14, 23, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:03', '2020-08-17 04:05:03'),
(15, 24, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:03', '2020-08-17 04:05:03'),
(16, 25, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:03', '2020-08-17 04:05:03'),
(17, 26, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:03', '2020-08-17 04:05:03'),
(18, 27, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:03', '2020-08-17 04:05:03'),
(19, 28, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:03', '2020-08-17 04:05:03'),
(20, 29, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:03', '2020-08-17 04:05:03'),
(21, 30, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:03', '2020-08-17 04:05:03'),
(22, 31, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:03', '2020-08-17 04:05:03'),
(23, 32, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:03', '2020-08-17 04:05:03'),
(24, 33, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:04', '2020-08-17 04:05:04'),
(25, 34, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:04', '2020-08-17 04:05:04'),
(26, 37, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:04', '2020-08-17 04:05:04'),
(27, 38, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:04', '2020-08-17 04:05:04'),
(28, 39, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:04', '2020-08-17 04:05:04'),
(29, 40, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:04', '2020-08-17 04:05:04'),
(30, 41, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:04', '2020-08-17 04:05:04'),
(31, 43, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:04', '2020-08-17 04:05:04'),
(32, 44, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:05', '2020-08-17 04:05:05'),
(33, 45, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:05', '2020-08-17 04:05:05'),
(34, 46, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:05', '2020-08-17 04:05:05'),
(35, 47, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:05', '2020-08-17 04:05:05'),
(36, 48, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:05', '2020-08-17 04:05:05'),
(37, 49, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:05', '2020-08-17 04:05:05'),
(38, 50, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:05', '2020-08-17 04:05:05'),
(39, 51, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:05', '2020-08-17 04:05:05'),
(40, 52, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:05', '2020-08-17 04:05:05'),
(41, 53, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:05', '2020-08-17 04:05:05'),
(42, 54, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:06', '2020-08-17 04:05:06'),
(43, 55, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:06', '2020-08-17 04:05:06'),
(44, 56, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:06', '2020-08-17 04:05:06'),
(45, 57, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:06', '2020-08-17 04:05:06'),
(46, 58, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:06', '2020-08-17 04:05:06'),
(47, 59, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:06', '2020-08-17 04:05:06'),
(48, 60, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:06', '2020-08-17 04:05:06'),
(49, 61, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:06', '2020-08-17 04:05:06'),
(50, 62, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:06', '2020-08-17 04:05:06'),
(51, 63, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:07', '2020-08-17 04:05:07'),
(52, 64, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:07', '2020-08-17 04:05:07'),
(53, 65, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:07', '2020-08-17 04:05:07'),
(54, 66, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:07', '2020-08-17 04:05:07'),
(55, 67, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:07', '2020-08-17 04:05:07'),
(56, 68, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:07', '2020-08-17 04:05:07'),
(57, 69, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:07', '2020-08-17 04:05:07'),
(58, 70, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:07', '2020-08-17 04:05:07'),
(59, 71, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:07', '2020-08-17 04:05:07'),
(60, 72, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:07', '2020-08-17 04:05:07'),
(61, 73, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:08', '2020-08-17 04:05:08'),
(62, 76, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:08', '2020-08-17 04:05:08'),
(63, 77, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:08', '2020-08-17 04:05:08'),
(64, 78, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:08', '2020-08-17 04:05:08'),
(65, 79, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:08', '2020-08-17 04:05:08'),
(66, 80, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:08', '2020-08-17 04:05:08'),
(67, 81, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:08', '2020-08-17 04:05:08'),
(68, 82, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:08', '2020-08-17 04:05:08'),
(69, 83, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:09', '2020-08-17 04:05:09'),
(70, 84, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:09', '2020-08-17 04:05:09'),
(71, 85, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:09', '2020-08-17 04:05:09'),
(72, 86, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:09', '2020-08-17 04:05:09'),
(73, 87, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:09', '2020-08-17 04:05:09'),
(74, 88, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:09', '2020-08-17 04:05:09'),
(75, 90, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:09', '2020-08-17 04:05:09'),
(76, 91, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:09', '2020-08-17 04:05:09'),
(77, 92, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:09', '2020-08-17 04:05:09'),
(78, 93, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:09', '2020-08-17 04:05:09'),
(79, 95, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:10', '2020-08-17 04:05:10'),
(80, 96, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:10', '2020-08-17 04:05:10'),
(81, 103, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:10', '2020-08-17 04:05:10'),
(82, 105, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:10', '2020-08-17 04:05:10'),
(83, 107, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:10', '2020-08-17 04:05:10'),
(84, 108, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:10', '2020-08-17 04:05:10'),
(85, 109, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:10', '2020-08-17 04:05:10'),
(86, 111, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:11', '2020-08-17 04:05:11'),
(87, 112, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:11', '2020-08-17 04:05:11'),
(88, 118, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:11', '2020-08-17 04:05:11'),
(89, 124, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:11', '2020-08-17 04:05:11'),
(90, 126, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:11', '2020-08-17 04:05:11'),
(91, 127, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:11', '2020-08-17 04:05:11'),
(92, 128, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:11', '2020-08-17 04:05:11'),
(93, 129, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:11', '2020-08-17 04:05:11'),
(94, 130, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:11', '2020-08-17 04:05:11'),
(95, 131, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:11', '2020-08-17 04:05:11'),
(96, 132, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:11', '2020-08-17 04:05:11'),
(97, 133, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:12', '2020-08-17 04:05:12'),
(98, 134, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:12', '2020-08-17 04:05:12'),
(99, 135, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:12', '2020-08-17 04:05:12'),
(100, 136, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:12', '2020-08-17 04:05:12'),
(101, 137, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:12', '2020-08-17 04:05:12'),
(102, 138, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:12', '2020-08-17 04:05:12'),
(103, 139, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:12', '2020-08-17 04:05:12'),
(104, 140, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:12', '2020-08-17 04:05:12'),
(105, 141, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:12', '2020-08-17 04:05:12'),
(106, 146, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:12', '2020-08-17 04:05:12'),
(107, 147, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:13', '2020-08-17 04:05:13'),
(108, 148, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:13', '2020-08-17 04:05:13'),
(109, 149, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:13', '2020-08-17 04:05:13'),
(110, 151, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:13', '2020-08-17 04:05:13'),
(111, 152, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:13', '2020-08-17 04:05:13'),
(112, 153, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:13', '2020-08-17 04:05:13'),
(113, 154, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:13', '2020-08-17 04:05:13'),
(114, 155, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:13', '2020-08-17 04:05:13'),
(115, 156, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:13', '2020-08-17 04:05:13'),
(116, 158, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:13', '2020-08-17 04:05:13'),
(117, 159, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:13', '2020-08-17 04:05:13'),
(118, 160, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:14', '2020-08-17 04:05:14'),
(119, 161, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:14', '2020-08-17 04:05:14'),
(120, 77777, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:14', '2020-08-17 04:05:14'),
(121, 77778, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:14', '2020-08-17 04:05:14'),
(122, 77779, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:14', '2020-08-17 04:05:14'),
(123, 77780, 'Fsquad', '1', 'random', 0, 1, '2020-08-17 04:05:14', '2020-08-17 04:05:14'),
(124, 77781, 'Fsquad', '0', 'random', 0, 1, '2020-08-17 04:05:14', '2020-08-17 04:05:14');

-- --------------------------------------------------------

--
-- Table structure for table `notification_badges`
--

CREATE TABLE `notification_badges` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `body` varchar(250) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `description` text,
  `hash_tags` text,
  `total_like` bigint(20) NOT NULL DEFAULT '0',
  `total_comments` bigint(20) NOT NULL DEFAULT '0',
  `total_share` bigint(20) NOT NULL DEFAULT '0',
  `total_wanna_visit` bigint(20) NOT NULL DEFAULT '0',
  `total_cooked_it` bigint(20) NOT NULL DEFAULT '0',
  `is_i_cooked_it` enum('Y','N') NOT NULL DEFAULT 'N',
  `location` text,
  `latitude` text,
  `longitude` text,
  `recipe_type` enum('MANUAL','VIDEO','TAGGED','NONE') DEFAULT NULL,
  `is_post_private` enum('Y','N') NOT NULL DEFAULT 'N',
  `viewer_is_friend` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_my_post` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_visible_by_me` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_commented_by_me` enum('Y','N') NOT NULL DEFAULT 'N',
  `last_comment` text,
  `is_liked_by_me` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `user_id`, `description`, `hash_tags`, `total_like`, `total_comments`, `total_share`, `total_wanna_visit`, `total_cooked_it`, `is_i_cooked_it`, `location`, `latitude`, `longitude`, `recipe_type`, `is_post_private`, `viewer_is_friend`, `is_my_post`, `is_visible_by_me`, `is_commented_by_me`, `last_comment`, `is_liked_by_me`, `created_at`, `updated_at`) VALUES
(1, 9, 'This is the first step', '#new_trends', 1, 2, 0, 0, 0, 'N', 'Kolkata', NULL, NULL, 'NONE', 'Y', 'N', 'N', 'N', 'N', 'Awesome Preparation', 'N', '2020-04-10 08:05:40', '2020-04-16 08:28:21'),
(2, 9, 'This is the first step', '#new_trends', 1, 0, 0, 0, 0, 'N', 'Kolkata', NULL, NULL, 'NONE', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-04-10 08:06:54', '2020-05-25 15:20:30'),
(3, 9, 'This is the first step video', '#covid-19', 0, 0, 0, 0, 0, 'N', 'Kolkata', NULL, NULL, 'NONE', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-04-10 08:07:26', '2020-04-10 08:07:26'),
(4, 9, 'This is the first step video', '#covid-19', 1, 0, 0, 0, 0, 'Y', 'Kolkata', NULL, NULL, 'VIDEO', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-04-10 08:13:12', '2020-05-25 16:11:22'),
(5, 9, 'This is the first step video', '#stay_home_stay_safe', 1, 0, 0, 0, 0, 'Y', 'Kolkata', NULL, NULL, 'TAGGED', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-04-10 09:23:08', '2020-05-15 13:28:51'),
(6, 9, 'This is the first step video', '#stay_home_stay_safe', 1, 0, 0, 0, 0, 'Y', 'Kolkata', NULL, NULL, 'TAGGED', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-04-10 09:24:35', '2020-05-25 18:29:34'),
(7, 9, 'This is the first step video', '#stay_home_cook_healthy', 1, 0, 1, 0, 0, 'Y', 'Kolkata', NULL, NULL, 'TAGGED', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-04-10 09:25:20', '2020-05-26 01:01:40'),
(8, 9, 'This is the first step video', '#stay_home_cook_healthy', 0, 0, 1, 0, 0, 'Y', 'Kolkata', NULL, NULL, 'MANUAL', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-04-10 09:53:06', '2020-05-16 06:50:15'),
(9, 9, 'This is the first step video', '#stay_home_cook_healthy', 1, 0, 0, 0, 0, 'Y', 'Kolkata', NULL, NULL, 'MANUAL', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-04-10 09:59:20', '2020-05-26 06:05:23'),
(10, 9, 'This is the first step video', '#be_qurantined_be_a_warior', 0, 0, 0, 0, 3, 'Y', 'Kolkata', NULL, NULL, 'MANUAL', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-04-10 10:33:24', '2020-04-16 08:48:13'),
(11, 10, 'This is the first step video', '#be_qurantined_be_a_warior', 0, 0, 1, 0, 0, 'Y', 'Kolkata', NULL, NULL, 'MANUAL', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-04-11 09:58:28', '2020-05-16 06:48:48'),
(12, 10, 'This is the Second video for user id 3', '#stay_home_stay_safe', 1, 1, 1, 0, 0, 'Y', 'Kolkata', NULL, NULL, 'MANUAL', 'Y', 'N', 'N', 'N', 'N', 'nyc', 'N', '2020-04-11 09:58:52', '2020-05-15 18:52:57'),
(13, 26, 'dshfuskdhfiush', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-15 08:56:15', '2020-05-15 08:56:15'),
(14, 30, 'Hello..Rana', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-15 14:50:52', '2020-05-15 14:50:52'),
(15, 31, 'hello Rana', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-15 14:59:38', '2020-05-15 14:59:38'),
(17, 40, 'Testing', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-16 06:05:43', '2020-05-20 11:50:27'),
(18, 40, 'hi', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-16 07:32:53', '2020-05-16 08:01:09'),
(19, 40, 'multiple image testing', NULL, 3, 0, 1, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-16 07:36:42', '2020-05-26 06:05:03'),
(20, 40, 'Ok', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-16 08:00:53', '2020-05-16 08:01:20'),
(21, 40, 'Hello All', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-16 08:03:15', '2020-05-16 08:03:15'),
(22, 40, 'set', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-16 08:40:45', '2020-05-16 08:40:45'),
(23, 40, 'ghfghfg', NULL, 0, 0, 1, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-16 08:51:35', '2020-05-18 13:51:52'),
(24, 46, 'hjkhkjhj', NULL, 2, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-19 16:08:03', '2020-06-05 07:13:48'),
(25, 46, 'Yo', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-20 05:11:52', '2020-05-30 19:30:27'),
(26, 52, 'Test', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-20 13:01:47', '2020-06-05 07:16:15'),
(27, 54, 'Testing', NULL, 1, 2, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Giods', 'N', '2020-05-20 15:12:51', '2020-06-05 07:16:09'),
(28, 54, 'Testing', NULL, 2, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-20 15:12:54', '2020-06-05 07:15:16'),
(29, 54, 'Testing', NULL, 2, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-20 15:28:35', '2020-06-05 07:15:09'),
(30, 54, 'Testing', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-20 15:28:36', '2020-06-05 07:15:04'),
(31, 54, 'Test', NULL, 1, 1, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Testing', 'N', '2020-05-20 15:31:01', '2020-06-05 07:14:55'),
(32, 73, 'Description of post', NULL, 1, 2, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Huiiiiisvsg', 'N', '2020-05-21 07:42:25', '2020-05-22 06:28:41'),
(33, 73, 'Description of post', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-21 07:42:36', '2020-05-22 05:53:23'),
(34, 78, 'Testing', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-22 07:27:09', '2020-06-05 07:17:29'),
(35, 78, 'Testung', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-22 07:28:21', '2020-06-01 05:30:34'),
(39, 81, 'testing', NULL, 1, 1, 1, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'green', 'N', '2020-05-22 07:59:44', '2020-05-23 22:59:42'),
(40, 90, 'Fhhh', NULL, 0, 1, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Uhello', 'N', '2020-05-23 23:21:32', '2020-05-25 15:18:19'),
(41, 90, 'Gggh iiii uii iikk iikk iikk iikk jj ki jkkk iikk iikk iikk iikk ikk niij ikkj ikkk iikk iiio iioo iiiio iioo iooo iooo ioook iiokkkkk ikkkk iikii injji jiooi ikkkk ookkkkkll kkkkl kkkk nokkjj ikkkkniikk kkk kkk kkkk kkkk kkkk kkkk kikkk kkoo kkkk ikkkk kkkk kkk', NULL, 2, 4, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Vhjjhhj', 'N', '2020-05-24 00:13:26', '2020-06-05 07:17:16'),
(42, 18, 'fjhhdfhhf', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-25 07:57:25', '2020-05-25 08:13:55'),
(43, 18, 'fjhhdfhhf', NULL, 1, 0, 1, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-25 07:57:32', '2020-05-25 08:13:47'),
(44, 18, 'fjhhdfhhf', NULL, 1, 1, 2, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'huuuj', 'N', '2020-05-25 08:00:07', '2020-06-05 07:17:05'),
(45, 18, 'fjhhdfhhf', NULL, 0, 0, 3, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-25 08:04:20', '2020-05-28 08:28:17'),
(53, 52, 'Testing', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-29 18:56:18', '2020-05-29 18:56:18'),
(54, 52, 'Testing', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-29 18:57:05', '2020-05-29 18:57:05'),
(55, 52, 'Testing', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-29 18:57:24', '2020-05-29 18:57:24'),
(56, 52, 'Testing', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-29 18:57:58', '2020-05-29 18:57:58'),
(57, 52, 'Testing', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-05-29 18:59:48', '2020-05-29 18:59:48'),
(58, 105, 'Hhhjj', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-04 10:19:32', '2020-06-04 10:25:44'),
(59, 105, 'Fhhgvh', NULL, 1, 1, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Hhhhh', 'N', '2020-06-04 10:28:20', '2020-06-05 06:03:03'),
(60, 105, 'Dbjdjd', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-05 07:11:12', '2020-06-05 07:11:12'),
(61, 105, 'Fgghh', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-05 07:36:31', '2020-06-05 10:07:45'),
(62, 105, '', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-05 10:07:20', '2020-06-11 07:21:57'),
(63, 105, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-05 10:07:23', '2020-06-05 10:07:23'),
(64, 105, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-05 12:09:15', '2020-06-05 12:09:15'),
(71, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-08 11:42:48', '2020-06-08 11:42:48'),
(72, 18, 'Ghjbgg', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-10 04:57:49', '2020-06-10 04:57:49'),
(73, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-10 04:58:49', '2020-06-10 04:58:49'),
(74, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-10 05:15:35', '2020-06-10 05:15:35'),
(75, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-10 05:16:54', '2020-06-10 05:16:54'),
(76, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-10 05:16:54', '2020-06-10 05:16:54'),
(77, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-10 05:39:50', '2020-06-10 05:39:50'),
(78, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-10 05:40:25', '2020-06-10 05:40:25'),
(79, 107, 'Thats my testing post', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-16 06:29:20', '2020-06-16 06:29:20'),
(80, 107, 'Thats my testing post', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-16 06:30:12', '2020-06-16 06:30:12'),
(81, 107, 'Thats my testing post', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-16 06:30:26', '2020-06-16 06:30:26'),
(82, 107, 'Thats my testing post', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-16 06:30:36', '2020-06-16 06:30:36'),
(83, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-16 10:15:34', '2020-06-16 10:15:34'),
(84, 18, 'My post', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-16 10:15:53', '2020-06-16 10:15:53'),
(85, 18, '', NULL, 1, 1, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Hhhh', 'N', '2020-06-16 10:16:35', '2020-06-18 09:39:17'),
(86, 18, 'This is my tagged user post', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-17 11:24:22', '2020-06-17 11:24:22'),
(87, 18, 'This is my post', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-17 11:28:31', '2020-06-17 11:28:31'),
(88, 18, 'Again post data', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-17 11:38:38', '2020-06-17 11:38:38'),
(89, 18, 'Again post data', NULL, 0, 0, 0, 0, 0, 'N', 'Indore', '35.377327', '7232737.88', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-17 12:01:54', '2020-06-17 12:01:54'),
(90, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-17 12:08:40', '2020-06-17 12:08:40'),
(91, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-17 12:18:24', '2020-06-17 12:18:24'),
(92, 18, '', NULL, 1, 0, 1, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-17 12:19:13', '2020-06-19 10:02:28'),
(93, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-17 12:20:01', '2020-06-17 12:20:01'),
(94, 18, '', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-17 12:27:18', '2020-06-17 13:57:27'),
(95, 74, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text,', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-18 08:18:35', '2020-06-18 08:18:35'),
(96, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-18 09:07:23', '2020-06-18 09:07:23'),
(97, 18, '', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-18 09:08:34', '2020-06-18 11:38:21'),
(98, 109, 'There are many official news websites available these days. I read the blogs in most of them. All are much similar and the source of information looks somilar.', NULL, 1, 15, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Hhhhh', 'N', '2020-06-18 11:05:38', '2020-06-18 12:58:32'),
(100, 103, '', NULL, 0, 3, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'hello', 'N', '2020-06-19 08:29:30', '2020-06-19 08:57:51'),
(101, 103, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-19 08:57:20', '2020-06-19 08:57:20'),
(102, 103, 'hi', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-19 09:01:20', '2020-06-19 10:53:15'),
(103, 109, 'Hello there idjdj diye did sks sisbs in wow sosb susbsbsud didi diddv did sks sos did doshs sowgs siwyyqid sosnab aisgdbd aooa kb d osbs sisusb doshsbdks dosns slks ss', NULL, 1, 3, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Bdkdjde', 'N', '2020-06-19 09:41:45', '2020-06-19 10:53:08'),
(104, 109, 'Well hello migantariddim', NULL, 1, 2, 1, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'There', 'N', '2020-06-19 10:50:43', '2020-06-21 14:06:21'),
(105, 109, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-19 11:06:53', '2020-06-19 11:06:53'),
(106, 109, 'Full moon', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-19 11:10:53', '2020-06-20 02:20:52'),
(107, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-19 12:40:12', '2020-06-19 12:40:12'),
(108, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-19 12:50:14', '2020-06-19 12:50:14'),
(109, 18, '', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-19 12:59:30', '2020-06-20 02:21:03'),
(110, 109, 'Gh', NULL, 0, 1, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'hello', 'N', '2020-06-19 13:02:14', '2020-06-19 15:41:57'),
(111, 74, '', NULL, 1, 1, 4, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Hello', 'N', '2020-06-19 13:36:15', '2020-06-21 14:11:49'),
(118, 123, 'ironman suite recipe', NULL, 2, 1, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'Y', 'N', 'N', 'N', 'N', 'G', 'N', '2020-06-21 14:25:08', '2020-07-01 09:12:26'),
(119, 123, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-21 14:28:12', '2020-06-21 14:28:12'),
(120, 115, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-06-22 13:48:51', '2020-06-24 09:31:26'),
(121, 115, '', NULL, 1, 3, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Hello', 'N', '2020-06-22 13:58:23', '2020-07-01 09:14:35'),
(122, 141, 'Zsdsdsd', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-02 05:08:55', '2020-07-02 05:08:55'),
(123, 141, 'Ssssss', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-02 05:11:06', '2020-07-02 05:11:06'),
(124, 141, 'Dddddd', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-02 05:11:33', '2020-07-20 04:39:20'),
(125, 141, 'De ccccc ccccc ccccc ccccc ccccc', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-02 05:12:41', '2020-07-02 05:12:41'),
(126, 141, 'Nnnnnnn', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-02 08:22:29', '2020-07-07 02:56:11'),
(127, 141, 'Yyyyyy gggg', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-02 08:49:05', '2020-07-15 21:55:44'),
(130, 18, 'props.description.length > 150 props.description.length > 150 props.description.length > 150 props.description.length > 150 props.description .length > 150 props.description.length > 150 props.description.length > 150 props.description.length > 150', NULL, 3, 15, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'L', 'N', '2020-07-06 06:36:13', '2020-07-20 06:14:18'),
(132, 18, '', NULL, 0, 1, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'hhh', 'N', '2020-07-13 07:27:20', '2020-07-15 05:20:27'),
(133, 18, 'Ffffffff', NULL, 0, 1, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'hekl', 'N', '2020-07-13 09:11:04', '2020-07-20 06:14:37'),
(134, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-13 09:17:38', '2020-08-03 14:39:13'),
(140, 156, '', NULL, 1, 1, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Hdhdjd', 'N', '2020-07-21 06:15:12', '2020-07-21 06:34:19'),
(141, 156, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-21 06:35:04', '2020-07-21 06:35:04'),
(142, 156, 'No description here why you want a description for these images. Only show how to publish', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-21 08:36:16', '2020-07-21 08:36:16'),
(143, 156, '', NULL, 0, 17, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'New3', 'N', '2020-07-21 08:39:34', '2020-07-31 13:53:01'),
(144, 156, '', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-21 09:11:05', '2020-07-22 04:39:42'),
(146, 109, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-21 10:46:01', '2020-07-21 10:46:01'),
(148, 152, '', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-21 10:55:11', '2020-07-21 12:17:37'),
(149, 109, '', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-21 10:56:20', '2020-08-01 16:52:04'),
(150, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-21 11:05:52', '2020-07-21 11:05:52'),
(151, 18, '', NULL, 3, 2, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'hello', 'N', '2020-07-21 11:06:33', '2020-07-23 11:08:10'),
(154, 18, '', NULL, 2, 2, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Check check check', 'N', '2020-07-23 10:54:11', '2020-08-01 16:54:44'),
(161, 160, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-27 14:47:44', '2020-07-27 14:47:44'),
(162, 161, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-27 15:01:32', '2020-07-27 15:01:32'),
(163, 161, 'Description #tags #works wells ?', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-27 15:22:36', '2020-07-27 15:22:36'),
(173, 77778, '', NULL, 0, 11, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'Y', 'N', 'N', 'N', 'N', 'Ndhxyhdxudhebd', 'N', '2020-07-30 22:09:39', '2020-07-31 00:32:39'),
(174, 77778, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-07-31 00:37:47', '2020-07-31 00:37:47'),
(175, 77779, 'Nice meal fo the day', NULL, 2, 2, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Cool!!!', 'N', '2020-07-31 09:49:10', '2020-08-01 16:45:42'),
(176, 77779, 'Cooking is always fun', NULL, 1, 14, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Dhghh', 'N', '2020-07-31 09:49:53', '2020-08-15 19:07:31'),
(177, 77779, '', NULL, 0, 2, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Hi', 'N', '2020-08-01 23:36:58', '2020-08-06 08:10:48'),
(178, 160, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-01 23:41:36', '2020-08-01 23:41:36'),
(179, 160, '', NULL, 0, 2, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'Y', 'N', 'N', 'N', 'N', 'Hello', 'N', '2020-08-04 15:06:08', '2020-08-06 10:08:04'),
(180, 77779, '', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-08 10:33:54', '2020-08-08 10:38:28'),
(181, 77779, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-08 10:48:18', '2020-08-08 10:48:18'),
(182, 77779, '', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-09 00:14:22', '2020-08-10 12:30:41'),
(183, 77779, '', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-09 00:21:21', '2020-08-10 12:30:34'),
(184, 77779, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-09 01:06:14', '2020-08-09 01:06:14'),
(185, 77779, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-09 01:40:01', '2020-08-09 01:40:01'),
(186, 77779, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-09 01:45:05', '2020-08-09 01:45:05'),
(187, 77779, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-09 01:46:02', '2020-08-09 01:46:02'),
(188, 77779, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-09 01:47:56', '2020-08-09 01:47:56'),
(189, 77779, '', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-09 01:56:32', '2020-08-15 18:55:51'),
(190, 77779, '', NULL, 1, 2, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'lll', 'N', '2020-08-09 02:44:05', '2020-08-11 06:37:53'),
(191, 160, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-10 17:04:31', '2020-08-10 17:04:31'),
(192, 103, 'ncbfhf', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-12 09:23:07', '2020-08-12 09:23:07'),
(193, 103, '', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-12 10:47:55', '2020-08-13 04:41:00'),
(194, 103, 'cbbhdhfb', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-12 10:48:46', '2020-08-13 04:21:02'),
(195, 103, '', NULL, 1, 4, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Five', 'N', '2020-08-12 11:57:38', '2020-08-13 04:48:06'),
(196, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-13 08:11:36', '2020-08-13 08:11:36'),
(197, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-13 08:22:36', '2020-08-13 08:22:36'),
(198, 18, 'Again post data', NULL, 0, 0, 0, 0, 0, 'N', 'Indore', '35.377327', '7232737.88', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-13 08:55:05', '2020-08-13 08:55:05'),
(199, 18, 'Again post data', NULL, 0, 0, 0, 0, 0, 'N', 'Indore', '35.377327', '7232737.88', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-13 08:59:40', '2020-08-13 08:59:40'),
(200, 18, 'Again post data', NULL, 0, 0, 0, 0, 0, 'N', 'Indore', '35.377327', '7232737.88', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-13 09:00:19', '2020-08-13 09:00:19'),
(201, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-13 09:06:49', '2020-08-13 09:06:49'),
(202, 18, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-13 09:07:29', '2020-08-13 09:07:29'),
(203, 161, '', NULL, 0, 1, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'Y', 'N', 'N', 'N', 'N', 'Hello', 'N', '2020-08-13 16:35:18', '2020-08-14 10:36:46'),
(204, 77778, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-13 17:07:14', '2020-08-13 17:07:14'),
(205, 77778, '', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'Y', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-13 17:08:43', '2020-08-14 10:44:26'),
(206, 103, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-14 06:17:49', '2020-08-14 06:17:49'),
(207, 77781, 'Durga Mandir, Simra', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-14 11:08:10', '2020-08-14 11:08:10'),
(208, 77781, 'Chh jjjdg iryj ji norh joyrg jgvb jffbjv jghkb .....,,g,gh he qty ygv huffv. Hufh hji', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-14 11:28:20', '2020-08-14 11:28:20'),
(209, 77781, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-14 11:47:50', '2020-08-14 11:47:50'),
(210, 77781, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-14 11:50:38', '2020-08-14 11:50:38'),
(211, 103, '', NULL, 0, 1, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', 'Haha....', 'N', '2020-08-14 12:08:45', '2020-08-14 14:08:33'),
(212, 103, 'l', NULL, 1, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-14 14:25:32', '2020-08-14 15:18:00'),
(213, 77779, '#hashtags', NULL, 2, 2, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', '@keerthi_7777801', 'N', '2020-08-14 16:21:32', '2020-08-15 22:06:41'),
(219, 77779, '', NULL, 0, 0, 0, 0, 0, 'N', 'indore', '6565.4', '215.12', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-15 22:26:19', '2020-08-15 22:26:19'),
(220, 146, 'Dsdsdsdsagd dsagdgasdgasdasd', NULL, 1, 0, 0, 0, 0, 'N', '13000 SD-244, Keystone, SD 57751, USA', '43.87910249999999', '-103.4590667', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-17 11:34:44', '2020-08-18 13:11:03'),
(221, 146, '', NULL, 0, 0, 0, 0, 0, 'N', '63, Sector 63 Rd, H Block, Sector 63, Noida, Uttar Pradesh 201301, India', '28.6306603', '77.38210839999999', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-18 12:12:08', '2020-08-18 12:12:08'),
(222, 146, '', NULL, 0, 0, 0, 0, 0, 'N', '63, Sector 63 Rd, H Block, Sector 63, Noida, Uttar Pradesh 201301, India', '28.6306603', '77.38210839999999', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-18 12:31:20', '2020-08-18 12:31:20'),
(223, 146, 'Exile', NULL, 0, 0, 0, 0, 0, 'N', '63, Sector 63 Rd, H Block, Sector 63, Noida, Uttar Pradesh 201301, India', '28.6306603', '77.38210839999999', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-18 13:13:14', '2020-08-18 13:13:14'),
(224, 146, 'Sdsdsds', NULL, 0, 0, 0, 0, 0, 'N', '63, Sector 63 Rd, H Block, Sector 63, Noida, Uttar Pradesh 201301, India', '28.6306603', '77.38210839999999', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-18 13:22:20', '2020-08-18 13:22:20'),
(225, 146, 'Sdsdsds', NULL, 0, 0, 0, 0, 0, 'N', '63, Sector 63 Rd, H Block, Sector 63, Noida, Uttar Pradesh 201301, India', '28.6306603', '77.38210839999999', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-18 13:27:00', '2020-08-18 13:27:00'),
(226, 146, '', NULL, 0, 0, 0, 1, 0, 'N', '63, Sector 63 Rd, H Block, Sector 63, Noida, Uttar Pradesh 201301, India', '28.6306603', '77.38210839999999', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-18 13:29:42', '2020-08-20 15:20:13'),
(227, 146, '', NULL, 0, 0, 0, 0, 1, 'Y', '63, Sector 63 Rd, H Block, Sector 63, Noida, Uttar Pradesh 201301, India', '28.6306603', '77.38210839999999', 'VIDEO', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-18 13:41:08', '2020-08-19 06:07:36'),
(228, 77779, '', NULL, 2, 0, 0, 0, 0, 'N', 'mobulous', '54.737377', '53.3525253', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-19 06:05:20', '2020-08-20 15:21:17'),
(229, 103, '@Viswa Chikkala', NULL, 0, 5, 0, 0, 0, 'N', 'indore', '26.7120971', '84.1255375', 'NONE', 'N', 'N', 'N', 'N', 'N', '@Viswa Chikkala ', 'N', '2020-08-20 09:54:51', '2020-08-20 11:03:26'),
(230, 103, '@gggggggggViswa Chikkala', NULL, 0, 0, 0, 1, 0, 'N', 'indore', '26.7050832', '84.1334542', 'NONE', 'N', 'N', 'N', 'N', 'N', NULL, 'N', '2020-08-20 11:36:13', '2020-08-20 12:44:30');

-- --------------------------------------------------------

--
-- Table structure for table `post_gamification`
--

CREATE TABLE `post_gamification` (
  `id` int(11) NOT NULL,
  `name` text,
  `image` text,
  `no_of_post` bigint(20) NOT NULL DEFAULT '0',
  `level` bigint(20) NOT NULL DEFAULT '0',
  `is_top` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post_gamification`
--

INSERT INTO `post_gamification` (`id`, `name`, `image`, `no_of_post`, `level`, `is_top`, `created_at`, `updated_at`) VALUES
(1, 'First post', 'pUyyJSzU40IV-1597210691.png', 1, 1, 'N', '2020-08-12 05:38:11', '2020-08-12 05:38:11'),
(2, '10 Posts', 'Duy1h2epr2Tq-1597210717.png', 10, 2, 'N', '2020-08-12 05:38:37', '2020-08-12 05:38:37'),
(3, '25 Posts', 'TeHkTwZ7NEjS-1597210736.png', 25, 3, 'N', '2020-08-12 05:38:56', '2020-08-12 05:38:56'),
(4, '50 Posts', 'OfqjWlblb0wI-1597210774.png', 50, 4, 'N', '2020-08-12 05:39:34', '2020-08-12 05:39:34'),
(5, '100 Posts', 'MDwm5AFRG169-1597210809.png', 100, 5, 'N', '2020-08-12 05:40:09', '2020-08-12 05:40:09');

-- --------------------------------------------------------

--
-- Table structure for table `post_ingredients`
--

CREATE TABLE `post_ingredients` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL DEFAULT '0',
  `recipe_id` bigint(20) NOT NULL DEFAULT '0',
  `ingredients_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post_ingredients`
--

INSERT INTO `post_ingredients` (`id`, `post_id`, `recipe_id`, `ingredients_id`, `created_at`, `updated_at`) VALUES
(1, 8, 2, 1, '2020-04-10 09:53:07', '2020-04-10 09:53:07'),
(2, 8, 2, 2, '2020-04-10 09:53:07', '2020-04-10 09:53:07'),
(3, 9, 3, 1, '2020-04-10 09:59:20', '2020-04-10 09:59:20'),
(4, 9, 3, 2, '2020-04-10 09:59:20', '2020-04-10 09:59:20'),
(5, 10, 4, 1, '2020-04-10 10:33:25', '2020-04-10 10:33:25'),
(6, 10, 4, 2, '2020-04-10 10:33:25', '2020-04-10 10:33:25'),
(7, 11, 5, 1, '2020-04-11 09:58:28', '2020-04-11 09:58:28'),
(8, 11, 5, 2, '2020-04-11 09:58:28', '2020-04-11 09:58:28'),
(9, 12, 6, 1, '2020-04-11 09:58:52', '2020-04-11 09:58:52'),
(10, 12, 6, 2, '2020-04-11 09:58:52', '2020-04-11 09:58:52');

-- --------------------------------------------------------

--
-- Table structure for table `post_recipe`
--

CREATE TABLE `post_recipe` (
  `id` int(11) NOT NULL,
  `post_id` bigint(20) NOT NULL DEFAULT '0',
  `title` text,
  `story` text,
  `youtube_video_url` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post_recipe`
--

INSERT INTO `post_recipe` (`id`, `post_id`, `title`, `story`, `youtube_video_url`, `created_at`, `updated_at`) VALUES
(1, 4, 'This is the first recipe from youtube', NULL, 'https://www.youtube.com/watch?v=-wXz4x8glYY', '2020-04-10 08:13:12', '2020-04-10 08:13:12'),
(2, 8, 'This is the first recipe from youtube', 'This is a long established story', NULL, '2020-04-10 09:53:07', '2020-04-10 09:53:07'),
(3, 9, 'This is the first recipe from youtube', 'This is a long established story', NULL, '2020-04-10 09:59:20', '2020-04-10 09:59:20'),
(4, 10, 'This is the first recipe from youtube', 'This is a long established story', NULL, '2020-04-10 10:33:25', '2020-04-10 10:33:25'),
(5, 11, 'This is the first recipe from youtube', 'This is a long established story', NULL, '2020-04-11 09:58:28', '2020-04-11 09:58:28'),
(6, 12, 'This is the first recipe from youtube', 'This is a long established story', NULL, '2020-04-11 09:58:52', '2020-04-11 09:58:52'),
(7, 227, 'Sdsdsd', NULL, 'Https://mobulous.com', '2020-08-18 13:41:08', '2020-08-18 13:41:08');

-- --------------------------------------------------------

--
-- Table structure for table `post_to_comments`
--

CREATE TABLE `post_to_comments` (
  `id` int(11) NOT NULL,
  `post_id` bigint(20) NOT NULL DEFAULT '0',
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `comment` text,
  `total_like` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post_to_comments`
--

INSERT INTO `post_to_comments` (`id`, `post_id`, `user_id`, `comment`, `total_like`, `created_at`, `updated_at`) VALUES
(1, 1, 10, 'Awesome Preparation', 0, '2020-04-16 08:22:01', '2020-04-16 08:22:01'),
(2, 1, 10, 'Awesome Preparation', 0, '2020-04-16 08:28:21', '2020-04-16 08:28:21'),
(3, 12, 15, 'nyc', 0, '2020-05-13 04:29:18', '2020-05-13 04:29:18'),
(4, 31, 72, 'Testing', 0, '2020-05-21 00:16:24', '2020-05-21 00:16:24'),
(5, 27, 76, 'Giod', 0, '2020-05-22 06:16:00', '2020-05-22 06:16:00'),
(6, 27, 76, 'Giods', 0, '2020-05-22 06:16:03', '2020-05-22 06:16:03'),
(7, 32, 76, 'Hii', 0, '2020-05-22 06:27:56', '2020-05-22 06:27:56'),
(8, 32, 76, 'Huiiiiisvsg', 0, '2020-05-22 06:28:41', '2020-05-22 06:28:41'),
(9, 39, 81, 'green', 0, '2020-05-22 08:01:01', '2020-05-22 08:01:01'),
(10, 41, 90, 'Ghhhhh', 0, '2020-05-24 00:25:51', '2020-05-24 00:25:51'),
(11, 41, 90, 'Ghjjj', 0, '2020-05-24 00:26:16', '2020-05-24 00:26:16'),
(12, 41, 90, 'Ghjjj', 0, '2020-05-24 00:27:34', '2020-05-24 00:27:34'),
(13, 41, 90, 'Vhjjhhj', 0, '2020-05-24 00:28:36', '2020-05-24 00:28:36'),
(14, 44, 18, 'huuuj', 0, '2020-05-25 14:02:56', '2020-05-25 14:02:56'),
(18, 52, 52, 'Gbbbbb', 0, '2020-05-29 08:10:57', '2020-05-29 08:10:57'),
(19, 52, 52, 'Gbbbbbggg', 0, '2020-05-29 08:11:06', '2020-05-29 08:11:06'),
(20, 59, 105, 'Hhhhh', 0, '2020-06-04 10:29:30', '2020-06-04 10:29:30'),
(21, 85, 74, 'Hhhh', 0, '2020-06-18 09:39:17', '2020-06-18 09:39:17'),
(22, 98, 109, 'Hey', 0, '2020-06-18 11:45:52', '2020-06-18 11:45:52'),
(23, 98, 18, 'Gfggfgfgff', 0, '2020-06-18 12:11:29', '2020-06-18 12:11:29'),
(24, 98, 18, 'Ssdsdsds', 0, '2020-06-18 12:12:27', '2020-06-18 12:12:27'),
(25, 98, 18, 'Ssdsdsds', 0, '2020-06-18 12:12:28', '2020-06-18 12:12:28'),
(26, 98, 18, 'Wwewewewew', 0, '2020-06-18 12:14:49', '2020-06-18 12:14:49'),
(27, 98, 18, 'Wwewewewew', 0, '2020-06-18 12:14:49', '2020-06-18 12:14:49'),
(28, 98, 18, 'Eeeee eeeee', 0, '2020-06-18 12:14:52', '2020-06-18 12:14:52'),
(29, 98, 18, 'Ddsdsdsds', 0, '2020-06-18 12:16:00', '2020-06-18 12:16:00'),
(30, 98, 18, 'Hi', 0, '2020-06-18 12:18:17', '2020-06-18 12:18:17'),
(31, 98, 18, 'Huu', 0, '2020-06-18 12:20:06', '2020-06-18 12:20:06'),
(32, 98, 18, 'Hhu', 0, '2020-06-18 12:20:18', '2020-06-18 12:20:18'),
(33, 98, 18, 'Hello', 0, '2020-06-18 12:20:29', '2020-06-18 12:20:29'),
(34, 98, 18, 'Sssss', 0, '2020-06-18 12:52:03', '2020-06-18 12:52:03'),
(35, 98, 18, 'Ggggggg', 0, '2020-06-18 12:58:22', '2020-06-18 12:58:22'),
(36, 98, 18, 'Hhhhh', 0, '2020-06-18 12:58:32', '2020-06-18 12:58:32'),
(37, 100, 103, 'hi', 0, '2020-06-19 08:57:42', '2020-06-19 08:57:42'),
(38, 100, 103, 'hello', 0, '2020-06-19 08:57:51', '2020-06-19 08:57:51'),
(39, 100, 103, 'hello', 0, '2020-06-19 08:57:51', '2020-06-19 08:57:51'),
(40, 103, 109, 'Okay', 0, '2020-06-19 10:03:29', '2020-06-19 10:03:29'),
(41, 103, 109, 'There\'re kdjdkd sowvs so soagw wks sowjs dlsbs slen dksbd dlnd dldhdidk dkdjebdkod d mm eodn dlehdbdkoe didje didbdndiejw e', 0, '2020-06-19 10:38:06', '2020-06-19 10:38:06'),
(42, 103, 109, 'Bdkdjde', 0, '2020-06-19 10:38:13', '2020-06-19 10:38:13'),
(43, 104, 109, 'Once upon a rime', 0, '2020-06-19 10:55:20', '2020-06-19 10:55:20'),
(44, 104, 109, 'There', 0, '2020-06-19 10:55:28', '2020-06-19 10:55:28'),
(51, 113, 123, 'Hey Starbucks!', 0, '2020-06-21 14:00:45', '2020-06-21 14:00:45'),
(52, 121, 18, 'Hi', 0, '2020-07-01 06:34:25', '2020-07-01 06:34:25'),
(53, 118, 141, 'G', 0, '2020-07-01 09:12:26', '2020-07-01 09:12:26'),
(54, 121, 141, 'Hi', 0, '2020-07-01 09:14:21', '2020-07-01 09:14:21'),
(55, 121, 141, 'Hello', 0, '2020-07-01 09:14:34', '2020-07-01 09:14:34'),
(56, 130, 18, 'Hi', 2, '2020-07-06 12:19:12', '2020-07-20 06:14:15'),
(57, 130, 18, 'Hello ', 0, '2020-07-06 12:50:45', '2020-07-06 12:50:45'),
(58, 130, 18, 'How are you ?', 0, '2020-07-06 12:50:57', '2020-07-06 12:50:57'),
(59, 130, 18, 'What are you doing ?', 0, '2020-07-06 12:51:13', '2020-07-06 12:51:13'),
(62, 132, 103, 'hhh', 0, '2020-07-15 05:20:27', '2020-07-15 05:20:27'),
(63, 130, 18, 'Hi', 1, '2020-07-15 08:11:19', '2020-07-15 09:02:27'),
(64, 130, 18, 'Hi', 1, '2020-07-15 08:28:01', '2020-07-15 09:02:29'),
(65, 130, 18, 'Ddddd', 1, '2020-07-15 08:28:13', '2020-07-15 09:02:31'),
(67, 130, 18, 'Hi', 1, '2020-07-15 08:51:33', '2020-07-15 09:02:35'),
(68, 130, 18, 'Hi', 0, '2020-07-15 08:53:40', '2020-07-15 08:53:40'),
(69, 130, 18, 'Jhhhhhh ', 0, '2020-07-15 09:02:49', '2020-07-15 09:02:49'),
(70, 130, 18, 'Hi', 1, '2020-07-15 09:03:59', '2020-07-15 09:04:05'),
(76, 143, 156, 'Jjfhjj', 1, '2020-07-21 08:52:53', '2020-07-21 09:04:15'),
(77, 143, 156, 'Jjh', 1, '2020-07-21 08:53:00', '2020-07-21 09:04:17'),
(78, 143, 156, '   ', 1, '2020-07-21 08:56:19', '2020-07-21 09:04:22'),
(79, 143, 156, '.', 1, '2020-07-21 08:56:57', '2020-07-21 09:04:21'),
(80, 143, 156, 'Iifhjj', 2, '2020-07-21 09:03:38', '2020-07-21 16:30:10'),
(81, 143, 156, 'Iifhjjbjjjj', 1, '2020-07-21 09:03:39', '2020-07-21 09:04:21'),
(82, 143, 156, 'Hhh', 1, '2020-07-21 09:03:40', '2020-07-21 09:04:07'),
(83, 143, 156, 'Ghhh', 1, '2020-07-21 09:03:41', '2020-07-21 09:04:07'),
(84, 143, 156, 'Ghjjcotiyt', 1, '2020-07-21 09:03:45', '2020-07-21 09:04:07'),
(85, 143, 156, 'Ttuiyt', 1, '2020-07-21 09:03:47', '2020-07-21 09:04:12'),
(86, 143, 156, 'Uhbhjn', 0, '2020-07-21 09:04:36', '2020-07-21 09:04:36'),
(87, 143, 156, 'Hhj', 0, '2020-07-21 09:04:43', '2020-07-21 09:04:43'),
(89, 151, 18, 'hi ', 1, '2020-07-23 11:07:23', '2020-07-23 11:07:54'),
(90, 151, 18, 'hello', 0, '2020-07-23 11:07:36', '2020-07-23 11:07:53'),
(105, 173, 77778, 'Jdkdjdhd', 0, '2020-07-31 00:32:04', '2020-07-31 00:32:04'),
(106, 173, 77778, 'Hdhdbdb', 0, '2020-07-31 00:32:07', '2020-07-31 00:32:07'),
(107, 173, 77778, 'Ndhxjdjsis', 0, '2020-07-31 00:32:12', '2020-07-31 00:32:12'),
(108, 173, 77778, 'Hshdjs', 0, '2020-07-31 00:32:14', '2020-07-31 00:32:14'),
(109, 173, 77778, 'Hdhsis', 0, '2020-07-31 00:32:17', '2020-07-31 00:32:17'),
(110, 173, 77778, 'Jdjdjs', 0, '2020-07-31 00:32:19', '2020-07-31 00:32:19'),
(111, 173, 77778, 'Ndjsns hejd', 0, '2020-07-31 00:32:24', '2020-07-31 00:32:24'),
(112, 173, 77778, 'Hdhdbd', 0, '2020-07-31 00:32:28', '2020-07-31 00:32:28'),
(113, 173, 77778, 'Hdhd', 0, '2020-07-31 00:32:30', '2020-07-31 00:32:30'),
(114, 173, 77778, 'Hdhd', 0, '2020-07-31 00:32:33', '2020-07-31 00:32:33'),
(115, 173, 77778, 'Ndhxyhdxudhebd', 0, '2020-07-31 00:32:39', '2020-07-31 00:32:39'),
(116, 166, 77778, 'Cclickf', 0, '2020-07-31 00:39:21', '2020-07-31 00:39:21'),
(117, 166, 77778, 'Jdjdd', 0, '2020-07-31 00:39:24', '2020-07-31 00:39:24'),
(118, 166, 77778, 'Hdkxnd', 0, '2020-07-31 00:55:58', '2020-07-31 00:55:58'),
(119, 166, 77778, 'Jdjxhbdud', 0, '2020-07-31 00:56:00', '2020-07-31 00:56:00'),
(120, 166, 77778, 'Jdhdbyd', 0, '2020-07-31 00:56:03', '2020-07-31 00:56:03'),
(121, 166, 77778, 'Hdhxhbd', 0, '2020-07-31 00:56:06', '2020-07-31 00:56:06'),
(122, 166, 77778, 'Hdhhd d', 0, '2020-07-31 00:56:08', '2020-07-31 00:56:08'),
(123, 166, 77778, 'Hxhdgyo', 0, '2020-07-31 00:56:11', '2020-07-31 00:56:11'),
(124, 166, 77778, 'Jdhhe', 0, '2020-07-31 00:56:14', '2020-07-31 00:56:14'),
(125, 166, 77778, 'Hxyhsvsjzyhexuhdb', 0, '2020-07-31 00:56:23', '2020-07-31 00:56:23'),
(126, 176, 77779, 'Great!!', 0, '2020-07-31 10:39:59', '2020-07-31 10:39:59'),
(127, 176, 77779, 'Good', 0, '2020-07-31 10:42:06', '2020-07-31 10:42:06'),
(128, 175, 77779, 'Yum!', 0, '2020-07-31 10:45:31', '2020-07-31 10:45:31'),
(129, 154, 77779, 'These r out of scope 😛', 0, '2020-07-31 10:46:25', '2020-07-31 10:46:25'),
(130, 175, 77779, 'Cool!!!', 0, '2020-07-31 10:47:58', '2020-07-31 10:47:58'),
(131, 176, 77779, 'Test..', 0, '2020-07-31 12:04:33', '2020-07-31 12:04:33'),
(132, 176, 77779, 'Test', 0, '2020-07-31 12:08:02', '2020-07-31 12:08:02'),
(133, 176, 77779, 'New1', 0, '2020-07-31 12:08:50', '2020-07-31 12:08:50'),
(134, 143, 77779, 'New1', 0, '2020-07-31 12:25:53', '2020-07-31 12:25:53'),
(135, 176, 77779, 'New2', 0, '2020-07-31 12:26:09', '2020-07-31 12:26:09'),
(136, 176, 77779, 'New3', 0, '2020-07-31 12:27:03', '2020-07-31 12:27:03'),
(137, 176, 77779, 'New4', 0, '2020-07-31 12:28:07', '2020-07-31 12:28:07'),
(138, 176, 77779, '5', 0, '2020-07-31 12:34:29', '2020-07-31 12:34:29'),
(139, 176, 77779, 'Vvvv', 0, '2020-07-31 13:37:10', '2020-07-31 13:37:10'),
(140, 176, 77779, '66', 0, '2020-07-31 13:37:53', '2020-07-31 13:37:53'),
(141, 176, 77779, '77', 0, '2020-07-31 13:51:53', '2020-07-31 13:51:53'),
(142, 143, 77779, 'New2', 0, '2020-07-31 13:52:17', '2020-07-31 13:52:17'),
(143, 143, 77779, 'New3', 0, '2020-07-31 13:53:01', '2020-07-31 13:53:01'),
(144, 176, 77779, 'Device', 0, '2020-08-01 16:54:12', '2020-08-01 16:54:12'),
(145, 154, 77779, 'Check check check', 0, '2020-08-01 16:54:44', '2020-08-01 16:54:44'),
(147, 177, 18, 'Hi', 0, '2020-08-06 08:10:48', '2020-08-06 08:10:48'),
(148, 179, 18, 'Hi', 0, '2020-08-06 10:07:55', '2020-08-06 10:07:55'),
(149, 179, 18, 'Hello', 1, '2020-08-06 10:08:04', '2020-08-10 05:36:44'),
(150, 190, 103, 'hi ', 1, '2020-08-10 05:12:52', '2020-08-10 05:36:56'),
(151, 190, 103, 'lll', 1, '2020-08-10 05:13:38', '2020-08-10 05:24:43'),
(152, 195, 77779, 'Hello', 0, '2020-08-13 04:05:49', '2020-08-13 04:05:49'),
(153, 195, 77779, 'Kjkj', 0, '2020-08-13 04:41:05', '2020-08-13 04:41:05'),
(154, 195, 77779, 'Fouh', 0, '2020-08-13 04:45:38', '2020-08-13 04:45:38'),
(155, 195, 77779, 'Five', 0, '2020-08-13 04:48:06', '2020-08-13 04:48:06'),
(156, 203, 77781, 'Hello', 1, '2020-08-14 10:36:46', '2020-08-14 10:42:24'),
(158, 213, 77778, 'Getting push??', 0, '2020-08-14 16:49:00', '2020-08-14 16:49:00'),
(160, 176, 77779, 'Dhghh', 0, '2020-08-15 19:07:31', '2020-08-15 19:07:31'),
(161, 229, 103, '@Keerthi Villa Chikkala ', 0, '2020-08-20 10:54:23', '2020-08-20 10:54:23'),
(162, 229, 103, '@Viswa Chikkala ', 0, '2020-08-20 10:56:35', '2020-08-20 10:56:35'),
(163, 229, 103, 'cvvv @Keerthi Villa Chikkala ', 0, '2020-08-20 10:58:47', '2020-08-20 10:58:47'),
(164, 229, 103, 'sgggg', 0, '2020-08-20 11:00:26', '2020-08-20 11:00:26'),
(165, 229, 103, '@Viswa Chikkala ', 1, '2020-08-20 11:03:26', '2020-08-20 11:06:07');

-- --------------------------------------------------------

--
-- Table structure for table `post_to_cooked_it`
--

CREATE TABLE `post_to_cooked_it` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `post_id` int(11) DEFAULT '0',
  `cooked_it_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post_to_cooked_it`
--

INSERT INTO `post_to_cooked_it` (`id`, `user_id`, `post_id`, `cooked_it_id`, `created_at`, `updated_at`) VALUES
(1, 9, 7, 1, '2020-04-10 09:25:21', '2020-04-10 09:25:21');

-- --------------------------------------------------------

--
-- Table structure for table `post_to_images`
--

CREATE TABLE `post_to_images` (
  `id` int(11) NOT NULL,
  `post_id` bigint(20) NOT NULL DEFAULT '0',
  `image` text,
  `is_default` enum('Y','N') NOT NULL DEFAULT 'N',
  `type` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post_to_images`
--

INSERT INTO `post_to_images` (`id`, `post_id`, `image`, `is_default`, `type`, `created_at`, `updated_at`) VALUES
(1, 1, '1-TM93M5d01586525740.png', 'Y', 1, '2020-04-10 08:05:40', '2020-04-10 08:05:40'),
(2, 1, '1-H6hxVdtY1586525740.png', 'N', 1, '2020-04-10 08:05:40', '2020-04-10 08:05:40'),
(3, 2, '2-BLPE3GWb1586525815.png', 'Y', 1, '2020-04-10 08:06:55', '2020-04-10 08:06:55'),
(4, 2, '2-vURvbX8F1586525815.png', 'N', 1, '2020-04-10 08:06:55', '2020-04-10 08:06:55'),
(5, 3, '3-fuECqH6A1586525846.png', 'Y', 1, '2020-04-10 08:07:26', '2020-04-10 08:07:26'),
(6, 3, '3-nqrIKCWY1586525847.png', 'N', 1, '2020-04-10 08:07:27', '2020-04-10 08:07:27'),
(7, 4, '4-tbLUDnkT1586526192.png', 'Y', 1, '2020-04-10 08:13:12', '2020-04-10 08:13:12'),
(8, 4, '4-njVrEjbX1586526192.png', 'N', 1, '2020-04-10 08:13:12', '2020-04-10 08:13:12'),
(9, 5, '5-ojcEPgiE1586530388.png', 'Y', 1, '2020-04-10 09:23:08', '2020-04-10 09:23:08'),
(10, 5, '5-3Sraz4Fw1586530388.png', 'N', 1, '2020-04-10 09:23:08', '2020-04-10 09:23:08'),
(11, 6, '6-ozyhIbJd1586530475.png', 'Y', 1, '2020-04-10 09:24:35', '2020-04-10 09:24:35'),
(12, 6, '6-sN8PwPFu1586530475.png', 'N', 1, '2020-04-10 09:24:35', '2020-04-10 09:24:35'),
(13, 7, '7-FqYCLWEP1586530520.png', 'Y', 1, '2020-04-10 09:25:20', '2020-04-10 09:25:20'),
(14, 7, '7-zy81sClq1586530520.png', 'N', 1, '2020-04-10 09:25:20', '2020-04-10 09:25:20'),
(15, 8, '8-RAtf7Uxw1586532186.png', 'Y', 1, '2020-04-10 09:53:06', '2020-04-10 09:53:06'),
(16, 8, '8-CwaaCKmG1586532187.png', 'N', 1, '2020-04-10 09:53:07', '2020-04-10 09:53:07'),
(17, 9, '9-Nxk71Swi1586532560.png', 'Y', 1, '2020-04-10 09:59:20', '2020-04-10 09:59:20'),
(18, 9, '9-yvoNwjMH1586532560.png', 'N', 1, '2020-04-10 09:59:20', '2020-04-10 09:59:20'),
(19, 10, '10-NpacHocH1586534605.png', 'Y', 1, '2020-04-10 10:33:25', '2020-04-10 10:33:25'),
(20, 10, '10-6vQPNwL91586534605.png', 'N', 1, '2020-04-10 10:33:25', '2020-04-10 10:33:25'),
(21, 11, '11-m3p4RE7M1586618908.png', 'Y', 1, '2020-04-11 09:58:28', '2020-04-11 09:58:28'),
(22, 11, '11-NlEiNMAr1586618908.png', 'N', 1, '2020-04-11 09:58:28', '2020-04-11 09:58:28'),
(23, 12, '12-VpA1ImcU1586618932.png', 'Y', 1, '2020-04-11 09:58:52', '2020-04-11 09:58:52'),
(24, 12, '12-rXZcxeEp1586618932.png', 'N', 1, '2020-04-11 09:58:52', '2020-04-11 09:58:52'),
(25, 13, '13-raRalC9D1589532975.jpg', 'Y', 1, '2020-05-15 08:56:15', '2020-05-15 08:56:15'),
(26, 14, '14-rPBgDDRV1589554252.jpg', 'Y', 1, '2020-05-15 14:50:52', '2020-05-15 14:50:52'),
(27, 15, '15-pxclQ8qn1589554778.jpg', 'Y', 1, '2020-05-15 14:59:38', '2020-05-15 14:59:38'),
(28, 16, '16-zAZs8qmC1589569004.jpg', 'Y', 1, '2020-05-15 18:56:44', '2020-05-15 18:56:44'),
(29, 16, '16-3Cd25SiY1589569004.jpg', 'N', 1, '2020-05-15 18:56:44', '2020-05-15 18:56:44'),
(30, 16, '16-E7hf4InV1589569004.jpg', 'N', 1, '2020-05-15 18:56:44', '2020-05-15 18:56:44'),
(31, 17, '17-jjdqhcrW1589609143.jpg', 'Y', 1, '2020-05-16 06:05:43', '2020-05-16 06:05:43'),
(32, 18, '18-YRUZHnQU1589614373.jpg', 'Y', 1, '2020-05-16 07:32:53', '2020-05-16 07:32:53'),
(33, 19, '19-ABOFwIHR1589614602.jpg', 'Y', 1, '2020-05-16 07:36:42', '2020-05-16 07:36:42'),
(34, 19, '19-MfJNqQkL1589614602.jpg', 'N', 1, '2020-05-16 07:36:42', '2020-05-16 07:36:42'),
(35, 20, '20-bM8xf5yM1589616053.jpg', 'Y', 1, '2020-05-16 08:00:53', '2020-05-16 08:00:53'),
(36, 20, '20-veAkHMOf1589616053.jpg', 'N', 1, '2020-05-16 08:00:53', '2020-05-16 08:00:53'),
(37, 21, '21-s8j0vo5B1589616195.jpg', 'Y', 1, '2020-05-16 08:03:15', '2020-05-16 08:03:15'),
(38, 21, '21-i3CoC6XP1589616195.jpg', 'N', 1, '2020-05-16 08:03:15', '2020-05-16 08:03:15'),
(39, 22, '22-hR5u2EFc1589618445.jpg', 'Y', 1, '2020-05-16 08:40:45', '2020-05-16 08:40:45'),
(40, 22, '22-16NUPjn21589618445.jpg', 'N', 1, '2020-05-16 08:40:45', '2020-05-16 08:40:45'),
(41, 23, '23-ONF9a4JN1589619095.jpg', 'Y', 1, '2020-05-16 08:51:35', '2020-05-16 08:51:35'),
(42, 24, '24-EWigNWeE1589904483.jpg', 'Y', 1, '2020-05-19 16:08:03', '2020-05-19 16:08:03'),
(43, 25, '25-1MHoI1w41589951512.jpg', 'Y', 1, '2020-05-20 05:11:52', '2020-05-20 05:11:52'),
(44, 26, '26-Cj5YZDe41589979707.jpg', 'Y', 1, '2020-05-20 13:01:47', '2020-05-20 13:01:47'),
(45, 27, '27-TNVqKc4I1589987571.jpg', 'Y', 1, '2020-05-20 15:12:51', '2020-05-20 15:12:51'),
(46, 28, '28-eKzg8pZa1589987574.jpg', 'Y', 1, '2020-05-20 15:12:55', '2020-05-20 15:12:55'),
(47, 29, '29-HWdUEJ341589988515.jpg', 'Y', 1, '2020-05-20 15:28:35', '2020-05-20 15:28:35'),
(48, 30, '30-AVyUXrZ81589988516.jpg', 'Y', 1, '2020-05-20 15:28:36', '2020-05-20 15:28:36'),
(49, 31, '31-GJPwo9W21589988661.jpg', 'Y', 1, '2020-05-20 15:31:01', '2020-05-20 15:31:01'),
(50, 32, '32-vlGWzCMK1590046945.jpg', 'Y', 1, '2020-05-21 07:42:25', '2020-05-21 07:42:25'),
(51, 33, '33-nfKDPXLj1590046956.jpg', 'Y', 1, '2020-05-21 07:42:36', '2020-05-21 07:42:36'),
(52, 34, '34-nHIw5ruS1590132429.jpg', 'Y', 1, '2020-05-22 07:27:09', '2020-05-22 07:27:09'),
(53, 35, '35-WrSyXlcT1590132501.jpg', 'Y', 1, '2020-05-22 07:28:21', '2020-05-22 07:28:21'),
(57, 39, '39-iWi16Yc51590134384.jpg', 'Y', 1, '2020-05-22 07:59:44', '2020-05-22 07:59:44'),
(58, 40, '40-AqyzrM6Y1590276092.jpg', 'Y', 1, '2020-05-23 23:21:32', '2020-05-23 23:21:32'),
(59, 41, '41-BsMMj8mH1590279206.jpg', 'Y', 1, '2020-05-24 00:13:26', '2020-05-24 00:13:26'),
(60, 42, '42-d6GkXIGG1590393445.jpg', 'Y', 1, '2020-05-25 07:57:25', '2020-05-25 07:57:25'),
(61, 42, '42-ia31xFnG1590393445.jpg', 'N', 1, '2020-05-25 07:57:25', '2020-05-25 07:57:25'),
(62, 42, '42-ZEn9e7In1590393445.jpg', 'N', 1, '2020-05-25 07:57:25', '2020-05-25 07:57:25'),
(63, 42, '42-NBUCXXS11590393445.jpg', 'N', 1, '2020-05-25 07:57:25', '2020-05-25 07:57:25'),
(64, 43, '43-vnHu9fyk1590393452.jpg', 'Y', 1, '2020-05-25 07:57:32', '2020-05-25 07:57:32'),
(65, 43, '43-NNYlPAiw1590393452.jpg', 'N', 1, '2020-05-25 07:57:32', '2020-05-25 07:57:32'),
(66, 43, '43-084Ya1Zf1590393452.jpg', 'N', 1, '2020-05-25 07:57:32', '2020-05-25 07:57:32'),
(67, 43, '43-n6bOGzfa1590393452.jpg', 'N', 1, '2020-05-25 07:57:32', '2020-05-25 07:57:32'),
(68, 44, '44-zzPCUNOG1590393607.jpg', 'Y', 1, '2020-05-25 08:00:07', '2020-05-25 08:00:07'),
(69, 44, '44-hgbhMJf91590393607.jpg', 'N', 1, '2020-05-25 08:00:07', '2020-05-25 08:00:07'),
(70, 44, '44-9Vd8ajye1590393607.jpg', 'N', 1, '2020-05-25 08:00:07', '2020-05-25 08:00:07'),
(71, 44, '44-R9hpHQZ41590393607.jpg', 'N', 1, '2020-05-25 08:00:07', '2020-05-25 08:00:07'),
(72, 45, '45-Kxjk9aPz1590393860.jpg', 'Y', 1, '2020-05-25 08:04:20', '2020-05-25 08:04:20'),
(73, 45, '45-44742Z6C1590393860.jpg', 'N', 1, '2020-05-25 08:04:20', '2020-05-25 08:04:20'),
(74, 45, '45-iM41PjAk1590393860.jpg', 'N', 1, '2020-05-25 08:04:20', '2020-05-25 08:04:20'),
(75, 45, '45-mPOPuiib1590393860.jpg', 'N', 1, '2020-05-25 08:04:20', '2020-05-25 08:04:20'),
(76, 46, '46-QeX2iIGS1590420290.jpg', 'Y', 1, '2020-05-25 15:24:50', '2020-05-25 15:24:50'),
(77, 47, '47-W2Lmb4We1590420487.jpg', 'Y', 1, '2020-05-25 15:28:07', '2020-05-25 15:28:07'),
(78, 48, '48-305XC7cn1590423071.jpg', 'Y', 1, '2020-05-25 16:11:11', '2020-05-25 16:11:11'),
(79, 49, '49-bB6uyvwv1590427640.jpg', 'Y', 1, '2020-05-25 17:27:20', '2020-05-25 17:27:20'),
(80, 50, '50-e94To1cI1590427705.jpg', 'Y', 1, '2020-05-25 17:28:25', '2020-05-25 17:28:25'),
(81, 51, '51-cVQl7nI71590455932.jpg', 'Y', 1, '2020-05-26 01:18:52', '2020-05-26 01:18:52'),
(82, 51, '51-8tzmwEjw1590455932.jpg', 'N', 1, '2020-05-26 01:18:52', '2020-05-26 01:18:52'),
(83, 52, '52-asUeNaF31590473061.jpg', 'Y', 1, '2020-05-26 06:04:21', '2020-05-26 06:04:21'),
(84, 58, '58-TkkNhQjO1591265972.jpg', 'Y', 1, '2020-06-04 10:19:32', '2020-06-04 10:19:32'),
(85, 58, '58-RhShcPzW1591265972.jpg', 'N', 1, '2020-06-04 10:19:32', '2020-06-04 10:19:32'),
(86, 58, '58-HodwzJNB1591265972.jpg', 'N', 1, '2020-06-04 10:19:32', '2020-06-04 10:19:32'),
(87, 58, '58-5tM54uxr1591265972.jpg', 'N', 1, '2020-06-04 10:19:32', '2020-06-04 10:19:32'),
(88, 59, '59-r6S75kvb1591266500.jpg', 'Y', 1, '2020-06-04 10:28:20', '2020-06-04 10:28:20'),
(89, 59, '59-9DL2zZwH1591266500.jpg', 'N', 1, '2020-06-04 10:28:20', '2020-06-04 10:28:20'),
(90, 59, '59-a8J0984g1591266500.jpg', 'N', 1, '2020-06-04 10:28:20', '2020-06-04 10:28:20'),
(91, 60, '60-JrXyt9x21591341072.jpg', 'Y', 1, '2020-06-05 07:11:12', '2020-06-05 07:11:12'),
(92, 60, '60-G5zbyP5i1591341072.jpg', 'N', 1, '2020-06-05 07:11:12', '2020-06-05 07:11:12'),
(93, 61, '61-5UmhuduR1591342591.jpg', 'Y', 1, '2020-06-05 07:36:31', '2020-06-05 07:36:31'),
(94, 61, '61-1abRXC5u1591342591.jpg', 'N', 1, '2020-06-05 07:36:31', '2020-06-05 07:36:31'),
(95, 61, '61-zJOLgNiC1591342591.jpg', 'N', 1, '2020-06-05 07:36:31', '2020-06-05 07:36:31'),
(96, 61, '61-KepOIqVy1591342591.jpg', 'N', 1, '2020-06-05 07:36:31', '2020-06-05 07:36:31'),
(97, 62, '62-RRjjHkbJ1591351640.jpg', 'Y', 1, '2020-06-05 10:07:20', '2020-06-05 10:07:20'),
(98, 63, '63-DjTYZQ861591351643.jpg', 'Y', 1, '2020-06-05 10:07:23', '2020-06-05 10:07:23'),
(99, 64, '64-7H1JRpn51591358955.jpg', 'Y', 1, '2020-06-05 12:09:15', '2020-06-05 12:09:15'),
(100, 70, '70-G9dBNFYW1591386186.jpg', 'Y', 1, '2020-06-05 19:43:06', '2020-06-05 19:43:06'),
(101, 74, '74-Dt3T1FhS1591766135.jpg', 'Y', 1, '2020-06-10 05:15:35', '2020-06-10 05:15:35'),
(102, 74, '74-lQ8jALK11591766135.jpg', 'N', 1, '2020-06-10 05:15:35', '2020-06-10 05:15:35'),
(103, 74, '74-J9sGx5PK1591766135.jpg', 'N', 1, '2020-06-10 05:15:35', '2020-06-10 05:15:35'),
(104, 74, '74-zwWv9hsO1591766135.jpg', 'N', 1, '2020-06-10 05:15:35', '2020-06-10 05:15:35'),
(105, 79, '79-upzo8NHV1592288960.jpg', 'Y', 1, '2020-06-16 06:29:20', '2020-06-16 06:29:20'),
(106, 80, '80-FhBBEHh81592289012.jpg', 'Y', 1, '2020-06-16 06:30:12', '2020-06-16 06:30:12'),
(107, 81, '81-od19IIEo1592289026.jpg', 'Y', 1, '2020-06-16 06:30:26', '2020-06-16 06:30:26'),
(108, 82, '82-ff6kHaEo1592289036.jpg', 'Y', 1, '2020-06-16 06:30:36', '2020-06-16 06:30:36'),
(109, 83, '83-PNP8R2pX1592302534.jpg', 'Y', 1, '2020-06-16 10:15:34', '2020-06-16 10:15:34'),
(110, 84, '84-TpAkGBJx1592302553.jpg', 'Y', 1, '2020-06-16 10:15:53', '2020-06-16 10:15:53'),
(111, 85, '85-MWh6Pa7h1592302595.jpg', 'Y', 1, '2020-06-16 10:16:35', '2020-06-16 10:16:35'),
(112, 86, '86-Olr8l32p1592393062.jpg', 'Y', 1, '2020-06-17 11:24:22', '2020-06-17 11:24:22'),
(113, 86, '86-NHJI3e1L1592393062.jpg', 'N', 1, '2020-06-17 11:24:22', '2020-06-17 11:24:22'),
(114, 86, '86-MMNMpilu1592393062.jpg', 'N', 1, '2020-06-17 11:24:22', '2020-06-17 11:24:22'),
(115, 87, '87-rKPVsDQU1592393311.jpg', 'Y', 1, '2020-06-17 11:28:31', '2020-06-17 11:28:31'),
(116, 88, '88-LqiyYcdY1592393918.jpg', 'Y', 1, '2020-06-17 11:38:38', '2020-06-17 11:38:38'),
(117, 89, '89-QFqICXcR1592395314.jpg', 'Y', 1, '2020-06-17 12:01:54', '2020-06-17 12:01:54'),
(118, 90, '90-pj629kqO1592395720.jpg', 'Y', 1, '2020-06-17 12:08:40', '2020-06-17 12:08:40'),
(119, 91, '91-bqcPlWXi1592396304.jpg', 'Y', 1, '2020-06-17 12:18:24', '2020-06-17 12:18:24'),
(120, 92, '92-A7LWO1sw1592396353.jpg', 'Y', 1, '2020-06-17 12:19:13', '2020-06-17 12:19:13'),
(121, 93, '93-kZt4jhWb1592396401.jpg', 'Y', 1, '2020-06-17 12:20:01', '2020-06-17 12:20:01'),
(122, 94, '94-hKO8qWf21592396838.jpg', 'Y', 1, '2020-06-17 12:27:18', '2020-06-17 12:27:18'),
(123, 95, '95-ie8QzCLR1592468315.jpg', 'Y', 1, '2020-06-18 08:18:35', '2020-06-18 08:18:35'),
(124, 95, '95-j3V4SOFX1592468315.jpg', 'N', 1, '2020-06-18 08:18:35', '2020-06-18 08:18:35'),
(125, 96, '96-qGe3jxZN1592471243.jpg', 'Y', 1, '2020-06-18 09:07:23', '2020-06-18 09:07:23'),
(126, 97, '97-iE46aP5k1592471314.jpg', 'Y', 1, '2020-06-18 09:08:34', '2020-06-18 09:08:34'),
(127, 98, '98-BNxjoKo21592478338.jpg', 'Y', 1, '2020-06-18 11:05:38', '2020-06-18 11:05:38'),
(129, 100, '100-GnDvWL0o1592555370.jpg', 'Y', 1, '2020-06-19 08:29:30', '2020-06-19 08:29:30'),
(130, 101, '101-2E81lP1q1592557040.jpg', 'Y', 1, '2020-06-19 08:57:20', '2020-06-19 08:57:20'),
(131, 101, '101-wm2093AT1592557040.jpg', 'N', 1, '2020-06-19 08:57:20', '2020-06-19 08:57:20'),
(132, 101, '101-zEOUR9QK1592557040.jpg', 'N', 1, '2020-06-19 08:57:20', '2020-06-19 08:57:20'),
(133, 102, '102-fXLchesA1592557280.jpg', 'Y', 1, '2020-06-19 09:01:20', '2020-06-19 09:01:20'),
(134, 103, '103-J3R4gBVr1592559705.jpg', 'Y', 1, '2020-06-19 09:41:45', '2020-06-19 09:41:45'),
(135, 104, '104-sj2CxD9d1592563843.jpg', 'Y', 1, '2020-06-19 10:50:43', '2020-06-19 10:50:43'),
(136, 104, '104-NCMSDnn81592563843.jpg', 'N', 1, '2020-06-19 10:50:43', '2020-06-19 10:50:43'),
(137, 104, '104-IdIwGLup1592563843.jpg', 'N', 1, '2020-06-19 10:50:43', '2020-06-19 10:50:43'),
(138, 105, '105-caaQp03v1592564813.jpg', 'Y', 1, '2020-06-19 11:06:53', '2020-06-19 11:06:53'),
(139, 106, '106-eLtH1TVT1592565053.jpg', 'Y', 1, '2020-06-19 11:10:53', '2020-06-19 11:10:53'),
(140, 109, '109-tbtTmfYC1592571570.jpg', 'Y', 1, '2020-06-19 12:59:30', '2020-06-19 12:59:30'),
(141, 109, '109-Mi8p4Rb81592571570.jpg', 'N', 1, '2020-06-19 12:59:30', '2020-06-19 12:59:30'),
(142, 110, '110-ve5Jknwn1592571734.jpg', 'Y', 1, '2020-06-19 13:02:14', '2020-06-19 13:02:14'),
(143, 110, '110-6pJVVWKF1592571734.jpg', 'N', 1, '2020-06-19 13:02:14', '2020-06-19 13:02:14'),
(144, 111, '111-afUytzaZ1592573775.jpg', 'Y', 1, '2020-06-19 13:36:15', '2020-06-19 13:36:15'),
(145, 112, '112-CzDnAdyY1592581249.jpg', 'Y', 1, '2020-06-19 15:40:49', '2020-06-19 15:40:49'),
(146, 113, '113-TfElkBS01592582577.jpg', 'Y', 1, '2020-06-19 16:02:57', '2020-06-19 16:02:57'),
(148, 115, '115-LpLMtdAq1592583119.jpg', 'Y', 1, '2020-06-19 16:11:59', '2020-06-19 16:11:59'),
(151, 117, '117-bve6pPh51592624739.jpg', 'Y', 1, '2020-06-20 03:45:39', '2020-06-20 03:45:39'),
(152, 118, '118-jstjjc3B1592749508.jpg', 'Y', 1, '2020-06-21 14:25:08', '2020-06-21 14:25:08'),
(153, 119, '119-ziHlKxP01592749692.jpg', 'Y', 1, '2020-06-21 14:28:12', '2020-06-21 14:28:12'),
(154, 119, '119-H1fq4FVu1592749692.jpg', 'N', 1, '2020-06-21 14:28:12', '2020-06-21 14:28:12'),
(155, 120, '120-5faHdjFQ1592833731.jpg', 'Y', 1, '2020-06-22 13:48:51', '2020-06-22 13:48:51'),
(156, 121, '121-UXvw6sqJ1592834303.jpg', 'Y', 1, '2020-06-22 13:58:23', '2020-06-22 13:58:23'),
(157, 122, '122-T80IhZGx1593666535.jpg', 'Y', 1, '2020-07-02 05:08:55', '2020-07-02 05:08:55'),
(158, 122, '122-yZbWMx3E1593666535.jpg', 'N', 1, '2020-07-02 05:08:55', '2020-07-02 05:08:55'),
(159, 122, '122-tHDGnV1E1593666535.jpg', 'N', 1, '2020-07-02 05:08:55', '2020-07-02 05:08:55'),
(160, 122, '122-8MXLJFR31593666535.jpg', 'N', 1, '2020-07-02 05:08:55', '2020-07-02 05:08:55'),
(161, 123, '123-fsBCynt11593666666.jpg', 'Y', 1, '2020-07-02 05:11:06', '2020-07-02 05:11:06'),
(162, 124, '124-WnG6uIsg1593666693.jpg', 'Y', 1, '2020-07-02 05:11:33', '2020-07-02 05:11:33'),
(163, 125, '125-Y3GexJJz1593666761.jpg', 'Y', 1, '2020-07-02 05:12:41', '2020-07-02 05:12:41'),
(164, 126, '126-TLo32FmL1593678149.jpg', 'Y', 1, '2020-07-02 08:22:29', '2020-07-02 08:22:29'),
(165, 126, '126-xP72bkpr1593678149.jpg', 'N', 1, '2020-07-02 08:22:29', '2020-07-02 08:22:29'),
(166, 127, '127-gd203q1C1593679745.jpg', 'Y', 1, '2020-07-02 08:49:05', '2020-07-02 08:49:05'),
(170, 130, '130-1DcFTb4O1594017373.jpg', 'Y', 1, '2020-07-06 06:36:13', '2020-07-06 06:36:13'),
(172, 132, '132-YgCBwhcG1594625240.jpg', 'Y', 1, '2020-07-13 07:27:20', '2020-07-13 07:27:20'),
(173, 132, '132-HtI9TW4Z1594625240.jpg', 'N', 1, '2020-07-13 07:27:20', '2020-07-13 07:27:20'),
(174, 132, '132-1wFlPFou1594625240.jpg', 'N', 1, '2020-07-13 07:27:20', '2020-07-13 07:27:20'),
(175, 132, '132-gv99HRPI1594625240.jpg', 'N', 1, '2020-07-13 07:27:20', '2020-07-13 07:27:20'),
(176, 133, '133-4xkL6pxG1594631464.jpg', 'Y', 1, '2020-07-13 09:11:04', '2020-07-13 09:11:04'),
(177, 133, '133-GYWSBPM11594631464.jpg', 'N', 1, '2020-07-13 09:11:04', '2020-07-13 09:11:04'),
(178, 134, '134-gKFlYuOT1594631858.jpg', 'Y', 1, '2020-07-13 09:17:38', '2020-07-13 09:17:38'),
(179, 134, '134-RVjCNUOi1594631858.jpg', 'N', 1, '2020-07-13 09:17:38', '2020-07-13 09:17:38'),
(180, 134, '134-r5DLkjKn1594631858.jpg', 'N', 1, '2020-07-13 09:17:38', '2020-07-13 09:17:38'),
(182, 136, '136-09F58O0R1595225445.jpg', 'Y', 1, '2020-07-20 06:10:45', '2020-07-20 06:10:45'),
(183, 137, '137-fYumLrta1595225445.jpg', 'Y', 1, '2020-07-20 06:10:45', '2020-07-20 06:10:45'),
(184, 138, '138-ghu09tIz1595225757.jpg', 'Y', 1, '2020-07-20 06:15:57', '2020-07-20 06:15:57'),
(185, 139, '139-NiI7DCfY1595225793.jpg', 'Y', 1, '2020-07-20 06:16:33', '2020-07-20 06:16:33'),
(186, 140, '140-hSjYJcEg1595312112.jpg', 'Y', 1, '2020-07-21 06:15:12', '2020-07-21 06:15:12'),
(187, 141, '141-0zLCVo6J1595313304.jpg', 'Y', 1, '2020-07-21 06:35:04', '2020-07-21 06:35:04'),
(188, 141, '141-dpYdacnJ1595313304.jpg', 'N', 1, '2020-07-21 06:35:04', '2020-07-21 06:35:04'),
(189, 142, '142-WMM42F541595320576.jpg', 'Y', 1, '2020-07-21 08:36:16', '2020-07-21 08:36:16'),
(190, 142, '142-6qujyFqK1595320576.jpg', 'N', 1, '2020-07-21 08:36:16', '2020-07-21 08:36:16'),
(191, 142, '142-dyUqjvxv1595320576.jpg', 'N', 1, '2020-07-21 08:36:16', '2020-07-21 08:36:16'),
(192, 142, '142-R7CaVjni1595320576.jpg', 'N', 1, '2020-07-21 08:36:16', '2020-07-21 08:36:16'),
(193, 143, '143-0tZj4g931595320774.jpg', 'Y', 1, '2020-07-21 08:39:34', '2020-07-21 08:39:34'),
(194, 144, '144-j195NA901595322665.jpg', 'Y', 1, '2020-07-21 09:11:05', '2020-07-21 09:11:05'),
(195, 145, '145-NWsMjaMg1595324726.jpg', 'Y', 1, '2020-07-21 09:45:26', '2020-07-21 09:45:26'),
(196, 146, '146-lDiSuf5F1595328361.jpg', 'Y', 1, '2020-07-21 10:46:01', '2020-07-21 10:46:01'),
(197, 147, '147-YWbp1fl31595328505.jpg', 'Y', 1, '2020-07-21 10:48:25', '2020-07-21 10:48:25'),
(198, 148, '148-2yl3gWHO1595328911.jpg', 'Y', 1, '2020-07-21 10:55:11', '2020-07-21 10:55:11'),
(199, 149, '149-6kAGQHNu1595328980.jpg', 'Y', 1, '2020-07-21 10:56:20', '2020-07-21 10:56:20'),
(200, 150, '150-1lOVybhK1595329552.jpg', 'Y', 1, '2020-07-21 11:05:52', '2020-07-21 11:05:52'),
(201, 151, '151-aggVVkm21595329593.jpg', 'Y', 1, '2020-07-21 11:06:33', '2020-07-21 11:06:33'),
(202, 151, '151-N9eaKqmm1595329593.jpg', 'N', 1, '2020-07-21 11:06:33', '2020-07-21 11:06:33'),
(203, 153, '153-Nj599VXo1595501483.jpg', 'Y', 1, '2020-07-23 10:51:23', '2020-07-23 10:51:23'),
(204, 154, '154-8rDw1Hik1595501651.jpg', 'Y', 1, '2020-07-23 10:54:11', '2020-07-23 10:54:11'),
(205, 155, '155-KICFR2qW1595502151.jpg', 'Y', 1, '2020-07-23 11:02:31', '2020-07-23 11:02:31'),
(206, 156, '156-ygSXnD4L1595502253.jpg', 'Y', 1, '2020-07-23 11:04:13', '2020-07-23 11:04:13'),
(207, 157, '157-fHMRaZcj1595504051.jpg', 'Y', 1, '2020-07-23 11:34:11', '2020-07-23 11:34:11'),
(208, 158, '158-DLq5BCHe1595849773.jpg', 'Y', 1, '2020-07-27 11:36:13', '2020-07-27 11:36:13'),
(210, 160, '160-3VhH68Zi1595857143.jpg', 'Y', 1, '2020-07-27 13:39:03', '2020-07-27 13:39:03'),
(211, 161, '161-qsRxzO6K1595861264.jpg', 'Y', 1, '2020-07-27 14:47:44', '2020-07-27 14:47:44'),
(212, 162, '162-dmGZgEpH1595862092.jpg', 'Y', 1, '2020-07-27 15:01:32', '2020-07-27 15:01:32'),
(213, 163, '163-yRB4rVk91595863356.jpg', 'Y', 1, '2020-07-27 15:22:36', '2020-07-27 15:22:36'),
(214, 163, '163-BtxhvNkv1595863356.jpg', 'N', 1, '2020-07-27 15:22:36', '2020-07-27 15:22:36'),
(215, 164, '164-sffVL6Hz1595875626.jpg', 'Y', 1, '2020-07-27 18:47:06', '2020-07-27 18:47:06'),
(216, 165, '165-UrLTt6vQ1595926233.jpg', 'Y', 1, '2020-07-28 08:50:33', '2020-07-28 08:50:33'),
(217, 166, '166-FcPK0REY1595967899.jpg', 'Y', 1, '2020-07-28 20:24:59', '2020-07-28 20:24:59'),
(218, 167, '167-IDrxa1vd1595978022.jpg', 'Y', 1, '2020-07-28 23:13:42', '2020-07-28 23:13:42'),
(219, 168, '168-BI6vdjFO1595992995.jpg', 'Y', 1, '2020-07-29 03:23:15', '2020-07-29 03:23:15'),
(220, 169, '169-RrMeUjwb1595993577.jpg', 'Y', 1, '2020-07-29 03:32:57', '2020-07-29 03:32:57'),
(224, 173, '173-BGcqHEeT1596146979.jpg', 'Y', 1, '2020-07-30 22:09:39', '2020-07-30 22:09:39'),
(225, 174, '174-LxE6eQwa1596155867.jpg', 'Y', 1, '2020-07-31 00:37:47', '2020-07-31 00:37:47'),
(226, 175, '175-SAmW4QjJ1596188950.jpg', 'Y', 1, '2020-07-31 09:49:10', '2020-07-31 09:49:10'),
(227, 176, '176-5HFmnB0b1596188993.jpg', 'Y', 1, '2020-07-31 09:49:53', '2020-07-31 09:49:53'),
(228, 177, '177-pqNkK2Wn1596325018.jpg', 'Y', 1, '2020-08-01 23:36:58', '2020-08-01 23:36:58'),
(229, 178, '178-7Lp1O8Am1596325296.jpg', 'Y', 1, '2020-08-01 23:41:36', '2020-08-01 23:41:36'),
(230, 179, '179-2yQjXLaU1596553568.jpg', 'Y', 1, '2020-08-04 15:06:08', '2020-08-04 15:06:08'),
(231, 179, '179-aiPaBjHW1596553568.jpg', 'N', 1, '2020-08-04 15:06:08', '2020-08-04 15:06:08'),
(232, 179, '179-WjGYEW2A1596553568.jpg', 'N', 1, '2020-08-04 15:06:08', '2020-08-04 15:06:08'),
(233, 180, '180-efUHtPut1596882834.jpg', 'Y', 1, '2020-08-08 10:33:54', '2020-08-08 10:33:54'),
(234, 180, '180-FejpjXrw1596882834.jpg', 'N', 1, '2020-08-08 10:33:54', '2020-08-08 10:33:54'),
(235, 180, '180-eYIqwpvX1596882834.jpg', 'N', 1, '2020-08-08 10:33:54', '2020-08-08 10:33:54'),
(236, 181, '181-XtsuGg8R1596883698.jpg', 'Y', 1, '2020-08-08 10:48:18', '2020-08-08 10:48:18'),
(237, 181, '181-Udp7g2vP1596883698.mp4', 'N', 1, '2020-08-08 10:48:18', '2020-08-08 10:48:18'),
(238, 182, '182-fz6Rs9lc1596932062.jpg', 'Y', 1, '2020-08-09 00:14:23', '2020-08-09 00:14:23'),
(239, 182, '182-fz7ji5QF1596932063.jpg', 'N', 1, '2020-08-09 00:14:23', '2020-08-09 00:14:23'),
(240, 183, '183-8Won9mpK1596932481.jpg', 'Y', 1, '2020-08-09 00:21:21', '2020-08-09 00:21:21'),
(241, 184, '184-qXhII1UY1596935174.jpg', 'Y', 1, '2020-08-09 01:06:14', '2020-08-09 01:06:14'),
(242, 185, '185-XZE7PhDZ1596937201.jpg', 'Y', 1, '2020-08-09 01:40:02', '2020-08-09 01:40:02'),
(243, 186, '186-u1rOahGD1596937505.jpg', 'Y', 1, '2020-08-09 01:45:05', '2020-08-09 01:45:05'),
(244, 186, '186-yb4KsIiw1596937505.jpg', 'N', 1, '2020-08-09 01:45:05', '2020-08-09 01:45:05'),
(245, 187, '187-ZxE2siWI1596937562.jpg', 'Y', 1, '2020-08-09 01:46:02', '2020-08-09 01:46:02'),
(246, 187, '187-gS9v55T91596937562.jpg', 'N', 1, '2020-08-09 01:46:02', '2020-08-09 01:46:02'),
(247, 188, '188-fFWVhTFX1596937676.jpg', 'Y', 1, '2020-08-09 01:47:56', '2020-08-09 01:47:56'),
(248, 188, '188-x9k7z3uF1596937676.jpg', 'N', 1, '2020-08-09 01:47:56', '2020-08-09 01:47:56'),
(249, 189, '189-a55T3g6K1596938192.jpg', 'Y', 1, '2020-08-09 01:56:32', '2020-08-09 01:56:32'),
(250, 189, '189-6HWvG8rt1596938192.jpg', 'N', 1, '2020-08-09 01:56:32', '2020-08-09 01:56:32'),
(251, 189, '189-Yrafcgz81596938192.jpg', 'N', 1, '2020-08-09 01:56:32', '2020-08-09 01:56:32'),
(252, 189, '189-7yFfilBI1596938192.jpg', 'N', 1, '2020-08-09 01:56:32', '2020-08-09 01:56:32'),
(253, 189, '189-PCZLdVUm1596938192.jpg', 'N', 1, '2020-08-09 01:56:32', '2020-08-09 01:56:32'),
(254, 189, '189-4mhw4qgC1596938192.jpg', 'N', 1, '2020-08-09 01:56:32', '2020-08-09 01:56:32'),
(255, 189, '189-uxojulN91596938192.jpg', 'N', 1, '2020-08-09 01:56:32', '2020-08-09 01:56:32'),
(256, 189, '189-VcKzzSFk1596938192.jpg', 'N', 1, '2020-08-09 01:56:32', '2020-08-09 01:56:32'),
(257, 189, '189-uGVleuh11596938192.jpg', 'N', 1, '2020-08-09 01:56:32', '2020-08-09 01:56:32'),
(258, 190, '190-RyBnCej31596941045.jpg', 'Y', 1, '2020-08-09 02:44:05', '2020-08-09 02:44:05'),
(259, 190, '190-FXbfpzHS1596941045.jpg', 'N', 1, '2020-08-09 02:44:05', '2020-08-09 02:44:05'),
(260, 191, '191-0Czm21eI1597079071.jpg', 'Y', 1, '2020-08-10 17:04:31', '2020-08-10 17:04:31'),
(261, 192, '192-AoPGt9jv1597224187.jpg', 'Y', 1, '2020-08-12 09:23:07', '2020-08-12 09:23:07'),
(262, 193, '193-ynEUzIWW1597229275.mp4', 'Y', 2, '2020-08-12 10:47:55', '2020-08-12 10:47:55'),
(263, 194, '194-eS70DfOF1597229326.mp4', 'Y', 2, '2020-08-12 10:48:46', '2020-08-12 10:48:46'),
(264, 195, '195-NQ2uXyld1597233458.mp4', 'Y', 1, '2020-08-12 11:57:38', '2020-08-12 11:57:38'),
(265, 196, '196-K6RcIJtq1597306296.mp4', 'Y', 1, '2020-08-13 08:11:36', '2020-08-13 08:11:36'),
(266, 197, '197-b36sVItz1597306956.mp4', 'Y', 1, '2020-08-13 08:22:36', '2020-08-13 08:22:36'),
(267, 198, '198-AiD54yxL1597308905.mp4', 'Y', 1, '2020-08-13 08:55:05', '2020-08-13 08:55:05'),
(268, 201, '201-yvmdr08W1597309609.mp4', 'Y', 2, '2020-08-13 09:06:49', '2020-08-13 09:06:49'),
(269, 202, '202-d3hWslz61597309649.mp4', 'Y', 2, '2020-08-13 09:07:29', '2020-08-13 09:07:29'),
(270, 203, '203-38uIdjZs1597336518.jpg', 'Y', 1, '2020-08-13 16:35:18', '2020-08-13 16:35:18'),
(271, 204, '204-KqBnj3nS1597338434.jpg', 'Y', 1, '2020-08-13 17:07:14', '2020-08-13 17:07:14'),
(272, 205, '205-HkEJI1SW1597338523.jpg', 'Y', 1, '2020-08-13 17:08:43', '2020-08-13 17:08:43'),
(273, 206, '206-owZABFE91597385869.jpg', 'Y', 1, '2020-08-14 06:17:49', '2020-08-14 06:17:49'),
(274, 207, '207-26LPbVwk1597403290.jpg', 'Y', 1, '2020-08-14 11:08:10', '2020-08-14 11:08:10'),
(275, 211, '211-abvGN97d1597406925.mp4', 'Y', 2, '2020-08-14 12:08:45', '2020-08-14 12:08:45'),
(276, 212, '212-nJqcT7Pd1597415132.jpg', 'Y', 1, '2020-08-14 14:25:32', '2020-08-14 14:25:32'),
(277, 213, '213-DWlST1S71597422092.jpg', 'Y', 1, '2020-08-14 16:21:32', '2020-08-14 16:21:32'),
(283, 219, '219-oewCsMm91597530379.jpg', 'Y', 1, '2020-08-15 22:26:19', '2020-08-15 22:26:19'),
(284, 219, '219-wOXDcxFf1597530379.jpg', 'N', 1, '2020-08-15 22:26:19', '2020-08-15 22:26:19'),
(285, 220, '220-rsPUtIM21597664084.mp4', 'Y', 2, '2020-08-17 11:34:44', '2020-08-17 11:34:44'),
(286, 221, '221-yfDBPfPZ1597752728.mp4', 'Y', 2, '2020-08-18 12:12:08', '2020-08-18 12:12:08'),
(287, 222, '222-28ey7EFm1597753880.mp4', 'Y', 2, '2020-08-18 12:31:20', '2020-08-18 12:31:20'),
(288, 223, '223-xs93L6Iq1597756394.mp4', 'Y', 2, '2020-08-18 13:13:14', '2020-08-18 13:13:14'),
(289, 224, '224-ayhAVO6k1597756940.mp4', 'Y', 2, '2020-08-18 13:22:20', '2020-08-18 13:22:20'),
(290, 226, '226-fsjTTVLN1597757382.mp4', 'Y', 2, '2020-08-18 13:29:42', '2020-08-18 13:29:42'),
(291, 227, '227-g9oyUe2W1597758068.mp4', 'Y', 2, '2020-08-18 13:41:08', '2020-08-18 13:41:08'),
(292, 228, '228-BeOhpxqE1597817120.jpg', 'Y', 1, '2020-08-19 06:05:20', '2020-08-19 06:05:20'),
(293, 229, '229-EOxHXxJ91597917291.jpg', 'Y', 1, '2020-08-20 09:54:51', '2020-08-20 09:54:51'),
(294, 230, '230-eMrBolxh1597923373.jpg', 'Y', 1, '2020-08-20 11:36:13', '2020-08-20 11:36:13');

-- --------------------------------------------------------

--
-- Table structure for table `post_to_like`
--

CREATE TABLE `post_to_like` (
  `id` int(11) NOT NULL,
  `post_id` bigint(20) NOT NULL DEFAULT '0',
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `is_liked` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post_to_like`
--

INSERT INTO `post_to_like` (`id`, `post_id`, `user_id`, `is_liked`, `created_at`, `updated_at`) VALUES
(1, 12, 10, 'Y', NULL, NULL),
(2, 1, 10, 'Y', '2020-04-15 11:05:34', '2020-04-16 08:03:47'),
(3, 9, 15, 'N', '2020-05-12 15:00:13', '2020-05-12 15:00:33'),
(4, 12, 15, 'N', '2020-05-13 04:28:23', '2020-05-13 05:40:55'),
(5, 4, 28, 'N', '2020-05-15 13:28:46', '2020-05-15 13:28:47'),
(6, 5, 28, 'Y', '2020-05-15 13:28:51', '2020-05-15 13:28:51'),
(10, 8, 40, 'N', '2020-05-16 06:50:12', '2020-05-16 06:50:15'),
(14, 16, 46, 'N', '2020-05-17 10:44:30', '2020-05-17 10:44:33'),
(15, 23, 46, 'N', '2020-05-18 13:51:40', '2020-05-18 13:51:52'),
(16, 19, 49, 'Y', '2020-05-19 12:23:00', '2020-05-19 12:23:00'),
(17, 17, 53, 'Y', '2020-05-20 11:50:27', '2020-05-20 11:50:27'),
(18, 24, 53, 'Y', '2020-05-20 14:12:49', '2020-05-20 14:12:49'),
(19, 28, 54, 'Y', '2020-05-20 15:13:44', '2020-05-20 15:13:44'),
(20, 29, 52, 'Y', '2020-05-20 16:52:31', '2020-05-20 16:52:36'),
(21, 28, 52, 'N', '2020-05-20 16:52:40', '2020-05-22 05:17:51'),
(22, 31, 52, 'N', '2020-05-20 17:11:58', '2020-05-20 17:12:01'),
(23, 30, 52, 'N', '2020-05-20 17:12:07', '2020-05-20 17:12:14'),
(24, 33, 76, 'Y', '2020-05-22 05:53:23', '2020-05-22 05:53:23'),
(25, 32, 76, 'Y', '2020-05-22 05:53:29', '2020-05-22 05:53:29'),
(26, 34, 79, 'Y', '2020-05-22 07:53:16', '2020-05-22 07:53:16'),
(27, 39, 79, 'Y', '2020-05-22 08:03:26', '2020-05-22 08:03:26'),
(28, 45, 18, 'N', '2020-05-25 08:07:09', '2020-05-25 08:07:10'),
(29, 43, 18, 'Y', '2020-05-25 08:13:45', '2020-05-25 08:13:47'),
(30, 42, 18, 'Y', '2020-05-25 08:13:55', '2020-05-25 08:13:55'),
(38, 52, 52, 'Y', '2020-05-29 08:10:42', '2020-05-29 08:10:42'),
(39, 52, 105, 'Y', '2020-05-30 06:14:56', '2020-05-30 06:14:56'),
(40, 52, 18, 'Y', '2020-05-30 18:44:02', '2020-06-01 13:47:30'),
(41, 51, 18, 'N', '2020-05-30 18:44:03', '2020-06-01 04:49:29'),
(42, 25, 18, 'Y', '2020-05-30 19:30:27', '2020-05-30 19:30:27'),
(43, 41, 18, 'Y', '2020-06-01 05:19:15', '2020-06-01 05:19:15'),
(44, 35, 18, 'Y', '2020-06-01 05:30:34', '2020-06-01 05:30:34'),
(45, 58, 105, 'Y', '2020-06-04 10:25:44', '2020-06-04 10:25:44'),
(46, 59, 105, 'Y', '2020-06-05 06:03:03', '2020-06-05 06:03:03'),
(47, 24, 105, 'Y', '2020-06-05 07:13:33', '2020-06-05 07:13:48'),
(48, 27, 105, 'Y', '2020-06-05 07:14:11', '2020-06-05 07:16:09'),
(49, 31, 105, 'Y', '2020-06-05 07:14:55', '2020-06-05 07:14:55'),
(50, 30, 105, 'Y', '2020-06-05 07:15:04', '2020-06-05 07:15:04'),
(51, 29, 105, 'Y', '2020-06-05 07:15:09', '2020-06-05 07:15:09'),
(52, 28, 105, 'Y', '2020-06-05 07:15:16', '2020-06-05 07:15:16'),
(53, 26, 105, 'Y', '2020-06-05 07:16:15', '2020-06-05 07:16:15'),
(54, 49, 105, 'Y', '2020-06-05 07:16:57', '2020-06-05 07:16:57'),
(55, 44, 105, 'Y', '2020-06-05 07:17:05', '2020-06-05 07:17:05'),
(56, 41, 105, 'Y', '2020-06-05 07:17:16', '2020-06-05 07:17:16'),
(57, 34, 105, 'N', '2020-06-05 07:17:26', '2020-06-05 07:17:29'),
(58, 61, 105, 'Y', '2020-06-05 10:07:45', '2020-06-05 10:07:45'),
(61, 62, 18, 'Y', '2020-06-11 07:21:57', '2020-06-11 07:21:57'),
(62, 85, 18, 'Y', '2020-06-16 11:18:50', '2020-06-16 11:18:50'),
(63, 94, 18, 'Y', '2020-06-17 13:57:27', '2020-06-17 13:57:27'),
(64, 98, 109, 'Y', '2020-06-18 11:26:42', '2020-06-18 11:26:56'),
(65, 97, 109, 'Y', '2020-06-18 11:38:21', '2020-06-18 11:38:21'),
(66, 92, 109, 'Y', '2020-06-19 09:52:35', '2020-06-19 09:52:35'),
(67, 103, 109, 'Y', '2020-06-19 10:03:05', '2020-06-19 10:03:05'),
(68, 103, 18, 'N', '2020-06-19 10:08:50', '2020-06-19 10:53:08'),
(69, 104, 18, 'N', '2020-06-19 10:52:00', '2020-06-19 10:52:59'),
(70, 102, 18, 'N', '2020-06-19 10:53:14', '2020-06-19 10:53:15'),
(71, 104, 109, 'Y', '2020-06-19 10:55:04', '2020-06-19 10:55:04'),
(78, 113, 116, 'Y', '2020-06-20 11:57:12', '2020-06-20 11:57:12'),
(79, 117, 117, 'Y', '2020-06-20 15:33:02', '2020-06-20 15:33:02'),
(80, 115, 117, 'Y', '2020-06-20 15:33:20', '2020-06-20 15:42:57'),
(81, 117, 121, 'Y', '2020-06-21 08:56:52', '2020-06-21 08:56:52'),
(82, 115, 121, 'Y', '2020-06-21 08:57:25', '2020-06-21 08:57:25'),
(83, 113, 121, 'Y', '2020-06-21 08:57:31', '2020-06-21 08:57:31'),
(84, 117, 123, 'Y', '2020-06-21 13:19:20', '2020-06-21 13:19:20'),
(85, 115, 123, 'Y', '2020-06-21 13:19:27', '2020-06-21 14:07:00'),
(86, 113, 123, 'Y', '2020-06-21 14:00:05', '2020-06-21 14:00:05'),
(87, 111, 123, 'Y', '2020-06-21 14:03:58', '2020-06-21 14:03:58'),
(88, 118, 115, 'Y', '2020-06-21 14:38:20', '2020-06-21 14:38:21'),
(89, 113, 115, 'Y', '2020-06-22 12:33:11', '2020-06-22 12:33:14'),
(90, 121, 115, 'N', '2020-06-24 09:31:21', '2020-06-24 09:31:24'),
(91, 120, 115, 'N', '2020-06-24 09:31:26', '2020-06-24 09:31:26'),
(92, 121, 126, 'Y', '2020-06-27 12:39:30', '2020-06-27 12:39:30'),
(93, 121, 141, 'N', '2020-07-01 05:54:25', '2020-07-01 06:02:10'),
(94, 112, 18, 'N', '2020-07-01 06:21:17', '2020-07-01 06:21:20'),
(95, 118, 141, 'Y', '2020-07-01 09:12:09', '2020-07-01 09:12:09'),
(100, 124, 18, 'Y', '2020-07-20 04:39:20', '2020-07-20 04:39:20'),
(103, 140, 109, 'Y', '2020-07-21 06:18:51', '2020-07-21 06:18:51'),
(104, 140, 156, 'N', '2020-07-21 06:33:39', '2020-07-21 06:34:12'),
(105, 148, 18, 'Y', '2020-07-21 12:17:32', '2020-07-21 12:17:37'),
(106, 151, 18, 'Y', '2020-07-21 12:22:39', '2020-07-23 11:08:10'),
(108, 151, 159, 'Y', '2020-07-22 04:39:22', '2020-07-22 04:39:24'),
(109, 144, 159, 'Y', '2020-07-22 04:39:42', '2020-07-22 04:39:42'),
(118, 169, 160, 'Y', '2020-07-30 12:21:46', '2020-07-30 12:21:46'),
(119, 166, 160, 'N', '2020-07-30 14:00:16', '2020-07-30 14:00:33'),
(120, 176, 77779, 'Y', '2020-07-31 10:48:21', '2020-07-31 10:48:21'),
(121, 143, 77779, 'N', '2020-07-31 12:54:08', '2020-07-31 12:54:11'),
(122, 175, 161, 'Y', '2020-08-01 11:53:18', '2020-08-01 11:53:18'),
(123, 154, 161, 'Y', '2020-08-01 11:53:30', '2020-08-01 11:53:30'),
(124, 175, 77779, 'Y', '2020-08-01 16:45:42', '2020-08-01 16:45:42'),
(125, 154, 77779, 'Y', '2020-08-01 16:45:53', '2020-08-01 16:45:53'),
(126, 149, 77779, 'Y', '2020-08-01 16:52:04', '2020-08-01 16:52:04'),
(127, 134, 160, 'N', '2020-08-03 14:39:13', '2020-08-03 14:39:13'),
(128, 180, 77779, 'Y', '2020-08-08 10:38:28', '2020-08-08 10:38:28'),
(129, 190, 77779, 'Y', '2020-08-09 04:44:50', '2020-08-11 06:37:53'),
(130, 183, 103, 'Y', '2020-08-10 12:30:34', '2020-08-10 12:30:34'),
(131, 182, 103, 'Y', '2020-08-10 12:30:41', '2020-08-10 12:30:41'),
(132, 195, 160, 'Y', '2020-08-12 22:29:24', '2020-08-12 22:29:24'),
(133, 195, 77779, 'N', '2020-08-13 04:03:38', '2020-08-13 04:40:26'),
(134, 194, 77779, 'N', '2020-08-13 04:03:50', '2020-08-13 04:21:02'),
(135, 193, 77779, 'Y', '2020-08-13 04:21:04', '2020-08-13 04:41:00'),
(136, 203, 77781, 'N', '2020-08-14 10:36:35', '2020-08-14 10:36:39'),
(137, 205, 103, 'Y', '2020-08-14 10:44:26', '2020-08-14 10:44:26'),
(138, 211, 77779, 'N', '2020-08-14 14:08:29', '2020-08-14 14:08:33'),
(139, 212, 77779, 'Y', '2020-08-14 15:18:00', '2020-08-14 15:18:00'),
(140, 213, 77779, 'Y', '2020-08-14 16:39:19', '2020-08-15 22:06:41'),
(141, 213, 77778, 'Y', '2020-08-14 16:48:37', '2020-08-14 16:48:37'),
(142, 189, 77779, 'Y', '2020-08-15 18:55:51', '2020-08-15 18:55:51'),
(143, 220, 146, 'Y', '2020-08-18 13:11:03', '2020-08-18 13:11:03'),
(144, 227, 77779, 'N', '2020-08-19 06:04:21', '2020-08-19 06:07:36'),
(145, 228, 77779, 'Y', '2020-08-19 06:05:37', '2020-08-19 06:08:26'),
(146, 228, 77778, 'Y', '2020-08-19 06:08:49', '2020-08-20 15:21:17');

-- --------------------------------------------------------

--
-- Table structure for table `post_to_share`
--

CREATE TABLE `post_to_share` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post_to_share`
--

INSERT INTO `post_to_share` (`id`, `post_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 12, 15, '2020-05-13 04:28:58', '2020-05-13 04:28:58'),
(2, 7, 40, '2020-05-16 06:00:43', '2020-05-16 06:00:43'),
(3, 8, 40, '2020-05-16 06:00:51', '2020-05-16 06:00:51'),
(4, 11, 40, '2020-05-16 06:48:48', '2020-05-16 06:48:48'),
(5, 23, 46, '2020-05-18 13:32:09', '2020-05-18 13:32:09'),
(6, 19, 49, '2020-05-19 12:23:13', '2020-05-19 12:23:13'),
(7, 39, 90, '2020-05-23 22:59:42', '2020-05-23 22:59:42'),
(8, 44, 18, '2020-05-25 08:06:37', '2020-05-25 08:06:37'),
(9, 44, 18, '2020-05-25 08:06:42', '2020-05-25 08:06:42'),
(10, 43, 18, '2020-05-25 08:06:51', '2020-05-25 08:06:51'),
(13, 45, 52, '2020-05-28 08:28:17', '2020-05-28 08:28:17'),
(14, 92, 109, '2020-06-19 10:02:28', '2020-06-19 10:02:28'),
(16, 113, 116, '2020-06-20 11:57:50', '2020-06-20 11:57:50'),
(17, 113, 116, '2020-06-20 11:57:51', '2020-06-20 11:57:51'),
(18, 113, 116, '2020-06-20 11:57:51', '2020-06-20 11:57:51'),
(19, 113, 116, '2020-06-20 11:58:12', '2020-06-20 11:58:12'),
(20, 117, 120, '2020-06-20 15:16:31', '2020-06-20 15:16:31'),
(21, 117, 120, '2020-06-20 15:16:32', '2020-06-20 15:16:32'),
(22, 115, 117, '2020-06-20 15:33:21', '2020-06-20 15:33:21'),
(24, 113, 121, '2020-06-21 08:57:40', '2020-06-21 08:57:40'),
(25, 111, 123, '2020-06-21 14:04:01', '2020-06-21 14:04:01'),
(26, 104, 123, '2020-06-21 14:06:20', '2020-06-21 14:06:20'),
(27, 111, 123, '2020-06-21 14:07:08', '2020-06-21 14:07:08'),
(28, 115, 123, '2020-06-21 14:08:19', '2020-06-21 14:08:19'),
(29, 112, 123, '2020-06-21 14:10:46', '2020-06-21 14:10:46'),
(30, 111, 123, '2020-06-21 14:11:49', '2020-06-21 14:11:49');

-- --------------------------------------------------------

--
-- Table structure for table `post_to_step`
--

CREATE TABLE `post_to_step` (
  `id` int(11) NOT NULL,
  `post_id` bigint(20) NOT NULL DEFAULT '0',
  `recipe_id` bigint(20) NOT NULL DEFAULT '0',
  `step` bigint(20) NOT NULL DEFAULT '0',
  `instruction` text,
  `media_file_1` text,
  `media_file_2` text,
  `media_file_3` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post_to_step`
--

INSERT INTO `post_to_step` (`id`, `post_id`, `recipe_id`, `step`, `instruction`, `media_file_1`, `media_file_2`, `media_file_3`, `created_at`, `updated_at`) VALUES
(1, 8, 2, 1, 'This is dummy instruction', '1vskK461586532187.jpg', '1KDamjf1586532187.jpg', '1Kg4MOK1586532187.mp4', '2020-04-10 09:53:07', '2020-04-10 09:53:07'),
(2, 9, 3, 1, 'This is 1st instruction', '2slhFUp1586532561.jpg', '2kW8a0g1586532561.jpg', '2zDb1Vc1586532561.mp4', '2020-04-10 09:59:20', '2020-04-10 09:59:21'),
(3, 9, 3, 2, 'This is 2nd instruction', '3a9mJtt1586532562.mp4', '3cyvJRt1586532562.mp4', '37XqZsl1586532562.mp4', '2020-04-10 09:59:21', '2020-04-10 09:59:22'),
(4, 10, 4, 1, 'This is 1st instruction', '4yhDqei1586534606.jpg', '4KUiKkA1586534606.jpg', '4opFEog1586534606.mp4', '2020-04-10 10:33:26', '2020-04-10 10:33:26'),
(5, 11, 5, 1, 'This is 1st instruction', '5iUGbn01586618909.jpg', '5LQ8fab1586618909.jpg', '5qu0fGF1586618909.mp4', '2020-04-11 09:58:28', '2020-04-11 09:58:29'),
(6, 12, 6, 1, 'This is 1st instruction', '6LAps7m1586618933.jpg', '6A0BOxZ1586618933.jpg', '6v1OYg31586618933.mp4', '2020-04-11 09:58:53', '2020-04-11 09:58:53');

-- --------------------------------------------------------

--
-- Table structure for table `post_to_tagged_user`
--

CREATE TABLE `post_to_tagged_user` (
  `id` int(11) NOT NULL,
  `post_id` bigint(20) NOT NULL DEFAULT '0',
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `friend_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post_to_tagged_user`
--

INSERT INTO `post_to_tagged_user` (`id`, `post_id`, `user_id`, `friend_id`, `created_at`, `updated_at`) VALUES
(1, 1, 9, 3, '2020-04-10 08:05:40', '2020-04-10 08:05:40'),
(2, 1, 9, 4, '2020-04-10 08:05:40', '2020-04-10 08:05:40'),
(3, 1, 9, 5, '2020-04-10 08:05:40', '2020-04-10 08:05:40'),
(4, 2, 9, 3, '2020-04-10 08:06:55', '2020-04-10 08:06:55'),
(5, 2, 9, 4, '2020-04-10 08:06:55', '2020-04-10 08:06:55'),
(6, 2, 9, 5, '2020-04-10 08:06:55', '2020-04-10 08:06:55'),
(7, 3, 9, 3, '2020-04-10 08:07:27', '2020-04-10 08:07:27'),
(8, 3, 9, 4, '2020-04-10 08:07:27', '2020-04-10 08:07:27'),
(9, 3, 9, 5, '2020-04-10 08:07:27', '2020-04-10 08:07:27'),
(10, 4, 9, 3, '2020-04-10 08:13:12', '2020-04-10 08:13:12'),
(11, 4, 9, 4, '2020-04-10 08:13:12', '2020-04-10 08:13:12'),
(12, 4, 9, 5, '2020-04-10 08:13:12', '2020-04-10 08:13:12'),
(13, 7, 9, 3, '2020-04-10 09:25:21', '2020-04-10 09:25:21'),
(14, 7, 9, 4, '2020-04-10 09:25:21', '2020-04-10 09:25:21'),
(15, 7, 9, 5, '2020-04-10 09:25:21', '2020-04-10 09:25:21'),
(16, 8, 9, 3, '2020-04-10 09:53:07', '2020-04-10 09:53:07'),
(17, 8, 9, 4, '2020-04-10 09:53:08', '2020-04-10 09:53:08'),
(18, 8, 9, 5, '2020-04-10 09:53:08', '2020-04-10 09:53:08'),
(19, 9, 9, 3, '2020-04-10 09:59:22', '2020-04-10 09:59:22'),
(20, 9, 9, 4, '2020-04-10 09:59:22', '2020-04-10 09:59:22'),
(21, 9, 9, 5, '2020-04-10 09:59:22', '2020-04-10 09:59:22'),
(22, 10, 9, 3, '2020-04-10 10:33:26', '2020-04-10 10:33:26'),
(23, 10, 9, 4, '2020-04-10 10:33:26', '2020-04-10 10:33:26'),
(24, 10, 9, 5, '2020-04-10 10:33:26', '2020-04-10 10:33:26'),
(25, 11, 10, 3, '2020-04-11 09:58:29', '2020-04-11 09:58:29'),
(26, 11, 10, 4, '2020-04-11 09:58:29', '2020-04-11 09:58:29'),
(27, 11, 10, 5, '2020-04-11 09:58:29', '2020-04-11 09:58:29'),
(28, 12, 10, 3, '2020-04-11 09:58:53', '2020-04-11 09:58:53'),
(29, 12, 10, 4, '2020-04-11 09:58:53', '2020-04-11 09:58:53'),
(30, 12, 10, 5, '2020-04-11 09:58:53', '2020-04-11 09:58:53'),
(31, 89, 18, 108, '2020-06-17 12:01:54', '2020-06-17 12:01:54'),
(32, 94, 18, 108, '2020-06-17 12:27:18', '2020-06-17 12:27:18'),
(33, 96, 18, 108, '2020-06-18 09:07:23', '2020-06-18 09:07:23'),
(34, 96, 18, 74, '2020-06-18 09:07:23', '2020-06-18 09:07:23'),
(35, 97, 18, 108, '2020-06-18 09:08:34', '2020-06-18 09:08:34'),
(36, 97, 18, 74, '2020-06-18 09:08:34', '2020-06-18 09:08:34'),
(37, 111, 74, 74, '2020-06-19 13:36:15', '2020-06-19 13:36:15'),
(38, 192, 103, 29, '2020-08-12 09:23:07', '2020-08-12 09:23:07'),
(43, 228, 77779, 43, '2020-08-19 06:05:20', '2020-08-19 06:05:20'),
(44, 229, 103, 29, '2020-08-20 09:54:51', '2020-08-20 09:54:51');

-- --------------------------------------------------------

--
-- Table structure for table `usage_gamification`
--

CREATE TABLE `usage_gamification` (
  `id` int(11) NOT NULL,
  `name` text,
  `image` text,
  `no_of_days` bigint(20) NOT NULL DEFAULT '0',
  `is_top` enum('Y','N') NOT NULL DEFAULT 'N',
  `level` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usage_gamification`
--

INSERT INTO `usage_gamification` (`id`, `name`, `image`, `no_of_days`, `is_top`, `level`, `created_at`, `updated_at`) VALUES
(6, 'New user signup', 'ruKW14dqrCjZ-1597210453.png', 1, 'N', 1, '2020-08-12 05:34:13', '2020-08-12 05:34:13'),
(7, '15 days of usage', 'ljCnLHg7asyq-1597210516.png', 15, 'N', 2, '2020-08-12 05:35:16', '2020-08-12 05:35:16'),
(8, '30 days of usage', 'Ilt66ZZVRBPl-1597210548.png', 30, 'N', 3, '2020-08-12 05:35:48', '2020-08-12 05:35:48'),
(9, '60 days of usage', 'EqmlNAuX1Ra2-1597210574.png', 60, 'N', 4, '2020-08-12 05:36:14', '2020-08-12 05:36:14'),
(10, '90 days of usage', 'vvOOQ6qm1K12-1597210608.png', 90, 'N', 5, '2020-08-12 05:36:48', '2020-08-12 05:36:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` text COLLATE utf8mb4_unicode_ci,
  `first_name` text COLLATE utf8mb4_unicode_ci,
  `last_name` text COLLATE utf8mb4_unicode_ci,
  `dob` date DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `profile_pic` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_token` text COLLATE utf8mb4_unicode_ci,
  `user_type` enum('A','U') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'U',
  `account_type` enum('PER','PRO') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PER',
  `is_post_private` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `professional_account_type` enum('BU','BL') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_website_url` text COLLATE utf8mb4_unicode_ci,
  `business_address` text COLLATE utf8mb4_unicode_ci,
  `registration_complete` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `status` enum('I','A') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `vcode` text COLLATE utf8mb4_unicode_ci,
  `device_registrartion_id` text COLLATE utf8mb4_unicode_ci,
  `is_online` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `device_type` enum('A','I') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `cover_pic` text COLLATE utf8mb4_unicode_ci,
  `last_used` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `first_name`, `last_name`, `dob`, `email_verified_at`, `password`, `bio`, `profile_pic`, `remember_token`, `user_token`, `user_type`, `account_type`, `is_post_private`, `professional_account_type`, `business_website_url`, `business_address`, `registration_complete`, `status`, `vcode`, `device_registrartion_id`, `is_online`, `device_type`, `cover_pic`, `last_used`, `created_at`, `updated_at`) VALUES
(1, 'Fsquad Admin', 'admin@fsquad.com', NULL, NULL, NULL, NULL, NULL, '$2y$10$6Jfkm8HyXA0tYG.cwfILi.ARezZBnShekfFLNsEn3UnaIJP2CtTcm', NULL, NULL, NULL, '$2y$10$RR0SkJLMWVHGTvWs4vID6.Ie5T7qIj8A8BGMt05Rpr.ZCtzVZr.ii', 'A', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'xxaaaasasasdds', 'N', 'A', NULL, NULL, '2020-02-29 09:29:42', '2020-04-15 10:02:50'),
(9, '@Gupta-Abhisek', 'abhishek.sun1996@gmail.com', NULL, 'Abhisek', 'Gupta', '1996-04-03', NULL, '$2y$10$6Jfkm8HyXA0tYG.cwfILi.ARezZBnShekfFLNsEn3UnaIJP2CtTcm', NULL, NULL, NULL, '$2y$10$IY2VTEvWLoDxL.OmUAgbd.UrTTM5kXkG663OD4Jv8Hk6Kah8TE6WS', 'U', 'PER', 'Y', NULL, NULL, NULL, 'N', 'A', NULL, 'xxaaaasasasdds', 'N', 'A', NULL, NULL, '2020-03-20 01:17:07', '2020-04-24 08:07:48'),
(10, '@Blogger-Abhisek#10yQSA78', 'blogger@fsquad.com', NULL, 'Blogger', 'Abhisek', '1992-03-12', NULL, '$2y$10$KyX/mImhv/OX888JQJDjIuW9N7B8Vdl6pTPNA6zI8f.ziPQyQdqBm', NULL, NULL, NULL, '$2y$10$tHgbnVdyxi5Lh/Lys5n9S.w6kxFUqXPUFagJSVCmUp2eQDPhdc9nG', 'U', 'PRO', 'Y', 'BU', NULL, 'Bhowanipore', 'N', 'A', NULL, 'xxaaaasasasdds', 'N', 'A', NULL, NULL, '2020-03-21 09:33:00', '2020-04-20 09:15:38'),
(11, 'RANAa', 'ranapratap98@gmail.com', '9891240184', 'Rana', 'Pandey', '1996-06-15', NULL, '$2y$10$hjEd/qC9/hi73Jizcp2V2ua1RGXlKQ568stDw2C5reqLMkyqM6O4W', 'Default', '/tmp/phppdKu0R', NULL, '$2y$10$C0xoAN9PC8XUik8vHcAfzOqGoXOFP.m8Ts5IaHCW5H5i1nctmhaLu', 'U', 'PER', 'Y', NULL, NULL, NULL, 'Y', 'A', NULL, 'qsqs', 'N', 'A', NULL, NULL, '2020-05-08 12:03:30', '2020-05-11 07:28:30'),
(12, '@Rana-Pandey#12npklqu', 'abcd@gmail.com', '9891240184', 'xyz', 'xyz', '1996-06-15', NULL, '$2y$10$thK0urjd.UA/wwci3dsXwuwG2RWJMgn.yLSkirR3O7S6Molqxt1fK', 'Default', NULL, NULL, '$2y$10$WxkbLJ9AVbx.UGp2EXxGtesGUhuBO8sxNc5GqwubWDkw8PxZ3JYU.', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'qsqs', 'N', 'A', NULL, NULL, '2020-05-11 07:47:42', '2020-05-11 08:28:04'),
(13, '@Rana-Pandey#138HY6Fn', 'email@gmail.com', '9891240184', 'Rana', 'Pandey', '2020-06-15', NULL, '$2y$10$fEmVqUZrthAEurc7jy09w.y/piFIniotYZ4kEt4U.s5bBcv2aauHq', 'Default', NULL, NULL, '$2y$10$RN.Uhtlq5O.vfszWMUMM..t3V.cAqJdVmrZd30Ug.62C8q4u5AmAO', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'qsqs', 'N', 'A', NULL, NULL, '2020-05-11 08:31:59', '2020-05-11 10:20:29'),
(14, '@Rana-Panday#14Uq4ZDK', 'aviksingh04@gmail.com', NULL, 'Rana', 'Panday', '1996-06-15', NULL, '$2y$10$H3WdTVPIhETb/nT0DCAq6OczDxsIbAQUqEsbQN3Z80WRoC695zB82', NULL, NULL, NULL, '$2y$10$PB2RMnuJPcbPjj37IqxYVejFU25wFkfOTxbzLjh6n9CMn5VTQ2M8W', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'qsqs', 'N', 'A', NULL, NULL, '2020-05-12 10:44:41', '2020-05-12 10:51:30'),
(15, 'Rana-Pandey', 'ranapratap698@gmail.com', '8182004007', 'Rana', 'Pandey', '1996-06-15', NULL, '$2y$10$v8C6hOsQsnC4e1FAXaICzONPomThtGCL7GqIXCAZNqwFIyJ0ToTSq', 'Default', NULL, NULL, '$2y$10$SYS4lfCATEctR7zHOsQPW..UeSy9W7TwE5MhXalDc8sm7lTEoyYoy', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'qsqs', 'N', 'A', NULL, NULL, '2020-05-12 13:16:20', '2020-05-13 12:29:17'),
(16, '@Rana -Hdjr#16i1GnhQ', 'Rana123@gmail.com', NULL, 'Rana ', 'Hdjr', '2020-05-11', NULL, '$2y$10$MFkDf/arWduTQj9GQaNOTuKCpUP0uLlvX6zkYHCxGBE4kDTenOSsu', NULL, NULL, NULL, '$2y$10$9h.xPFCJa5o0rlTDQkXgg.vReY2vXYi/SfoF3..MNDBbkKkA3xMVK', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dTnEPghvkBI:APA91bGujU6zWtZvo4H_XICSB_7Aqjiced0w-knVp_Ax1_5l2jyH_FTHdsFNRwKi4pOzP8fyM1S52gUkXBGJLNL5bRuC8Bo53n4-QF1gU-8RUVbIsRi-kiJWygHaImENAe5t75pSdT6h', 'N', 'A', NULL, NULL, '2020-05-13 05:44:41', '2020-05-15 11:35:31'),
(18, 'inshad_150pS', NULL, '7275583095', 'inshad', 'ansari', '2020-07-17', NULL, '$2y$10$NKHerohKWKokFC4h/EGCHe05UyCgGKk2cV.5biiIMAstqQU2KRyFy', 'default', NULL, NULL, '$2y$10$O.aaAnRVpFPbu/dxlnfSg.GZfhhMNJfeo88rnmiqyL5bpo11QKgwm', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', '228531', 'hhdhdfd', 'N', 'A', NULL, 1408492800, '2020-07-17 12:56:00', '2020-08-14 03:21:06'),
(19, '@rana-pandey#19SThf0r', 'Mobolous@gmail.com', '8340748427', 'rana', 'pandey', '1999-05-01', NULL, '$2y$10$bWAHNgQNM3NlpITQSRwJF.Eg.4mSzDzCqlz4eYsoEj30aaY6.7Pai', 'Default', '19-ynpviNvS1589377197.jpg', NULL, '$2y$10$.cMVQgHGPDj5DAjMigrrUeipPoWSTxOGPl5GfveUoTIeeAkI92yW.', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'ef759cS6q1E:APA91bGgnDxsc9PauDfTtn5wgnLyQWQOSAo_QWuRhyxdVFO8CqM0t9tjk7tMUiZEMONPITUXZsAXJRfx_6aZsMc5qU_DBMI7jKRrptnhzXqyrGgevHtO0_iZ4z8JeexjtPqPpryMfiQg', 'N', 'A', NULL, NULL, '2020-05-13 12:37:25', '2020-05-13 13:39:57'),
(21, '@Rana-Pandey#21tSiUL9', 'Mob@gmail.com', '8340748427', 'Rana', 'Pandey', '1996-05-16', NULL, '$2y$10$GCyps3CbZzz.pcql148ST.jRY5DFyVA1W0tUZNA87BO9ZudJP3Z8G', 'Default', '21-XZDDB0BW1589383053.jpg', NULL, '$2y$10$OZm4VPcAbECvKkwqpEeWsObqdkZa0jAWp2QE0QW8VO62xG.5zeI56', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'ef759cS6q1E:APA91bGgnDxsc9PauDfTtn5wgnLyQWQOSAo_QWuRhyxdVFO8CqM0t9tjk7tMUiZEMONPITUXZsAXJRfx_6aZsMc5qU_DBMI7jKRrptnhzXqyrGgevHtO0_iZ4z8JeexjtPqPpryMfiQg', 'N', 'A', NULL, NULL, '2020-05-13 14:59:03', '2020-05-13 15:17:33'),
(22, '@Rana-pandey#22hLvPa2', 'Ranaprtap698@gmail.com', '98989898989', 'Rana', 'pandey', '1996-05-15', NULL, '$2y$10$8eSL8S2L6hDBNQuyRIgeaubO8IwEnCWb2/PDban2gh50d4jQ/UaOS', 'Default', '22-u970O1Kg1589433077.jpg', NULL, '$2y$10$SFdnwOlaDQm5dfJD2ocvMOtV0NvMy5SqA64Q.sssi5uV4HWr2s4Lq', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'ef759cS6q1E:APA91bGgnDxsc9PauDfTtn5wgnLyQWQOSAo_QWuRhyxdVFO8CqM0t9tjk7tMUiZEMONPITUXZsAXJRfx_6aZsMc5qU_DBMI7jKRrptnhzXqyrGgevHtO0_iZ4z8JeexjtPqPpryMfiQg', 'N', 'A', NULL, NULL, '2020-05-14 04:45:32', '2020-05-14 05:11:17'),
(23, 'Mobolous', 'rana@gmail.com', '7277466124', 'rana', 'pandey', '1996-05-15', NULL, '$2y$10$W5hIadMpTdKSmSRu6CZ.OOhN0FOYs9cSJCWZPsGiKnq2TW8B/.zd6', NULL, NULL, NULL, '$2y$10$cARenSO0DpT5SsT3Lb6CoexipV.4WHmI1yqc09r.j1CR0Cq5eFjSO', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'ef759cS6q1E:APA91bGgnDxsc9PauDfTtn5wgnLyQWQOSAo_QWuRhyxdVFO8CqM0t9tjk7tMUiZEMONPITUXZsAXJRfx_6aZsMc5qU_DBMI7jKRrptnhzXqyrGgevHtO0_iZ4z8JeexjtPqPpryMfiQg', 'N', 'A', NULL, NULL, '2020-05-14 05:30:25', '2020-05-14 06:19:07'),
(24, 'ranapandey', 'Mobiloitte@gmail.com', '8340748427', 'mobolous', 'technologies', '2014-05-14', NULL, '$2y$10$4bBZMZoF4xkHcOMkwnnIHe7xm55bkpF10Stin4tDwPuQAIHTLXfWu', 'Default', '24-E6Kg6w021589439169.jpg', NULL, '$2y$10$8kb2A2dEW5wJX4yJ02LPc.1XvHzpTtJG1os64G7egHzDcPMhk2b.O', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'ef759cS6q1E:APA91bGgnDxsc9PauDfTtn5wgnLyQWQOSAo_QWuRhyxdVFO8CqM0t9tjk7tMUiZEMONPITUXZsAXJRfx_6aZsMc5qU_DBMI7jKRrptnhzXqyrGgevHtO0_iZ4z8JeexjtPqPpryMfiQg', 'N', 'A', NULL, NULL, '2020-05-14 06:21:00', '2020-05-14 06:52:49'),
(25, 'RanaPandey', 'rana698@gmail.com', '8340748427', 'Mobolous', 'Technologies', '2013-05-14', NULL, '$2y$10$VK7Jll8zvbUgYvRo7YJtD.EMSV6RNz2rxz7wghg63pHmmdrx8uv4q', 'Default', '25-fBlw6Rv11589441117.jpg', NULL, '$2y$10$k9uudqpxjy5AQNp7Y4bF5.BplSmmn4Foxz8TLFigUmNT.pSALg8wy', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'qsqs', 'N', 'A', NULL, NULL, '2020-05-14 07:19:42', '2020-05-14 07:31:31'),
(26, '@rana-pandey#26ZIStf3', 'mon698@gmail.com', NULL, 'rana', 'pandey', '2016-05-15', NULL, '$2y$10$JCYiEVBtAegz2tFE.5c9MONr0kHNic1.8XFr8ybzFqriCGbdgHuxG', NULL, NULL, NULL, '$2y$10$EyeisPgVKEPEIrJ0Ic0AFO2DKSEvqs6FZENhNOgnwipYXYYAmNzaK', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'ef759cS6q1E:APA91bGgnDxsc9PauDfTtn5wgnLyQWQOSAo_QWuRhyxdVFO8CqM0t9tjk7tMUiZEMONPITUXZsAXJRfx_6aZsMc5qU_DBMI7jKRrptnhzXqyrGgevHtO0_iZ4z8JeexjtPqPpryMfiQg', 'N', 'A', NULL, NULL, '2020-05-15 08:54:04', '2020-05-15 08:54:51'),
(27, 'Rana', 'Hdjdkdk', '8340748427', 'Rana', 'Pandey', '2020-05-18', NULL, '$2y$10$zdQJJ8GcY0lBpJlFg0s5r.gerJ9PI6Y3/cgSHeQEVy18koOUX4tJ.', 'Default', '27-nSaVEnyQ1589545909.jpg', NULL, '$2y$10$ENx3SHXDODGnKlu/DZxmNeRdnv23wDwpl8ewVATAOMkWrcv36KG/m', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dTnEPghvkBI:APA91bGujU6zWtZvo4H_XICSB_7Aqjiced0w-knVp_Ax1_5l2jyH_FTHdsFNRwKi4pOzP8fyM1S52gUkXBGJLNL5bRuC8Bo53n4-QF1gU-8RUVbIsRi-kiJWygHaImENAe5t75pSdT6h', 'N', 'A', NULL, NULL, '2020-05-15 12:29:14', '2020-05-15 12:32:02'),
(28, '@Rounaque123', 'rounaqueqa@gmail.com', '9667365084', 'Rounaque', 'Afreen', '2020-05-16', NULL, '$2y$10$qxhAnLlyMli9.86.W2..Bu7G6wQfVAX1tqzyE3WAyDr2J/meLM.Wm', 'Default', '28-YDjNE7iO1589549197.jpg', NULL, '$2y$10$d/gYY8S/5XA23D6nGSypaObtTwg0fxQgY6xJ8bk0pLVgMV7VLHMNa', 'U', 'PRO', 'N', 'BU', NULL, 'Testing', 'Y', 'A', NULL, 'eKNt3sHUqjc:APA91bHZoA2-68wvp-Psdw8CctH-fYSlaxVgVKdAgT14T077VcwfgONP4vaPk5RxPZaiBsszfZfAjlasJ-h12UX1DI7w_WS_623QnP0h2tdv_eOsJZKcJhuc0aijnsOYF0A6-bc9ToJB', 'N', 'A', NULL, NULL, '2020-05-15 13:18:24', '2020-05-15 13:26:37'),
(29, 'Rana', 'Mobulous@gmail.com', '8340748427', 'Rana', 'Pandey', '2020-05-10', NULL, '$2y$10$rGUI7xYikKUjVvqk0YEZuejctV3C3wsvd.dRDVnw.IYMmQ12r6VdW', 'Default', '29-z0LgNtXN1589565537.jpg', NULL, '$2y$10$mHIXlgVQEaIWFqWtYLQJt.IKDOeS1YyKtU5Efne9uIMmoFZCMSWCC', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dTnEPghvkBI:APA91bGujU6zWtZvo4H_XICSB_7Aqjiced0w-knVp_Ax1_5l2jyH_FTHdsFNRwKi4pOzP8fyM1S52gUkXBGJLNL5bRuC8Bo53n4-QF1gU-8RUVbIsRi-kiJWygHaImENAe5t75pSdT6h', 'N', 'A', NULL, NULL, '2020-05-15 13:44:32', '2020-05-15 17:58:57'),
(30, 'AviK', 'aviksingh004@gmail.com', '9891240184', 'Avinesh', 'Singh', '2020-05-15', NULL, '$2y$10$b0rnVVXuI3mQU2A1kGjtV.Gwn74/vWWPL/zycomioTfSUZb6dsw56', 'Default', '30-guNEpyPf1589553143.jpg', NULL, '$2y$10$r9nTPGRkCDRuLdTxYLrXKO0t6kE9b1VJ2t8kb1N7qgHOrw/KPw3pq', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'ef759cS6q1E:APA91bGgnDxsc9PauDfTtn5wgnLyQWQOSAo_QWuRhyxdVFO8CqM0t9tjk7tMUiZEMONPITUXZsAXJRfx_6aZsMc5qU_DBMI7jKRrptnhzXqyrGgevHtO0_iZ4z8JeexjtPqPpryMfiQg', 'N', 'A', NULL, NULL, '2020-05-15 14:20:58', '2020-05-15 14:32:23'),
(31, '@Rana-Pandey#312FJCpn', 'Mobu@gmail.com', '8340748427', 'Rana', 'Pandey', '1996-05-13', NULL, '$2y$10$HifuWV0yE3xJQfyaBTQkceCGj9fAY7wzFTCHjQjQdRYbuewIt2iZm', 'Default', '31-0EpyiO6p1589554602.jpg', NULL, '$2y$10$slyuCs7VgmMWSq7/Lkb6VuQQS8Uf.CfMr7he.9bqm3rpCVmuTBLN.', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'ef759cS6q1E:APA91bGgnDxsc9PauDfTtn5wgnLyQWQOSAo_QWuRhyxdVFO8CqM0t9tjk7tMUiZEMONPITUXZsAXJRfx_6aZsMc5qU_DBMI7jKRrptnhzXqyrGgevHtO0_iZ4z8JeexjtPqPpryMfiQg', 'N', 'A', NULL, NULL, '2020-05-15 14:53:34', '2020-05-15 14:56:42'),
(32, '@Rana-Pandey#32V8uw0v', 'rrr@gmail.com', NULL, 'Rana', 'Pandey', '2014-05-06', NULL, '$2y$10$eTkTkXCGjk6qfMRzez2K5uf511yo5VUnyWELS4/sj.UaMYBWwsMfe', NULL, NULL, NULL, '$2y$10$kmBtB89tBBLpoLlkWbRw7.3GVjRS1BX6sMcjiP/ecIjfmvdZwNANi', 'U', 'PRO', 'N', 'BU', NULL, NULL, 'N', 'A', NULL, 'ef759cS6q1E:APA91bGgnDxsc9PauDfTtn5wgnLyQWQOSAo_QWuRhyxdVFO8CqM0t9tjk7tMUiZEMONPITUXZsAXJRfx_6aZsMc5qU_DBMI7jKRrptnhzXqyrGgevHtO0_iZ4z8JeexjtPqPpryMfiQg', 'N', 'A', NULL, NULL, '2020-05-15 15:07:21', '2020-05-15 16:06:15'),
(33, '@Rounaque-Afreen#33Gg4Azj', 'rounaque221418@gmail.com', '8010251243', 'Rounaque', 'Afreen', '2020-05-16', NULL, '$2y$10$j7DrXW7U2ODc7sGZGu/Ntu0fW20Gx/Mi.gbYnwMV80TuIyXwWYWS2', 'Default', '33-jhU1BBEb1589565525.jpg', NULL, '$2y$10$DA03KB6RMugDDyhqZUWRae.Au4ERpZgE7VUH65f5rrl8odiVI.he6', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'eORJEYGX2AU:APA91bF54DWyQB73pZZsH19s6Ac91qTG2oBLYu4kklZmuTfo5SPeMBNGF05rDEUBzefx4ECKEPqq_xPbEABgcMES_9MOB2rjup8rxpBahJWY-kwTCzEPe_DbCzEU8sobPPoXl45bUlpN', 'N', 'A', NULL, NULL, '2020-05-15 17:56:49', '2020-05-15 17:58:45'),
(34, '@Rana-Pandey#34wBGe0J', 'Rrrrr@gmail.com', NULL, 'Rana', 'Pandey', '2020-05-20', NULL, '$2y$10$BdBKWQYgf0ekIxL7Wakfr.z8L0W2apESqK5Wxn.ukWh5Tm00aA1CC', NULL, NULL, NULL, '$2y$10$S7DmDVL4xnQNM6GjVz1xU.S7AagIGhh0.1LzaMIZ25QtxSZqytShO', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dTnEPghvkBI:APA91bGujU6zWtZvo4H_XICSB_7Aqjiced0w-knVp_Ax1_5l2jyH_FTHdsFNRwKi4pOzP8fyM1S52gUkXBGJLNL5bRuC8Bo53n4-QF1gU-8RUVbIsRi-kiJWygHaImENAe5t75pSdT6h', 'N', 'A', NULL, NULL, '2020-05-15 17:59:31', '2020-05-15 18:01:19'),
(37, '@Rana-Pandey#37nMRVL9', 'mmm@gmail.com', NULL, 'Rana', 'Pandey', '1996-06-15', NULL, '$2y$10$YytC0OfU92lu/KE.MmbGGOS9V9j12/W19rs6xl1fjWKA04CijcVEy', NULL, NULL, NULL, '$2y$10$yaQqwN7QprbdOdu4X1Ty/eOC3erJJYnTqiiEovJmWuIlGGp0WUFFm', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dTnEPghvkBI:APA91bGujU6zWtZvo4H_XICSB_7Aqjiced0w-knVp_Ax1_5l2jyH_FTHdsFNRwKi4pOzP8fyM1S52gUkXBGJLNL5bRuC8Bo53n4-QF1gU-8RUVbIsRi-kiJWygHaImENAe5t75pSdT6h', 'N', 'A', NULL, NULL, '2020-05-15 19:06:45', '2020-05-15 19:07:47'),
(38, '@Rana-Pandey#382EjsSU', 'mmmm@gmail.com', NULL, 'Rana', 'Pandey', '1996-06-15', NULL, '$2y$10$DayskJeZ8jrH7cAkJR9JU.aHPUOOYaVl2Qp03NJCt8KCbEg7TRkXy', NULL, NULL, NULL, '$2y$10$XfGpXeNUfIx5G2AUFGcLQ.Z35WoGY972Kxm6Sf.1TIAcbBwC.GpTC', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dTnEPghvkBI:APA91bGujU6zWtZvo4H_XICSB_7Aqjiced0w-knVp_Ax1_5l2jyH_FTHdsFNRwKi4pOzP8fyM1S52gUkXBGJLNL5bRuC8Bo53n4-QF1gU-8RUVbIsRi-kiJWygHaImENAe5t75pSdT6h', 'N', 'A', NULL, NULL, '2020-05-15 19:10:30', '2020-05-15 19:11:50'),
(39, 'Rana', 'rana3752@gmail.com', '8340748427', 'Rana', 'Pandey', '1996-05-16', NULL, '$2y$10$J4moZcW3Z3hrD4cxGDq63ueesqkegyOif.NaeXbWWeRY1CoFTG1fK', 'Default', '39-0Bdb8FlG1589606513.jpg', NULL, '$2y$10$nyiz9kicKO/qonl4Jsl6pOvPuJSz5NLZCeuN.nIWx1M8v0BAaWh4u', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'ejgf8XK0Uss:APA91bH08rTORWbUoAKVFILrg51L0k0dJmvTgPpeIq-v_kpZDdIcBhmtZi_g8wpcW3mlu-rzZ13wJNzBcN4nBwQFlEZ9C5ssLNoiB0LrLzNeCYl93B9WcKfPaiDXLLL4FCGzQj01qTtj', 'N', 'A', NULL, NULL, '2020-05-16 05:17:05', '2020-05-16 05:21:53'),
(40, '@Rana-Pandey#40NtlbK1', 'r@gmail.com', '9891240184', 'Rana', 'Pandey', '1996-05-15', NULL, '$2y$10$nfINh5Jc35j3Vxcuv/ArC.p/zoZjWooMEKfTijYKZJtKrbZ8VAHwm', 'Default', '40-JiwOzq4F1589634048.jpg', NULL, '$2y$10$YjJoRH/RaA65RNWHAKSOMuTzVIQjnsY2zFrWv17hHYT1iBowY/XZm', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'qsqs', 'N', 'A', NULL, NULL, '2020-05-16 05:31:32', '2020-05-16 13:00:48'),
(41, '@Rana-Pandey#41FmXX4v', 'pandey@gmail.com', NULL, 'Rana', 'Pandey', '1996-06-15', NULL, '$2y$10$QAyeCGhm8Sr0tfqxV5Lw6O22yVaDHufMttBxIlGAEyeJqsgmMy26q', NULL, NULL, NULL, '$2y$10$qCeXLeZQuo/buB208YUT1Oot/rLMr0J5GEBpOJErcs96QvvXi//wK', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'cITefN9L-4E:APA91bF4zn717fmM37z6Sw_Vy_2l8gegK9h5gXetFrTed_5U-JOaOIklz2pO_tjs53Dn78Zvr1aCIFecmbe7SRc50W5p4-m_661bbyX-aE_rh1pHeylMUVVevjbFlysJIqkqsP0-l4jB', 'N', 'A', NULL, NULL, '2020-05-16 06:30:13', '2020-05-16 06:31:16'),
(43, '@Rana-Pandey#43jtfZYL', 'mo@gmail.com', NULL, 'Rana', 'Pandey', '1996-05-16', NULL, '$2y$10$TDI5tl1UvCcYMezo7A11DujMdo.WW0UnZtGoUHmbBVQOEhdfMffLG', NULL, NULL, NULL, '$2y$10$35bfoi19anSL0pWsllvsguEwiZLKaIqPGJRJXzUjVyzrrGEBryUCi', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'cITefN9L-4E:APA91bF4zn717fmM37z6Sw_Vy_2l8gegK9h5gXetFrTed_5U-JOaOIklz2pO_tjs53Dn78Zvr1aCIFecmbe7SRc50W5p4-m_661bbyX-aE_rh1pHeylMUVVevjbFlysJIqkqsP0-l4jB', 'N', 'A', NULL, NULL, '2020-05-16 13:20:32', '2020-05-16 13:21:50'),
(44, '@Rana-Pandey#44CWpyOZ', 'e@gmail.com', NULL, 'Rana', 'Pandey', '1996-05-15', NULL, '$2y$10$ykduQepGzwoO7EfU9T4CmuGdokkYK/wo10y/qEUzMhcCWOeOzUk8G', NULL, NULL, NULL, '$2y$10$R56VdcKS60IUt/Nn0v1Xsutkkr9Lu5.m6dAShIW56GE2kQhi8m3lO', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'cITefN9L-4E:APA91bF4zn717fmM37z6Sw_Vy_2l8gegK9h5gXetFrTed_5U-JOaOIklz2pO_tjs53Dn78Zvr1aCIFecmbe7SRc50W5p4-m_661bbyX-aE_rh1pHeylMUVVevjbFlysJIqkqsP0-l4jB', 'N', 'A', NULL, NULL, '2020-05-16 13:22:50', '2020-05-16 13:24:20'),
(45, '@Rana-Pandey#45xNIXee', 'avineshkumar.singh.5@facebook.com', NULL, 'Rana', 'Pandey', '1996-05-16', NULL, '$2y$10$9pGY4ZqGyTnlOOhDIGlOG.68fHJGVhVICHZk/Wbyqm0f5Xxx3e/Uu', NULL, NULL, NULL, '$2y$10$SuZFo6/k8deEGVsDGxkO5ep7V2bgsVZuid36F5yvPjKVpP9khXOnK', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'qsqs', 'N', 'A', NULL, NULL, '2020-05-16 15:51:04', '2020-05-16 15:57:57'),
(46, 'Rana-Pandey', 'G@GMAIL.COM', '8340748427', 'Rana', 'Pandey', '1996-06-15', NULL, '$2y$10$PQ7g9h.Av3pk83MhFqReMO9ggMSfahPrRNu6R1IDSn69g1kajrHCG', 'Default', '46-ALiFcRc31589950053.jpg', NULL, '$2y$10$S7CGwt7XmLmdyXufUuPb/OY27C771r1rcz2LwcxW1sZBPpcVozVVK', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'qsqs', 'N', 'A', NULL, NULL, '2020-05-17 08:42:49', '2020-05-20 04:47:33'),
(47, '@Tarun-Chauhan#47zxCndG', NULL, '8800190916', 'Tarun', 'Chauhan', '2019-08-15', NULL, '$2y$10$GfeT3XwvUhJKY0tgKF/LmeSE/Z9mVQRxln9ywZIQ4LBKvuiqV9ChW', NULL, NULL, NULL, '$2y$10$h2h6okMggN9CJqC12bOX4.7kkECTzDlKf2N/amZQGTGat3yCIJeA2', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'eTi7WcUC56k:APA91bG-0v5HddwOe4jBukH9hhBKn-TOyafmbOWYSdhlnCtf3DO4isTtTIlwQI79YRjyAlZBNVQashnF9pN4Yg27ay3wwd6l6DDL0FvhVyxiyfSVssgNeGrUJfGOTTLFMvglSiQoZH9v', 'N', 'A', NULL, NULL, '2020-05-19 11:17:39', '2020-05-19 11:19:02'),
(48, '@Rana-Pandey#48u7DlMe', NULL, '9999999999', 'Rana', 'Pandey', '1996-05-19', NULL, '$2y$10$uC2WbriItWWcqzNurkT7b.OWnFeRoDBNr9YXjUAVuD7/aC4KhLQb2', NULL, NULL, NULL, '$2y$10$YLJxBRTXz7ntRWG.gkaovOK9hP02KCCmsz83FBSQTWofwSaZRZqt6', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'cITefN9L-4E:APA91bF4zn717fmM37z6Sw_Vy_2l8gegK9h5gXetFrTed_5U-JOaOIklz2pO_tjs53Dn78Zvr1aCIFecmbe7SRc50W5p4-m_661bbyX-aE_rh1pHeylMUVVevjbFlysJIqkqsP0-l4jB', 'N', 'A', NULL, NULL, '2020-05-19 11:19:10', '2020-05-19 11:20:41'),
(49, '@taram-Tesr#49VKsMtB', 'taram@gmail.com', 'null', 'taram', 'Tesr', '2020-05-12', NULL, '$2y$10$DB79kyPWPMwR9XT8sGNku.gp6wT44pWKBiQhz/bSdAdl1lc9MgyG.', 'Default', '49-BAAhRStq1589888119.jpg', NULL, '$2y$10$/8yqrwf/i/RoNe6xZY9ISeqcgaj.6d9S5yX2pB2iea4DlGmOuLO7q', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'qsqs', 'N', 'A', NULL, NULL, '2020-05-19 11:30:06', '2020-05-19 11:35:19'),
(50, NULL, NULL, '7878786868668868', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'cITefN9L-4E:APA91bF4zn717fmM37z6Sw_Vy_2l8gegK9h5gXetFrTed_5U-JOaOIklz2pO_tjs53Dn78Zvr1aCIFecmbe7SRc50W5p4-m_661bbyX-aE_rh1pHeylMUVVevjbFlysJIqkqsP0-l4jB', 'N', 'A', NULL, NULL, '2020-05-19 11:50:51', '2020-05-19 11:50:51'),
(51, NULL, NULL, '9953237951', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'eTi7WcUC56k:APA91bG-0v5HddwOe4jBukH9hhBKn-TOyafmbOWYSdhlnCtf3DO4isTtTIlwQI79YRjyAlZBNVQashnF9pN4Yg27ay3wwd6l6DDL0FvhVyxiyfSVssgNeGrUJfGOTTLFMvglSiQoZH9v', 'N', 'A', NULL, NULL, '2020-05-19 12:52:16', '2020-05-19 12:52:16'),
(52, '@Rana-Pandey#52CB3O6s', 'k@gmail.com', NULL, 'Rana', 'Pandey', '1996-06-15', NULL, '$2y$10$xxNpGGAH7TyNUpexUb9NWO5cQjb498Rjf./1gfJTkadD3cQQmbvbW', NULL, NULL, NULL, '$2y$10$MeSdOhLi1B1KXwmv51DGF.e4dnR4ONfQnX9k18ypuYhpR.EPKPIP6', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'qsqs', 'N', 'A', NULL, NULL, '2020-05-20 05:36:46', '2020-05-29 18:52:09'),
(53, '@Afreen_12', 'afreen@gmail.com', '9900190918', 'Rounaque', 'Afreen', '1994-02-18', NULL, '$2y$10$DeYqv8aiDgkWxxZkMw8I2uAxzQUfkld0ayWmJ.HwjkfvOD6HGZerW', 'Default', '53-4Kk7M0xm1589974745.jpg', NULL, '$2y$10$167snTYYh2hdUkD0pfVaCudnSxMP6TlMVIz1mfHhC7lvNuwyT1WRa', 'U', 'PRO', 'Y', 'BL', NULL, NULL, 'Y', 'A', NULL, 'cqUyGRHGdE4:APA91bEIZn9-WMjbBJIcv6g3TEhWr9pq_ZdIO5_HTsiwV_6o7xnqtVb7L4ku5IE5S05oESFEHZDQTw4BM7SANjZx_AIF6AbvIX9x0IS2xdmyvYc0zXw4I64KCAiorEWt5_TM917ZeoMc', 'N', 'A', NULL, NULL, '2020-05-20 10:19:56', '2020-05-20 11:39:05'),
(54, '@Afreen_14', NULL, '9667365081', 'Afreen', 'Afrren', '2020-05-19', NULL, '$2y$10$YFgjkpZCQAZi2BR0bukKcOa8f..Q4lsDXw68Es13lvWQ9QXi0pDrq', NULL, NULL, NULL, '$2y$10$tRkJMko7hZnpfmSVEYRfSekyNSoDAH9ZMjn.HAqKeMk8LnI5VMVU6', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'cqUyGRHGdE4:APA91bEIZn9-WMjbBJIcv6g3TEhWr9pq_ZdIO5_HTsiwV_6o7xnqtVb7L4ku5IE5S05oESFEHZDQTw4BM7SANjZx_AIF6AbvIX9x0IS2xdmyvYc0zXw4I64KCAiorEWt5_TM917ZeoMc', 'N', 'A', NULL, NULL, '2020-05-20 15:09:28', '2020-05-20 15:10:52'),
(55, '@Rounaqu-Af#55tV', NULL, '8800190912', 'Rounaqu', 'Af', '2020-05-20', NULL, '$2y$10$jYc6LzGpJcUnHW6BgGRHROxYHIgBALPhX0NAKEEilBbfu8FUanS7G', NULL, NULL, NULL, '$2y$10$m4t6WwTbsSGak9BzG2u3eeE5AoIofK6EJVr/9HnrS/aXC7rqqE7ca', 'U', 'PRO', 'N', 'BU', NULL, 'Testing', 'Y', 'A', NULL, 'cqUyGRHGdE4:APA91bEIZn9-WMjbBJIcv6g3TEhWr9pq_ZdIO5_HTsiwV_6o7xnqtVb7L4ku5IE5S05oESFEHZDQTw4BM7SANjZx_AIF6AbvIX9x0IS2xdmyvYc0zXw4I64KCAiorEWt5_TM917ZeoMc', 'N', 'A', NULL, NULL, '2020-05-20 15:33:26', '2020-05-20 16:16:45'),
(56, '@Hshshs-Hshsh927#563C', 'ranap698@gmail.com', NULL, 'Hshshs', 'Hshsh927', '2020-05-20', NULL, '$2y$10$/Io1TeZOR9iZBkWbC1hl7e98Zls/iTBDeei1stHsh9wPm8BWXgua.', NULL, NULL, NULL, '$2y$10$nCxu5kZzybWgFH6wwiVlve8mfklt0W7sy1w1ec9neJbPdLf9vT872', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 15:49:25', '2020-05-20 15:52:29'),
(57, NULL, 'L@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 16:12:38', '2020-05-20 16:12:38'),
(58, '@Fshdj-Hejej#58b8', NULL, '6', 'Fshdj', 'Hejej', '2020-05-20', NULL, '$2y$10$3nRHXcHzTE12.djwNN7xBuQrYrojTxsDWLeyH/ilM504NVQCh5iTK', NULL, NULL, NULL, '$2y$10$T.buDWMx0fs6zZ6Dkz87TuMSa2vD73Lk0vvv.YiUJ7N0ddgM1Pzui', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 16:23:55', '2020-05-20 16:24:15'),
(59, NULL, NULL, '9876543210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 16:25:53', '2020-05-20 16:25:53'),
(60, NULL, NULL, '8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 16:49:02', '2020-05-20 16:49:02'),
(61, '@Rana-Pandey#616W', 'N@gmail.com', NULL, 'Rana', 'Pandey', '2020-05-11', NULL, '$2y$10$xo1vzNbZmVOjWMXLV.WoZO3VsO.jsIjX1kikugT1boWJPy1vtkfme', NULL, NULL, NULL, '$2y$10$FeI/KfLkUgeZUQuTMXA8h.Kj6FmHO/LCn8ENurrVIa.tRBfnltwja', 'U', 'PRO', 'N', 'BL', NULL, NULL, 'Y', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 18:25:09', '2020-05-20 18:47:01'),
(62, NULL, 'T@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 18:48:29', '2020-05-20 18:48:29'),
(63, NULL, 'Ak@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 18:50:02', '2020-05-20 18:50:02'),
(64, NULL, 'Avi@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 18:50:54', '2020-05-20 18:50:54'),
(65, NULL, NULL, '1234567890', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 18:51:24', '2020-05-20 18:51:24'),
(66, '@Rana-Pandey#66CI', 'KAk@gmail.com', NULL, 'Rana', 'Pandey', '1996-05-21', NULL, '$2y$10$yUp5dxiBiZ7/uCEl7SMlgeHESAP/zO3xbG3gMSKvbIXY7suVvamL2', NULL, NULL, NULL, '$2y$10$8J4HLMWAo1uDzyG.ifWXSu7iwkW3gDRtS069g4RcnMqBzMij4iUSC', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 18:53:22', '2020-05-20 18:57:15'),
(67, NULL, 'ap698@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 19:58:50', '2020-05-20 19:58:50'),
(68, NULL, 'ap698@gmail.com7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 19:59:32', '2020-05-20 19:59:32'),
(69, NULL, 'U@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 20:07:55', '2020-05-20 20:07:55'),
(70, '@Jdjd-Pandeu#700r', 'Jdkdhdj@gmail.com', NULL, 'Jdjd', 'Pandeu', '2020-05-21', NULL, '$2y$10$wXr1Jr0mbQyUFy3QAiKwQOF1MmbXmDxixSLNRoS1aQsopsiwtri5W', NULL, NULL, NULL, '$2y$10$GdK6tvMtNKlSuQlHn7VXjOqeRTSRHESE/oyFZTFKkwpIYPm9jW8hq', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 20:18:21', '2020-05-20 21:53:24'),
(71, '@Hrjrjr-Hfjfjd#71KZ', 'Mobilo@gmail.com', NULL, 'Hrjrjr', 'Hfjfjd', '1996-05-01', NULL, '$2y$10$OLKuPW19pJStytgOsd2PKeV7X1tDyQqyqlIKB8h89qtb0h/S.tB3S', NULL, NULL, NULL, '$2y$10$SvtFPSP.kp44Bjv1SbhQ7.UvaDJJTFgGGx8Zteo/kXwHXt6ztPmvm', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 21:54:19', '2020-05-20 22:51:26'),
(72, '@Rana-Pandey#75HM', 'Iii@gmail.com', NULL, 'Rana', 'Pandey', '1996-05-01', NULL, '$2y$10$K4SSlzGkjNLhS4lAGM8m3.4EVpXJg9WDoD94/grpAi5KWZuBYn5Re', NULL, NULL, NULL, '$2y$10$mjkxdsgzDK9sCNo4dI8ViuykftyPjvI8GwIYnRuwRRnxMqA1KifrW', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-21 08:43:55', '2020-05-21 08:50:53'),
(73, '@Rana-Pandey#73gJ', 'Gdhdh@gmail.com', NULL, 'Rana', 'Pandey', '1996-05-01', NULL, '$2y$10$V1o8Zk/bhtBrT8ObWLL3gOVhAWHzwHfgkELfbv5hMO9YKeTq3pZqi', NULL, NULL, NULL, '$2y$10$Xr6Db18wigWThX5ziZDUFek.UBE3tGZr0fzMjitO0sIFe./IpcP4q', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-21 07:01:19', '2020-05-21 07:15:11'),
(76, '@AfreenRR', NULL, '8800190913', 'Rounaqueg', 'Afreeb', '1995-02-18', NULL, '$2y$10$WZ0J3m.ZjbnrI6TesrwG8ug8kd45e4E22SD2eOaJGjXm2z3P.4kty', NULL, NULL, NULL, '$2y$10$tjpeedtNy10K/sIuwoTjpOv1BqB9SHov8rQj1b7hD6qKrQ5qNpvmy', 'U', 'PRO', 'N', 'BL', NULL, NULL, 'Y', 'A', NULL, 'fbncMtGtD3M:APA91bEHRkWPO4oJ3i1dJ5C7iz-FRg72A-W2guwuf0rNLTwIVF27F517Fh4fDRZ1H-2iKI7rcm5TlI16mxAzabzuPHBzeemhW8wDEioE8mKOmPnnIIBfq_6nfEpVMroo7dfcKLDff9BQ', 'N', 'A', NULL, NULL, '2020-05-22 05:35:57', '2020-05-22 06:12:35'),
(77, '@Rana-Pandey#7797', 'ghj@gmail.com', NULL, 'Rana', 'Pandey', '1996-05-01', NULL, '$2y$10$4n5Oz52yYolPhcNHWRR7o.iZJp4n1K4EszaI.CCqpxfDif.efwPVC', NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dAfN4e-QJDU:APA91bE-_v4BO1Clv1K3ob77gBPdzbkkjyDlHQ3SlbwMNj2GrOpTLhqJJprbCD5hh7zcEmNuW8XLmh2OwEwkoisMsjbK2c53VfPQjOEnehQ2gfws6_-t3oy2fJ4CVIU9h-cY5y4srgGs', 'N', 'A', NULL, NULL, '2020-05-22 05:48:22', '2020-05-22 06:03:40'),
(78, '@Rounaque13', 'rounaque@gmail.com', '8800190918', 'Rounaque', 'Afreen', '2020-04-24', NULL, '$2y$10$ukiKLqY5wCZv9MOewYuubeMSQdhYAJcWusJipWsq55.XKXff.631S', 'Default', '78-4RJ1jBz01590132035.jpg', NULL, '$2y$10$WtR3GMrq69hDTEG1xcnfQem4A/iO5.sK61z7eJz9a.4RaKV0ox0Mm', 'U', 'PRO', 'N', 'BL', NULL, NULL, 'Y', 'A', NULL, 'fbncMtGtD3M:APA91bEHRkWPO4oJ3i1dJ5C7iz-FRg72A-W2guwuf0rNLTwIVF27F517Fh4fDRZ1H-2iKI7rcm5TlI16mxAzabzuPHBzeemhW8wDEioE8mKOmPnnIIBfq_6nfEpVMroo7dfcKLDff9BQ', 'N', 'A', NULL, NULL, '2020-05-22 07:18:10', '2020-05-22 07:20:35'),
(79, '@Rounaque-Afreen#790k', 'tara@gmail.com', '8010251233', 'Rounaque', 'Afreen', '2020-05-22', NULL, '$2y$10$5a1mPuoci8jNbufcIBvu/eMxf1hQOLdyvC4iEQRL1Boq93V/BX.ZC', 'Default', '79-sgmVDIec1590138091.jpg', NULL, '$2y$10$FDF7sTQ9NZcbMMAgggQUfO9VG.WDsQzolyOFtIPGPoh.rAM6KVI3e', 'U', 'PRO', 'N', 'BU', NULL, 'Testibg', 'Y', 'A', NULL, 'fbncMtGtD3M:APA91bEHRkWPO4oJ3i1dJ5C7iz-FRg72A-W2guwuf0rNLTwIVF27F517Fh4fDRZ1H-2iKI7rcm5TlI16mxAzabzuPHBzeemhW8wDEioE8mKOmPnnIIBfq_6nfEpVMroo7dfcKLDff9BQ', 'N', 'A', NULL, NULL, '2020-05-22 07:34:49', '2020-05-22 09:01:31'),
(80, '@insta-gram#80kY', NULL, '0000000000', 'insta', 'gram', '2020-05-22', NULL, '$2y$10$AGdNiKfVTAv6OVwLdheWf.ADec3Nm65hKppnyPpbzUHN/og2Mcgu2', NULL, NULL, NULL, '$2y$10$6Z/SKuvhzVZ39HsNy50rdOB6/e8IVmSNaEjNi4y1t1z2jBQ605Daq', 'U', 'PRO', 'N', 'BL', NULL, NULL, 'Y', 'A', NULL, 'dAfN4e-QJDU:APA91bE-_v4BO1Clv1K3ob77gBPdzbkkjyDlHQ3SlbwMNj2GrOpTLhqJJprbCD5hh7zcEmNuW8XLmh2OwEwkoisMsjbK2c53VfPQjOEnehQ2gfws6_-t3oy2fJ4CVIU9h-cY5y4srgGs', 'N', 'A', NULL, NULL, '2020-05-22 07:41:13', '2020-05-22 07:45:54'),
(81, '@insta-foodApp#81de', NULL, '0098989897987', 'insta', 'foodApp', '2020-05-22', NULL, '$2y$10$xII9uxU//RUe/5k8MDvHiOQP8n3Ps5iuYi4rXkwVne5.E/Np6PfgG', NULL, NULL, NULL, '$2y$10$DqKES/4eMSPXyQnSgZYJOejsBSExyFAdfuv1bis80MTZFL55Ke3d2', 'U', 'PRO', 'N', 'BU', NULL, 'fdgjojfdobjiogfiobjg', 'Y', 'A', NULL, 'dAfN4e-QJDU:APA91bE-_v4BO1Clv1K3ob77gBPdzbkkjyDlHQ3SlbwMNj2GrOpTLhqJJprbCD5hh7zcEmNuW8XLmh2OwEwkoisMsjbK2c53VfPQjOEnehQ2gfws6_-t3oy2fJ4CVIU9h-cY5y4srgGs', 'N', 'A', NULL, NULL, '2020-05-22 07:49:50', '2020-05-22 07:58:02'),
(82, '@Rana-Pandey#82et', 'ranapratap698@gmail.co', NULL, 'Rana', 'Pandey', '1996-06-15', NULL, '$2y$10$srcUfmE.Jyqc.cuey9yci.vHbGcs06euFiurLQoEWvfQoqQTjjm.i', NULL, NULL, NULL, '$2y$10$qPkfX28MzdIFgTsfVtYQPegNKYhIuYiRfvst8wU.RMGBznQsOQLFK', 'U', 'PRO', 'N', 'BU', NULL, 'rehuihiuhiuhj', 'Y', 'A', NULL, 'dAfN4e-QJDU:APA91bE-_v4BO1Clv1K3ob77gBPdzbkkjyDlHQ3SlbwMNj2GrOpTLhqJJprbCD5hh7zcEmNuW8XLmh2OwEwkoisMsjbK2c53VfPQjOEnehQ2gfws6_-t3oy2fJ4CVIU9h-cY5y4srgGs', 'N', 'A', NULL, NULL, '2020-05-22 09:26:21', '2020-05-22 09:33:13'),
(83, NULL, 'funnymatchvideo@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'cwaL_bEJB00:APA91bGlSknHCM_HIyggHceGEGFtwucOPUVop512Kk0dyKTj2BcrcHQG2oqLXrkU6Ml4Ug1ZTMAa3u4U-bbRLimvZPaVkXqo5yVuzekeC9WakboCGS9ujv73p5X9zkmdg9XEvsAH3Kgp', 'N', 'A', NULL, NULL, '2020-05-23 09:12:00', '2020-05-23 09:12:00'),
(84, NULL, 'Ranamobolous@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'cwaL_bEJB00:APA91bGlSknHCM_HIyggHceGEGFtwucOPUVop512Kk0dyKTj2BcrcHQG2oqLXrkU6Ml4Ug1ZTMAa3u4U-bbRLimvZPaVkXqo5yVuzekeC9WakboCGS9ujv73p5X9zkmdg9XEvsAH3Kgp', 'N', 'A', NULL, NULL, '2020-05-23 09:14:54', '2020-05-23 09:14:54'),
(85, NULL, NULL, '8700914643', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'cwaL_bEJB00:APA91bGlSknHCM_HIyggHceGEGFtwucOPUVop512Kk0dyKTj2BcrcHQG2oqLXrkU6Ml4Ug1ZTMAa3u4U-bbRLimvZPaVkXqo5yVuzekeC9WakboCGS9ujv73p5X9zkmdg9XEvsAH3Kgp', 'N', 'A', NULL, NULL, '2020-05-23 09:23:25', '2020-05-23 09:23:25'),
(86, '@Rana-Pandey#86ya', NULL, '9860103075', 'Rana', 'Pandey', '1996-05-01', NULL, '$2y$10$lS/qz/bWd0QU.izDRgfL/uYG/yfekUIDr1ULODkrMIzR163xIAvIu', NULL, NULL, NULL, '$2y$10$1bMS2ojbo18/oF2d/K/WneJk1eWFw8OMOd6gRcbKPNYRHxFIOWDxS', 'U', 'PRO', 'N', 'BU', NULL, 'Tttttttt', 'Y', 'A', NULL, 'cwaL_bEJB00:APA91bGlSknHCM_HIyggHceGEGFtwucOPUVop512Kk0dyKTj2BcrcHQG2oqLXrkU6Ml4Ug1ZTMAa3u4U-bbRLimvZPaVkXqo5yVuzekeC9WakboCGS9ujv73p5X9zkmdg9XEvsAH3Kgp', 'N', 'A', NULL, NULL, '2020-05-23 09:24:53', '2020-05-23 09:34:45'),
(87, '@Rana-Pandey#876t', NULL, '9546647238', 'Rana', 'Pandey', '1996-05-01', NULL, '$2y$10$FhdZ8rfmoXwonwcqjDshGOfF01meVIWiHDUzTts.Zb1ywmp.sLcQe', NULL, NULL, NULL, '$2y$10$BIc6qJ148gMWYBXX/MlIg.ZPDxbCdzl.AOlR1J9xQwqgXAIRyJL5m', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'cwaL_bEJB00:APA91bGlSknHCM_HIyggHceGEGFtwucOPUVop512Kk0dyKTj2BcrcHQG2oqLXrkU6Ml4Ug1ZTMAa3u4U-bbRLimvZPaVkXqo5yVuzekeC9WakboCGS9ujv73p5X9zkmdg9XEvsAH3Kgp', 'N', 'A', NULL, NULL, '2020-05-23 09:36:01', '2020-05-23 09:37:01'),
(88, '@Rana-Pandey#88ff', 'node-trainee@mobiloitte.com', NULL, 'Rana', 'Pandey', '1996-05-01', NULL, '$2y$10$8a7HxNn0GEbO2C17Zf0mLevDx29mQLcEWQTRn51OrfetUtPB/NpoW', NULL, NULL, NULL, '$2y$10$OqsAbHXJ1wqEbk/K7z1lrOKRvjgS1odEDag6Vj5/7qMMiIj09ZW6K', 'U', 'PRO', 'N', 'BL', NULL, NULL, 'Y', 'A', NULL, 'qsqs', 'N', 'A', NULL, NULL, '2020-05-23 13:00:38', '2020-05-23 13:12:45'),
(90, '@Inshad-Pro#90fY', 'Inshad@mailinator.com', 'null', 'Inshad', 'Pro', '2004-05-01', NULL, '$2y$10$CpR4atlpdW4a4HLKtZvR5.wMY2ZGhr6WKItivABr1lcUMGG/frTu2', 'Default', '90-4A6jx71r1590286311.jpg', NULL, '$2y$10$Y9SiMYnLAgIKvR34FgRWIuUz5Fn1Xe0JPT0wYjeHNF0XbTMY9esx6', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'd89aVRq_sdE:APA91bFwGHoxKNxGSMtCc93kRKxTmpqbaLAAmCerVhP1ZPTnTYs8_TpCNpfENNv9gYibNnmLHRok7m5Iy-m9M-7Kq_pUst-EeQPxmA4qT1I6BtMgGjMSByUCDJgb8fbLuFrSP-G5KAbK', 'N', 'A', NULL, NULL, '2020-05-23 20:06:50', '2020-05-24 02:11:51'),
(91, '@Ravi -Kumar#91LZ', 'Ins888@mailinator.com', NULL, 'Ravi ', 'Kumar', '1998-05-01', NULL, '$2y$10$qlxNuqM04U4gkoh5jX/FXef6f2dCBK2heblgNYyumIXvUrg4eHGZy', NULL, NULL, NULL, '$2y$10$kfzVgndJNcKW5IoRVjg0C.9fwCIsRsrGXEppf8AT92G4SKAH4eN6m', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-05-24 16:58:25', '2020-05-24 16:59:03'),
(92, NULL, 'Ins7220@mailinator.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-05-25 05:30:40', '2020-05-25 05:30:40'),
(93, '@Inshad-New#93Tk', 'Ins1@mailinator.com', NULL, 'Inshad', 'New', '2007-05-01', NULL, '$2y$10$3jLy18YOaZ.fYnYUS2yGZ.ea.tWpcMzRqh0MnSd9mtzLjzuRM4bQW', NULL, NULL, NULL, '$2y$10$9DeDu5iduUveGkAdp6L7hekGp8eIflx2jMqpYdCXlO3HKe.WZ0Dai', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'qsqs', 'N', 'A', NULL, NULL, '2020-05-25 05:36:29', '2020-05-25 12:16:06'),
(95, '@Phone -Test#95HX', NULL, '+14705892605', 'Phone ', 'Test', '2017-05-01', NULL, '$2y$10$f/TvRjH5g4d71cQbcriU9.GKRK6HRzjGo0oCes9OsTCnXhm00D1Wm', NULL, NULL, NULL, '$2y$10$Oj7wKeRbgXSd0C9YdhqbDOboJ/GOidC2/xnZGzinF4Jmn1nK2u3x.', 'U', 'PRO', 'N', 'BL', NULL, NULL, 'Y', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-05-25 07:17:13', '2020-05-25 07:18:02'),
(96, '@Ddd-Ddddd#96k9', NULL, '3042026284', 'Ddd', 'Ddddd', '2007-05-01', NULL, '$2y$10$924J252XHM3nP41HnWRBPOCDQ3GfrIHynglVF07sf5iqbb/bgYWx6', NULL, NULL, NULL, '$2y$10$Hpf.wJ4ttmWi2lxu9/oyeuIeSblA5IC/.zbX7n.qup5J7/TP6u59S', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'qsqs', 'N', 'A', NULL, NULL, '2020-05-25 07:35:01', '2020-05-25 07:47:28'),
(103, '@ibfvv-zcgg#1036W', '2921941514588579', NULL, 'ibfvv', 'zcgg', '2007-05-01', NULL, '$2y$10$Bn0dXBc.x4WHDfLL.ngbpeUXOWdu42AMpQjYf0EapuDIxyCT4N7QW', 'default', '103-d5QyEBao1592556381.jpg', NULL, '$2y$10$qzNQJOalXXGnyfnYbOrSyeKW1bMR8svEtX1WwXOHFgDINzMWiN4se', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'edqniNAiT4i62hMau3gewa:APA91bFhzGX61HsswJ1M2OjLOz5Pzw2u0kyDFokrANRzwbww2uKfuDwsR5CAtmuLeK6eGJVRmgZIUIGk0pRvFu7Y9BXwUTmGaoEt13nHOIwf9VDQUMhrG9WLIMCPs04F_MfxaLPNf7Wg', 'Y', 'A', '103-s7MbWSFH1592556994.jpg', 1597881600, '2020-05-25 11:53:41', '2020-08-20 09:17:42'),
(105, '@Mmm-Nnn#105AY', '157283989196688', 'null', 'Mmm', 'Nnn', '2020-05-30', NULL, '$2y$10$KQrtZozantaBcGGQ3kJGEO6SZk5k2ThVPbS7qXgKE785pF74Lu4uy', 'Default', '105-GjQCRH8A1591336138.jpg', NULL, '$2y$10$amLYVSX84CwI74A1rotEtuhEjjt8EsmT4zH1HUnpTbaOJJ9wqW3ie', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'c8k4Zjeq3_H-PGXk0Dj_nM:APA91bHh5ZH-ipc0niFBEHsaGOBf6500Dq3FT_kzRSVQ0axArjVoo7pNwJ7bHBeMXer_VU9GMyFkwhqLPGYPPoLIv3aUIHj-4yqLNCTpB8lLNYWMEE-SKkYonko9pIA0SoSUhwBRAfHN', 'N', 'A', NULL, 1566259200, '2020-05-30 06:06:31', '2020-08-19 11:16:26'),
(107, '@Ankit-Mittal#107Dd', 'ankit.mittal@mobulous.com', NULL, 'Ankit', 'Mittal', '1998-12-18', NULL, '$2y$10$lG9YohsHOf6Tqj3fNIKEqOdBrTffegqUMVtw229QdeC./lt2x7FUu', NULL, NULL, NULL, '$2y$10$gfraJ.uzIe60OTZ4XQRreOkSGuzyVEUFg20DoRNH5KSIlBTU8/dOG', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', '', 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-16 06:25:29', '2020-07-21 07:54:04'),
(108, '@Ankita-Mittala#108Qi', 'ankit05.mittal@gmail.com', NULL, 'Ankita', 'Mittala', '2020-05-01', NULL, '$2y$10$Uq7nkqrGZO76mVUOWLn/m.0MT.BamH4vsjwqeBhX6VZTH0.uEsBdS', NULL, NULL, NULL, '$2y$10$efYVeKEGYdz3Xhuf8oRfb.HBFm1x6DyDZdSyxzX6e87TEf9e1321C', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-16 06:35:26', '2020-06-16 06:36:22'),
(109, 'Gaurav_kr96', NULL, '7014245511', 'Gaurav', 'Kumar', '1993-11-06', NULL, '$2y$10$1cmTtAKFRWLxi4qabjNnUe1mfenAdIsk71V3IV45cMzGAX9hfVGi.', 'Flipkart hccjk vkuv kkvhdgxk jgjck l kvjvk kbjvjvk kvjcjvk kvjvgdulck. Klblbuckvk bjvkvk l jc be vjcj ovufigkvkvgifigkbhufivbbbigk igiv obivi obivivk kvjcuvk lbo', '109-Myj7e8A21592559835.jpg', NULL, '$2y$10$BFCAHOe7i.RiF77t9GBG..mQNh0IhP5s3a4a6aSgApibbrXE/FceS', 'U', 'PRO', 'N', 'BL', NULL, NULL, 'Y', 'A', '897700', 'e90WNDN6ScmVPmwdiv2_HZ:APA91bGgTzLUKGqgvwBVX6aklnT0jzKPfHmc4OA91yNe_0Sb6JNjt8B1G95PKCUrj2KdMLrPwz6Rbqm8TZElwy0g353fKmgzGVq2B14MjLRpKwoB0y9GSOahNEA3Bb5FjydwSYm44CaZ', 'Y', 'A', '109-RGHAApJM1592559835.jpg', 1408492800, '2020-06-18 11:00:43', '2020-08-14 10:59:03'),
(111, 'Demo_111F8', 'Ins7221@mailinator.com', NULL, 'Demo', 'T', '1974-05-01', NULL, '$2y$10$F06smW/Imnqui1Jb.fY03eRiFtQrq38jI7/ujEHnbbysW8jX4QnL.', NULL, NULL, NULL, '$2y$10$qmDQTcNXwypFj4fZB/E3uuRMG8qTv5YCJAsMy1V9yAZopcHXcrmMa', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-19 13:10:58', '2020-06-19 13:11:27'),
(112, NULL, '10220525258620322', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-19 15:33:06', '2020-06-19 15:33:06'),
(118, NULL, 'royalimperialfoods@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-20 12:30:46', '2020-06-20 12:30:46'),
(124, NULL, NULL, '9490047176', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-21 13:29:54', '2020-06-21 13:29:54'),
(125, 'Sreekaaaanth_12598', 'Tsn47176@gmail.com', NULL, 'Sreekaaaanth', 'Tt', '1977-05-01', NULL, '$2y$10$qaI5D6ujR367XB3wsOaJbeoTS33MNGMs.xWkZhzUmMt4vAStmOKNC', NULL, NULL, NULL, '$2y$10$uXXeu6tOIT902OnuCfH30eNHtpUZbcn0qlowNGgIlBWvfE2IqjU96', 'U', 'PRO', 'Y', 'BL', NULL, NULL, 'Y', 'I', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-21 13:41:05', '2020-08-15 20:02:29'),
(126, 'Shakti', '2502998849841136', NULL, 'Shakti', 'Shakti', '1995-06-27', NULL, '$2y$10$S.HEtwx7xvocQqEAWoErC.8KmiQj7KOhy7L2hX9aEtyp3uCcyAG6q', NULL, NULL, NULL, '$2y$10$uD1UAFC/tQ1sBIaoD0Ri1O5RyRHaUO6cqsvz3CZ.bjMNbvd7yeUTK', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-27 12:34:09', '2020-06-27 12:35:04'),
(127, 'rose_1276V', 'demo@mailinator.com', NULL, 'rose', 'rose', '2020-06-29', NULL, '$2y$10$2AJRE.fWXoratxJN4pKWbeTfnSg0GMVgB.y/2L.Zn89W6Uy843tpu', NULL, NULL, NULL, '$2y$10$Xv/dhvLroxs/nV/aYHB30eBnE5WrbeAzsddCF.gSMura7/.HXRu.a', 'U', 'PRO', 'N', 'BL', NULL, NULL, 'Y', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-29 11:47:33', '2020-06-29 11:48:22'),
(128, NULL, 'Infooo@mailinator.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-30 11:24:54', '2020-06-30 11:24:54'),
(129, NULL, 'Infoooo@mailinator.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-30 11:26:58', '2020-06-30 11:26:58'),
(130, NULL, 'Inff@mailinator.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-30 11:30:55', '2020-06-30 11:30:55'),
(131, 'Sss_131Vf', 'Infff@mailinator.com', NULL, 'Sss', 'Www', '2020-06-30', NULL, '$2y$10$ycNJSEyE5gQ2..2jI3oCiuk.Uoeq.MfWEtgh9tjJVnnns7q9ZVT4S', NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-30 11:34:04', '2020-06-30 11:42:56'),
(132, 'Demo_132b8', 'Iinfo@mailinator.com', NULL, 'Demo', 'Demo', '2020-06-30', NULL, '$2y$10$UfywZonYF84.kICUfH7v8uWx2Xs1XfQ2cKIdKF9zhWGbCUBOHU.zC', NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-30 11:45:49', '2020-06-30 11:46:15'),
(133, 'B_133js', 'Iinnfo@mailinator.com', NULL, 'B', 'Ddd', '2020-06-30', NULL, '$2y$10$fQfvLzCnQPRqMOnWg0.ukOX9gzS6VPL5/TKSIzO8KYa3azdGvV0n.', NULL, NULL, NULL, '$2y$10$IIeq8iZB.tOP6.9G5AzoRe5wYBU0G1EVsZ0TY70s4r/jHXWgFrjky', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-30 11:52:24', '2020-06-30 11:59:40'),
(134, 'Ddd_134L2', 'Ik@mailinator.com', NULL, 'Ddd', 'Qwqee', '2020-06-30', NULL, '$2y$10$Abu56c0NxCpLdw8CriXt4.UueVXcffitnLK8mlUmZP0E.kVV4W442', NULL, NULL, NULL, NULL, 'U', 'PRO', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-30 12:07:02', '2020-06-30 12:08:00'),
(135, NULL, 'E@mailinator.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-30 12:15:21', '2020-06-30 12:15:21'),
(136, 'R_136AR', 'G@mailinator.com', NULL, 'R', 'F', '2020-06-30', NULL, '$2y$10$B7InWtzXGGy3Rnb1DOXTs.TP1QAj9goAt.S8CGh5uXm/8Y.4t9XI6', NULL, NULL, NULL, NULL, 'U', 'PRO', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-06-30 12:16:18', '2020-06-30 12:16:54'),
(137, 'D_137rz', 'D@mailinator.com', NULL, 'D', 'C', '2020-07-01', NULL, '$2y$10$vCdAh9TA.D2bbpxYL4aCuujavs15UPc823XG/WtHec.KOgsEPg8BK', NULL, NULL, NULL, '$2y$10$hqS70pD35bOss7YQawH2huIKWhx/td4lhkgErqlseibp9ktnMcowG', 'U', 'PRO', 'N', 'BU', NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-07-01 04:49:20', '2020-07-01 04:57:02'),
(138, 'C_138hQ', 'S@mailinator.com', NULL, 'C', 'V', '2020-07-01', NULL, '$2y$10$tH8H78P4A6zFE3fqygYl5uS7o8EmB0Q2H14VcVr.7gGUqT7.3ZXeu', NULL, NULL, NULL, '$2y$10$ucNxyMUbI9J4/HdFpIVozOHHDk3zJovT0Auu7glLgD4a8tuYYTRcy', 'U', 'PRO', 'N', 'BL', NULL, NULL, 'Y', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-07-01 05:01:28', '2020-07-01 05:02:15'),
(139, 'C_139Hg', 'F@mailinator.com', NULL, 'C', 'C', '2020-07-01', NULL, '$2y$10$i/jUOhnaoB74QOtVQ2RdJOobJBZRVZrDcV8JTtKO0QfHPo557JmN2', NULL, NULL, NULL, '$2y$10$dFXRodFxH8aBUDcx1geO1OJ2NFFHblEjozSesdEipnh376.yWkyr.', 'U', 'PRO', 'N', 'BU', NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-07-01 05:04:55', '2020-07-01 05:05:26'),
(140, 'Ss_140ac', 'T@mailinator.com', NULL, 'Ss', 'S', '2020-07-01', NULL, '$2y$10$sghYlvRkdkM5BL7BOTED9OV3D1DQKrpmHR3Fp1uz7Aqmo3zwHBbLK', NULL, NULL, NULL, '$2y$10$mTMNXvNWPORbvRgUs59GQOJCP/6lPYcxzHBvmop7sEK5xwC/pzPMS', 'U', 'PRO', 'N', 'BU', NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-07-01 05:20:40', '2020-07-01 05:21:07'),
(141, 'W_141iY', 'B@mailinator.com', NULL, 'W', 'C', '2020-07-01', NULL, '$2y$10$S11L3N94jGtWLrGt9Q17G.VtxbGqf8SZ5HJo4JJc0dRlgtVai46j.', 'default', '141-ZyVm8LTo1593600823.jpg', NULL, '$2y$10$qM4sNHdIn.N00N6PGsYuy.cDTc.s0DPsen2LgqNLO/T8LXA5MSnAK', 'U', 'PRO', 'N', 'BU', NULL, 'Asaadsdsdsdsd', 'Y', 'A', NULL, 'dkkdk', 'N', 'A', '141-gaJit0vJ1593600823.jpg', NULL, '2020-07-01 05:22:17', '2020-07-01 10:53:43'),
(146, 'Inshad_146lj', '3026037440845652', NULL, 'Inshad', 'Ansari', '2020-07-03', NULL, '$2y$10$qcPyUSmw3MhzKGa18oeBQewxjaGjYM8rXsfYNlG0Og9x2RjuCJgju', NULL, NULL, NULL, '$2y$10$sTue9isVj8lpyiMmkatSd.oUEhyPOYWD97WqLh0gspauWKJEXLn5O', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dboip00ASkokuZiGlDUCyT:APA91bFqevoUFDEYrL4gtX656O3r83QvDR69Gwb-UwiVRzIgDhxfuoItAJ-DkhQTegtROIBIynpYmCJU6hv8ieCmnD-wRgvgP9_pIa8KM-ICGvmWQlPBQXHSl9bR9ou7MlYbNHQ7-Rer', 'N', 'A', NULL, 1597881600, '2020-07-03 09:29:06', '2020-08-20 08:39:16'),
(147, 'Inshad_14727', 'Demof@mailinator.com', NULL, 'Inshad', 'Ansari', '2020-07-06', NULL, '$2y$10$8NigKSpXizSwqBmkeOOwfu86Ji26SaCm5.Lmu1FC1k1PDNTlnopnG', NULL, NULL, NULL, NULL, 'U', 'PRO', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-07-06 02:54:36', '2020-07-06 02:55:08'),
(148, 'zebra_148Em', 'Zebra@mailinator.com', NULL, 'zebra', 'Kotec', '2020-07-17', NULL, '$2y$10$s7GrQ29u7oYxUFMiv8BxZeHmy8mq.kWFUfWWWGJLbpzBNfHeUTgn.', NULL, NULL, NULL, '$2y$10$3oB8Uvx27t6XccOo0DK2u.qhwLWBsR5AkOC3vqmBoKiKhc3xY.ALi', 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'hhdhdfd', 'N', 'A', NULL, NULL, '2020-07-17 11:07:02', '2020-07-17 11:12:27'),
(149, 'Zebra h_149vW', 'Zebra2@mailinator.com', NULL, 'Zebra h', 'Zebra', '2020-07-17', NULL, '$2y$10$u.FQOxBIjW8RcZhYsY3WrOayGVQ34tVwqvp1FBvGCThWhf9XoZgLG', NULL, NULL, NULL, '$2y$10$ahKKvmYMbzqpm5QjSxVHm.5cAwYEbL6MdO749LbsRvggReTHDO05a', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'hhdhdfd', 'N', 'A', NULL, NULL, '2020-07-17 11:15:19', '2020-07-17 12:43:25'),
(151, NULL, 'grv@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-07-20 06:50:06', '2020-07-20 06:50:06'),
(152, 'Saurabh_123', NULL, '7367022995', 'Saurabh', 'Dev', '1992-05-01', NULL, '$2y$10$hqkUKH0kuzbTo1VN6IuJJOi5J3Ic7cwJweliLXqWwouJJpYfQkVrW', 'default', NULL, NULL, '$2y$10$Na7SJ.jBNTgHMTk7Ep64ueNdIErpuB34aHYauR/MyD5yFrBxjgsDy', 'U', 'PRO', 'N', 'BU', NULL, 'Nxndjdndbdj', 'Y', 'A', '726318', 'hhdhdfd', 'N', 'A', '152-cEi2Ybmb1595333274.jpg', NULL, '2020-07-20 06:57:43', '2020-07-21 12:07:54'),
(153, NULL, 'rid@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-07-20 08:05:09', '2020-07-20 08:05:09'),
(154, NULL, 'wid@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-07-20 08:06:15', '2020-07-20 08:06:15'),
(155, NULL, 'hid@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-07-20 08:09:41', '2020-07-20 08:09:41'),
(156, 'Saurabh_12', 'gif@yopmail.com', '7014245511', 'Hhh', 'Vhg', '1988-05-01', NULL, '$2y$10$iu7ob7/IQJ9Q7Rj03sq3P.Kh.S0M89739pLCP/J13CxcE0WWfyU3G', 'No bio', '156-S78lvcNT1595313390.jpg', NULL, '$2y$10$2pZ1MbBoc3QUeBicr.qJi.ppgZbyKpbZZU70FX.VzONoLfSW3Uf7W', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-07-20 08:23:13', '2020-07-21 06:36:30'),
(157, NULL, NULL, '8482489560', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', 'PER', 'N', NULL, NULL, NULL, 'N', 'I', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-07-21 00:49:47', '2020-08-15 19:59:58'),
(158, 'Ankit_158A0', NULL, '9911818192', 'Ankit', 'Mitta', '2018-06-21', NULL, '$2y$10$fqKjMZCQYdlaZoeXoYQfNe9pbVQt5vRT2Adf2k4VmXy1vjFY7e3U6', NULL, NULL, NULL, '$2y$10$Ba24i9at/qC4rFMzWIT2HuLQoqE0n/sjLmlRE8sSog7StWZS041tS', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', '137229', 'dkkdk', 'N', 'A', NULL, NULL, '2020-07-21 05:47:05', '2020-07-21 08:11:24'),
(159, 'demo _159VE', 'demoo@mailinator.com', NULL, 'demo ', 'ankit', '2020-07-21', NULL, '$2y$10$2yLKz39DXGdBwEKRHLfegeL1QkD5.OWau5eZDJKmCiSXedg2fjZO2', NULL, NULL, NULL, '$2y$10$v7XfXAmuemUIrnAkoumCduWWxjxzrWYSxuTjKXDP3UoK/znFLLIA.', 'U', 'PER', 'Y', NULL, NULL, NULL, 'Y', 'A', '', 'hhdhdfd', 'N', 'A', NULL, NULL, '2020-07-21 06:06:45', '2020-07-21 11:38:45');
INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `first_name`, `last_name`, `dob`, `email_verified_at`, `password`, `bio`, `profile_pic`, `remember_token`, `user_token`, `user_type`, `account_type`, `is_post_private`, `professional_account_type`, `business_website_url`, `business_address`, `registration_complete`, `status`, `vcode`, `device_registrartion_id`, `is_online`, `device_type`, `cover_pic`, `last_used`, `created_at`, `updated_at`) VALUES
(160, 'Privateacc', 'Privacc.fs@gmail.com', NULL, 'Private', 'Account', '1997-09-28', NULL, '$2y$10$FepUC.EPyRzu59KhpZno2uSSNHhdpZPNrmroCdYxtRNjDbzsccGUS', 'Testing.....', '160-Q4gCXw7n1596329523.jpg', NULL, '$2y$10$bMfnAnq19ENfh4U51D4g1uZb5QUEM.NYpZSlQGWgPtNErDr5I0XHi', 'U', 'PER', 'Y', NULL, NULL, NULL, 'Y', 'A', NULL, 'hhdhdfd', 'N', 'A', '160-hI4dZgyz1596329546.jpg', NULL, '2020-07-27 14:44:35', '2020-08-13 07:56:38'),
(161, 'Blog_161wo', 'Blogacc.fs@gmail.com', NULL, 'Blog', 'Account', '1999-05-01', NULL, '$2y$10$pCjJDKBsjVEts70R/7eYPu0Kw4NTCMrkHb7bqxt2zHkZAnntS2.hu', 'Hello', NULL, NULL, '$2y$10$AFx/.hLOS5HgeGmwesKeOec9kVYmj/KEvBhMZW6ASU32L33NzHlGe', 'U', 'PRO', 'Y', 'BL', NULL, NULL, 'Y', 'A', NULL, 'hhdhdfd', 'N', 'A', '161-F75KTBkF1596306977.jpg', NULL, '2020-07-27 14:57:18', '2020-08-13 16:34:39'),
(77777, '@Rana-Pandey#726M', 'Ghjk@gmail.com', 'null', 'Rana', 'Pandey', '1996-05-01', NULL, '$2y$10$fkFK6f3eolc1Vnf7oED7DuDLRZ69E8Okkj08oMxNmfFct0nqGbUk2', 'Default', '72-cWK5KGcB1590019495.jpg', NULL, '$2y$10$wlkF1ZfNCpxUGAzB3mVD9O0CxuKJebzRaj5jF7cXBfqz8Lzq4eN6y', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dbkcDrK1mrk:APA91bFa2-vZRxE6H9EDJBNPiuRFIvUaqI4V3ve9SBexoo3eeKGIs1pCRfa7B-BdtBk1e5PiK5XloyuyN2_E4EP9TL42PpPw8OBxcH0Ip6MsqQQMIjCZfxHz-2wpa_na8ZGq7_RatSJN', 'N', 'A', NULL, NULL, '2020-05-20 23:05:38', '2020-05-21 00:04:55'),
(77778, 'Keerthi_77778O1', '1371927283001476', NULL, 'Keerthi', 'Villa Chikkala', '1996-02-02', NULL, '$2y$10$FVivg9yLBYgoaYVBXFAh3.qaVAaXGXJ4ENYyR5Lnx7DVOwB8xRj/.', 'default', '77778-2Z2cnlnO1596146995.jpg', NULL, '$2y$10$WH6iNSIG0WpxaY91Lq9R8ujIxFYFFO5ef4qBAbWkJT/ALD16LmqVa', 'U', 'PER', 'Y', NULL, NULL, NULL, 'Y', 'A', NULL, 'hhhh', 'N', 'A', '77778-WTTLipQl1596147020.jpg', NULL, '2020-07-30 22:06:07', '2020-08-16 01:30:43'),
(77779, 'Viswa_77779VY', 'Vamsichikkala@gmail.com', NULL, 'Viswa', 'Chikkala', '1994-09-15', NULL, '$2y$10$9i1eCkKhWIUz4e2aI2BfOex8r0zmTM6PaSEKhTqMYXhrqu8k49U.W', 'Founder Fsquad', '77779-V8bA08jl1596236833.jpg', NULL, '$2y$10$z5Mk726MCPg0lwggSDbQh.Q1d7R4LXzfDlwY07OcCN8mP.DjttI1W', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'eU5GuD-tQW-Wtrxjp81Ap3:APA91bEpKPk6Pht7w6mwcU837_7mqFO1ROVl27cdqAabrozDkUTA1sQ-rti7I709rC4hZHkynyjTVX_vcw6Le34XEKkcbGa3RggY8bYC664h5socTmcrwsf3zdHJ8ZrpFLmixAn66chw', 'Y', 'A', '77779-pNnNSktW1596188825.jpg', 1597881600, '2020-07-31 09:43:21', '2020-08-20 15:18:08'),
(77780, 'sivam', 'sivamsoftwsolutions@gmail.com', NULL, 'Sivam', 'Solution', '2020-08-05', NULL, '$2y$10$aAwHqR3bu8.B3UM7qcMs5.pw1WRIWV8W4FpYc92oQPmQqr8QSFsdu', NULL, NULL, NULL, '$2y$10$CKh.uXNSv5k0Jmk/oWHSQ.pJiKUZnHv318TRxX1YvYZtYh.VrJiwa', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dkkdk', 'N', 'A', NULL, NULL, '2020-08-05 10:16:05', '2020-08-05 10:16:41'),
(77781, 'Saurabh_kr96', NULL, '9631529624', 'Saurabh', 'Thakur', '2005-05-01', NULL, '$2y$10$fDNMxj9Lmgatlbfd26fHT.JJDqp9XE..yt00vJcRJi/B/J4ajhU/.', NULL, NULL, NULL, '$2y$10$u6ciXRc5iuK4Y5kpVJeSDuTksLW2cjt7uqjUmHT6k6NIAEh9xkXxG', 'U', 'PER', 'N', NULL, NULL, NULL, 'Y', 'A', NULL, 'dkkdk', 'Y', 'A', NULL, 1408492800, '2020-08-14 10:33:10', '2020-08-14 10:34:28');

-- --------------------------------------------------------

--
-- Table structure for table `user_games`
--

CREATE TABLE `user_games` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `post` int(11) DEFAULT NULL,
  `friend` int(11) DEFAULT NULL,
  `cooked` int(11) DEFAULT NULL,
  `video` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_games`
--

INSERT INTO `user_games` (`id`, `user_id`, `active`, `post`, `friend`, `cooked`, `video`, `status`, `created_at`, `updated_at`) VALUES
(1, 103, 2, 0, 0, 0, 1, 1, '2020-08-11 12:34:52', '2020-08-11 13:08:32');

-- --------------------------------------------------------

--
-- Table structure for table `user_to_business`
--

CREATE TABLE `user_to_business` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `day` enum('MON','TUE','WED','THR','FRI','SAT','SUN') DEFAULT NULL,
  `from_time` text,
  `to_time` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_to_business`
--

INSERT INTO `user_to_business` (`id`, `user_id`, `day`, `from_time`, `to_time`, `created_at`, `updated_at`) VALUES
(2, 10, 'SUN', '10:20', '10:20', '2020-03-21 10:52:46', '2020-03-21 10:52:46'),
(8, 28, 'SAT', '18:40', '18:40', '2020-05-15 13:22:40', '2020-05-15 13:22:40'),
(14, 55, 'SAT', '01:00', '01:00', '2020-05-20 16:16:45', '2020-05-20 16:16:45'),
(20, 79, 'SAT', '01:00', '01:00', '2020-05-22 07:35:54', '2020-05-22 07:35:54'),
(26, 81, 'SAT', '01:00', '01:00', '2020-05-22 07:58:02', '2020-05-22 07:58:02'),
(32, 82, 'SAT', '01:00', '01:00', '2020-05-22 09:33:13', '2020-05-22 09:33:13'),
(38, 86, 'SAT', '01:00', '01:00', '2020-05-23 09:34:44', '2020-05-23 09:34:44'),
(44, 141, 'SAT', '01:00', '01:00', '2020-07-01 05:27:00', '2020-07-01 05:27:00'),
(56, 152, 'SAT', '01:00', '01:00', '2020-07-20 06:59:46', '2020-07-20 06:59:46');

-- --------------------------------------------------------

--
-- Table structure for table `user_to_chat`
--

CREATE TABLE `user_to_chat` (
  `id` int(11) NOT NULL,
  `sender_id` bigint(20) NOT NULL DEFAULT '0',
  `receiver_id` bigint(20) NOT NULL DEFAULT '0',
  `content` text,
  `content_type` enum('T','I','V') DEFAULT NULL,
  `is_read` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_to_friend`
--

CREATE TABLE `user_to_friend` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `friend_id` bigint(20) NOT NULL DEFAULT '0',
  `request_status` enum('WAITING','REJECTED','FRIEND') NOT NULL DEFAULT 'WAITING',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_to_friend`
--

INSERT INTO `user_to_friend` (`id`, `user_id`, `friend_id`, `request_status`, `created_at`, `updated_at`) VALUES
(7, 108, 18, 'FRIEND', '2020-06-16 10:36:42', '2020-06-16 10:42:27'),
(8, 74, 18, 'FRIEND', '2020-06-18 07:23:05', '2020-06-18 09:04:44'),
(9, 109, 103, 'FRIEND', '2020-06-19 09:44:24', '2020-08-14 14:22:21'),
(10, 109, 18, 'FRIEND', '2020-06-19 12:50:30', '2020-06-19 13:07:42'),
(23, 117, 123, 'WAITING', '2020-06-26 11:44:28', '2020-06-26 11:44:28'),
(28, 149, 18, 'FRIEND', '2020-07-17 11:16:09', '2020-07-21 07:32:22'),
(29, 103, 18, 'FRIEND', '2020-07-17 12:33:58', '2020-07-21 07:32:25'),
(34, 109, 156, 'WAITING', '2020-07-21 06:20:26', '2020-07-21 06:20:26'),
(36, 159, 156, 'WAITING', '2020-07-21 07:16:18', '2020-07-21 07:16:18'),
(38, 159, 18, 'FRIEND', '2020-07-21 11:39:33', '2020-07-21 11:39:52'),
(41, 103, 77779, 'FRIEND', '2020-08-10 04:59:31', '2020-08-13 07:07:29'),
(42, 77779, 160, 'FRIEND', '2020-08-10 09:29:42', '2020-08-13 07:12:04'),
(43, 77778, 77779, 'FRIEND', '2020-08-10 16:15:40', '2020-08-11 06:34:38'),
(44, 77779, 77778, 'FRIEND', '2020-08-11 06:38:33', '2020-08-14 16:36:37'),
(45, 77779, 161, 'FRIEND', '2020-08-13 07:42:48', '2020-08-13 07:44:18'),
(46, 161, 160, 'FRIEND', '2020-08-13 07:45:38', '2020-08-13 07:57:10'),
(47, 103, 103, 'WAITING', '2020-08-13 09:47:18', '2020-08-13 09:47:18'),
(48, 103, 160, 'WAITING', '2020-08-13 09:47:46', '2020-08-13 09:47:46'),
(49, 103, 77779, 'WAITING', '2020-08-13 09:47:58', '2020-08-13 09:47:58'),
(50, 77778, 160, 'WAITING', '2020-08-13 17:06:32', '2020-08-13 17:06:32'),
(51, 103, 77778, 'FRIEND', '2020-08-14 06:27:34', '2020-08-14 16:36:53'),
(52, 109, 77781, 'FRIEND', '2020-08-14 13:11:32', '2020-08-14 13:12:09'),
(53, 77779, 103, 'WAITING', '2020-08-14 17:01:44', '2020-08-14 17:01:44'),
(54, 146, 146, 'WAITING', '2020-08-19 11:44:26', '2020-08-19 11:44:26'),
(55, 146, 103, 'WAITING', '2020-08-20 12:44:24', '2020-08-20 12:44:24');

-- --------------------------------------------------------

--
-- Table structure for table `user_verify`
--

CREATE TABLE `user_verify` (
  `id` int(11) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `otpcode` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_verify`
--

INSERT INTO `user_verify` (`id`, `email`, `otpcode`, `created_at`, `updated_at`) VALUES
(28, 'randhirpandey02@gmail.com', 678357, '2020-05-22 15:52:21', '2020-05-22 15:52:21'),
(29, 'ranapartap698@gmail.com', 338160, '2020-05-22 15:55:11', '2020-05-22 15:55:11'),
(30, '9860103075', 507222, '2020-05-22 16:43:46', '2020-05-22 16:43:46'),
(31, 'avineshkumarsingh300@gmail.com', 969033, '2020-05-22 16:52:10', '2020-05-22 16:52:10'),
(32, 'avineshkumarsingh300@gmail.com', 729373, '2020-05-22 17:10:09', '2020-05-22 17:10:09'),
(33, '9546647238', 964797, '2020-05-22 20:49:36', '2020-05-22 20:49:36'),
(34, '9934513017', 430562, '2020-05-22 20:50:38', '2020-05-22 20:50:38'),
(35, 're-shivamsingh@mobiloitte.com', 587545, '2020-05-22 20:58:25', '2020-05-22 20:58:25'),
(36, 're-ranapandey@mobiloitte.com', 595580, '2020-05-22 21:00:34', '2020-05-22 21:00:34'),
(37, 'niraj@gmail.com', 127088, '2020-05-22 21:28:31', '2020-05-22 21:28:31'),
(38, 'Ranamobolous@gmail.com', 884951, '2020-05-22 21:38:02', '2020-05-22 21:38:02'),
(39, '9911818192', 47606, '2020-05-23 06:06:15', '2020-05-23 06:06:15'),
(40, '7838380295', 875283, '2020-05-23 06:11:12', '2020-05-23 06:11:12'),
(41, '7890831734', 674911, '2020-05-23 06:14:23', '2020-05-23 06:14:23'),
(42, '+918840606093', 787751, '2020-05-23 06:17:10', '2020-05-23 06:17:10'),
(43, '+918700914623', 164102, '2020-05-23 06:18:30', '2020-05-23 06:18:30'),
(44, '+919661363658', 813446, '2020-05-23 06:25:05', '2020-05-23 06:25:05'),
(45, '+918400728210', 599083, '2020-05-23 06:26:41', '2020-05-23 06:26:41'),
(46, '+919973518058', 512545, '2020-05-23 07:04:16', '2020-05-23 07:04:16'),
(47, 'avi4dummy@gmail.com', 675673, '2020-05-23 07:05:10', '2020-05-23 07:05:10'),
(48, 'avi4github@gmail.com', 248523, '2020-05-23 07:08:41', '2020-05-23 07:08:41'),
(49, '+919438509849', 746921, '2020-05-23 07:53:47', '2020-05-23 07:53:47'),
(50, '+918755552590', 733103, '2020-05-23 08:26:18', '2020-05-23 08:26:18'),
(51, '+918340748233', 663711, '2020-05-23 08:30:27', '2020-05-23 08:30:27'),
(52, '+917787879899', 289114, '2020-05-23 08:36:25', '2020-05-23 08:36:25'),
(53, 'rounaque123@gmail.com', 3862, '2020-05-23 09:13:15', '2020-05-23 09:13:15'),
(54, 'rounaque123@gmail.com', 3733, '2020-05-23 09:13:16', '2020-05-23 09:13:16'),
(55, 'rounaque123@gmail.com', 5016, '2020-05-23 09:13:17', '2020-05-23 09:13:17'),
(56, 'rounaque123@gmail.com', 7440, '2020-05-23 09:13:17', '2020-05-23 09:13:17'),
(57, 'rounaque123@gmail.com', 4935, '2020-05-23 09:13:17', '2020-05-23 09:13:17'),
(58, '3749667365084', 1027, '2020-05-23 09:15:15', '2020-05-23 09:15:15'),
(59, '3749667365084', 5324, '2020-05-23 09:15:15', '2020-05-23 09:15:15'),
(60, '+918800190911', 1671, '2020-05-23 09:27:01', '2020-05-23 09:27:01'),
(61, '+918010251243', 7814, '2020-05-23 09:30:55', '2020-05-23 09:30:55'),
(62, '+918010251243', 4873, '2020-05-23 09:45:57', '2020-05-23 09:45:57'),
(63, '7217476782', 8749, '2020-05-23 10:07:00', '2020-05-23 10:07:00'),
(64, '7217476782', 7263, '2020-05-23 10:07:04', '2020-05-23 10:07:04'),
(65, '6201187960', 6786, '2020-05-23 10:09:41', '2020-05-23 10:09:41'),
(66, '8368200452', 3072, '2020-05-23 10:11:40', '2020-05-23 10:11:40'),
(67, '9456681442', 3175, '2020-05-23 10:15:28', '2020-05-23 10:15:28'),
(68, '8209041254', 1079, '2020-05-23 10:21:40', '2020-05-23 10:21:40'),
(69, '8349965977', 8063, '2020-05-23 10:25:29', '2020-05-23 10:25:29'),
(70, '7783006860', 4426, '2020-05-23 10:38:10', '2020-05-23 10:38:10'),
(71, '7017381081', 6867, '2020-05-23 10:41:10', '2020-05-23 10:41:10'),
(72, '9867456734', 6515, '2020-05-23 10:48:23', '2020-05-23 10:48:23'),
(73, '9891240186', 7102, '2020-05-23 11:02:27', '2020-05-23 11:02:27'),
(74, '9083787876', 1193, '2020-05-23 11:09:42', '2020-05-23 11:09:42'),
(75, '8601396602', 9045, '2020-05-23 11:11:57', '2020-05-23 11:11:57'),
(76, '9717057785', 3858, '2020-05-23 11:14:57', '2020-05-23 11:14:57'),
(77, '8340748487', 8763, '2020-05-23 11:19:14', '2020-05-23 11:19:14'),
(78, '+918340878378', 9491, '2020-05-23 11:20:37', '2020-05-23 11:20:37'),
(79, '+919896695335', 6336, '2020-05-23 11:24:47', '2020-05-23 11:24:47'),
(80, '+919334898337', 1227, '2020-05-23 11:34:46', '2020-05-23 11:34:46'),
(81, '+919675725006', 4127, '2020-05-23 11:39:44', '2020-05-23 11:39:44'),
(83, 'aasutosh895356@gmail.com', 8827, '2020-05-23 12:22:25', '2020-05-23 12:22:25'),
(93, '+919717057785', 3227, '2020-05-23 13:05:03', '2020-05-23 13:05:03'),
(98, 'Ins234@mailinator.com', 2176, '2020-05-25 04:04:05', '2020-05-25 04:04:05'),
(99, 'Ins8383@mailinator.com', 6538, '2020-05-25 04:54:34', '2020-05-25 04:54:34'),
(101, 'Ins7220@mailinator.om', 2563, '2020-05-25 05:28:50', '2020-05-25 05:28:50'),
(103, 'Ins7221@mailinato.com', 4020, '2020-05-25 05:35:18', '2020-05-25 05:35:18'),
(119, '+19132451413', 2499, '2020-06-20 03:21:39', '2020-06-20 03:21:39'),
(123, '+91', 2965, '2020-06-20 12:19:18', '2020-06-20 12:19:18'),
(124, '+91', 6089, '2020-06-20 12:20:06', '2020-06-20 12:20:06'),
(125, '+91', 9753, '2020-06-20 12:20:32', '2020-06-20 12:20:32'),
(127, '+91', 5418, '2020-06-20 12:26:38', '2020-06-20 12:26:38'),
(128, '+91', 6756, '2020-06-20 12:27:25', '2020-06-20 12:27:25'),
(129, '+91', 5500, '2020-06-20 12:27:41', '2020-06-20 12:27:41'),
(138, '+91', 7932, '2020-06-21 12:35:39', '2020-06-21 12:35:39'),
(139, '+91', 5934, '2020-06-21 12:35:48', '2020-06-21 12:35:48'),
(144, '+918187017176', 1339, '2020-06-21 14:41:29', '2020-06-21 14:41:29'),
(145, '+919491687254', 8927, '2020-06-21 14:42:58', '2020-06-21 14:42:58'),
(147, 'Demo@mailinato.com', 8047, '2020-06-29 10:34:15', '2020-06-29 10:34:15'),
(148, 'Demo@mailinato.com', 5682, '2020-06-29 10:34:20', '2020-06-29 10:34:20'),
(149, 'Demo@mailinato.com', 6796, '2020-06-29 10:34:20', '2020-06-29 10:34:20'),
(151, 'Demo@mailinato.com', 9921, '2020-06-29 11:01:33', '2020-06-29 11:01:33'),
(152, 'Demo@mailinato.com', 6719, '2020-06-29 11:03:20', '2020-06-29 11:03:20'),
(155, 'Demo@mailinato.com', 7622, '2020-06-29 11:12:30', '2020-06-29 11:12:30'),
(159, 'Demo@ommailinator.com', 9085, '2020-06-29 11:37:24', '2020-06-29 11:37:24'),
(163, 'Ins720@mailinato.com', 9225, '2020-06-30 11:06:42', '2020-06-30 11:06:42'),
(165, 'Info@mailiantor.com', 4062, '2020-06-30 11:20:50', '2020-06-30 11:20:50'),
(186, 'Demof@mailinato.com', 5456, '2020-07-06 02:51:59', '2020-07-06 02:51:59'),
(188, '+918482389560', 5422, '2020-07-07 01:43:20', '2020-07-07 01:43:20'),
(191, '+91633636326', 9814, '2020-07-17 12:11:48', '2020-07-17 12:11:48'),
(192, '+91', 7593, '2020-07-17 12:13:30', '2020-07-17 12:13:30'),
(193, '+91', 1652, '2020-07-17 12:14:54', '2020-07-17 12:14:54'),
(194, '+91', 6202, '2020-07-17 12:17:44', '2020-07-17 12:17:44'),
(195, 'Xt@mailinato.com', 8947, '2020-07-17 12:19:38', '2020-07-17 12:19:38'),
(196, '7275583095', 7885, '2020-07-17 12:54:51', '2020-07-17 12:54:51'),
(197, '7275583095', 6593, '2020-07-17 12:55:33', '2020-07-17 12:55:33'),
(199, '+919667365084', 9703, '2020-07-20 05:45:18', '2020-07-20 05:45:18'),
(206, 'gif@yopmail.con', 3097, '2020-07-20 08:21:30', '2020-07-20 08:21:30'),
(208, '+918482489560', 9327, '2020-07-21 00:49:23', '2020-07-21 00:49:23'),
(215, 'Pivateacc.fs@gmail.com', 3722, '2020-07-27 14:43:33', '2020-07-27 14:43:33');

-- --------------------------------------------------------

--
-- Table structure for table `video_post_gamification`
--

CREATE TABLE `video_post_gamification` (
  `id` int(11) NOT NULL,
  `name` text,
  `image` text,
  `no_of_video_posted` bigint(20) NOT NULL DEFAULT '0',
  `level` int(11) DEFAULT '0',
  `is_top` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `video_post_gamification`
--

INSERT INTO `video_post_gamification` (`id`, `name`, `image`, `no_of_video_posted`, `level`, `is_top`, `created_at`, `updated_at`) VALUES
(6, '1 Video post added', '5JsQPnbb1P9s-1597295496.gif', 1, 1, 'N', '2020-08-13 05:11:36', '2020-08-13 05:11:36'),
(7, '3 video post added', 'ATlXj1MMK8mE-1597295520.gif', 3, 2, 'N', '2020-08-13 05:12:00', '2020-08-13 05:12:00'),
(8, '5 Video posts  added', '5tyygGsTbVq9-1597295985.gif', 5, 3, 'N', '2020-08-13 05:19:45', '2020-08-13 05:19:45');

-- --------------------------------------------------------

--
-- Table structure for table `wanna_visit`
--

CREATE TABLE `wanna_visit` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wanna_visit`
--

INSERT INTO `wanna_visit` (`id`, `post_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 230, 146, '2020-08-20 12:44:30', '2020-08-20 12:44:30'),
(2, 226, 77779, '2020-08-20 15:20:13', '2020-08-20 15:20:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_room`
--
ALTER TABLE `chat_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment_to_tagged_user`
--
ALTER TABLE `comment_to_tagged_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commment_to_like`
--
ALTER TABLE `commment_to_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cookedit_gamification`
--
ALTER TABLE `cookedit_gamification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cooked_it`
--
ALTER TABLE `cooked_it`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `description_to_tagged_user`
--
ALTER TABLE `description_to_tagged_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `freind_gamification`
--
ALTER TABLE `freind_gamification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_badges`
--
ALTER TABLE `notification_badges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_gamification`
--
ALTER TABLE `post_gamification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_ingredients`
--
ALTER TABLE `post_ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_recipe`
--
ALTER TABLE `post_recipe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_to_comments`
--
ALTER TABLE `post_to_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_to_cooked_it`
--
ALTER TABLE `post_to_cooked_it`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_to_images`
--
ALTER TABLE `post_to_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_to_like`
--
ALTER TABLE `post_to_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_to_share`
--
ALTER TABLE `post_to_share`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_to_step`
--
ALTER TABLE `post_to_step`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_to_tagged_user`
--
ALTER TABLE `post_to_tagged_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usage_gamification`
--
ALTER TABLE `usage_gamification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_games`
--
ALTER TABLE `user_games`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_to_business`
--
ALTER TABLE `user_to_business`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_to_chat`
--
ALTER TABLE `user_to_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_to_friend`
--
ALTER TABLE `user_to_friend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_verify`
--
ALTER TABLE `user_verify`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_post_gamification`
--
ALTER TABLE `video_post_gamification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wanna_visit`
--
ALTER TABLE `wanna_visit`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `chat_room`
--
ALTER TABLE `chat_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `comment_to_tagged_user`
--
ALTER TABLE `comment_to_tagged_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `commment_to_like`
--
ALTER TABLE `commment_to_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `cookedit_gamification`
--
ALTER TABLE `cookedit_gamification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cooked_it`
--
ALTER TABLE `cooked_it`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `description_to_tagged_user`
--
ALTER TABLE `description_to_tagged_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `freind_gamification`
--
ALTER TABLE `freind_gamification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ingredients`
--
ALTER TABLE `ingredients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;
--
-- AUTO_INCREMENT for table `notification_badges`
--
ALTER TABLE `notification_badges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;
--
-- AUTO_INCREMENT for table `post_gamification`
--
ALTER TABLE `post_gamification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `post_ingredients`
--
ALTER TABLE `post_ingredients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `post_recipe`
--
ALTER TABLE `post_recipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `post_to_comments`
--
ALTER TABLE `post_to_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;
--
-- AUTO_INCREMENT for table `post_to_cooked_it`
--
ALTER TABLE `post_to_cooked_it`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `post_to_images`
--
ALTER TABLE `post_to_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=295;
--
-- AUTO_INCREMENT for table `post_to_like`
--
ALTER TABLE `post_to_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;
--
-- AUTO_INCREMENT for table `post_to_share`
--
ALTER TABLE `post_to_share`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `post_to_step`
--
ALTER TABLE `post_to_step`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `post_to_tagged_user`
--
ALTER TABLE `post_to_tagged_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `usage_gamification`
--
ALTER TABLE `usage_gamification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77783;
--
-- AUTO_INCREMENT for table `user_games`
--
ALTER TABLE `user_games`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_to_business`
--
ALTER TABLE `user_to_business`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `user_to_chat`
--
ALTER TABLE `user_to_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_to_friend`
--
ALTER TABLE `user_to_friend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `user_verify`
--
ALTER TABLE `user_verify`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;
--
-- AUTO_INCREMENT for table `video_post_gamification`
--
ALTER TABLE `video_post_gamification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `wanna_visit`
--
ALTER TABLE `wanna_visit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
